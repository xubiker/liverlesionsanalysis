clear all;
close all;

src = imread('c:\users\xubik\desktop\phantoms\pic4.png');
%src = imread('c:\users\xubik\desktop\phantoms\pic2.jpgp');
src = imresize(src, 0.5);

if size(src, 3) == 3, src = rgb2gray(src); end
src = im2double(src);
srcN = addSpeckleNoise(src, 0.5, 0.2);

src = src * 0.8 + 0.1;
srcN = srcN * 0.8 + 0.1;

la = 1.5;
la = 120;

delta = 1e-9;

z0 = srcN;

gdx_f = @(z) cat(2, z(:,2:end), z(:,end)) - z;
gdy_f = @(z) cat(1, z(2:end,:), z(end,:)) - z;
gdx_b = @(z) z - cat(2, z(:,1), z(:,1:end-1));
gdy_b = @(z) z - cat(1, z(1,:), z(1:end-1,:));

integral = @(z) sum(z(:));
JS = @(z) integral(((z - z0).^2) ./ z) / numel(z);
JTV = @(z) integral(sqrt(gdx_f(z).^2 + gdx_b(z).^2 + gdy_f(z).^2 + gdy_b(z).^2 + delta)) / numel(z);
J = @(z) JS(z) + la * JTV(z); % <----- we are going to minimize this function

JSd = @(z) (-(z0./z).^2 + 1) / numel(z);
phi = @(z) gdx_b(z) - gdx_f(z) + gdy_b(z) - gdy_f(z);
JTVd = @(z) phi(z) ./ (sqrt(gdx_f(z).^2 + gdx_b(z).^2 + gdy_f(z).^2 + gdy_b(z).^2) + delta) / numel(z);
Jd = @(z) JSd(z) + la * JTVd(z); % <----- function derivative

n_iter = 100;

sd_vals = cell(n_iter, 1);  % steepest directions
cd_vals = cell(n_iter, 1);  % conjugate gradients
z_vals = cell(n_iter, 1);   % image filtering iterations
j_vals = zeros(n_iter, 1);  % J-function values

%zF = imfilter(srcN, fspecial('gaussian', 3, 0.7), 'replicate'); % initial image
zF = srcN;

sd0 = - Jd(zF);
sd_vals{1} = sd0;
cd_vals{1} = sd0;
z_vals{1} = zF;
j_vals(1) = J(zF);


q = 10;

q1 = 10;
q2 = 1e-3;

q = exp(log(q1) + ((1)/n_iter) * log(q2));

qVals = zeros(n_iter, 1);

for i = 2 : n_iter
% ---  subgradient method ---
    %q = 10 * (1e-3 ^ ((i-2) / n_iter));
    
    q = exp(log(q) + (1/n_iter)*log(q2));
    
    qVals(i) = q;
    
    z_prev = z_vals{i - 1};
    sd_curr = - Jd(z_prev);
    z_curr = z_prev + q * sd_curr;
    j_curr = J(z_curr);
    fprintf('J = %f\n', j_curr);
    z_vals{i} = z_curr;
    j_vals(i) = j_curr;
    
% ---  conjugate gradient method ---
%     sd_prev = sd_vals{i - 1};
%     cd_prev = cd_vals{i - 1};
%     z_prev = z_vals{i - 1};
%     % calculate the new steepest direction (sd)
%     sd_curr = - Jd(z_prev);
%     % calculate the new Polak�Ribiere coefficient:
%     beta = dot(sd_curr(:), sd_curr(:) - sd_prev(:)) / dot(sd_prev(:), sd_prev(:));
%     beta = max([0 beta]);
%     % update the conjugate direction (cd)
%     cd_curr = sd_curr + beta * cd_prev;
%     % perform line search
%     stepFunc = @(alpha) J(z_prev + alpha * cd_curr);
%     [alpha_min, j_curr] = fminsearch(stepFunc, 0, optimset('Display','off', 'TolX', 1e-08, 'TolFun', 1e-08));
%     z_curr = z_prev + alpha_min * cd_curr;
%     fprintf('alpha = %f, beta = %f, J = %f\n', alpha_min, beta, j_curr);
%     sd_vals{i} = sd_curr;
%     cd_vals{i} = cd_curr;
%     z_vals{i} = z_curr;
%     j_vals(i) = j_curr;
end

z_res = z_vals{end};
noise = abs((z_res - z0) ./ sqrt(z_res));

subplot(1, 4, 1);
imshow(z0);
subplot(1, 4, 2);
imshow(z_res);
subplot(1, 4, 3);
plot(j_vals);
subplot(1, 4, 4);
imshow(noise, []);