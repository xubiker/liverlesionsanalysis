clear all;
close all;

%src = (imread('c:\users\xubik\desktop\phantoms\pic4R.png'));
%srcN = (imread('c:\users\xubik\desktop\phantoms\pic4Rn.png'));

src = imread('c:\users\xubik\desktop\phantoms\pic4.png');
src = imresize(src, 0.5);

if size(src, 3) == 3
    src = rgb2gray(src);
end
src = im2double(src);

y = src + randn(size(src,1))*.08;

imshow(y);


lambda = .2;

K = @(x)grad(x);
KS = @(x)-div(x);

Amplitude = @(u)sqrt(sum(u.^2,3));
F = @(u)lambda*sum(sum(Amplitude(u)));
G = @(x)1/2*norm(y-x,'fro')^2;

Normalize = @(u)u./repmat( max(Amplitude(u),1e-10), [1 1 2] );
ProxF = @(u,tau)repmat( perform_soft_thresholding(Amplitude(u),lambda*tau), [1 1 2]).*Normalize(u);
ProxFS = compute_dual_prox(ProxF);

ProxG = @(x,tau)(x+tau*y)/(1+tau);

options.report = @(x)G(x) + F(K(x));
options.niter = 300;
[xAdmm,EAdmm] = perform_admm(y, K,  KS, ProxFS, ProxG, options);

imageplot(xAdmm);