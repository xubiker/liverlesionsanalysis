function [fx,fy] = grad(M, bound)

% grad - gradient, forward differences
%
%   [gx,gy] = grad(M, bound);
% or
%   g = grad(M, bound);

if nargin == 1
    bound = 'sym';
end

if strcmp(bound, 'sym')
    fx = M([2:end end],:)-M;
    fy = M(:,[2:end end])-M;
else
    fx = M([2:end 1],:)-M;
    fy = M(:,[2:end 1])-M;
end

if nargout==1
    fx = cat(3,fx,fy);
end