function [ res ] = J_TV(f, sqDelta)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

N = numel(f);
sqGrad = gradient(f,1).^2 + gradient(f,2).^2;
v = sqGrad ./ (sqGrad.^2 + sqDelta);
res = sum(v(:)) / N;

end

