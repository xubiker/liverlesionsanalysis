function [idx, res] = globMin(F, a, b, eps, disp)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if nargin == 4
    disp = false
end

n = (b - a) / eps;
vals = ones(n, 1) * F(a);
for i = 1 : n
    vals(i) = F(a + (i-1) * eps);
end
[res, idx] = min(vals);

idx = a + (idx-1) * eps;

if disp
    plot(vals);
end

end