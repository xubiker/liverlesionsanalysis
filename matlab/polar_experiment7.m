%i = im2double(rgb2gray(imread('C:\\pic1.jpg')));
i = im2double(rgb2gray(imread('C:\\us_real_cr3.png')));

xi = 102;
WW = size(i,2);

theta = 16*pi/180;
tau = 34*pi/180;
hh = (WW - xi) / (2*sin(tau));
alpha = atan(xi/(2*(WW/2/tan(theta) - hh*cos(tau))));

eta = WW/2/tan(theta) - hh*cos(tau) - xi/2/sin(alpha)*cos(theta);

blackArea = zeros(round(2.6*eta), size(i,2));
i = cat(1, blackArea, i);

i = imresize(i, 0.5);
subplot(1, 2, 1);
imshow(i);

[h, w] = size(i);
[X, Y] = meshgrid(1 : w, 1 : h);

d = w / theta;
H0 = d * cos(theta);
H = d + h - H0;
W = 2*(h+d)*sin(theta);

% ----- direct -----
% phi = atan((X*W/w - W/2)./(H0 + Y*H/h));
% XX = phi.*w/(2*theta0) + w/2;
% rho = (Y*H/h + H0)./cos(phi);
% YY = rho-d;
% 
% ip = interp2(X, Y, i, XX, YY, 'cubic');
% subplot(1, 3, 2);
% imshow(ip);

ip = i;
% ----- inverse -----
phi = (X - w/2)*theta/(w/2);
rho = Y + d;
XX = (W/2 + rho .* sin(phi)) * (w/W);
YY = (rho .* cos(phi) - H0) * (h/H);

i2 = interp2(X, Y, ip, XX, YY, 'cubic');
subplot(1, 2, 2);
imshow(i2);
