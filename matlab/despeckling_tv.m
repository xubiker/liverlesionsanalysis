close all;

src = (imread('c:\users\xubik\desktop\phantoms\pic4.png'));
if size(src,3) == 3
    src = rgb2gray(src);
end
src = im2double(src);

srcN = addSpeckleNoise(src, 0.5, 0.4);

% srcN = (imread('c:\users\xubik\desktop\phantoms\pic4Rn.png'));
% 
% if size(srcN,3) == 3
%     srcN = rgb2gray(srcN);
% end
% 
% srcN = im2double(srcN);

% o = struct('lambda_min', eps, 'lambda_max', 1, 'niter', 250, 'tau', 1/300);
% denoised = perform_tv_denoising(srcN, o);
% 
% figure;
% subplot(1, 2, 1);
% imshow(srcN);
% subplot(1, 2, 2);
% imshow(denoised);

wvName = 'sym2';

[LL, LH, HL, HH] = dwt2(srcN, wvName);
% % imshow([LL LH; HL HH], []);
% 
o = struct('lambda_min', eps, 'lambda_max', 1, 'niter', 250, 'tau', 1/300);
LL_ = perform_tv_denoising(LL, o);
 
LH_ = SoftShrinkage(LH);
HL_ = SoftShrinkage(HL);
HH_ = SoftShrinkage(HH);

res = idwt2(LL_, LH_, HL_, HH_, wvName);

figure;
subplot(1, 2, 1);
imshow(srcN);
subplot(1, 2, 2);
imshow(res);
 
% N = idwt2(LL,LH, HL, HH, wvName);
% figure;
% subplot(1, 2, 1);
% imshow(srcN, []);
% subplot(1, 2, 2);
% imshow(N, []);

%softThresh = @(x, T) reshape(wthresh(x(:), 's', T), size(x));

%Define the window size
% sz = 3;
% window = ones(sz) / sz.^2;
% I = srcN;
% 
% %Find the local mean
% mu = conv2(I, window, 'same');
% 
% %Find the local Variance
% II = conv2(I.^2, window, 'same');
% Lvar = II - mu.^2;
% 
% 
% LvarT = softThresh(Lvar, 0.1);
% 
% subplot(2, 2, 1);
% imshow(Lvar, []);
% subplot(2, 2, 2);
% imshow(LvarT, []);
% 
% shrinked = softThreshWithMask(I, Lvar, 100, 2e03);
% subplot(2, 2, 3);
% imshow(shrinked, []);
% 
% 
% % figure,imagesc(Lvar);colormap(gray);title('Local Variance of the image');
% 
