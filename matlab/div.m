function fd = div(P, bound)

Py = P(:,:,2);
Px = P(:,:,1);

if nargin == 1
    bound = 'sym';
end

if strcmp(bound, 'sym')
    fx = Px-Px([1 1:end-1],:);
    fy = Py-Py(:,[1 1:end-1]);
    fd = fx+fy;
else
    fx = Px-Px([end 1:end-1],:);
    fy = Py-Py(:,[end 1:end-1]);
    fd = fx+fy;
end