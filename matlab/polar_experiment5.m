i = im2double(rgb2gray(imread('C:\\pic1.jpg')));
i = imresize(i, 0.1);
subplot(1, 3, 1);
imshow(i);

[h, w] = size(i);

theta0 = pi/6;

[X, Y] = meshgrid(1 : w, 1 : h);

phi = pi/12;

phi = (X - w/2)*theta/(w/2);

XX = X .* cos(phi) - Y .* sin(phi) + w/2;
YY = X .* sin(phi) + Y .* cos(phi);

% rho = Y + d;
% phi = (X - w/2)*theta0/(w/2);
% XX = W/2 + rho .* sin(phi);
% YY = rho .* cos(phi) - H0;

ip = interp2(X, Y, i, XX, YY, 'cubic');
subplot(1, 3, 2);
imshow(ip);

[h,w] = size(ip);
[X, Y] = meshgrid(1 : w, 1 : h);

phi = -pi/12;

XX = X .* cos(phi) - Y .* sin(phi) - w/2;
YY = X .* sin(phi) + Y .* cos(phi);

ip2 = interp2(X, Y, ip, XX, YY, 'cubic');
subplot(1, 3, 3);
imshow(ip2);
