clear all;
close all;

src = imread('c:\users\xubik\desktop\phantoms\pic4.png');
src = imresize(src, 0.5);

if size(src, 3) == 3, src = rgb2gray(src); end
src = im2double(src);

%srcN = addSpeckleNoise(src, 0.5, 0.3);
srcN = imnoise(src, 'gaussian', 0, 0.01);

% src = src + eps;
% srcN = srcN + eps;

la = 0.1; % regularization parameter

z0 = imfilter(srcN, fspecial('gaussian', 5, 1.), 'replicate'); % initial image

integral = @(z) sum(z(:));
gradModSq = @(z) gradient(z,1).^2 + gradient(z,2).^2;
JS = @(z) integral((z - z0).^2) / numel(z);%integral(((z - z0).^2) ./ z);
JTV = @(z, deltaSq) integral(sqrt(gradModSq(z) + deltaSq));
J = @(z, deltaSq) JS(z) + la * JTV(z, deltaSq); % <----- we are going to minimize this function

JSd = @(z) 2 * (z - z0) / numel(z);%-(z0./z).^2 + 1;
phi = @(z) gradient(z,1).*gradient(gradient(z,1),1) + gradient(z,2).*gradient(gradient(z,2),2);
JTVd = @(z, deltaSq) phi(z) ./ sqrt(gradModSq(z)+deltaSq);
Jd = @(z, deltaSq) JSd(z) + la * JTVd(z, deltaSq); % <----- function derivative

deltaSq = 100;

n_iter = 20;

sd_vals = cell(n_iter, 1);  % steepest directions
cd_vals = cell(n_iter, 1);  % conjugate gradients
z_vals = cell(n_iter, 1);   % image filtering iterations
j_vals = zeros(n_iter, 1);  % J-function values

sd0 = - Jd(z0, deltaSq);
sd_vals{1} = sd0;
cd_vals{1} = sd0;
z_vals{1} = z0;
j_vals(1) = J(z0, deltaSq);

for i = 2 : n_iter
    
    deltaSq = deltaSq / 4;

    sd_prev = sd_vals{i - 1};
    cd_prev = cd_vals{i-1};
    z_prev = z_vals{i - 1};
    
    % calculate the new steepest direction (sd)
    sd_curr = - Jd(z_prev, deltaSq);
    
    % calculate the new Polak�Ribiere coefficient:
    beta = dot(sd_curr(:), sd_curr(:) - sd_prev(:)) / dot(sd_prev(:), sd_prev(:));
    beta = max([0 beta]);
    
    % update the conjugate direction (cd)
    cd_curr = sd_curr + beta * cd_prev;
   
    % perform line search
    stepFunc = @(alpha) J(z_prev + alpha * cd_curr, deltaSq);
    [alpha_min, j_curr] = fminsearch(stepFunc, 0, optimset('Display','off', 'TolX', 1e-08, 'TolFun', 1e-08));
    
    z_curr = z_prev + alpha_min * cd_curr;
    fprintf('alpha = %f, beta = %f, J = %f\n', alpha_min, beta, j_curr);
    
    sd_vals{i} = sd_curr;
    cd_vals{i} = cd_curr;
    z_vals{i} = z_curr;
    j_vals(i) = j_curr;
end

z_res = z_vals{end};
z_noise = abs(z0 - z_res);


subplot(1, 3, 1);
imshow(z0);
subplot(1, 3, 2);
imshow(z_res);
subplot(1, 3, 3);
plot(j_vals);