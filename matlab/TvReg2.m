clear all;
close all;

src = imread('c:\users\xubik\desktop\phantoms\pic4.png');
src = imresize(src, 0.5);

if size(src, 3) == 3, src = rgb2gray(src); end
src = im2double(src);

srcN = addSpeckleNoise(src, 0.5, 0.2);

srcN = srcN + 1e-2;

%la = 5*1e8; % regularization parameter
la = 1e2;
%z0 = imfilter(srcN, fspecial('gaussian', 5, 1.), 'replicate'); % initial image

z0 = srcN;

gdx_f = @(z) cat(2, z(:,2:end), z(:,end)) - z;
gdy_f = @(z) cat(1, z(2:end,:), z(end,:)) - z;
gdx_b = @(z) cat(2, z(:,1), z(:,1:end-1)) - z;
gdy_b = @(z) cat(1, z(1,:), z(1:end-1,:)) - z;

integral = @(z) sum(z(:));
JS = @(z) integral(((z - z0).^2) ./ z) / numel(z);
JTV = @(z, deltaSq) integral(sqrt(gdx_f(z).^2 + gdy_f(z).^2 + deltaSq)) / numel(z);
JTV2 = @(z, deltaSq) integral(abs(gdx_f(z)) + abs(gdx_f(z))) / numel(z);

JTV3 = @(z, deltaSq) integral(sqrt(gdx_f(z).^2 + gdx_b(z).^2 + gdy_f(z).^2 + gdy_b(z).^2 + deltaSq)) / numel(z);
J = @(z, deltaSq) JS(z) + la * JTV(z, deltaSq); % <----- we are going to minimize this function

JSd = @(z) (-(z0./z).^2 + 1) / numel(z);
phi = @(z) gdx_f(z) + gdy_f(z);
JTVd = @(z, deltaSq) phi(z) ./ sqrt(gdx_f(z).^2 + gdy_f(z).^2 + deltaSq) / numel(z);
JTV2d = @(z, deltaSq) (sign(gdx_f(z)) + sign(gdy_f(z))) / numel(z);

phi3 = @(z) gdx_b(z) - gdx_f(z) + gdy_b(z) - gdy_f(z);
JTV3d = @(z, deltaSq) phi3(z) ./ sqrt(gdx_f(z).^2 + gdx_b(z).^2 + gdy_f(z).^2 + gdy_b(z).^2 + deltaSq) / numel(z);

Jd = @(z, deltaSq) JSd(z) + la * JTVd(z, deltaSq); % <----- function derivative

deltaSq = 100;

n_iter = 30;

sd_vals = cell(n_iter, 1);  % steepest directions
cd_vals = cell(n_iter, 1);  % conjugate gradients
z_vals = cell(n_iter, 1);   % image filtering iterations
j_vals = zeros(n_iter, 1);  % J-function values

zF = imfilter(srcN, fspecial('gaussian', 5, 1.), 'replicate'); % initial image

q1 = JSd(zF);
q2 = JTV2d(zF, deltaSq);
sd0 = - Jd(zF, deltaSq);
sd_vals{1} = sd0;
cd_vals{1} = sd0;
z_vals{1} = zF;
j_vals(1) = J(zF, deltaSq);

for i = 2 : n_iter
    
    deltaSq = deltaSq / 10;

    sd_prev = sd_vals{i - 1};
    cd_prev = cd_vals{i-1};
    z_prev = z_vals{i - 1};
    
    % calculate the new steepest direction (sd)
    sd_curr = - Jd(z_prev, deltaSq);
    
    % calculate the new Polak�Ribiere coefficient:
    beta = dot(sd_curr(:), sd_curr(:) - sd_prev(:)) / dot(sd_prev(:), sd_prev(:));
    beta = max([0 beta]);
    
    % update the conjugate direction (cd)
    cd_curr = sd_curr + beta * cd_prev;
   
    % perform line search
    stepFunc = @(alpha) J(z_prev + alpha * cd_curr, deltaSq);
    [alpha_min, j_curr] = fminsearch(stepFunc, 0, optimset('Display','off', 'TolX', 1e-08, 'TolFun', 1e-08));
    
    z_curr = z_prev + alpha_min * cd_curr;
    fprintf('alpha = %f, beta = %f, J = %f\n', alpha_min, beta, j_curr);
    
    sd_vals{i} = sd_curr;
    cd_vals{i} = cd_curr;
    z_vals{i} = z_curr;
    j_vals(i) = j_curr;
end

z_res = z_vals{end};
noise = abs((z_res - z0) ./ sqrt(z_res));

subplot(1, 4, 1);
imshow(z0);
subplot(1, 4, 2);
imshow(z_res);
subplot(1, 4, 3);
plot(j_vals);
subplot(1, 4, 4);
imshow(noise, []);