img = imread('C:\\pic1.jpg');
img = rgb2gray(img);
img = imresize(img, 0.25);

[h, w] = size(img);
[x, y] = meshgrid(1:w,1:h);
theta0 = pi / 6;

d = w / theta0;
H0 = d * cos(theta0);
W = 2*(h+d)*sin(theta0);
H = d + h - H0;
% res = zeros(ceil(H), ceil(W));
% for i = 1 : h
%     for j = 1 : w
%         v = img(i,j);
%         x = j;
%         y = i;
%         newx = round((h+d)*sin(theta0) + (d+y)*sin((x-w/2)*theta0/(w/2)));
%         newy = round((d+y)*cos((x-w/2)*theta0/(w/2)) - d*cos(theta0));
%         res(newy, newx) = v;
%     end
% end
% imshow(res);

phi = (x - w/2).*theta0/(w/2);
fprintf('max phi: %f\n', max(phi(:)));
rho = d + y;

X = W/2 + rho .* sin(phi);
Y = rho .* cos(phi) - H0;


% r1 = 100;
% 
% h = y + r1;
% phi = (x - w/2) * (theta0/(2*pi)) / (w/2);
% 
% W = (r1 + h) * sin(theta0);
% 
% rho = W + rho * sin(phi);
% theta = rho * cos(phi);


%rho = sqrt((X/2).^2 + Y.^2);


%# show pixel locations (subsample to get less dense points)
XX = x(1:8:end,1:4:end);
YY = y(1:8:end,1:4:end);
tt = X(1:8:end,1:4:end);
rr = Y(1:8:end,1:4:end);
subplot(121), scatter(XX(:),YY(:),3,'filled'), axis ij image
subplot(122), scatter(tt(:),rr(:),3,'filled'), axis ij square tight

%# show images
figure
subplot(121);
imshow(img, map), axis on
% subplot(122), warp(theta, rho, zeros(size(theta)), img, map)
subplot(122), warp(X, Y, zeros(max(size(X),size(Y))), img, map)
view(2), axis square