function [res] = SoftShrinkage(I)
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here

sz = 3;
window = ones(sz) / sz.^2;

%Find the local mean
mu = conv2(I, window, 'same');

%Find the local Variance
II = conv2(I.^2, window, 'same');
lvar = II - mu.^2;

m = min(lvar(:));
M = max(lvar(:));
t = m + (M - m) / 5;

%res = I;

XT = 0.8;

MM = abs(I) - XT;
MM(MM < 0) = 0;
res = sign(I) .* MM;

res(lvar <= t) = 0;

end

