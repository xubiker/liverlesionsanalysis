close all;

% example of matrix-gradient
% A = [1 2; 3 4];
% J = @(m) m(1,1).^2 + m(2,2).^2;
% g = gradX(J, A, 0.01);


% A = [2 -1; -1 2];
% b = [1 0]';
% 
% x = pcg(A,b);
% disp(x);

F = @(X)(X(1)+6).^2 + (X(2)-5).^2 + (X(3)+2.5).^4 + 4;
X0 = [0; 0; 0];
eps = 0.00001;

stDsc_0 = -gradX(F, X0, eps);

stepFunc = @(alpha) F(X0 + alpha * stDsc_0);
alpha_min = fminbnd(stepFunc, -100, 100);

X1 = X0 + alpha_min * stDsc_0;

disp(X0');
disp(X1');

n_iter = 10;

X_vals = cell(n_iter + 1, 1);
stDsc_vals = cell(n_iter + 1, 1);
SCG_vals = cell(n_iter + 1, 1);

X_vals{1} = X0;
%X_vals{2} = X1;
stDsc_vals{1} = stDsc_0;
SCG_vals{1} = stDsc_0;


for n = 2 : n_iter
    
    X_prev = X_vals{n-1};
    stDsc_prev = stDsc_vals{n-1};
    SCG_prev = SCG_vals{n-1};
    
    % calculate the steepest direction
    stDsc_n = -gradX(F, X_prev, eps);
    
    % calculate Polak�Ribiere coefficient:
    beta = dot(stDsc_n, stDsc_n - stDsc_prev) / norm(stDsc_prev).^2;
    beta = max([0 beta]);
    
    SCG_n = stDsc_n + beta * SCG_prev;
    
    stepFunc = @(alpha) F(X_prev + alpha * SCG_n);
    alpha_min = fminbnd(stepFunc, -100, 100);
       
    X_n = X_prev + alpha_min * SCG_n;
    
    stDsc_vals{n} = stDsc_n;
    SCG_vals{n} = SCG_n;
    X_vals{n} = X_n;
    
    fprintf('it %d, beta = %f, alpha = %f\n', n, beta, alpha_min);
    disp(X_n');
end


% [C,h] = contour(X, Y, Z);
% h.LevelStep = 5;
% hold on;
% quiver(x, y, dx, dy);






