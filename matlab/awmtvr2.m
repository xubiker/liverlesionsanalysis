a = [1 2 3; 4 5 6; 7 8 9];
%a = [1 1; 1 1];

integral = @(f) sum(f(:));
innerProduct = @(f,g) sum(f(:).*g(:));
%gradMod = @(f) sqrt(gradient(f,1).^2 + gradient(f,2).^2);
gradMod = @(f) abs(gradient(f,1)) + abs(gradient(f,2));
normX = @(f) sum(reshape(gradMod(f), [numel(f), 1]));
tv = @(f) integral(gradMod(f));


% JR = @(f) integral(((d - f).^2) ./ f) / integral(f.^2);
% d = magic(size(a,1));
% J = @(f) integral(((d - f) .^ 2) ./ f);
% g = gradX(J, a, 0.000001);
% disp(g);
% f = a;
% b = (2 * (f - d) .* f - (d - f).^2) ./ (f .^ 2);
% disp(b);

eps = 0.0000001;

delta = 13;

g = @(f) gradMod(f);

JS = @(f) integral(gradMod(f));
JTV = @(f) integral(g(f) ./ (g(f).^2 + delta)) / numel(f);
g1 = @(f) gradX(JTV, f, eps);

JTV_grad = @(f) ((-g(f).^2 + delta) ./ (g(f).^2 + delta).^2) .* gradX(JS, f, eps) / numel(f);

disp(g1(a));
disp(JTV_grad(a));

% disp(gradMod(a));
% disp(gradient(a,1).^2 + gradient(a,2).^2);

% 
% k = [0 1 0; 1 -2 1; 0 1 0];
% disp(conv2(a, k));
