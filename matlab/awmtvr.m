clear all;
close all;

integral = @(f) sum(f(:));
innerProduct = @(f,g) sum(f(:).*g(:));
gMod = @(f) sqrt(gradient(f,1).^2 + gradient(f,2).^2);
gModSq = @(f) gradient(f,1).^2 + gradient(f,2).^2;
normX = @(f) sum(reshape(gMod(f), [numel(f), 1]));
tv = @(f) integral(gMod(f));

src = (imread('c:\users\xubik\desktop\phantoms\pic4R.png'));
srcN = (imread('c:\users\xubik\desktop\phantoms\pic4Rn.png'));

if size(src, 3) == 3
    src = rgb2gray(src);
end
if size(srcN,3) == 3
    srcN = rgb2gray(srcN);
end

src = im2double(src);
srcN = im2double(srcN);

d = srcN;

ggMod = @(f) (gradient(f,1) .* gradient(gradient(f,1),1) + gradient(f,2) .* gradient(gradient(f,2),2)) ./ sqrt(gradient(f,1).^2 + gradient(f,2).^2);

JR = @(f) integral(((d - f).^2) ./ f) / integral(f.^2);
%deltaSq = @(f) normX(1 ./ abs(d - f));
deltaSq = @(f) 1 / sqrt(sum(reshape(d-f, numel(f), 1).^2));
JTV = @(f) integral(gMod(f) ./ (gModSq(f) + deltaSq(f))) / numel(f);
J = @(f) JR(f) * JTV(f);

JR_grad = @(f) (2 * (f-d) .* f - (d-f).^2) ./ (f.^2);
% JS = @(f) integral(gMod(f));
% JTV_grad = @(f) ((-gMod(f).^2 + deltaSq(f)) ./ (gMod(f).^2 + deltaSq(f)).^2) .* gradX(JS, f, 0.00001) / numel(f);
JTV_grad2 = @(f, dsq) (ggMod(f) .* (gModSq(f) - dsq) ./ (gModSq(f) + dsq) ) / numel(f);


% ----------------------- initialization of optimization process ----------
n_iter = 6;

f0 = imfilter(d, fspecial('gaussian', 5, 0.7), 'replicate');

f_vals = cell(n_iter + 1, 1);
G_vals = cell(n_iter + 1, 1);
CG_vals = cell(n_iter + 1, 1);
delta_vals = zeros(n_iter + 1, 1);
JTV_vals = zeros(n_iter + 1, 1);
JR_vals = zeros(n_iter + 1, 1);
J_vals = zeros(n_iter + 1, 1);

%eta = 1.e-08;
eta = 1 / integral(f0.^2);
CG_0 = - eta * ((d.^2 - f0.^2) ./ (f0.^2));

f_vals{1} = f0;
G_vals{1} = CG_0;
CG_vals{1} = CG_0;
delta_vals(1) = deltaSq(f0);
JTV_vals(1) = JTV(f0);
JR_vals(1) = JR(f0);
J_vals(1) = J(f0);

for n = 2 : n_iter
    
    f_prev = f_vals{n-1};
    G_prev = G_vals{n-1};
    CG_prev = CG_vals{n-1};
    
    % calculate the steepest direction
    %G_n = gradX(JTV, f_prev, eps) * JR(f_prev) + gradX(JR, f_prev, eps) * JTV(f_prev); 
    %G_n = JTV_grad(f_prev) * JR(f_prev) + JR_grad(f_prev) * JTV(f_prev); 
    G_n = JTV_grad2(f_prev, deltaSq(f_prev)) * JR(f_prev) + JR_grad(f_prev) * JTV(f_prev); 
    %G_n = gradX(J, f_prev, eps);
    
    % calculate Polak�Ribiere coefficient:
    b1 = innerProduct(G_n, G_n - G_prev);
    b2 = normX(G_prev)^2;
    b3 = innerProduct(G_prev, G_prev);
    beta = b1 / b3;
    beta = max([0 beta]);
    fprintf('beta: %f, delta: %f\n', beta, deltaSq(f_prev));
    
    CG_n = G_n + beta * CG_prev;
    
    %---------------------- solve minimization problem and find alpha -----
    stepFunc = @(alpha) J(f_prev + alpha * CG_n);
    
    %alpha_min = fminbnd(stepFunc, -100, 100);
    [alpha_min, ~] = globMin(stepFunc, -1, 1, 0.001, true);
    %alpha_min = fminsearch(stepFunc, 0);
    f_n = f_prev + alpha_min * CG_n;
    
    G_vals{n} = G_n;
    CG_vals{n} = CG_n;
    f_vals{n} = f_n;
    delta_vals(n) = deltaSq(f_n);
    JTV_vals(n) = JTV(f_n);
    JR_vals(n) = JR(f_n);
    J_vals(n) = J(f_n);
    
    fprintf('it %d, beta = %f, alpha = %f\n', n, beta, alpha_min);
    %disp(f_next');
end

close all;
imshow(d); figure;
for i = 1 : n_iter
    imshow(f_vals{i});
    figure;
end
plot(delta_vals);

[~, i] = min(J_vals);
f_res = f_vals{i};

%noise_add = abs(f_res - d);

figure;
subplot(2, 3, 1);
imshow(d);
subplot(2, 3, 2);
imshow(f0);
subplot(2, 3, 3);
imshow(f_res);
subplot(2, 3, 4);
plot(delta_vals);
subplot(2, 3, 5);
plot(J_vals);
figure; plot(JR_vals);
figure; plot(JTV_vals);