a = ones(100, 100) * 0.5;
%imshow(a);

b = addSpeckleNoise(a, 0.5, 0.1);

imshow(b);

histogram(b(:));

i = im2double(rgb2gray(imread('c:\us_real.png')));
i = i(210:400,310:500);
m = imfilter(i, fspecial('gaussian', 15, 3), 'replicate');
i = i - m;
subplot(1, 2, 1);
imshow(abs(i));
subplot(1, 2, 2);
histogram(i(:), 800);
