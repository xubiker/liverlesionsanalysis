i = im2double(rgb2gray(imread('C:\\pic1.jpg')));
%i = imresize(i, 0.5);
subplot(1, 3, 1);
imshow(i);

[h, w] = size(i);

theta0 = pi/6;

d = w / theta0;
H0 = d * cos(theta0);
H = d + h - H0;
W = 2*(h+d)*sin(theta0);

[X, Y] = meshgrid(1 : w, 1 : h);

% data = [];
% for x = 1 : w
%     y = (1:h);
%         v = i(y,x);
% %         phi = atan((x*W/w - W/2)./(H0 + y*H/h));
% %         newx = phi.*w/(2*theta0) + w/2;
% %         rho = (y*H/h + H0)./cos(phi);
% %         newy = rho-d;
%         rho = y + d;
%         phi = ((x - w/2)*theta0)/(w/2);
%         newx = (rho .* sin(phi) + W/2) * (w/W);
%         newy = (rho .* cos(phi) - H0) * (h/H);
%         data = [data [newx', newy', v]'];
% %    end
% end
% f = scatteredInterpolant(data(1,:)', data(2, :)', data(3,:)');
% ip = f(X, Y);
% subplot(1, 3, 2);
% imshow(ip);

% rho = Y + d;
% phi = (X - w/2)*theta0/(w/2);
% XX = W/2 + rho .* sin(phi);
% YY = rho .* cos(phi) - H0;

phi = atan((X*W/w - W/2)./(H0 + Y*H/h));
XX = phi.*w/(2*theta0) + w/2;
rho = (Y*H/h + H0)./cos(phi);
YY = rho-d;

ip = interp2(X, Y, i, XX, YY, 'cubic');
subplot(1, 3, 2);
imshow(ip);

%---------------------------
[h, w] = size(ip);


d = w / theta0;
H0 = d * cos(theta0);
H = d + h - H0;
W = 2*(h+d)*sin(theta0);

[X, Y] = meshgrid(1 : w, 1 : h);

% data = [];
% for x = 1 : w
%     y = (1:h);
%         v = ip(y,x);
%         phi = atan((x*W/w - W/2)./(H0 + y*H/h));
%         newx = phi.*w/(2*theta0) + w/2;
%         rho = (y*H/h + H0)./cos(phi);
%         newy = rho-d;
% %         rho = y + d;
% %         phi = (x - w/2)*theta0/(w/2);
% %         newx = W/2 + rho .* sin(phi);
% %         newy = rho .* cos(phi) - H0;
%         data = [data [newx', newy', v]'];
% %    end
% end
% f = scatteredInterpolant(data(1,:)', data(2, :)', data(3,:)');
% vv = f(X, Y);
% subplot(1, 3, 3);
% imshow(vv);

phi = (X - w/2)*theta0/(w/2);
rho = Y + d;
newx = (W/2 + rho .* sin(phi)) * (w/W);
newy = (rho .* cos(phi) - H0) * (h/H);

i2 = interp2(X, Y, ip, newx, newy, 'cubic');
subplot(1, 3, 3);
imshow(i2);

