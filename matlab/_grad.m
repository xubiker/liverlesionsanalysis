close all;

i = rgb2gray(imread('c:\users\xubik\desktop\pic2.jpg'));
i = im2double(i) / 255;
i_n = addSpeckleNoise(i, 0.5, 0.03);


d = i_n;

n_iter = 20;

%f_prev = d;

f_prev = i_n;

gr1 = gradient(f_prev, 1);
gr2 = gradient(f_prev, 2);
gr3 = gradient(f_prev);


CGU_vals = cell(n_iter + 1, 1);     % zeta_n 
f_vals = cell(n_iter + 1, 1);       % f_n
JR_vals = zeros(n_iter + 1, 1);     % J_R_n
JTV_vals = zeros(n_iter + 1, 1);    % J_TV_n
J_vals = zeros(n_iter + 1, 1);
g_vals = zeros(n_iter + 1, 1);

f_0 = zeros(size(d));
f_0(:) = mean(d(:));
CGU_0 = (d.^2 - f_0.^2) ./ (f_0.^2);

f_vals(1) = {f_0};
CGU_vals(1) = {CGU_0}; 

JR_0 = J_R(d, f_0);
JTV_0 = 1;
JR_vals(1) = JR_0;
JTV_vals(1) = JTV_0;
J_vals(1) = JR_0 * JTV_0;
qq = gradient(J_vals(1:1));

sqDelta = 1 ./ abs(d - f_0);
mg = matGrad(@J, f_0, d, sqDelta, 0.0001);

j = zeros(201, 1);
for i = 1 : 100;
    alpha = -0.1 + i * 0.002;
    f = f_0 - alpha * mg;
    j(i) = J_R(d, f);
end
plot(j);

% for i = 2 : n_iter + 1
%     f_prev = f_vals{i-1};
%     sqDelta = 1 ./ abs(d - f_prev);
%     J = J_R(d, f_prev) * J_TV(f_prev, sqDelta);
%     g = gradient(J);
%     g_val = g(end);
%     disp(g_val);
% end



% G_vals = zeros(n_iter + 1, 1);

% G_vals(1) = g0 = 1;

% for i = 1 : n_iter
%     f_new = imfilter(f_prev, fspecial('gaussian'));
%     JR_vals(i) = J_R(d, f_prev);
%     sqDelta = 1 ./ abs(d - f_prev);
%     JTV_vals(i) = J_TV(f_prev, sqDelta);
%     J_vals(i) = JR_vals(i) * JTV_vals(i);
%     
%     J_gr = gradient(J_vals(1:i));
%     g = J_gr(end);
%     disp(g);
%     
%     f_prev = f_new;
% end
% 
% J_vals_gr = gradient(J_vals);


% imshow(i * 255);
% figure;
% imshow(i_n * 255);
% figure;
% imshow(f_prev * 255);
% figure;
% plot(JR_vals);

% imshow(gr1 * 255);
% figure;
% imshow(gr2 * 255);
% figure;
% imshow(gr3 * 255);
% figure;
% imshow(sqrt(gr1.^2 + gr2.^2) * 255);

% x = -2:0.1:2;
% z = x .* exp(-x.^2);
% q = gradient(z, 2.5);
% plot(q)

% v = -2:0.2:2;
% [x,y] = meshgrid(v);
% z = x .* exp(-x.^2 - y.^2);
% [px,py] = gradient(z,.2,.2);
% 
% contour(v,v,z)
% hold on
% quiver(v,v,px,py)
% hold off
