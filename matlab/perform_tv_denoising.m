function [xtv] = perform_tv_denoising(x, options)

% perform_tv_denoising - denoising with TV minimization
%
%   [xtv,err,tv] = perform_tv_denoising(x,options);
%
%   Solve the lagragian TV minimization (Rudin-Osher-Fatemi)
%       xtv = argmin_u 1/2 * |x-u|^2 + lambda*TV(u)
%   where TV is the discrete TV norm
%       1D:  TV(u) = sum_i |u(i)-u(i-1)|
%       2D:  TV(u) = sum_i sqrt( ux(i)^2 + uy(i) )
%   where ux,uy are partial derivative (finite difference along x/y).
%
%   It implements Chambolle's iterative fixed point algorithm
%       A. Chambolle: An Algorithm for Total Variation Minimization and Applications.
%       Journal of Mathematical Imaging and Vision 20 (1-2): 89-97, January - March, 2004
%
%   Works for 1D signal and 2D images.
%
%   lambda controls how much denoising you want.
%   You can track the denoising level in various way:
%       * simply set options.lambda
%       * set values options.lambda_max and options.lambda_min
%       and the algorithm will linearely decrease the value of 
%       lambda between these values.
%       If you set (in an oracle fashion) options.x0, then 
%       the algorihtm will return the xtv that is the closest to x0.
%       * set an error tolerance options.etgt and the algorithm
%       will return the xtv such that norm(xtv-x,'fro')=etgt
%
%   To perform TV inpainting, you can set options.mask and options.xtgt
%   where mask(i)=1 is a missing pixel and mask(i)=0 impose value xtgt(i)
%   for the solution.
%
%   Gabriel Peyre


options.null = 0;

if ~ismatrix(x)
    error('invalid input');
end

la = getoptions(options, 'lambda', []);
lamax = .4;
lamin = eps;
if not(isempty(la))
    lamax = la; lamin = la;
end
lamax = getoptions(options, 'lambda_max', lamax);
lamin = getoptions(options, 'lambda_min', lamin);
lamin = max(lamin,eps);

% possible use for inpainting
mask = getoptions(options, 'mask', []);
xtgt = getoptions(options, 'xtgt', []);

niter = getoptions(options, 'niter', 3000);
% max iteration for inner loop
niter_inner = getoptions(options, 'niter_inner', 30);
% tolerance for early stop in inner loop
tol = getoptions(options, 'tol', 5*1e-4);

tau = getoptions(options, 'tau', 2/8);
if tau > 2/8
    warning('tau is too large for convergence');
    tau = 2/8;
end
xi = zeros([size(x) 2]);
x1 = x;
xi = getoptions(options,'xi', xi );
TVoperator = getoptions(options, 'TVoperator', []);
if not(isempty(TVoperator))
    y = getoptions(options, 'TVrhs', [], 1); % mendatory
end

if isempty(TVoperator)
    niter_inner = 1;
end

lalist = linspace(lamax, lamin, niter);

ndisp = max(round(niter/100), 2);

clf;

errs = [];
for i = 1 : niter
    
    progressbar(i, niter);
    
    la = lalist(i);
    
    if not(isempty(xtgt)) && not(isempty(mask))
        % inpainting mode
        x = x - la * div(xi);
        x(mask==0) = xtgt(mask==0);
    end
    if not(isempty(TVoperator))
        % perform projection on constraintes
        x = x - la * div(xi);
        r = y - feval(TVoperator, x, +1, options);
        x = x + feval(TVoperator, r, -2, options);
        x = real(x);
    end
    
    %%% INNER LOOP
    %for iinner = 1 : niter_inner
        % chambolle step
        gdv = grad(div(xi) - x / la);
        d = sqrt(sum(gdv.^2, 3));
        d = repmat(d, [1 1 2]);
        xi = (xi + tau * gdv) ./ (1 + tau * d);        
        % reconstruct
        x2 = x - la * div(xi);
        % early stop ?
        relerr = norm(x2 - x1, 'fro') / sqrt(numel(x1));
        if ~isempty(errs) && relerr > errs(end)
            break;
        end
        errs = [errs relerr];
%         if relerr < tol
%             x1 = x2;
%             break;
%         end
        x1 = x2; % new solution
    %end
    
    %if mod(i,ndisp) == 1
        imageplot(x1);
%         pause(0.1);
        drawnow;
    %end
     
end
xtv = x1;
% imageplot(xtv);
