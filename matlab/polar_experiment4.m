IML = im2double(rgb2gray(imread('C:\\pic1.jpg')))';
im = IML';   % scan_lines (rotated for now)

load res;
imPolar = res.CData;

%------
theta0 = pi/6;
H0 = 300;
%------

col_size = size(IML,1);
zeros_row = zeros(H0, col_size);
im = [zeros_row; im];

[nY, nX] = size(im);

min_ang = 2 * theta0 / nX;
theta = -theta0 : min_ang : theta0 - min_ang;
min_rho = 1 / nY;
rho = 0 : min_rho : 1 - min_rho;

resX = 1000; % image resolution [pix]
resY = 1000;

%plot input image
subplot(1,2,1); imagesc(theta, rho, fliplr(IML')); colormap(gray)

% create regular Cartesian grid to remap to
vecY = (0 : 1/resY : 1) - 1/2;
vecX = 0 : 1/resX : 1;
[matX, matY] = ndgrid(vecX, vecY');

% convert new points to polar coordinates
[th_cart, r_cart] = cart2pol(matX, matY);
% interpolate using linear interpolation
op = interp2(theta, rho, im, th_cart, r_cart, 'cubic').';
op(isnan(op)) = 0;

subplot(1,2,2); 
res = imagesc(vecY, vecX, op');
%save res;
colormap(gray);

[matXX, matYY] = pol2cart(th_cart, r_cart);
op2 = interp2(-th_cart, r_cart, imPolar, matXX, matYY);

