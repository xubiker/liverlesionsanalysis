function [ noisyImage ] = addSpeckleNoise( image, gamma, stdev)
%ADDSPECKLENOISE adds speckle noise with given parameters to the image
%   input: image, gamma, stdev

    noise = normrnd(0, stdev, size(image));
    gammaImage = image .^ gamma;
    noisyImage = image + gammaImage .* noise;
end