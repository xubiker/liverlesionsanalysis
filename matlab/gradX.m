function [grad] = gradX(J, x, eps)
%UNTITLED12 Summary of this function goes here
%   Detailed explanation goes here

if ~ismatrix(x)
    error('unsupported variable type');
end

if size(x, 2) == 1 % ------------------------------- case of a vector -----
    
    n = numel(x);
    grad = zeros(n, 1);
    for i = 1 : n
        alpha = zeros(n, 1);
        alpha(i) = eps;
        grad(i) = (J(x + alpha) - J(x)) / eps;
    end
    
elseif size(x, 2) > 1 % ---------------------------- case of a matrix -----

    [h, w] = size(x);
    grad = zeros(h,w);
    g = x;
    J_x = J(x);
    for i = 1 : h
        for j = 1 : w
            g(i,j) = g(i,j) + eps;
            grad(i,j) = (J(g) - J_x) / eps;
            g(i,j) = g(i,j) - eps;
        end
    end
    
%     grad2 = zeros(h,w);
%     g2 = x;
%     n = numel(x);
%     g2r = reshape(g2, n, 1);
%     for i = 1 : n
%         g2r(i) = g2r(i) + eps;
%         grad2(i) = (J(reshape(g2r, h, w)) - J_x) / eps;
%         g2r(i) = g2r(i) - eps;
%     end
    
end

end

