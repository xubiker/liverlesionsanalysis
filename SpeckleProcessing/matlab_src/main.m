warning('off', 'all');
close all;

folderPath = 'c:\\dev\\LiverFibrosisAnalysis\\SpeckleProcessing\\brodatz_textures\\';
imageNames = [
    '1.3.01.tiff' ...
    '1.3.04.tiff' ...
    '1.3.05.tiff' ...
    '1.3.06.tiff' ...
    '1.3.07.tiff' ...
    '1.3.08.tiff' ...
    '1.3.10.tiff' ...
    '1.3.11.tiff' ...
    {} ...
];

% ----- prepare images -----
images = cell(numel(imageNames), 1);
for i = 1 : numel(imageNames)
    image = imread([folderPath imageNames{i}]);
    if size(image, 3) == 3, image = rgb2gray(image); end
    %imadjust(image, stretchlim(image), []);
    image = im2double(image);
    images{i} = image;
    %imshow(image); figure;
end

% ----- prepare speckle parameters -----
speckle_gamma = 0.5;
speckle_stdev = 0 : 0.1 : 1.0;

% ----- add speckle to images -----
progressH = waitbar(0, '', 'Name', 'Adding speckle noise...');
noisyImages = cell(numel(images), numel(speckle_stdev));
for i = 1 : numel(images)
    image = images{i};
    for j = 1 : numel(speckle_stdev)
        noisyImage = addSpeckleNoise(image, speckle_gamma, speckle_stdev(j));
        %imshow(noisyImage); pause(0.3);
        noisyImages{i, j} = noisyImage;
        prc = (numel(speckle_stdev) *(i - 1) + j) / (numel(images) * numel(speckle_stdev));
        waitbar(prc, progressH, sprintf('%.1f%%', prc * 100));
    end
end
close(progressH);

% ========== configure textureDscr params =================================

% --- plain texture params ---
originalParams = {{}};
originalStatistics = {'MEAN', 'SD', 'VAR', 'ASM', 'SKEW', 'KURT', 'ENT'};

% --- laws' params ---
lawsParams = {
               {'LL'}, {'LE'}, {'LS'}, {'LR'}, {'LW'}, ...
               {'EE'}, {'ES'}, {'ER'}, {'EW'}, ...
               {'SS'}, {'SR'}, {'SW'}, ...
               {'RR'}, {'RW'}, ...
                {'WW'}
             };
lawsStatistics = {'MEAN', 'SD', 'SKEW', 'KURT', 'ENT', 'ASM', 'HOM', 'CON'};

% --- glcms' params ---
dRange = 1 : 20;
postLevels = 256;
symmetric = true;
glcmParams = cell(numel(dRange*3), 1);
for i = 1 : numel(dRange)
    glcmParams{i} = {postLevels, [0, dRange(i)]', symmetric};
    glcmParams{numel(dRange) + i} = {postLevels, [dRange(i), 0]', symmetric};
    glcmParams{numel(dRange) * 2 + i} = {postLevels, [dRange(i), dRange(i)]', symmetric};
end
glcmStatistics = {'MEAN', 'SD', 'ASM', 'ENT', 'CON', 'HOM', 'DIS', 'COR'};

% --- glrlms' params ---
glrlmParams = { 
                {postLevels, 0}, ...
                {postLevels, 45}, ...
                {postLevels, 90}, ...
                {postLevels, 135}
              };
glrlmStatistics = { 'SRE', 'LRE', 'LGRE', 'HGRE', ...
                    'SRLGE', 'SRHGE', 'LRLGE', 'LRHGE', ...
                    'GLNU', 'RLNU' };

                
f = calcFeatureSeries(noisyImages, 'original', {{}}, originalStatistics, speckle_stdev, imageNames, true);
                
                
% ========== process features =============================================

roiSize = [100 100];
roiSpeckle_gamma = 0.5;
roiSpeckle_stdev = 0 : 0.1 : 0.5;
rois = cell(numel(images), 1);
spoiledRois = cell(numel(images), numel(roiSpeckle_stdev));
for i = 1 : numel(images)
    roi = extractRoi(images{i}, roiSize, 'random');
    rois{i} = roi;
    for j = 1 : numel(roiSpeckle_stdev)
        spoiledRois{i, j} = addSpeckleNoise(roi, roiSpeckle_gamma, roiSpeckle_stdev(j));
    end
end

rf1 = calcFeatureSeries(spoiledRois, 'original', {{}}, originalStatistics, speckle_stdev, imageNames, false);
rf2 = calcFeatureSeries(spoiledRois, 'glcm', glcmParams, glcmStatistics, speckle_stdev, imageNames, false);
rf3 = calcFeatureSeries(spoiledRois, 'laws', lawsParams, lawsStatistics, speckle_stdev, imageNames, false);
rf4 = calcFeatureSeries(spoiledRois, 'glrlm', glrlmParams, glrlmStatistics, speckle_stdev, imageNames, false);
rf = cat(3, rf3);


for i = 1 : numel(images)
    r = extractRoi(images{i}, roiSize, 'random');
    for j = 1 : numel(roiSpeckle_stdev)
        testRoi = addSpeckleNoise(r, roiSpeckle_gamma, roiSpeckle_stdev(j));
    
        tf1 = calcFeatureSeries({testRoi}, 'original', {{}}, originalStatistics, speckle_stdev, imageNames, false);
        tf2 = calcFeatureSeries({testRoi}, 'glcm', glcmParams, glcmStatistics, speckle_stdev, imageNames, false);
        tf3 = calcFeatureSeries({testRoi}, 'laws', lawsParams, lawsStatistics, speckle_stdev, imageNames, false);
        tf4 = calcFeatureSeries({testRoi}, 'glrlm', glrlmParams, glrlmStatistics, speckle_stdev, imageNames, false);
        tf = cat(3, tf3);
        
        distMatrix = zeros(size(spoiledRois));

        for p = 1 : size(distMatrix, 1)
            for q = 1 : size(distMatrix, 2)
                diff = abs(rf(p,q,:) - tf);
                distMatrix(p,q) = norm(diff(:));
            end
        end
        [minVal, idx] = min(distMatrix(:));
        [p, q] = ind2sub(size(distMatrix), idx);
        if (i == p) && (j == q), res = 'correct'; else res = 'incorrect'; end
        fprintf('(%d, %d) classified as (%d, %d) - %s\n', i, j, p, q, res);
    
    end
end



% f1 = calcFeatureSeries(noisyImages, 'original', {{}}, originalStatistics, speckle_stdev, imageNames, true);
% f2 = calcFeatureSeries(noisyImages, 'glcm', glcmParams, glcmStatistics, speckle_stdev, imageNames, true);
% f3 = calcFeatureSeries(noisyImages, 'laws', lawsParams, lawsStatistics, speckle_stdev, imageNames, true);
% f4 = calcFeatureSeries(noisyImages, 'glrlm', glrlmParams, glrlmStatistics, speckle_stdev, imageNames, true);

% f = cat(3, f1, f4);