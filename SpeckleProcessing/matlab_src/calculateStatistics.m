function [value] = calculateStatistics(matrix, statistic, optionalParams)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

if nargin < 2 || nargin > 3 || nargout ~= 1
    error('invalid input/output args cnt');
end
    
if ~isreal(matrix) || ~isa(matrix, 'double') || ~ismatrix(matrix) || issparse(matrix)
    error('invalid matrix');
end

if ~isreal(statistic) || ~isa(statistic, 'char')
    error('invalid statistic');
end

if nargin == 2
    optionalParams = {};
end

value = calculateStatistics_mx(matrix, statistic, optionalParams);

end

