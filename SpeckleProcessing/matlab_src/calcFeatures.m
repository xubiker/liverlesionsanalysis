function [features] = calcFeatures(images, textureDscrType, textureDscrParams, statistics)

features = zeros(size(images, 1), size(images, 2), numel(statistics));

for i = 1 : size(images, 1)
   for j = 1 : size(images, 2)
       image = images{i, j};
       textureDscr = calcTextureDscr(image, textureDscrType, textureDscrParams);
       for k = 1 : numel(statistics)
           val = calculateStatistics(textureDscr, statistics{k});
           features(i, j, k) = val;
       end
   end
end

end