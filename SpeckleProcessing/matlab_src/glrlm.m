function [ glrlm_res ] = glrlm(matrix, direction)
%GLCM Summary of this function goes here
%   Detailed explanation goes here

if nargin ~= 2 || nargout ~= 1
    error('invalid input/output args cnt');
end
    
if ~isreal(matrix) || ~isa(matrix, 'double') || ~ismatrix(matrix) || issparse(matrix)
    error('invalid matrix');
end

if ~isreal(direction) || ~isa(direction, 'double') || ~ismatrix(direction) ...
    || size(direction, 1) ~= 1 || size(direction, 2) ~= 1
    error('invalid dist');
end

glrlm_res = (glrlm_mx(matrix, direction));

end

