function [ glcm_res ] = glcm( matrix, dist, simmetric )
%GLCM Summary of this function goes here
%   Detailed explanation goes here

if nargin ~= 3 || nargout ~= 1
    error('invalid input/output args cnt');
end
    
if ~isreal(matrix) || ~isa(matrix, 'double') || ~ismatrix(matrix) || issparse(matrix)
    error('invalid matrix');
end

% if ~isreal(nlevels) || ~isa(nlevels, 'uint16') || numel(nlevels) ~= 1 
%     error('invalid nlevels');
% end
    
if ~isreal(dist) || ~isa(dist, 'double') || ~ismatrix(dist) ...
    || size(dist, 1) ~= 2 || size(dist, 2) ~= 1
    error('invalid dist');
end

if ~isreal(simmetric) || ~isa(simmetric, 'logical') || ~numel(simmetric)
    error('invalid simmetric');
end

if simmetric
    glcmMatrix1 = glcm_mx(matrix, dist);
    glcmMatrix2 = glcm_mx(matrix, -dist);
    glcm_res = (glcmMatrix1 + glcmMatrix2) / 2;
else
    glcm_res = glcm_mx(matrix, dist);
end

end

