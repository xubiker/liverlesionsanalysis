function [string] = toString(var)

switch class(var)
    case 'double'
        if ~ismatrix(var)
            error('unsupported input');
        end
        string = num2str(var(:)');
    case 'char'
        string = var;
    case 'logical'
        if var == true, string = 'true';
        else string = 'false'; end
    otherwise
        error('unsupported input');
end

end
