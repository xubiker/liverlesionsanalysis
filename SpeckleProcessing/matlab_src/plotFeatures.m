function [] = plotFeatures(features, speckleStdevRange, ...
    imageLabels, transformLabel, statisticsLabels)

figureH = figure('name', transformLabel);
set(gca, 'Position',[0 0 1 1]);
set(gcf, 'Position', get(0,'Screensize'));
set(gcf, 'PaperPositionMode','auto');

numOfStatistics = size(features, 3);
subplotHCnt = floor(sqrt(numOfStatistics));
subplotWCnt = ceil((numOfStatistics) / subplotHCnt);

for i = 1 : numOfStatistics
    
    subplot(subplotHCnt, subplotWCnt, i);
    
    hold all;
    for j = 1 : size(features,1)
        plot(speckleStdevRange, features(j,:,i));
    end
    hold off;

%     legend(imageLabels);
    title([transformLabel ' - ' statisticsLabels{i}]);
%     xlabel([num2str(speckleStdevRange(1)) ' \leq \sigma \leq ' num2str(speckleStdevRange(end))]);
    
end

legend(imageLabels);%, 'Location', 'NorthOutside');
legend('hide');

savefig([pwd() '\\plots\\' transformLabel '.fig']);
print(figureH, '-dpng', [pwd() '\\plots\\' transformLabel '.png']);

close(figureH);

end