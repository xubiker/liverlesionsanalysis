function [ B ] = arrayProduct( m, A )
%ARRAYPRODUCT Summary of this function goes here
%   Detailed explanation goes here

if nargin ~= 2 || nargout ~= 1
    error('invalid input/output args cnt');
end
    
if ~isreal(A) || ~isa(A, 'double') || ndims(A) ~= 1 || issparse(A)
    error('invalid A');
end

if ~isreal(m) || ~isa(m, 'double') || ndims(m) ~= 1 || numel(m) ~= 1
    error('invalid m');
end

B = arrayProdcut_mx(m, A);

end

