#include "mex.h"
#include <math.h>

//=================================================================================================
void posterizeGrayScale(double* inMatrix, double* outMatrix, int nrows, int ncols, double minVal, double maxVal, int intensCnt) {
	
	double step = (maxVal - minVal) / (intensCnt - 1);

	for (mwSize j = 0; j < ncols; j++) {
		for (mwSize i = 0; i < nrows; i++) {
			double val = inMatrix[j * nrows + i];
			double valPost = floor((val - minVal) / step);
			valPost = fmax(valPost, 0);
			valPost = fmin(valPost, intensCnt - 1);
            outMatrix[j * nrows + i] = valPost;
		}
	}
}

//=================================================================================================
void mexFunction(int nlhs, mxArray *plhs[],	int nrhs, const mxArray *prhs[]) {

	double* inMatrix = mxGetPr(prhs[0]);
	size_t ncols = mxGetN(prhs[0]);
	size_t nrows = mxGetM(prhs[0]);

	double minVal = mxGetScalar(prhs[1]);
	double maxVal = mxGetScalar(prhs[2]);

	auto nlevels = static_cast<int>(mxGetScalar(prhs[3]));

	plhs[0] = mxCreateDoubleMatrix(nrows, ncols, mxREAL);
	double* outMatrix = mxGetPr(plhs[0]);

	posterizeGrayScale(inMatrix, outMatrix, nrows, ncols, minVal, maxVal, nlevels);
}
