#include "mex.h"

//=================================================================================================
int getNLevels(double* matrix, int nrows, int ncols) {
	
	double maxVal = matrix[0];
	for (mwSize j = 0; j < ncols; j++) {
		for (mwSize i = 0; i < nrows; i++) {
			double v = matrix[j * nrows + i];
			if (v > maxVal)
				maxVal = v;
		}
	}
	int nlevels = static_cast<int>(maxVal) + 1;
	return nlevels;
}

//=================================================================================================
void calculateGLCMN(double* image, int nrows, int ncols, double* glcm, int nlevels, int dr, int dc, bool normalize = true) {

	double sum = 0;

	int rowsBegin = (dr > 0) ? 0 : - dr;
	int rowsEnd   = (dr > 0) ? nrows - dr : nrows;
	int colsBegin = (dc > 0) ? 0 : - dc;
	int colsEnd   = (dc > 0) ? ncols - dc : ncols;
        
	for (auto j = colsBegin; j < colsEnd; j++) {
		for (auto i = rowsBegin; i < rowsEnd; i++) {
			int v1 = static_cast<int>(image[j * nrows + i]);
			int v2 = static_cast<int>(image[(j + dc) * nrows + i + dr]);

			if ((v1 >= 0) && (v1 < nlevels) && (v2 >= 0) && (v2 < nlevels)) {
				glcm[v1 * nlevels + v2]++;
				sum += 1;
			}
		}
	}

	// --- normalize ---
	if (normalize) {
		for (mwSize i = 0; i < nlevels; i++) {
			for (mwSize j = 0; j < nlevels; j++) {
				glcm[i * nlevels + j] /= sum;
			}
		}
	}

}

//=================================================================================================
void mexFunction(int nlhs, mxArray *plhs[],	int nrhs, const mxArray *prhs[]) {

	double* inputMatrix = mxGetPr(prhs[0]);
	size_t ncols = mxGetN(prhs[0]);
	size_t nrows = mxGetM(prhs[0]);

	//int nlevels = static_cast<int>(mxGetScalar(prhs[1]));
	int nlevels = getNLevels(inputMatrix, nrows, ncols);

	double* dist = mxGetPr(prhs[1]);
	int dr = static_cast<int>(dist[0]);
	int dc = static_cast<int>(dist[1]);

	//plhs[0] = mxCreateNumericMatrix(nlevels, nlevels, mxUINT32_CLASS, mxREAL);
	//unsigned int* glcmMatrix = reinterpret_cast<unsigned int*>(mxGetPr(plhs[0]));
	plhs[0] = mxCreateDoubleMatrix(nlevels, nlevels, mxREAL);
	double* glcmMatrix = mxGetPr(plhs[0]);

	calculateGLCMN(inputMatrix, nrows, ncols, glcmMatrix, nlevels, dr, dc);
}