#include "mex.h"
#include <algorithm>

//=================================================================================================
int getNLevels(double* matrix, int nrows, int ncols) {

	double maxVal = matrix[0];
	for (mwSize j = 0; j < ncols; j++) {
		for (mwSize i = 0; i < nrows; i++) {
			double v = matrix[j * nrows + i];
			if (v > maxVal)
				maxVal = v;
		}
	}
	int nlevels = static_cast<int>(maxVal) + 1;
	return nlevels;
}

//=================================================================================================
int getRunLength(int nrows, int ncols) {
	return std::max(nrows, ncols);
}

//=================================================================================================
void calculateGLRLM(double* image, int nrows, int ncols, double* glrlm, int nlevels, int runLength, int direction) {

	// no need to clear memory at glrlm array (it is initialized by matlab)

	switch (direction) {

		case 0: // stupid matlab indexing way [in normal, it's 90]
			for (auto j = 0; j < ncols; j++) {
				auto startVal = static_cast<int>(image[j]);
				int currLength = 1;
				for (auto i = 1; i < nrows; i++) {
					auto currVal = static_cast<int>(image[i * ncols + j]);
					if (currVal == startVal) {
						currLength++;
					} else {
						glrlm[(currLength - 1) * nlevels + startVal]++;
						//glrlm[startVal * runLength + currLength - 1]++;
						startVal = currVal;
						currLength = 1;
					}
				}
				glrlm[(currLength - 1) * nlevels + startVal]++;
				//glrlm[startVal * runLength + currLength - 1]++;
			}
			break;

		case 90: // stupid matlab indexing way [in normal, it's 0]
			for (auto i = 0; i < nrows; i++) {
				int startVal = static_cast<int>(image[i * ncols]);
				int currLength = 1;
				for (auto j = 1; j < ncols; j++) {
					auto currVal = static_cast<int>(image[i * ncols + j]);
					if (currVal == startVal) {
						currLength++;
					} else {
						glrlm[(currLength - 1) * nlevels + startVal]++;
						//glrlm[startVal * runLength + currLength - 1]++;
						startVal = currVal;
						currLength = 1;
					}
				}
				glrlm[(currLength - 1) * nlevels + startVal]++;
				//glrlm[startVal * runLength + currLength - 1]++;
			}
			break;
		case 135: // stupid matlab indexing way [in normal, it's 45]
			for (auto i = 0; i < ncols + nrows - 1; i++) {
				int startPosC, startPosR, lineLength;
				if (i < nrows) {
					startPosR = i;
					startPosC = ncols - 1;
					lineLength = std::min(ncols, nrows - i);
				} else {
					startPosR = 0;
					startPosC = i - nrows;
					lineLength = std::min(nrows, i - nrows + 1);
				}
				auto startVal = static_cast<int>(image[startPosR * ncols + startPosC]);
				int currLength = 1;
				for (auto j = 1; j < lineLength; j++) {
					auto currVal = static_cast<int>(image[(startPosR + j) * ncols + startPosC - j]);
					if (currVal == startVal) {
						currLength++;
					} else {
						glrlm[(currLength - 1) * nlevels + startVal]++;
						//glrlm[startVal * runLength + currLength - 1]++;
						startVal = currVal;
						currLength = 1;
					}
				}
				glrlm[(currLength - 1) * nlevels + startVal]++;
				//glrlm[startVal * runLength + currLength - 1]++;
			}
			break;
		case 45: // stupid matlab indexing way [in normal, it's 135]
			for (auto i = 0; i < ncols + nrows - 1; i++) {
				int startPosC, startPosR, lineLength;
				if (i < nrows) {
					startPosR = i;
					startPosC = 0;
					lineLength = std::min(ncols, nrows - i);
				} else {
					startPosR = 0;
					startPosC = i - nrows + 1;
					lineLength = std::min(nrows, nrows + ncols - i - 1);
				}
				auto startVal = static_cast<int>(image[startPosR * ncols + startPosC]);
				int currLength = 1;
				for (auto j = 1; j < lineLength; j++) {
					auto currVal = static_cast<int>(image[(startPosR + j) * ncols + startPosC + j]);
					if (currVal == startVal) {
						currLength++;
					} else {
						glrlm[(currLength - 1) * nlevels + startVal]++;
						//glrlm[startVal * runLength + currLength - 1]++;
						startVal = currVal;
						currLength = 1;
					}
				}
				glrlm[(currLength - 1) * nlevels + startVal]++;
				//glrlm[startVal * runLength + currLength - 1]++;
			}
			break;

	}   
}

//=================================================================================================
void mexFunction(int nlhs, mxArray *plhs[],	int nrhs, const mxArray *prhs[]) {

	double* inputMatrix = mxGetPr(prhs[0]);
	int ncols = static_cast<int>(mxGetN(prhs[0]));
	int nrows = static_cast<int>(mxGetM(prhs[0]));

	//int nlevels = static_cast<int>(mxGetScalar(prhs[1]));
	int nlevels = getNLevels(inputMatrix, nrows, ncols);

	double* direction = mxGetPr(prhs[1]);
	int d = static_cast<int>(direction[0]);

	int runLength = getRunLength(nrows, ncols);

	//plhs[0] = mxCreateNumericMatrix(nlevels, nlevels, mxUINT32_CLASS, mxREAL);
	//unsigned int* glcmMatrix = reinterpret_cast<unsigned int*>(mxGetPr(plhs[0]));
	plhs[0] = mxCreateDoubleMatrix(nlevels, runLength, mxREAL);
	double* glrlm = mxGetPr(plhs[0]);

	calculateGLRLM(inputMatrix, nrows, ncols, glrlm, nlevels, runLength, d);
}