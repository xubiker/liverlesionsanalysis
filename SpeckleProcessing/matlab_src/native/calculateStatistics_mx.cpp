#include "mex.h"
#include <iostream>
#include <string>

using namespace std;

//=================================================================================================
double calculateStatistics_min(double* matrix, int nrows, int ncols) {

	double minVal = matrix[0];
	for (mwSize j = 0; j < ncols; j++) {
		for (mwSize i = 0; i < nrows; i++) {
			double v = matrix[j * nrows + i];
			if (v < minVal)
				minVal = v;
		}
	}
	return minVal;
}

//=================================================================================================
double calculateStatistics_max(double* matrix, int nrows, int ncols) {

	double maxVal = matrix[0];
	for (mwSize j = 0; j < ncols; j++) {
		for (mwSize i = 0; i < nrows; i++) {
			double v = matrix[j * nrows + i];
			if (v > maxVal)
				maxVal = v;
		}
	}
	return maxVal;
}

//=================================================================================================
double calculateStatistics_mean(double* matrix, int nrows, int ncols) {

	double mean = 0;
	for (mwSize j = 0; j < ncols; j++) {
		for (mwSize i = 0; i < nrows; i++) {
			mean += matrix[j * nrows + i];
		}
	}
	mean /= (nrows * ncols);
	return mean;
}

//=================================================================================================
double calculateStatistics_sd(double* matrix, int nrows, int ncols) {

	double mean = calculateStatistics_mean(matrix, nrows, ncols);
	double sd = 0;
	for (mwSize j = 0; j < ncols; j++) {
		for (mwSize i = 0; i < nrows; i++) {
			double v = matrix[j * nrows + i];
			sd += (v - mean) * (v - mean);
		}
	}
	sd = sqrt(sd / (nrows * ncols));
	return sd;
}

//=================================================================================================
double calculateStatistics_var(double* matrix, int nrows, int ncols) {

	double sd = calculateStatistics_sd(matrix, nrows, ncols);
	double mean = calculateStatistics_mean(matrix, nrows, ncols);
	double var = sd / mean;
	return var;
}

//=================================================================================================
double calculateStatistics_energy(double* matrix, int nrows, int ncols) {

	double energy = 0;
	for (mwSize j = 0; j < ncols; j++) {
		for (mwSize i = 0; i < nrows; i++) {
			double v = matrix[j * nrows + i];
			energy += v * v;
		}
	}
	//energy /= (nrows * ncols);
	return energy;
}

//=================================================================================================
double calculateStatistics_skewness(double* matrix, int nrows, int ncols) {

	double sd = calculateStatistics_sd(matrix, nrows, ncols);
	double mean = calculateStatistics_mean(matrix, nrows, ncols);
	double skewness = 0;
	for (mwSize j = 0; j < ncols; j++) {
		for (mwSize i = 0; i < nrows; i++) {
			double v = matrix[j * nrows + i];
			skewness += (v - mean) * (v - mean) * (v - mean);
		}
	}
	skewness = skewness / (sd * sd * sd * nrows * ncols);
	return skewness;
}

//=================================================================================================
double calculateStatistics_kurtosis(double* matrix, int nrows, int ncols) {

	double sd = calculateStatistics_sd(matrix, nrows, ncols);
	double mean = calculateStatistics_mean(matrix, nrows, ncols);
	double kurtosis = 0;
	for (mwSize j = 0; j < ncols; j++) {
		for (mwSize i = 0; i < nrows; i++) {
			double v = matrix[j * nrows + i];
			kurtosis += (v - mean) * (v - mean) * (v - mean) * (v - mean);
		}
	}
	kurtosis = kurtosis / (sd * sd * sd * sd * nrows * ncols);
	return kurtosis;
}

//=================================================================================================
double calculateStatistics_entropy(double* matrix, int nrows, int ncols) {

	double entropy = 0;
	for (mwSize j = 0; j < ncols; j++) {
		for (mwSize i = 0; i < nrows; i++) {
			double v = matrix[j * nrows + i];
			if (v > 0)
				entropy += v * log(v);
		}
	}
	entropy = -entropy;
	return entropy;
}

//=================================================================================================
double calculateStatistics_contrast(double* matrix, int nrows, int ncols) {

	double contrast = 0;
	for (mwSize j = 0; j < ncols; j++) {
		for (mwSize i = 0; i < nrows; i++) {
			double v = matrix[j * nrows + i];
			contrast += v * (i - j) * (i - j);
		}
	}
	return contrast;
}

//=================================================================================================
double calculateStatistics_homogeneity(double* matrix, int nrows, int ncols) {

	double homogeneity = 0;
	for (mwSize j = 0; j < ncols; j++) {
		for (mwSize i = 0; i < nrows; i++) {
			double v = matrix[j * nrows + i];
			homogeneity += v / (1 + abs(i - j));
		}
	}
	return homogeneity;
}

//=================================================================================================
double calculateStatistics_dissimilarity(double* matrix, int nrows, int ncols) {

	double dissimilarity = 0;
	for (mwSize j = 0; j < ncols; j++) {
		for (mwSize i = 0; i < nrows; i++) {
			double v = matrix[j * nrows + i];
			dissimilarity += v * abs(i - j);
		}
	}
	return dissimilarity;
}

//=================================================================================================
double calculateStatistics_correlation(double* matrix, int nrows, int ncols) {

	double mu_x = 0, mu_y = 0;
	for (mwSize j = 0; j < ncols; j++) {
		for (mwSize i = 0; i < nrows; i++) {
			double v = matrix[j * nrows + i];
			mu_x += v * i;
			mu_y += v * j;
		}
	}

	double stdev_x = 0, stdev_y = 0;
	for (mwSize j = 0; j < ncols; j++) {
		for (mwSize i = 0; i < nrows; i++) {
			double v = matrix[j * nrows + i];
			stdev_x += (-mu_x + i) * (-mu_x + i) * v;
			stdev_y += (-mu_y + j) * (-mu_y + j) * v;
		}
	}

	double correlation = 0;
	for (mwSize j = 0; j < ncols; j++) {
		for (mwSize i = 0; i < nrows; i++) {
			double v = matrix[j * nrows + i];
			correlation += (-mu_x + i) * (-mu_y + j) / sqrt(stdev_x * stdev_y) * v;
		}
	}

	return correlation;
}

//=================================================================================================
double calculateStatistics_SRE(double* matrix, int nrows, int ncols) {

	double sum = 0;
	double sre = 0;
	for (mwSize j = 0; j < ncols; j++) {
		for (mwSize i = 0; i < nrows; i++) {
			double v = matrix[j * nrows + i];
			sum += v;
	 		sre += v / ((j + 1) * (j + 1));
		}
	}
	sre /= sum;
	return sre;
}

//=================================================================================================
double calculateStatistics_LRE(double* matrix, int nrows, int ncols) {

	double sum = 0;
	double lre = 0;
	for (mwSize j = 0; j < ncols; j++) {
		for (mwSize i = 0; i < nrows; i++) {
			double v = matrix[j * nrows + i];
			sum += v;
	 		lre += v * ((j + 1) * (j + 1));
		}
	}
	lre /= sum;
	return lre;
}

//=================================================================================================
double calculateStatistics_LGRE(double* matrix, int nrows, int ncols) {

	double sum = 0;
	double lgre = 0;
	for (mwSize j = 0; j < ncols; j++) {
		for (mwSize i = 0; i < nrows; i++) {
			double v = matrix[j * nrows + i];
			sum += v;
	 		lgre += v / ((i + 1) * (i + 1));
		}
	}
	lgre /= sum;
	return lgre;
}

//=================================================================================================
double calculateStatistics_HGRE(double* matrix, int nrows, int ncols) {

	double sum = 0;
	double hgre = 0;
	for (mwSize j = 0; j < ncols; j++) {
		for (mwSize i = 0; i < nrows; i++) {
			double v = matrix[j * nrows + i];
			sum += v;
	 		hgre += v * ((i + 1) * (i + 1));
		}
	}
	hgre /= sum;
	return hgre;
}

//=================================================================================================
double calculateStatistics_SRLGE(double* matrix, int nrows, int ncols) {

	double sum = 0;
	double srlge = 0;
	for (mwSize j = 0; j < ncols; j++) {
		for (mwSize i = 0; i < nrows; i++) {
			double v = matrix[j * nrows + i];
			sum += v;
	 		srlge += v / ((i + 1) * (i + 1) * (j + 1) * (j + 1));
		}
	}
	srlge /= sum;
	return srlge;
}

//=================================================================================================
double calculateStatistics_SRHGE(double* matrix, int nrows, int ncols) {

	double sum = 0;
	double srhge = 0;
	for (mwSize j = 0; j < ncols; j++) {
		for (mwSize i = 0; i < nrows; i++) {
			double v = matrix[j * nrows + i];
			sum += v;
	 		srhge += v * ((i + 1) * (i + 1)) / ((j + 1) * (j + 1));
		}
	}
	srhge /= sum;
	return srhge;
}

//=================================================================================================
double calculateStatistics_LRLGE(double* matrix, int nrows, int ncols) {

	double sum = 0;
	double lrlge = 0;
	for (mwSize j = 0; j < ncols; j++) {
		for (mwSize i = 0; i < nrows; i++) {
			double v = matrix[j * nrows + i];
			sum += v;
	 		lrlge += v * ((j + 1) * (j + 1)) / ((i + 1) * (i + 1));
		}
	}
	lrlge /= sum;
	return lrlge;
}

//=================================================================================================
double calculateStatistics_LRHGE(double* matrix, int nrows, int ncols) {

	double sum = 0;
	double lrhge = 0;
	for (mwSize j = 0; j < ncols; j++) {
		for (mwSize i = 0; i < nrows; i++) {
			double v = matrix[j * nrows + i];
			sum += v;
	 		lrhge += v * ((i + 1) * (i + 1) * (j + 1) * (j + 1));
		}
	}
	lrhge /= sum;
	return lrhge;
}

//=================================================================================================
double calculateStatistics_GLNU(double* matrix, int nrows, int ncols) {

	double sum = 0;
	double glnu = 0;
	for (mwSize i = 0; i < nrows; i++) {
		double s = 0;
		for (mwSize j = 0; j < ncols; j++) {
			double v = matrix[j * nrows + i];
			sum += v;
	 		s += v;
		}
		glnu += s * s;
	}
	glnu /= sum;
	return glnu;
}

//=================================================================================================
double calculateStatistics_RLNU(double* matrix, int nrows, int ncols) {

	double sum = 0;
	double rlnu = 0;
	for (mwSize j = 0; j < ncols; j++) {
		double s = 0;
		for (mwSize i = 0; i < nrows; i++) {
			double v = matrix[j * nrows + i];
			sum += v;
	 		s += v;
		}
		rlnu += s * s;
	}
	rlnu /= sum;
	return rlnu;
}

//=================================================================================================
double calculateStatistics_RPC(double* matrix, int nrows, int ncols, double numberOfPixels) {

	double rpc = 0;
	for (mwSize j = 0; j < ncols; j++) {
		for (mwSize i = 0; i < nrows; i++) {
			double v = matrix[j * nrows + i];
			rpc += v;
		}
	}
	rpc /= numberOfPixels;
	return rpc;
}

//=================================================================================================
double calculateStatistics(double* matrix, int nrows, int ncols, char *statistic_label, double optionalParam_1 = 0) {

	string statistic_label_str(statistic_label);

	if (statistic_label_str.compare("energy") == 0 || statistic_label_str.compare("ASM") == 0) {
		return calculateStatistics_energy(matrix, nrows, ncols);
	}
	if (statistic_label_str.compare("min") == 0 || statistic_label_str.compare("MIN") == 0) {
		return calculateStatistics_min(matrix, nrows, ncols);
	}
	if (statistic_label_str.compare("max") == 0 || statistic_label_str.compare("MAX") == 0) {
		return calculateStatistics_max(matrix, nrows, ncols);
	}
	if (statistic_label_str.compare("mean") == 0 || statistic_label_str.compare("MEAN") == 0) {
		return calculateStatistics_mean(matrix, nrows, ncols);
	}
	if (statistic_label_str.compare("sd") == 0 || statistic_label_str.compare("SD") == 0) {
		return calculateStatistics_sd(matrix, nrows, ncols);
	}
	if (statistic_label_str.compare("var") == 0 || statistic_label_str.compare("VAR") == 0) {
		return calculateStatistics_var(matrix, nrows, ncols);
	}
	if (statistic_label_str.compare("skewness") == 0 || statistic_label_str.compare("SKEW") == 0) {
		return calculateStatistics_skewness(matrix, nrows, ncols);
	}
	if (statistic_label_str.compare("kurtosis") == 0 || statistic_label_str.compare("KURT") == 0) {
		return calculateStatistics_kurtosis(matrix, nrows, ncols);
	}
	if (statistic_label_str.compare("entropy") == 0 || statistic_label_str.compare("ENT") == 0) {
		return calculateStatistics_entropy(matrix, nrows, ncols);
	}
	if (statistic_label_str.compare("contrast") == 0 || statistic_label_str.compare("CON") == 0) {
		return calculateStatistics_contrast(matrix, nrows, ncols);
	}
	if (statistic_label_str.compare("homogeneity") == 0 || statistic_label_str.compare("HOM") == 0) {
		return calculateStatistics_homogeneity(matrix, nrows, ncols);
	}
	if (statistic_label_str.compare("dissimilarity") == 0 || statistic_label_str.compare("DIS") == 0) {
		return calculateStatistics_dissimilarity(matrix, nrows, ncols);
	}
	if (statistic_label_str.compare("correlation") == 0 || statistic_label_str.compare("COR") == 0) {
		return calculateStatistics_correlation(matrix, nrows, ncols);
	}

	// ---------- statistics typical for GLRLM ----------
	if (statistic_label_str.compare("SRE") == 0) {
		return calculateStatistics_SRE(matrix, nrows, ncols);
	}
	if (statistic_label_str.compare("LRE") == 0) {
		return calculateStatistics_LRE(matrix, nrows, ncols);
	}
	if (statistic_label_str.compare("LGRE") == 0) {
		return calculateStatistics_LGRE(matrix, nrows, ncols);
	}
	if (statistic_label_str.compare("HGRE") == 0) {
		return calculateStatistics_HGRE(matrix, nrows, ncols);
	}
	if (statistic_label_str.compare("SRLGE") == 0) {
		return calculateStatistics_SRLGE(matrix, nrows, ncols);
	}
	if (statistic_label_str.compare("SRHGE") == 0) {
		return calculateStatistics_SRHGE(matrix, nrows, ncols);
	}
	if (statistic_label_str.compare("LRLGE") == 0) {
		return calculateStatistics_LRLGE(matrix, nrows, ncols);
	}
	if (statistic_label_str.compare("LRHGE") == 0) {
		return calculateStatistics_LRHGE(matrix, nrows, ncols);
	}
	if (statistic_label_str.compare("GLNU") == 0) {
		return calculateStatistics_GLNU(matrix, nrows, ncols);
	}
	if (statistic_label_str.compare("RLNU") == 0) {
		return calculateStatistics_RLNU(matrix, nrows, ncols);
	}
	if (statistic_label_str.compare("RPC") == 0) {
		mexPrintf(("optional param: " + to_string(optionalParam_1) + "\n").c_str());
		return calculateStatistics_RPC(matrix, nrows, ncols, optionalParam_1);
	}

}

//=================================================================================================
void mexFunction(int nlhs, mxArray *plhs[],	int nrhs, const mxArray *prhs[]) {

	double* matrix;
	size_t ncols, nrows;
	char* statistic_label;

	matrix = mxGetPr(prhs[0]);
	ncols = mxGetN(prhs[0]);
	nrows = mxGetM(prhs[0]);
	statistic_label = mxArrayToString(prhs[1]);

	auto optionalParamsCnt = mxGetN(prhs[2]);
	double* optionalParams = new double[optionalParamsCnt];
	for (auto i = 0; i < optionalParamsCnt; i++) {
		mxArray* optionalParam = mxGetCell(prhs[2], i);
		optionalParams[i] = *mxGetPr(optionalParam);
	}

	// for (auto i = 0; i < optionalParamsCnt; i++) {
	// 	mexPrintf((to_string(optionalParams[i]) + "\n").c_str());
	// }

	auto value = calculateStatistics(matrix, nrows, ncols, statistic_label, optionalParams[0]);

	plhs[0] = mxCreateDoubleScalar(value);

}
