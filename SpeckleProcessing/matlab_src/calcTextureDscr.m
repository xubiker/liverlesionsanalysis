function [ descriptor ] = calcTextureDscr(image, descriptorType, descriptorParam)
%TEXTURETRANSFORM Summary of this function goes here
%   Detailed explanation goes here

switch descriptorType
    case 'original'
        descriptor = image;
    case 'laws'
        descriptor = laws(image, descriptorParam{1});
    case 'glcm'
        posterized = posterizeGrayScale(image, 0.0, 1.0, uint16(descriptorParam{1}));
        descriptor = glcm(posterized, descriptorParam{2}, descriptorParam{3});
    case 'glrlm'
        posterized = posterizeGrayScale(image, 0.0, 1.0, uint16(descriptorParam{1}));
        descriptor = glrlm(posterized, descriptorParam{2});
    otherwise
        error('unsupported texture descriptor');
end
    
end

