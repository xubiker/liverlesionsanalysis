function [s] = generatePlotName(textureDscr, textureDscrParam)
%GENERATE Summary of this function goes here
%   Detailed explanation goes here

s = sprintf('%s', textureDscr);
for j = 1 : numel(textureDscrParam)
    param = textureDscrParam{j};
    s = [s '. ' toString(param)];
end

end

