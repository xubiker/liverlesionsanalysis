function [res] = posterizeGrayScale(matrix, minVal, maxVal, intensCnt)
%POSTERIZEGRAYSCALE Summary of this function goes here
%   Detailed explanation goes here

if nargin ~= 4 || nargout ~= 1
    error('invalid input/output args cnt');
end
    
if ~isreal(matrix) || ~isa(matrix, 'double') || ~ismatrix(matrix) || issparse(matrix)
    error('invalid matrix');
end

if ~isreal(minVal) || ~isa(minVal, 'double') || numel(minVal) ~= 1
    error('invalid minVal');
end

if ~isreal(maxVal) || ~isa(maxVal, 'double') || numel(maxVal) ~= 1
    error('invalid maxVal');
end

if ~isreal(intensCnt) || ~isa(intensCnt, 'uint16') || numel(intensCnt) ~= 1
    error('invalid intensCnt');
end

res = posterizeGrayScale_mx(matrix, minVal, maxVal, intensCnt);

end

