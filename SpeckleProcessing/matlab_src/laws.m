function [outImage] = laws(image, lawsParam)
%LAWS Summary of this function goes here
%   Detailed explanation goes here

% ----- check if image is a coloured one -----
if size(image, 3) == 3
    error('Laws: input image should be grayscale!');
end

mask_1D_1 = getMask(lawsParam(1));
mask_1D_2 = getMask(lawsParam(2));

filter1 = mask_1D_1' * mask_1D_2;
filter2 = mask_1D_1 * mask_1D_2';

% ----- apply 2D filter (obtaining rotation invarience) -----
TI_1 = conv2(image, filter1, 'same');
TI_2 = conv2(image, filter2', 'same');

% ----- apply average filter -----
% TEMF = ones(avgSize) / (avgSize * avgSize);
% TEM_1 = conv2(TI_1, TEMF, 'same');
% TEM_2 = conv2(TI_2, TEMF, 'same');

% ----- get result -----
% outImage = (TEM_1 + TEM_2) / 2;
outImage = (TI_1 + TI_2) / 2;

end


function [lawsMask] = getMask(name)

L = [ 1  4  6  4  1];
E = [-1 -2  0  2  1];
S = [-1  0  2  0 -1];
R = [ 1 -4  6 -4  1];
W = [-1  2  0 -2  1];

switch name(1)
    case 'L'
        lawsMask = L;
    case 'E'
        lawsMask = E;
    case 'S'
        lawsMask = S;
    case 'R'
        lawsMask = R;
    case 'W'
        lawsMask = W;
    otherwise
        error('unsupported laws kernel');
end

end

