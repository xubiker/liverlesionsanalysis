function [roi] = extractRoi(image, roiSize, param)
%EXTRACTROI Summary of this function goes here
%   Detailed explanation goes here

if nargin ~= 3
    error('invalid number of input params');
end

switch param
    case 'random'
        h = randi(size(image, 1) - roiSize(1));
        w = randi(size(image, 2) - roiSize(2));
        roi = image(h : h + roiSize(1), w : w + roiSize(2));
    otherwise
        error('invalid param');
end


end

