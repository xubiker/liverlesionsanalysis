% maxVal = calculateStatistics(image, 'max');
% minVal = calculateStatistics(image, 'min');
% 
% mean = calculateStatistics(image, 'mean');
% sd = calculateStatistics(image, 'sd');
% var = calculateStatistics(image, 'var');
% energy = calculateStatistics(image, 'energy');
% skewness = calculateStatistics(image, 'skewness');
% kurtosis = calculateStatistics(image, 'kurtosis');
% entropy = calculateStatistics(image, 'entropy');
% 
% postLevels = uint16(64);
% 
% image = posterizeGrayScale(image, minVal, maxVal, postLevels);
% 
% glcmMatrix_1 = glcm(image, [10 0], false);
% glcmMatrix_2 = glcm(image, [10 0], true);
% 
% contrast = calculateStatistics(glcmMatrix_1, 'CON');
% homogeneity = calculateStatistics(glcmMatrix_1, 'HOM');
% dissimilarity = calculateStatistics(glcmMatrix_1, 'DIS');
% correlation = calculateStatistics(glcmMatrix_1, 'COR');
% 
% 
% image = image / double(postLevels);
% 
% % imshow(image);
% % figure;
% subplot(1, 2, 1);
% imshow(imadjust(log(1 + glcmMatrix_1)));
% subplot(1, 2, 2);
% imshow(imadjust(log(1 + glcmMatrix_2)));
  
%digits(32);

% image = [1 2 3; 4 5 6; 7 8 9];
% 
% glcmMatrix = glcm(image, 100);
% 
% m = [0.001 2 3 4 5; 1 -2 -3 -4 -5];
% 
% energy = sum(m(:) .^ 2);
% 
% disp(energy);
% 
% disp(vpa(pi));
% 
% stat_energy = calculateStatistics(m, 'energy');
% disp(vpa(stat_energy));

% x = 10 + 0.00001;
% y = x * 100;
% disp(x);
% disp(y);

% image = imread('C:\\us_emu_4.png');
% image = rgb2gray(image);
% image = im2double(image);
% 
% speckle = addSpeckleNoise(image, 0.5, 0.25);
% 
% subplot(1,2,1), imshow(image);
% subplot(1,2,2), imshow(speckle);
