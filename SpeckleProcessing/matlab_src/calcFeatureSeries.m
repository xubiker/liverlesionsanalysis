function [features] = calcFeatureSeries(images, ...
    textureDscr, textureDscrParams, statistics, speckleStdev, imageNames, plotGraph)
%CALCANDPLOTFEATURES Summary of this function goes here
%   Detailed explanation goes here

textureDscrParamsCnt = numel(textureDscrParams);

% if textureDscrParamsCnt > 4
%     parfor k = 1 : textureDscrParamsCnt
%         process(images, textureDscr, textureDscrParams{k}, statistics, speckleStdev, imageNames);
%     end
%     return;
% else
    for j = 1 : textureDscrParamsCnt
        f = process(images, textureDscr, textureDscrParams{j}, statistics, speckleStdev, imageNames, plotGraph);
        if j == 1
            features = f;
        else
            features = cat(3, features, f);
        end
    end
%     return;
% end

end

%==========================================================================
function [f] = process(images, textureDscr, textureDscrParam, statistics, speckleStdev, imageNames, plotGraph)

s = generatePlotName(textureDscr, textureDscrParam);
% fprintf('processing: %s\n', s);
f = calcFeatures(images, textureDscr, textureDscrParam, statistics);
if plotGraph
    plotFeatures(f, speckleStdev, imageNames, s, statistics);
end

end