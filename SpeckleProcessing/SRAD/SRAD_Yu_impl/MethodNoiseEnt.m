function [ J ] = MethodNoiseEnt( methodNoise, cor_rad)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

Ir = methodNoise;

Ir(isnan(Ir)) = 0;

m = mean(Ir(:));
sigma = sqrt(var(Ir(:)));
alpha = 1.2;

In =  floor((8 / pi) * atan ((Ir - m) / (alpha * sigma)));

r1 = cor_rad;
r2 = floor(sqrt(2 * cor_rad^2));

D_0 = In(:, 1 : end - r1) - In(:, 1 + r1 : end);
D_90 = In(1 : end - r1, :) - In(1 + r1 : end, :);
D_45 = In(1 : end - r2, 1 : end - r2) - In(1 + r2 : end, 1 + r2 : end);
D_135 = In(1 : end - r2, 1 + r2 : end) - In(1 + r2 : end, 1 : end - r2);

[~, bins1] = xhist(D_0, -8, 8);
[~, bins2] = xhist(D_90, -8, 8);
[~, bins3] = xhist(D_45, -8, 8);
[~, bins4] = xhist(D_135, -8, 8);

bins = bins1 + bins2 + bins3 + bins4;
binsN = bins / sum(bins);

binsN(binsN == 0) = 1;

J = - sum(binsN .* log2(binsN));

end

