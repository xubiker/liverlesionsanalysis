clear all
close all;

path = 'c:\\dev\\LiverLesionsAnalysis\\SpeckleProcessing\\';

fileName1 = [path 'brodatz_textures\\1.1.01.tiff'];
fileName2 = [path 'brodatz_textures\\1.1.02.tiff'];
fileName3 = [path 'brodatz_textures\\1.1.03.tiff'];
fileName4 = [path 'brodatz_textures\\1.1.04.tiff'];
fileName5 = [path 'brodatz_textures\\1.1.05.tiff'];
fileName6 = [path 'brodatz_textures\\1.1.06.tiff'];
fileName7 = [path 'brodatz_textures\\1.1.07.tiff'];
fileName8 = [path 'brodatz_textures\\1.1.08.tiff'];
fileName9 = [path 'brodatz_textures\\1.1.09.tiff'];
fileName10 = [path 'brodatz_textures\\1.1.10.tiff'];
fileName11 = [path 'brodatz_textures\\1.1.11.tiff'];
fileName12 = [path 'brodatz_textures\\1.1.12.tiff'];
fileName13 = [path 'brodatz_textures\\1.1.13.tiff'];

fileName_3 = 'ultrasound phantom.bmp';
fileName_4 = 'C:\\us_emu_4.png';

fileNameL1 = [path 'liver_samples\\00.png'];
fileNameL2 = [path 'liver_samples\\01.png'];
fileNameL3 = [path 'liver_samples\\02.png'];

fileNameT1 = [path 'liver_samples\\t_1.jpg'];
fileNameT2 = [path 'liver_samples\\t_2.jpg'];
fileNameT3 = [path 'liver_samples\\t_3.jpg'];

sigma = 0.4;

USE_REAL_DATA = false;

fileName = fileName1;

if USE_REAL_DATA
    I1 = imread(fileName);
    if size(I1, 3) == 3, I1 = rgb2gray(I1); end
    I1 = im2double(I1);
    imshow(I1);
    roi = getrect;
    I1 = imcrop(I1, roi);
    I = zeros(size(I1));
    imshow(I1);
    rect = getrect;
else 
    I = imread(fileName);
    if size(I, 3) == 3, I = rgb2gray(I); end
    I = im2double(I);
    I1 = addSpeckleNoise(I, 0.5, sigma);
    rect = [1 1 20 20];
end

% I1 = imnoise(I, 'speckle', sigma);
% mn = min(I1(:));
% mx = max(I1(:));
% I1 = (I1 - mn) / (mx - mn);

set(gcf,'units','normalized','outerposition',[0 0 1 1]);
subplot(2, 4, 1); imshow(I);
subplot(2, 4, 2); imshow(I1);

Ip = I1;

N_ITER = 200;
N_STEP = 2;
SRAD_STEP = 0.05;
MNE_rad = 10;

DISP = true;

psnrVals1 = zeros(floor(N_ITER/N_STEP), 1);
ssimVals1 = zeros(floor(N_ITER/N_STEP), 1);
psnrVals2 = zeros(floor(N_ITER/N_STEP), 1);
ssimVals2 = zeros(floor(N_ITER/N_STEP), 1);
psnrVals3 = zeros(floor(N_ITER/N_STEP), 1);
ssimVals3 = zeros(floor(N_ITER/N_STEP), 1);
mneMulVals = zeros(floor(N_ITER/N_STEP), 1);
mneAddVals = zeros(floor(N_ITER/N_STEP), 1);

J = exp(I1);

for i = 1 : N_ITER
    prevJ = J;
    J = SRAD_iter(J, SRAD_STEP, rect);
    
    if mod(i, N_STEP) == 0

        j = floor(i / N_STEP);
        
%         Jp = log(Jp) * (mx - mn) + mn;
        Jp = log(J);
        prevJp = log(prevJ);
        
        noise_a = 0.5 + Ip - Jp;
        noise_m = 0.5 + (Ip - Jp) ./ sqrt(abs(Jp));
        noise = stickImages(imadjust(noise_a, [0 1]), imadjust(noise_m, [0 1]), 'horizontal');
        
        psnrVals1(j) = psnr(I, Jp);
        ssimVals1(j) = ssim(I, Jp);
        psnrVals2(j) = psnr(Jp, Ip);
        ssimVals2(j) = ssim(Jp, Ip);
        psnrVals3(j) = psnr(Jp, prevJp);
        ssimVals3(j) = ssim(Jp, prevJp);
        mneMulVals(j) = MethodNoiseEnt((prevJp - Jp) ./ sqrt(abs(Jp)), MNE_rad);
%         mneAddVals(j) = MethodNoiseEnt(prevJp - Jp, MNE_rad);

        if DISP
            subplot(2, 4, 3); imshow(Jp);
            subplot(2, 4, 4); imshow(5 * abs(I - Jp));
            subplot(2, 4, 5); imshow(noise);

            subplot(2, 4, 6);
            hold on;
            plot(psnrVals1(1:j), 'color', 'blue');
            plot(psnrVals2(1:j), 'color', 'green');
            plot(psnrVals3(1:j), 'color', 'red');
            hold off;
            title('PSNR');

            subplot(2, 4, 7);
            hold on;
            plot(ssimVals1(1:j), 'color', 'blue');
            plot(ssimVals2(1:j), 'color', 'green');
            plot(ssimVals3(1:j), 'color', 'red');
            hold off;
            title('SSIM');

            subplot(2, 4, 8);
            hold on;
            plot(mneMulVals(1:j), 'color', 'blue');
%             plot(mneAddVals(1:j), 'color', 'red');
            hold off;
            title('MNE');

            s = sprintf('i=%d, PSNR=%f, SSIM=%f', i, psnrVals1(j), ssimVals1(j));
            set(gcf, 'name', s, 'numbertitle', 'off');
            drawnow;
        end
    end
end

[val, idx] = max(mneMulVals(:));

optimal_i = idx * N_STEP;


J = exp(I1);
for i = 1 : optimal_i
    prevJ = J;
    J = SRAD_iter(J, SRAD_STEP, rect);
end
srad_res = log(J);
figure;
imshow(srad_res);


% 
% 
% rect = [10 15 40 40];
% 
% [J,rect] = SRAD(I, 400, 0.1, rect);
% 
% figure
% subplot(1,2,1)
% imshow(I,[])
% subplot(1,2,2)
% imshow(J,[])    
% 
% noise_a = abs(I - J);
% noise_m = abs((I - J) ./ sqrt(J));
% noise_a = imadjust(noise_a , stretchlim(noise_a),[]);
% noise_m = imadjust(noise_m , stretchlim(noise_m),[]);
% figure;
% subplot(1, 2, 1);
% imshow(noise_a);
% subplot(1, 2, 2);
% imshow(noise_m);
