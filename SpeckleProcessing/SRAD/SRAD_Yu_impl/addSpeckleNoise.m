function [ noisyImage ] = addSpeckleNoise( image, gamma, stdev)
%ADDSPECKLENOISE adds speckle noise with given parameters to the image
%   input: image, gamma, stdev

    h = size(image, 1);
    w = size(image, 2);
    noise = normrnd(0, stdev, h, w);
    
    noisyImage = image + image .^ gamma .* noise;
end