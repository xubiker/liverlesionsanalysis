function [J] = SRAD_iter(I, lambda, rect)

[M,N] = size(I);

% image indices (using boudary conditions)
iN = [1, 1 : M - 1];
iS = [2 : M, M];
jW = [1, 1 : N - 1];
jE = [2 : N, N];

% log uncompress (also eliminates zero value pixels)
% I = exp(I);

% speckle scale function
Iuniform = imcrop(I,rect);
q0_squared = var(Iuniform(:)) / (mean(Iuniform(:))^2);

% differences
dN = I(iN, :) - I;
dS = I(iS, :) - I;
dW = I(:, jW) - I;
dE = I(:, jE) - I;

% normalized discrete gradient magnitude squared (equ 52,53)
G2 = (dN.^2 + dS.^2 + dW.^2 + dE.^2) ./ I.^2;

% normalized discrete laplacian (equ 54)
L = (dN + dS + dW + dE) ./ I;

% ICOV (equ 31/35)
num = (.5*G2) - ((1/16)*(L.^2));
den = (1 + ((1/4)*L)).^2;
q_squared = num ./ (den + eps);

% diffusion coefficent (equ 33)
den = (q_squared - q0_squared) ./ (q0_squared *(1 + q0_squared) + eps);
c = 1 ./ (1 + den);

% saturate diffusion coefficent
c(c<0) = 0;
c(c>1) = 1;

% divergence (equ 58)
cS = c(iS, :);
cE = c(:,jE);
D = (c.*dN) + (cS.*dS) + (c.*dW) + (cE.*dE);

% update (equ 61)
I = I + (lambda / 4) * D;

% log compression & output
J = I;
% J = log(I);

return