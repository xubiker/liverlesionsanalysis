function [ res ] = stickImages( image1, image2, direction )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if size(image1) ~= size(image2)
    error('mismatch image size');
end

sz = size(image1);

switch direction
    case 'horizontal'
        res = cat(2, image1(:, 1 : uint16(sz(2)/2)), image2(:, uint16(sz(2)/2) : end));
        res(:, uint16(sz(2)/2)) = 255;
    case 'vertical'
        res = cat(1, image1(1 : uint16(sz(1)/2), :), image2(uint16(sz(1)/2) : end, :)); 
        res(uint16(sz(2)/2), :) = 255;
    otherwise
        error('unsupportd parameter');
end


end

