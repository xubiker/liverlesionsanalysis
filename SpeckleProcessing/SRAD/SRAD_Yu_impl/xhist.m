function [ h, bins ] = xhist( image, lowV, highV )
%XHIST Summary of this function goes here
%   Detailed explanation goes here

h = zeros(highV - lowV + 1, 1);
bins = zeros(highV - lowV + 1, 1);

for v = lowV : highV
    h(v - lowV + 1) = v;
    bins(v - lowV + 1) = sum(image(:) == v);
end

end

