package base.backgroundexecution;

import com.google.inject.Guice;
import com.google.inject.Injector;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Before;
import org.junit.Test;

//**************************************************************************************************
public class XBackgroundWorkerTest {
    
    private Injector injector;
    
    //==============================================================================================
    @Before
    public void init() {
        injector = Guice.createInjector(new XBackgroundModule());
    }
    
    //==============================================================================================
    @Test
    public void XBackgroundWorkerTest() {

        IBackgroundFactory backgroundFactory = injector.getInstance(IBackgroundFactory.class);

        Consumer<XBackgroundResult> afterAction = (res) -> {
            if (res.succeeded()) {
                System.out.println("finished: " + res.getResult());
            } else if (res.canceled()) {
                System.out.println("cancelled");
            }
        };

        IBackgroundWizard wizard = backgroundFactory.createWizard(null);
        wizard.addTask(new XBackgroundTask(controller -> doComputations(controller)));
        wizard.start();

        for (int q = 0; q < 10; q++) {
            try {
                Thread.sleep(30);
            } catch (InterruptedException ex) {
                Logger.getLogger(XBackgroundWorkerTest.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("do some work");
        }
        
    }
    
    //==============================================================================================
    private Double doComputations(IBackgroundMonitor controller) {
        controller.updateProgress(0);
        double sum = 0;
        for (int i = 0; i < 100; i++) {
            System.out.println("progress: " + i + 1);
            for (int j = 0; j < 100; j++) {
                for (int k = 0; k < 100; k++) {
                    for (int z = 0; z < 20; z++) {
                        if (controller.isWorkerCancelled()) {
                            return null;
                        }
                        sum += (Math.sin((double)i * j * Math.exp(k / 10))) / 20;
                    }
                }
            }
            controller.updateProgress(i + 1);
        }
        return sum;
    }
    
}
