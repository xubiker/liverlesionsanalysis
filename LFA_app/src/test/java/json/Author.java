package json;

public class Author {

    private int id;
    private String name;
    
    void setId(int id) {
        this.id = id;
    }

    void setName(String name) {
        this.name = name;
    }
    
    @Override
    public String toString() {
        return name + "[" + id + "]";
    }
    
}
