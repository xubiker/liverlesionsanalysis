package json;

public class Book {

    private Author[] authors;
    private String isbn10;
    private String isbn13;
    private String title;

    void setTitle(String title) {
        this.title = title;
    }

    void setIsbn10(String bn10) {
        this.isbn10 = bn10;
    }

    void setIsbn13(String bn13) {
        this.isbn13 = bn13;
    }

    void setAuthors(Author[] authors) {
        this.authors = authors;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("--- Book ---\n");
        sb.append("title: ").append(title).append("\n");
        sb.append("authors: ");
        for (Author author : authors) {
            sb.append(author).append(", ");
        }
        sb.setLength(sb.length() - 2);
        sb.append("\n");
        sb.append("isbn10: ").append(isbn10).append("\n");
        sb.append("isbn13: ").append(isbn13).append("\n");
        return sb.toString();
    }

}
