package json;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;

public class AuthorDeserializer implements JsonDeserializer<Author> {

    @Override
    public Author deserialize(
            final JsonElement json,
            final Type typeOfT,
            final JsonDeserializationContext context) throws JsonParseException {
        
        final JsonObject jsonObject = json.getAsJsonObject();

        final Author author = new Author();
        author.setId(jsonObject.get("id").getAsInt());
        author.setName(jsonObject.get("name").getAsString());
        return author;
    }

}
