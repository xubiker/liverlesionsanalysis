package lfa.general;

import com.google.inject.Guice;
import com.google.inject.Injector;
import java.util.function.Function;
import lfa.packing.IPackable;
import lfa.packing.IPacker;
import lfa.packing.IUnpackable;
import lfa.packing.PackingModule;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

//**************************************************************************************************
public class PackingTest {
    
    private static Injector injector;
    
    //**********************************************************************************************
    private static final class SimpleClass implements IUnpackable {

        private final String value;

        public SimpleClass(ComplicatedClass cc) {
            this.value = Integer.toString(cc.getValue());
        }
        
        public String getValue() {
            return value;
        }

        @Override
        public Function<IUnpackable, IPackable> getUnpackAction() {
            Function<IUnpackable, IPackable> unpack = input -> {
                SimpleClass packed = (SimpleClass) input;
                return new ComplicatedClass(Integer.parseInt(packed.value));
            };
            return unpack;
        }

        @Override
        public Class<?> getUnpackT() {
            return ComplicatedClass.class;
        }

    }
    
    //**********************************************************************************************
    private static final class ComplicatedClass implements IPackable {

        private final int value;

        public ComplicatedClass(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        @Override
        public Function<IPackable, IUnpackable> getPackAction() {
            Function<IPackable, IUnpackable> pack = input -> {
                ComplicatedClass unpacked = (ComplicatedClass) input;
                return new SimpleClass(unpacked);
            };
            return pack;
        }

        @Override
        public Class<?> getPackT() {
            return SimpleClass.class;
        }

    }

    //==============================================================================================
    @BeforeClass
    public static void setUpClass() {
        injector = Guice.createInjector(new PackingModule());
    }
    
    //==============================================================================================
    @Test
    public void packUnpackTest() {
        ComplicatedClass cc = new ComplicatedClass(123);
        IPacker packer = injector.getInstance(IPacker.class);
        IUnpackable pack = packer.pack(cc);
        ComplicatedClass cc2 = (ComplicatedClass) packer.unpack(pack);
        Assert.assertEquals(cc.getValue(), cc2.getValue());
    }
    
}
