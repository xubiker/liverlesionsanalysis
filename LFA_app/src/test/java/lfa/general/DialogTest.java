package lfa.general;

import com.google.inject.Guice;
import com.google.inject.Injector;
import lfa.gui.dialog.DialogModule;
import lfa.gui.dialog.IDialog;
import org.junit.Before;
import org.junit.Test;

//**************************************************************************************************
public class DialogTest {
    
    private Injector injector;
    
    //==============================================================================================
    @Before
    public void init() {
        injector = Guice.createInjector(
                new DialogModule()
        );
    }
    
    //==============================================================================================
    @Test
    public void chooseFolderTest() throws Exception {

        IDialog dialog = injector.getInstance(IDialog.class);
        String choosenFolder = dialog.chooseFolder();
        System.out.println("chosen folder: " + choosenFolder);
    }
    
}
