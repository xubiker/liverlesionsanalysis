package lfa.general;

import com.google.inject.Guice;
import com.google.inject.Injector;
import java.util.Arrays;
import java.util.List;

import lfa.diagnosis.Diagnosis;
import lfa.diagnosis.Diagnosis$;
import lfa.diagnosis.MultiDiagnosis;
import lfa.data.*;
import lfa.gui.dialog.DialogModule;
import lfa.packing.PackingModule;
import lfa.storage.IPackedStorage;
import lfa.storage.StorageFactory;
import lfa.storage.StorageModule;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.BeforeClass;
import org.junit.Test;

//**************************************************************************************************
public class XmlWriteAndReadTest {

    private static Injector injector;
    
    @BeforeClass
    public static void setUpClass() {
        injector = Guice.createInjector(new PackingModule(), new StorageModule(), new DialogModule());
    }
    
    //==============================================================================================
    @Test
    public void writeAndReadSampleDistributionToXml() throws Exception {

        String xmlFilePath = "C:\\dev\\sampleDistribution_test.xml";
        
        SampleCollection sCollection = new SampleCollection();
        for (int i = 0; i < 10; i++) {
            sCollection.addCategory(0, 0, i, SampleCategory.TEST_SET);
            sCollection.addCategory(0, 1, i, SampleCategory.TEST_SET);
            sCollection.addCategory(1, 0, i, SampleCategory.TRAIN_SET);
            sCollection.addCategory(2, 0, i, SampleCategory.TRAIN_SET);
        }
        sCollection.addCategory(2, 0, 10, SampleCategory.TEST_SET);
        sCollection.removeCategories(0, 0);
        
        try {
            sCollection.addCategory(2, 0, 10, SampleCategory.TRAIN_SET);
            fail();
        } catch (Exception ex) {}
        
        try {
            SampleCategory category = sCollection.getCategory(5);
            if (category != null) fail();
        } catch (Exception ex) {}
        
        try {
            sCollection.setCategory(0, 1, 15, SampleCategory.TRAIN_SET);
            fail();
        } catch (Exception ex) {}
        
        assertEquals(SampleCategory.TEST_SET, sCollection.getCategory(0));
        assertEquals(SampleCategory.MIXED, sCollection.getCategory(2, 0));
        
        sCollection.removeCategory(2, 0, 10);
        assertEquals(SampleCategory.TRAIN_SET, sCollection.getCategory(2, 0));
        
        IPackedStorage storage = injector.getInstance(StorageFactory.class).createPackedStorage();
        storage.exportInstance(sCollection, xmlFilePath);

        SampleCollection sCollection2 = (SampleCollection) storage.importInstance(SampleCollection.class, xmlFilePath);
        
        sCollection2.toList().stream().forEach(p -> {
            System.out.println(p);
        });
        
    }

    //==============================================================================================
    @Test
    public void writePatietCollectionToXml() throws Exception {

        String xmlFilePath = "C:\\dev\\patientCollection_test.xml";

        Diagnosis d1 = Diagnosis$.MODULE$.apply("FIBROSIS", "METAVIR", 2).get();
        Diagnosis d2 = Diagnosis$.MODULE$.apply("STEATOSIS", "DSS", 1).get();
        MultiDiagnosis md = new MultiDiagnosis(Arrays.asList(d1, d2));
        Patient p1 = Patient.create("Ivan", "Ivanov", 30, PatientGender.MALE, md, "path1");
        p1.setLabel("label1");
        p1.setAnalysis(new Analysis(0.256f, 0, 0, 0));
        List<USnapshot> snapshots = Arrays.asList(
                new USnapshot("image1.png", LiverCutType.CUT1),
                new USnapshot("image2.png", LiverCutType.CUT2),
                new USnapshot("image3.png", LiverCutType.CUT3)
        );
        p1.setUltrasound(new USnapshotCollection("snapshothsPath", snapshots));

        Patient p2 = Patient.create("Alexey", "Tolstoy", 50, PatientGender.MALE, md, "path2");
        Patient p3 = Patient.create("Elena", "Sydorova", 40, PatientGender.FEMALE, md, "path3");

        List<Patient> patients = Arrays.asList(p1, p2, p3);

        PatientCollection patientCollection = new PatientCollection("pCollPath", patients);

        IPackedStorage storage = injector.getInstance(StorageFactory.class).createPackedStorage();
        storage.exportInstance(patientCollection, xmlFilePath);

    }

    //==============================================================================================
    @Test
    public void writeRoiCollectionToXml() throws Exception {

        String xmlFilePath = "C:\\dev\\roiCollection_test.xml";
        
        List<Roi> rois = Arrays.asList(
            new Roi(0, 0, 16, 15.0, 1, 0),
            new Roi(0, 0, 16, 15.0, 1, 1),
            new Roi(0, 0, 16, 15.0, 1, 2),
            new Roi(0, 0, 16, 15.0, 2, 1)
        );
        
        RoiCollection roiCollection = new RoiCollection(rois);

        IPackedStorage storage = injector.getInstance(StorageFactory.class).createPackedStorage();
        storage.exportInstance(roiCollection, xmlFilePath);
        
    }

    //==============================================================================================
    private void assertFalse() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
