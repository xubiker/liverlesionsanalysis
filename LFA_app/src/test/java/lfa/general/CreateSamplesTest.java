package lfa.general;

import com.google.inject.Guice;
import com.google.inject.Injector;
import java.util.logging.Level;
import java.util.logging.Logger;
import lfa.data.SampleCategory;
import lfa.datamanager.DataManagerModule;
import lfa.datamanager.IDataManager;
import lfa.gui.dialog.DialogModule;
import lfa.packing.PackingModule;
import lfa.storage.StorageModule;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

//**************************************************************************************************
public class CreateSamplesTest {

    private static Injector injector;
    
    //==============================================================================================
    @BeforeClass
    public static void setUpClass() {
        injector = Guice.createInjector(
                new StorageModule(),
                new DataManagerModule(),
                new PackingModule(),
                new DialogModule()
                );
    }
  
    //==============================================================================================
    @AfterClass
    public static void tearDownClass() {
    }
    
    //==============================================================================================
    @Test
    public void run() {
        IDataManager datamanager = injector.getInstance(IDataManager.class);
        datamanager.loadPatients();
        datamanager.loadRois();
        datamanager.createAndFillSamples(SampleCategory.UNUSED);
        datamanager.saveSamples();
    }

}
