package lfa.general;

import java.util.HashMap;
import java.util.Map;

public class ShallowAndDeepCopy {

    public static class Clx {
        int a = -1, b = -1;
        int[] q = new int[2];
        String s = "default";
        
        public Clx(int a, int b, int[] q, String s) {
            this.a = a;
            this.b = b;
            this.s = s;
            this.q = q;
        }
        
        @Override
        public String toString() {
            return "a = " + a + ", b = " + b + ", s = " + s + ", q = [" + q[0] + "," + q[1] + "]";
        }
        
    };
    
    
    public static void main(String[] args) {

        Map<Integer, Map<Integer, Clx>> m1 = new HashMap<>();

        Clx clx1 = new Clx(13, 180, new int[]{1, 2}, "rt");
        Clx clx2 = new Clx(23, 280, new int[]{5, 6}, "2rt");
        
        Map<Integer, Clx> m11 = new HashMap<>();
        m11.put(1, clx1);
        m11.put(2, clx2);
        m1.put(1, m11);
        m1.put(2, m11);
        
        System.out.println("map1:");
        System.out.println(m1);

        Map<Integer, Map<Integer, Clx>> m2 = new HashMap<>(m1);
        
        System.out.println("map2:");
        System.out.println(m2);
        
        
    }
    
}
