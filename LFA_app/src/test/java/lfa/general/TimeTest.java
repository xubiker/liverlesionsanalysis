package lfa.general;

import base.time.TimeManager;
import org.junit.Assert;
import static org.junit.Assert.fail;
import org.junit.Test;

//**************************************************************************************************
public class TimeTest {

    //==============================================================================================
    @Test
    public void simpleTimeTest() {
        String timeStamp_1 = TimeManager.getTimeInString();
        long timeMillis_1 = TimeManager.convertTimeStampToMillis(timeStamp_1);
        String timeStamp_2 = TimeManager.convertMillisToTimeStamp(timeMillis_1);
        long timeMillis_2 = TimeManager.convertTimeStampToMillis(timeStamp_2);
        Assert.assertEquals(timeStamp_1, timeStamp_2);
        Assert.assertEquals(timeMillis_1, timeMillis_2);
        long time1 = TimeManager.getTimeInMillis();
        long time2 = TimeManager.getTimeInMillis();
        if (time1 > time2) {
            fail();
        }
    }

}
