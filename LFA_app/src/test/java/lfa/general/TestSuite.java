package lfa.general;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    XmlWriteAndReadTest.class,
    XEventTest.class,
    PackingTest.class
})
public class TestSuite {
    
}
