package parallel;

import base.time.TimeManager;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.junit.Test;

//**************************************************************************************************
public class ParallelTest {

    public static class ProgressController {
        int i = 0;
        
        public synchronized void inc(String who) {
            i++;
            System.out.println(who + " increased progress to " + i);
        }
    };
    
    private double doLongComputations(double input) {
        double sum = 0;
        for (int i = 0; i < 100; i++) {
            sum *= i;
            for (int q = 0; q < 100; q++) {
                for (int j = 0; j < 10000; j++) {
                    sum += Math.sqrt(i * j % 83);
                }
            }
        }
        return sum % (int)input;
    }
    
    @Test
    public void test1() {
        System.out.println("test1");
        long initTime = TimeManager.getTimeInMillis();
        double res = doLongComputations(13);
        long endTime = TimeManager.getTimeInMillis();
        System.out.println("time: " + TimeManager.convertMillisToSeconds(endTime - initTime));
        System.out.println("res: " + res);
    }
    
//    @Test
//    public void test2() {
//        System.out.println("test2");
//        List<Double> list = Arrays.asList(2., 3., 4., 5.);
//        long initTime = TimeManager.getTimeInMillis();
//        list.stream()
//                .map(l -> doLongComputations(l))
//                .forEach(r -> System.out.println("res: " + r));
//        long endTime = TimeManager.getTimeInMillis();
//        System.out.println("time: " + TimeManager.convertMillisToSeconds(endTime - initTime));
//    }

    @Test
    public void test3() {
        System.out.println("test3");
        List<Double> list = Arrays.asList(2., 3., 4., 5.);
        long initTime = TimeManager.getTimeInMillis();
        list.parallelStream()
                .map(l -> doLongComputations(l))
                .forEach(r -> System.out.println("res: " + r));
        long endTime = TimeManager.getTimeInMillis();
        System.out.println("time: " + TimeManager.convertMillisToSeconds(endTime - initTime));
    }
    
    @Test
    public void test4() throws InterruptedException, ExecutionException {
        System.out.println("test4");
        CompletableFuture future1 = CompletableFuture.supplyAsync(() -> doLongComputations(5));
        CompletableFuture future2 = CompletableFuture.supplyAsync(() -> doLongComputations(6));
        CompletableFuture future3 = CompletableFuture.supplyAsync(() -> doLongComputations(7));
        System.out.println(future1.get());
        System.out.println(future2.get());
        System.out.println(future3.get());
    }
    
    @Test
    public void test5() {
        System.out.println("----------------------------------test5");
        IntStream range = IntStream.range(1, 50);
        
        AtomicInteger counter = new AtomicInteger(0);
        
        ProgressController controller = new ProgressController();
        
        range.parallel().forEach(v -> {
            double res = doLongComputations(v);
            int counterV = counter.incrementAndGet();
            System.out.println("counterV: " + counterV);
            controller.inc("task " + v);
        });
        
//        ProgressController controller = new ProgressController();
//        list.parallelStream().forEach(l -> {
//            doLongComputations(l);
//            controller.inc("task " + l);
//        });
        
    }
    
}
