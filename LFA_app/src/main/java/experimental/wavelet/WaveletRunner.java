package experimental.wavelet;

import base.GraphicsConverter;
import com.google.inject.Guice;
import com.google.inject.Injector;
import lfa.gui.scrollableimage.IScrollableImage_VM;
import lfa.gui.scrollableimage.ScrollableImage_Module;
import scala.collection.immutable.Range;
import xubiker.XImageTools.base.XImage;
import xubiker.XImageTools.filters.wavelets.WaveletTransform;
import xubiker.XImageTools.filters.wavelets.WaveletType;

import java.util.List;

//**************************************************************************************************
public class WaveletRunner {
    
    public static void main(String[] args) throws Exception {
        
        WaveletFrame frame = new WaveletFrame();
        Injector injector = Guice.createInjector(
            new ScrollableImage_Module()
        );

        IScrollableImage_VM scrImageVM1 = injector.getInstance(IScrollableImage_VM.class);
        
        scrImageVM1.setView(frame.scrollableImage_V1);
        
        //Mat image = imread("c:\\2.jpg");
        XImage i = XImage.readGray("c:\\dev\\1.3.05.tiff");
        XImage image = XImage.XImageOps(i).crop(new Range(0, 500, 1), new Range(0, 500, 1), true);
        System.out.println(image.size());

        int it = 3;
        List<XImage> waveletes = WaveletTransform.performWithExtAndSplit(image, WaveletType.Haar, 2, it, true);
        waveletes.forEach(wv -> System.out.println(wv.size()));
        XImage res = waveletes.get(waveletes.size() - 1);
        scrImageVM1.updateImage(GraphicsConverter.matToBufferedImage(res.cvData()));
        
//        int lh = 500;
//        double[] x = new double[lh];
//        for (int i = 0; i < lh; i++) {
//            x[i] = Math.sin((double)i / (Math.PI * 4));
//        }
//        double[] x2 = DWT.padPow2(x);
//        double[] dwtRes = DWT.forwardDwt(x2, WaveletType.Daubechies, 4, 8);
//        //List<Double> srcData = Arrays.stream(x).boxed().collect(Collectors.toList());
//        List<Double> dwtData = Arrays.stream(dwtRes).boxed().collect(Collectors.toList());
//        ChartGenerator chartGenerator = new ChartGenerator();
//        //List<Pair<List<Double>, String>> srcDataList = Arrays.asList(new Pair<>(srcData, "signal"));
//        List<Pair<List<Double>, String>> dwtDataList = Arrays.asList(new Pair<>(dwtData, "dwt"));
        frame.setVisible(true);
//        frame.xChartPanel1.update(chartGenerator.generateXYChart1(dwtDataList, "", "x", "y"));
    }
    
}
