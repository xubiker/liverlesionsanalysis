package lfa.major;

import base.backgroundexecution.XBackgroundTask;

import java.util.List;

//**************************************************************************************************
public interface IMajor_VC {

    void fv_loadPatients();
    void fv_loadRois();
    void fv_loadSamples();
    void fv_savePatients();
    void fv_saveRois();
    void fv_saveSamples();

    void fv_roiRadSelected(List<Integer> rads);
    void fv_roiRotationChanged(boolean value);
    void fv_roiDispModeChanged(boolean value);
    void fv_dispOutliersChanged(boolean value);
    
    void fv_loadMeasurements();
    void fv_saveMeasurements();
    void fv_performMeasurements();
    void fv_removeOutliers();
    XBackgroundTask gt_performMeasurements();
    XBackgroundTask gt_removeOutliers();

    void fv_trainClassifiers();
    void fv_testClassifiers();
    XBackgroundTask gt_trainClassifiers();
    XBackgroundTask gt_testClassifiers();
//    XBackgroundTask gt_testClassifier(String classifierName);

    void fv_exportWekaInstances();
    void fv_evaluateAllCLassifiers();
    XBackgroundTask gt_evaluateAllCLassifiers();
//    XBackgroundTask gt_evaluateCLassifier(ExtClassifierShadow shadow);

    void fv_detectUsCones(boolean forceRecalculate);
    
    void fv_performPreprocessing();
}
