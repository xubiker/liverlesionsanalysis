package lfa.major;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import lfa.gui.advancedtable.AdvancedTable_Module;
import lfa.gui.classifierselector.ClResSelector_Module;
import lfa.gui.detailpanel.DetailPanel_Module;
import lfa.gui.enumselector.EnumSelector_Module;
import lfa.gui.scrollableimage.ScrollableImage_Module;
import lfa.gui.roistatisticstable.RoiStatisticsTable_Module;
import lfa.gui.zoomedimage.ZoomedImage_Module;

//**************************************************************************************************
public class Major_Module extends AbstractModule {

    @Override
    protected void configure() {
        bind(IMajor_VM.class).to(Major_VM.class).in(Singleton.class);
        bind(IMajor_VC.class).to(Major_VM.class).in(Singleton.class);
        bind(Major_V.class).in(Singleton.class);

        install(new AdvancedTable_Module());
        install(new ScrollableImage_Module());
        install(new DetailPanel_Module());
        install(new ZoomedImage_Module());
        install(new EnumSelector_Module());
        install(new ClResSelector_Module());
        install(new RoiStatisticsTable_Module());
    }
    
}
