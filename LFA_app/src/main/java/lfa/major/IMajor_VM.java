package lfa.major;

import base.events.XEventBroadcaster;

//**************************************************************************************************
public interface IMajor_VM extends XEventBroadcaster {

    void setView(Major_V view);
    void run();
}
