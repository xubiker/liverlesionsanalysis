package lfa.major;

import base.backgroundexecution.XBackgroundTask;
import base.collections.Pair;
import base.events.*;
import com.google.inject.Inject;
import lfa.image.ImageAdjustMethod;
import lfa.cl.*;
import lfa.colortags.ColorTagConverter;
import lfa.colortags.IColorTagManager;
import lfa.data.LiverCutType;
import lfa.data.Patient;
import lfa.data.SampleCategory;
import lfa.data.USnapshot;
import lfa.datamanager.DataManagerXEvent;
import lfa.datamanager.IDataManager;
import lfa.events.*;
import lfa.gui.advancedtable.IAdvancedTableSuitable;
import lfa.gui.advancedtable.IAdvancedTable_VC;
import lfa.gui.advancedtable.IAdvancedTable_VM;
import lfa.gui.advancedtable.TableItemSelectedXEvent;
import lfa.gui.classifierselector.IClResSelector_VC;
import lfa.gui.classifierselector.IClResSelector_VM;
import lfa.gui.detailpanel.IDetailPanel_VM;
import lfa.gui.enumselector.IEnumSelector_VC;
import lfa.gui.enumselector.IEnumSelector_VM;
import lfa.gui.roistatisticstable.IRoiStatisticsTable_VM;
import lfa.gui.scrollableimage.IScrollableImage_VC;
import lfa.gui.scrollableimage.IScrollableImage_VM;
import lfa.gui.zoomedimage.IZoomedImage_VM;
import lfa.keyprocessor.IKeyProcessor;
import lfa.measurement.IMeasurementProcessor;
import lfa.roiStatistics.IRoiStatisticsManager;
import lfa.roiStatistics.RoiStatisticsChangedXEvent;
import lfa.roilayouteditor.IRoiLayoutEditor;
import xubiker.XImageTools.base.XImage;

import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

//**************************************************************************************************
class Major_VM implements IMajor_VM, IMajor_VC, XEventListener {

    private final IDataManager dataManager;
    private final IKeyProcessor keyProcessor;
    private final IRoiLayoutEditor roiLayoutEditor;
    
    private final IAdvancedTable_VM patientsTableVM;
    private final IAdvancedTable_VM snapshotsTableVM;
    private final IScrollableImage_VM previewImageVM;
    private final IDetailPanel_VM detailPanelVM;
    private final IZoomedImage_VM zoomedImageVM;
    private final IEnumSelector_VM<SampleCategory> sampleSelectorVM;
    private final IEnumSelector_VM<ImageAdjustMethod> imageAdjustSelectorVM;
    private final IClResSelector_VM clResSelectorVM;
    private final IRoiStatisticsTable_VM roiStatisticsTableVM;

    private final IColorTagManager colorTagManager;
    private final IRoiStatisticsManager roiStatisticsManager;

    private final IMeasurementProcessor measurementProcessor;

    private IPrimalClassifier primalClassifier;
    private IPrimalOutlierRemover primalOutlierRemover;
    private final IPrimalEvaluator primalEvaluator;
    
    private Major_V view;
    
    private Patient currentPatient;
    private USnapshot currentSnapshot;
    
    
    private final XEventListenerManager xlm = XEventFactory.createListener();
    private final XEventBroadcasterManager xbm = XEventFactory.createBroadcaster();

    //==============================================================================================
    @Override
    public void setView(Major_V view) {
        this.view = view;
        initializeDataManager();
        initializeKeyProcessor();
        initializeColorTagManager();
        initializeTables();
        initializePreviewImage();
        initializeDetailInfoPanel();
        initializeZoomedImage();
        initializeRoiLayoutEditor();
        initializeSampleSelector();
        initializeClassifierSelector();
        initializeImageAdjustSelector();
        initializeRoiStatisticsManager();
        initializeRoiStatisticsTable();
        initializeOutlierRemover();
        initializePrimalClassifier();
    }
    
    //==============================================================================================
    @Inject
    public Major_VM (   IKeyProcessor keyProcessor,
                        IRoiLayoutEditor roiLayoutEditor,
                        IDataManager datamanager,
                        IAdvancedTable_VM patientsTableVM,
                        IAdvancedTable_VM snapshotsTableVM,
                        IScrollableImage_VM previewImageVM,
                        IDetailPanel_VM detailPanelVM,
                        IZoomedImage_VM zoomedImageVM,
                        IEnumSelector_VM<SampleCategory> sampleSelectorVM,
                        IEnumSelector_VM<ImageAdjustMethod> imageAdjustSelectorVM,
                        IClResSelector_VM classifierSelectorVM,
                        IRoiStatisticsTable_VM roiStatisticsTableVM,
                        IColorTagManager colorTagManager,
                        IRoiStatisticsManager roiStatisticsManager,
                        IMeasurementProcessor measurementManager,
                        IPrimalClassifier primalClassifier,
                        IPrimalOutlierRemover primalOutlierRemover,
                        IPrimalEvaluator primalEvaluator
                    ) {
        
        this.keyProcessor = keyProcessor;
        this.roiLayoutEditor = roiLayoutEditor;
        this.dataManager = datamanager;
        this.patientsTableVM = patientsTableVM;
        this.snapshotsTableVM = snapshotsTableVM;
        this.previewImageVM = previewImageVM;
        this.detailPanelVM = detailPanelVM;
        this.zoomedImageVM = zoomedImageVM;
        this.sampleSelectorVM = sampleSelectorVM;
        this.imageAdjustSelectorVM = imageAdjustSelectorVM;
        this.clResSelectorVM = classifierSelectorVM;
        this.colorTagManager = colorTagManager;
        this.roiStatisticsTableVM = roiStatisticsTableVM;
        this.roiStatisticsManager = roiStatisticsManager;
        this.measurementProcessor = measurementManager;
        this.primalClassifier = primalClassifier;
        this.primalOutlierRemover = primalOutlierRemover;
        this.primalEvaluator = primalEvaluator;
    }

    //<editor-fold defaultstate="collapsed" desc="initializing objects">
    //==============================================================================================
    private void initializeKeyProcessor() {
        this.keyProcessor.setActionKeyMaps(view.getRootPane().getInputMap(), view.getRootPane().getActionMap());
        keyProcessor.registerXEventListener(PatientSwitchXEvent.class, this);
        keyProcessor.registerXEventListener(SnapshotSwitchXEvent.class, this);
        keyProcessor.registerXEventListener(RoiXEvent.class, roiLayoutEditor);
        keyProcessor.registerXEventListener(SnapshotModifiedXEvent.class, this);
        xlm.bindXEventHandler(PatientSwitchXEvent.class,
                e -> patientSwitchXEventHandler((PatientSwitchXEvent) e));
        xlm.bindXEventHandler(SnapshotSwitchXEvent.class,
                e -> snapshotSwitchXEventHandler((SnapshotSwitchXEvent) e));
        xlm.bindXEventHandler(SnapshotModifiedXEvent.class,
                e -> snapshotModifiedXEventHandler((XEventExtension) e));
    }
    
    //==============================================================================================
    private void initializeDataManager() {
        dataManager.registerXEventListener(DataManagerXEvent.class, this);
        xlm.bindXEventHandler(DataManagerXEvent.class, e -> dataManagerXEventHandler((DataManagerXEvent) e));
        dataManager.registerXEventListener(CollectionUpdateXEvent.class, this);
        xlm.bindXEventHandler(CollectionUpdateXEvent.class, e -> collectionUpdateXEventHandler((XEventExtension) e));
    }
    
    //==============================================================================================
    private void initializeTables() {
        view.colorDataTable_V1.setViewModel((IAdvancedTable_VC) patientsTableVM);
        view.colorDataTable_V2.setViewModel((IAdvancedTable_VC) snapshotsTableVM);
        patientsTableVM.setView(view.colorDataTable_V1);
        snapshotsTableVM.setView(view.colorDataTable_V2);
        
        Map<Integer, Color> pcm = new HashMap<>();
        //pcm.put(ColorTagConverter.convert(new Pair<>("fibrosis", FibrosisStage.UNKNOWN.ordinal())), new Color(200, 200, 200));
        pcm.put(ColorTagConverter.convert(new Pair<>("fibrosis", 0)), new Color(255, 255, 255));
        pcm.put(ColorTagConverter.convert(new Pair<>("fibrosis", 1)), new Color(190, 190, 255));
        pcm.put(ColorTagConverter.convert(new Pair<>("fibrosis", 2)), new Color(120, 120, 255));
        pcm.put(ColorTagConverter.convert(new Pair<>("fibrosis", 3)), new Color(80, 80, 255));
        pcm.put(ColorTagConverter.convert(new Pair<>("fibrosis", 4)), new Color(0, 0, 255));
        
        pcm.put(ColorTagConverter.convert(new Pair<>("cc", SampleCategory.MIXED.ordinal())), new Color(170, 15, 170));
        pcm.put(ColorTagConverter.convert(new Pair<>("cc", SampleCategory.NONE.ordinal())), null);
        pcm.put(ColorTagConverter.convert(new Pair<>("cc", SampleCategory.TEST_SET.ordinal())), new Color(255, 128, 128));
        pcm.put(ColorTagConverter.convert(new Pair<>("cc", SampleCategory.TRAIN_SET.ordinal())), new Color(128, 255, 128));
        pcm.put(ColorTagConverter.convert(new Pair<>("cc", SampleCategory.UNUSED.ordinal())), new Color(210, 210, 210));

        patientsTableVM.setColorMap(pcm);
        patientsTableVM.setHeadersFromInstance(Patient.create());

        Map<Integer, Color> scm = new HashMap<>();
        scm.put(ColorTagConverter.convert(new Pair<>("cut", LiverCutType.UNUSED.ordinal())), null);
        scm.put(ColorTagConverter.convert(new Pair<>("cut", LiverCutType.CUT1.ordinal())), new Color(180, 180, 255));
        scm.put(ColorTagConverter.convert(new Pair<>("cut", LiverCutType.CUT2.ordinal())), new Color(180, 255, 180));
        scm.put(ColorTagConverter.convert(new Pair<>("cut", LiverCutType.CUT3.ordinal())), new Color(255, 180, 180));
        scm.put(ColorTagConverter.convert(new Pair<>("cut", LiverCutType.CUT4.ordinal())), new Color(255, 255, 180));
        snapshotsTableVM.setColorMap(scm);
        snapshotsTableVM.setHeadersFromInstance(new USnapshot());
        
        patientsTableVM.registerXEventListener(TableItemSelectedXEvent.class, this);
        snapshotsTableVM.registerXEventListener(TableItemSelectedXEvent.class, this);
        xlm.bindXEventHandler(TableItemSelectedXEvent.class, e -> tableItemSelectedXEventHandler((TableItemSelectedXEvent) e));
    }
    
    //==============================================================================================
    private void initializeColorTagManager() {
        patientsTableVM.setColorTagManager(colorTagManager);
        snapshotsTableVM.setColorTagManager(colorTagManager);
    }
    
    //==============================================================================================
    private void initializePreviewImage() {
        view.scrollableImage_V1.setViewModel((IScrollableImage_VC) previewImageVM);
        previewImageVM.setView(view.scrollableImage_V1);
        previewImageVM.setProcessMouseEventsFlg(true);
    }
    
    //==============================================================================================
    private void initializeRoiLayoutEditor() {
        previewImageVM.registerXEventListener(MouseXEvent.class, roiLayoutEditor);
        roiLayoutEditor.registerXEventListener(PreviewUpdateXEvent.class, previewImageVM);
        roiLayoutEditor.registerXEventListener(ZoomedImageUpdateXEvent.class, zoomedImageVM);
        this.registerXEventListener(RoiXEvent.class, roiLayoutEditor);
    }
    
    //==============================================================================================
    private void initializeDetailInfoPanel() {
        detailPanelVM.setView(view.detailPanel_V1);
    }
    
    //==============================================================================================
    private void initializeZoomedImage() {
        zoomedImageVM.setView(view.zoomedImage_V1);
        zoomedImageVM.setZoomFactor(2);
        zoomedImageVM.setInterpolation(IZoomedImage_VM.Interpolation.LANCZOS4);
    }
    
    //==============================================================================================
    private void initializeSampleSelector() {
        sampleSelectorVM.setView(view.sampleCategorySelector);
        view.sampleCategorySelector.setViewModel((IEnumSelector_VC) sampleSelectorVM);
        sampleSelectorVM.registerXEventListener(SampleCategoryModifiedXEvent.class, this);
        xlm.bindXEventHandler(SampleCategoryModifiedXEvent.class,
                e -> sampleCategoryModifiedXEventHandler((SampleCategoryModifiedXEvent) e));
    }
    
    //==============================================================================================
    private void initializeImageAdjustSelector() {
        imageAdjustSelectorVM.setView(view.imageAdjustSelector);
        view.imageAdjustSelector.setViewModel((IEnumSelector_VC) imageAdjustSelectorVM);
        imageAdjustSelectorVM.registerXEventListener(ImageAdjustXEvent.class, roiLayoutEditor);
    }

    //==============================================================================================
    private void initializeClassifierSelector() {
        clResSelectorVM.setView(view.clResSelector_V1);
        view.clResSelector_V1.setViewModel((IClResSelector_VC) clResSelectorVM);
        clResSelectorVM.registerXEventListener(ClassificationXEvent.ClassifierSelected.class, roiLayoutEditor);
        primalClassifier.registerXEventListener(ClassificationXEvent.ClassifiersUpdated.class, clResSelectorVM);
    }

    //==============================================================================================
    private void initializeRoiStatisticsManager() {
        roiStatisticsManager.registerXEventListener(RoiStatisticsChangedXEvent.class, roiStatisticsTableVM);
        dataManager.registerXEventListener(DataManagerXEvent.class, roiStatisticsManager);
        dataManager.getSamples().registerXEventListener(CollectionUpdateXEvent.class, roiStatisticsManager);
    }
    
    //==============================================================================================
    private void initializeRoiStatisticsTable() {
        this.registerXEventListener(RoiXEvent.class, roiStatisticsTableVM);
        roiStatisticsTableVM.setView(view.statisticsTable_V1);
        view.statisticsTable_V1.setVM(roiStatisticsTableVM);
    }
    
    //==============================================================================================
    private void initializeOutlierRemover() {
        primalOutlierRemover.registerXEventListener(OutlierRemoverUpdateXEvent.class, roiLayoutEditor);
    }
    
    //==============================================================================================
    private void initializePrimalClassifier() {
        primalClassifier.registerXEventListener(ClassificationXEvent.class, this);
        xlm.bindXEventHandler(ClassificationXEvent.class,
                (e) -> classificationXEventHandler((ClassificationXEvent) e));
    }
    //</editor-fold>
    
    //==============================================================================================
    private void dataManagerXEventHandler(DataManagerXEvent e) {
        switch (e.getUpdatedDataType()) {
            case PATIENTS:
                patientsTableVM.setItems(dataManager.getPatients().toList(), false);
                break;
            case ROIS:
                roiLayoutEditor.update();
                break;
            case SAMPLES:
                patientsTableVM.updateCellColorMap();
                break;
            case MEASUREMENTS:
                // do nothing
                break;
            default:
                throw new AssertionError(e.getUpdatedDataType().name());
        }
        if (view != null) {
            view.updateRoiRadSelection();
        }
    }
    
    //==============================================================================================
    private void patientSwitchXEventHandler(PatientSwitchXEvent e) {
        switch (e.getSwitchDirection()) {
            case NEXT:
                patientsTableVM.moveSelection(1);
                break;
            case PREV:
                patientsTableVM.moveSelection(-1);
                break;
            case NEXT_5:
                patientsTableVM.moveSelection(5);
                break;
            case PREV_5:
                patientsTableVM.moveSelection(-5);
                break;
            case FIRST:
                patientsTableVM.moveSelection(Integer.MIN_VALUE);
                break;
            case LAST:
                patientsTableVM.moveSelection(Integer.MAX_VALUE);
                break;
            default:
                throw new AssertionError(e.getSwitchDirection().name());
        }
    }
    
    //==============================================================================================
    private void snapshotSwitchXEventHandler(SnapshotSwitchXEvent e) {
        switch (e.getSwitchDirection()) {
            case NEXT:
                snapshotsTableVM.moveSelection(1);
                break;
            case PREV:
                snapshotsTableVM.moveSelection(-1);
                break;
            default:
                throw new AssertionError(e.getSwitchDirection().name());
        }
    }
    
    //==============================================================================================
    private void tableItemSelectedXEventHandler(TableItemSelectedXEvent e) {
        if (e.getSender() == patientsTableVM) {
            patientSelected((Patient) e.getSelectedItem());
        } else if (e.getSender() == snapshotsTableVM) {
            snapshotSelected((USnapshot) e.getSelectedItem());
        }
    }
    
    //==============================================================================================
    private void patientSelected(Patient p) {
        currentPatient = p;
        detailPanelVM.updateData(currentPatient);
        try {
            SampleCategory category = dataManager.getSamples().getCategory(p.getId());
            sampleSelectorVM.setItem(category);
        } catch (Exception ex) {}
        List<IAdvancedTableSuitable> snapshots = currentPatient.getUltrasound().toMap().values().stream().collect(Collectors.toList());
        snapshotsTableVM.setItems(snapshots, false);
        if (snapshots.isEmpty()) {
            snapshotSelected(null);
        }
    }
    
    //==============================================================================================
    private void snapshotSelected(USnapshot s) {
        currentSnapshot = s;
        XImage image = dataManager.loadImage(currentPatient, currentSnapshot, false);
        roiLayoutEditor.update(currentPatient, currentSnapshot, image);
    }
    
    //==============================================================================================
    private void snapshotModifiedXEventHandler(XEventExtension e) {
        if (currentSnapshot == null) return;
        if (e.getClass() == SnapshotModifiedXEvent.CutTypeChanged.class) {
            SnapshotModifiedXEvent.CutTypeChanged event = (SnapshotModifiedXEvent.CutTypeChanged) e;
            LiverCutType cutType = event.getCutType();
            System.out.println("snapshotModified: " + cutType);
            USnapshot newSnapshot = new USnapshot(currentSnapshot);
            newSnapshot.setCutType(cutType);
            try {
                dataManager.getPatients().toMap()
                        .get(currentPatient.getId()).getUltrasound()
                        .modifySnapshot(currentSnapshot.getId(), newSnapshot);
            } catch (Exception ex) {
                Logger.getLogger(Major_VM.class.getName()).log(Level.SEVERE, null, ex);
            }
            currentSnapshot = newSnapshot;
            snapshotsTableVM.updateSelectedItem(currentSnapshot);
        }
    }
    
    //==============================================================================================
    private void collectionUpdateXEventHandler(XEventExtension ext) {
        if (ext.getClass() == CollectionUpdateXEvent.ElementsAddedWithInfo.class) {
            CollectionUpdateXEvent.ElementsAddedWithInfo e = (CollectionUpdateXEvent.ElementsAddedWithInfo) ext;
            if (e.getDataType() == SampleCategory.class) {
                SampleCategory c = dataManager.getSamples().getCategory(currentPatient.getId());
                sampleSelectorVM.setItem(c);
                patientsTableVM.updateSelectedItem(currentPatient);
            }
            return;
        }
        if (ext.getClass() == CollectionUpdateXEvent.ElementsRemovedWithInfo.class) {
            CollectionUpdateXEvent.ElementsRemovedWithInfo e = (CollectionUpdateXEvent.ElementsRemovedWithInfo) ext;
            if (e.getDataType() == SampleCategory.class) {
                SampleCategory c = dataManager.getSamples().getCategory(currentPatient.getId());
                sampleSelectorVM.setItem(c);
                patientsTableVM.updateSelectedItem(currentPatient);
            }
            return;
        }
        if (ext.getClass() == CollectionUpdateXEvent.ElementsModifiedWithInfo.class) {
            CollectionUpdateXEvent.ElementsModifiedWithInfo e = (CollectionUpdateXEvent.ElementsModifiedWithInfo) ext;
            if (e.getDataType() == SampleCategory.class) {
                SampleCategory c = dataManager.getSamples().getCategory(currentPatient.getId());
                System.out.println(c);
                sampleSelectorVM.setItem(c);
                patientsTableVM.updateSelectedItem(currentPatient);
            }
            return;
        }
    }
    
    //==============================================================================================
    private void sampleCategoryModifiedXEventHandler(SampleCategoryModifiedXEvent e) {
        if (IEnumSelector_VM.class.isAssignableFrom(e.getSender().getClass())) {
            try {
                SampleCategory old_cc = dataManager.getSamples().getCategory(currentPatient.getId());
                SampleCategory new_cc = e.getCategory();
                if (new_cc != old_cc) {
                    dataManager.getSamples().setCategory(currentPatient.getId(), e.getCategory());
                    patientsTableVM.updateSelectedItem(currentPatient);
                    roiLayoutEditor.update();
                }
            } catch (Exception ex) {}
        }
    }
    
    //==============================================================================================
    private void classificationXEventHandler(ClassificationXEvent e) {
        System.out.println("---------- testing finished ----------");
    }
    
    //==============================================================================================
    @Override
    public void run() {
        view.setVisible(true);
    }
    
    //==============================================================================================
    @Override
    public void fv_loadPatients() {
        dataManager.loadPatients();
    }
    
    //==============================================================================================
    @Override
    public void fv_loadRois() {
        dataManager.loadRois();
    }
    
    //==============================================================================================
    @Override
    public void fv_loadSamples() {
        dataManager.loadSamples();
    }

    //==============================================================================================
    @Override
    public void fv_savePatients() {
        dataManager.savePatients();
    }

    //==============================================================================================
    @Override
    public void fv_saveRois() {
        dataManager.saveRois();
    }
    
    //==============================================================================================
    @Override
    public void fv_saveSamples() {
        dataManager.saveSamples();
    }
    
    //==============================================================================================
    @Override
    public void fv_roiRadSelected(List<Integer> rads) {
        notifyXEventListeners(new RoiXEvent.ActiveRadChanged(this, rads));
    }
    
    //==============================================================================================
    @Override
    public void fv_roiRotationChanged(boolean value) {
        notifyXEventListeners(new RoiXEvent.RotationModeChanged(this, value));
    }
    
    //==============================================================================================
    @Override
    public void fv_roiDispModeChanged(boolean value) {
        notifyXEventListeners(new RoiXEvent.DisplayRoisModeChanged(this, value));
    }
    
    //==============================================================================================
    @Override
    public void fv_dispOutliersChanged(boolean value) {
        notifyXEventListeners(new RoiXEvent.DispOutliersModeChanged(this, value));
    }

    //==============================================================================================
    @Override
    public void fv_loadMeasurements() {
        dataManager.loadMeasurements();
    }
    
    //==============================================================================================
    @Override
    public void fv_saveMeasurements() {
        dataManager.saveMeasurements();
    }
    
    //==============================================================================================
    @Override
    public void fv_performMeasurements() {
        measurementProcessor.performMeasurements();
    }

    //==============================================================================================
    @Override
    public XBackgroundTask gt_performMeasurements() {
        return measurementProcessor.gt_performMeasurements();
    }

    //==============================================================================================
    @Override
    public void fv_performPreprocessing() {
        measurementProcessor.performPreprocessing();
    }

    //==============================================================================================
    @Override
    public void fv_removeOutliers() {
        primalOutlierRemover.processAll();
    }

    //==============================================================================================
    @Override
    public XBackgroundTask gt_removeOutliers() {
        return primalOutlierRemover.gt_processAll();
    }

    //==============================================================================================
    @Override
    public void fv_evaluateAllCLassifiers() {
        primalEvaluator.evaluateAllClassifiers();
    }

    //==============================================================================================
    @Override
    public XBackgroundTask gt_evaluateAllCLassifiers() {
        return primalEvaluator.gt_evaluateAllClassifiers();
    }

    //==============================================================================================
    @Override
    public void fv_detectUsCones(boolean forceRecalculate) {
        roiLayoutEditor.detectUsCones(forceRecalculate);
    }

    //==============================================================================================
    @Override
    public void fv_trainClassifiers() {
        primalClassifier.train();
    }

    //==============================================================================================
    @Override
    public XBackgroundTask gt_trainClassifiers() {
        return primalClassifier.gt_train();
    }

    //==============================================================================================
    @Override
    public void fv_testClassifiers() {
        primalClassifier.test();
    }

    //==============================================================================================
    @Override
    public XBackgroundTask gt_testClassifiers() {
        return primalClassifier.gt_test();
    }

    //==============================================================================================
    @Override
    public void fv_exportWekaInstances() {
        //primalClassifier.exportWekaInstances();
    }

    //==============================================================================================
    @Override
    public void registerXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        xbm.registerXEventListener(event_t, listener);
    }

    //==============================================================================================
    @Override
    public void eventHappened(XEvent event) {
        xlm.eventHappened(event);
    }

    //==============================================================================================
    @Override
    public void unregisterXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        xbm.unregisterXEventListener(event_t, listener);
    }

    //==============================================================================================
    private void notifyXEventListeners(XEvent event) {
        xbm.notifyXEventListeners(event);
    }

}
