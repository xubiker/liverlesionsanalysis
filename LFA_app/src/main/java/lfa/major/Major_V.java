package lfa.major;

import base.resource.IResourceLoader;
import com.google.inject.Inject;
import java.awt.Frame;
import java.awt.Image;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.swing.AbstractButton;
import lfa.image.ImageAdjustMethod;
import lfa.data.SampleCategory;


//**************************************************************************************************
public class Major_V extends javax.swing.JFrame {

    private IMajor_VC vm;
    
    //==============================================================================================
    public void setViewModel(IMajor_VC vm) {
        this.vm = vm;
        initialize();
    }
    
    //==============================================================================================
    @Inject
    public Major_V (IResourceLoader resourceLoader) {
        try {
            Image icon = resourceLoader.loadAppIcon();
            if (icon != null) {
                setIconImage(icon);
            }
        } catch (Exception ex) {
            System.err.println("exception: " + ex);
        }
        
        initComponents();
    
        setExtendedState(Frame.MAXIMIZED_BOTH);
        
        ItemListener ROIRadChangeListener = e -> ROIRadChanged();
        
        dispROI_CheckBox.addItemListener(e -> dispROIsChanged());
        autoROIRotation_CheckBox.addItemListener(e -> autoROIRotationChanged());
        dispOutliersCheckBox.addItemListener(e -> dispOutliersChanged());

        roiRad_RadioBtn1.addItemListener(ROIRadChangeListener);
        roiRad_RadioBtn2.addItemListener(ROIRadChangeListener);
        roiRad_RadioBtn3.addItemListener(ROIRadChangeListener);
        roiRad_RadioBtn4.addItemListener(ROIRadChangeListener);
        roiRad_RadioBtn5.addItemListener(ROIRadChangeListener);

        roiRadGroup.add(roiRad_RadioBtn1);
        roiRadGroup.add(roiRad_RadioBtn2);
        roiRadGroup.add(roiRad_RadioBtn3);
        roiRadGroup.add(roiRad_RadioBtn4);
        roiRadGroup.add(roiRad_RadioBtn5);
        
    }
    
    //==============================================================================================
    private void initialize() {
        System.out.println("initializing view");

        roiRad_RadioBtn4.setSelected(true);
        dispROI_CheckBox.setSelected(true);
        autoROIRotation_CheckBox.setSelected(true);
    }
    
    //==============================================================================================
    public void updateRoiRadSelection() {
        ROIRadChanged();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        roiRadGroup = new javax.swing.ButtonGroup();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        dispROI_CheckBox = new javax.swing.JCheckBox();
        roiRadPanel = new javax.swing.JPanel();
        roiRad_RadioBtn3 = new javax.swing.JRadioButton();
        roiRad_RadioBtn2 = new javax.swing.JRadioButton();
        roiRad_RadioBtn1 = new javax.swing.JRadioButton();
        roiRad_RadioBtn4 = new javax.swing.JRadioButton();
        roiRad_RadioBtn5 = new javax.swing.JRadioButton();
        autoROIRotation_CheckBox = new javax.swing.JCheckBox();
        dispOutliersCheckBox = new javax.swing.JCheckBox();
        jScrollPane2 = new javax.swing.JScrollPane();
        colorDataTable_V1 = new lfa.gui.advancedtable.AdvancedTable_V();
        jScrollPane5 = new javax.swing.JScrollPane();
        colorDataTable_V2 = new lfa.gui.advancedtable.AdvancedTable_V();
        scrollableImage_V1 = new lfa.gui.scrollableimage.ScrollableImage_V();
        detailPanel_V1 = new lfa.gui.detailpanel.DetailPanel_V();
        zoomedImage_V1 = new lfa.gui.zoomedimage.ZoomedImage_V();
        jScrollPane1 = new javax.swing.JScrollPane();
        statisticsTable_V1 = new lfa.gui.roistatisticstable.RoiStatisticsTable_V();
        sampleCategorySelector = new lfa.gui.enumselector.EnumSelector_V<>(SampleCategory.class);
        imageAdjustSelector = new lfa.gui.enumselector.EnumSelector_V(ImageAdjustMethod.class);
        jLabel1 = new javax.swing.JLabel();
        clResSelector_V1 = new lfa.gui.classifierselector.ClResSelector_V();
        jLabel2 = new javax.swing.JLabel();
        menuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        loadPatientsMenuItem = new javax.swing.JMenuItem();
        loadRoisMenuItem = new javax.swing.JMenuItem();
        loadSampleMenuItem = new javax.swing.JMenuItem();
        saveMenuItem = new javax.swing.JMenuItem();
        saveRoisMenuItem = new javax.swing.JMenuItem();
        saveSamplesMenuItem = new javax.swing.JMenuItem();
        exitMenuItem = new javax.swing.JMenuItem();
        editMenu = new javax.swing.JMenu();
        detectUsConesLightMenuItem = new javax.swing.JMenuItem();
        detectUsConesFullMenuItem = new javax.swing.JMenuItem();
        recalrRoiRotationMenuItem = new javax.swing.JMenuItem();
        measurementsMenu = new javax.swing.JMenu();
        loadMeasurementsMenuItem = new javax.swing.JMenuItem();
        saveMeasurementsMenuItem = new javax.swing.JMenuItem();
        performPreprocessingMenuItem = new javax.swing.JMenuItem();
        performMeasurementsMenuItem = new javax.swing.JMenuItem();
        removeOutliersMenuItem = new javax.swing.JMenuItem();
        classificationMenu = new javax.swing.JMenu();
        prepareClassificationMenuItem = new javax.swing.JMenuItem();
        trainClassifiersMenuItem = new javax.swing.JMenuItem();
        testClassifiersMenuItem = new javax.swing.JMenuItem();
        exportWekaInstancesMenuItem = new javax.swing.JMenuItem();
        evaluateClassifiersMenuItem = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        contentsMenuItem = new javax.swing.JMenuItem();
        aboutMenuItem = new javax.swing.JMenuItem();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(jTable1);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("LFA application");
        setFocusable(false);

        dispROI_CheckBox.setText("display ROIs");
        dispROI_CheckBox.setFocusable(false);

        roiRadPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("ROI radius"));
        roiRadPanel.setEnabled(false);

        roiRad_RadioBtn3.setText("20");
        roiRad_RadioBtn3.setEnabled(false);
        roiRad_RadioBtn3.setFocusable(false);

        roiRad_RadioBtn2.setText("16");
        roiRad_RadioBtn2.setEnabled(false);
        roiRad_RadioBtn2.setFocusable(false);

        roiRad_RadioBtn1.setText("8");
        roiRad_RadioBtn1.setEnabled(false);
        roiRad_RadioBtn1.setFocusable(false);

        roiRad_RadioBtn4.setText("all");
        roiRad_RadioBtn4.setEnabled(false);
        roiRad_RadioBtn4.setFocusable(false);

        roiRad_RadioBtn5.setText("24");
        roiRad_RadioBtn5.setEnabled(false);
        roiRad_RadioBtn5.setFocusable(false);

        javax.swing.GroupLayout roiRadPanelLayout = new javax.swing.GroupLayout(roiRadPanel);
        roiRadPanel.setLayout(roiRadPanelLayout);
        roiRadPanelLayout.setHorizontalGroup(
            roiRadPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(roiRad_RadioBtn2, javax.swing.GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE)
            .addComponent(roiRad_RadioBtn1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(roiRad_RadioBtn3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(roiRad_RadioBtn4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(roiRad_RadioBtn5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        roiRadPanelLayout.setVerticalGroup(
            roiRadPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, roiRadPanelLayout.createSequentialGroup()
                .addComponent(roiRad_RadioBtn1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(roiRad_RadioBtn2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(roiRad_RadioBtn3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(roiRad_RadioBtn5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(roiRad_RadioBtn4))
        );

        autoROIRotation_CheckBox.setText("ROI auto rotation");
        autoROIRotation_CheckBox.setFocusable(false);

        dispOutliersCheckBox.setText("display outliers");
        dispOutliersCheckBox.setFocusable(false);

        colorDataTable_V1.setFocusable(false);
        jScrollPane2.setViewportView(colorDataTable_V1);

        colorDataTable_V2.setFocusable(false);
        jScrollPane5.setViewportView(colorDataTable_V2);

        detailPanel_V1.setFocusable(false);

        zoomedImage_V1.setFocusable(false);

        javax.swing.GroupLayout zoomedImage_V1Layout = new javax.swing.GroupLayout(zoomedImage_V1);
        zoomedImage_V1.setLayout(zoomedImage_V1Layout);
        zoomedImage_V1Layout.setHorizontalGroup(
            zoomedImage_V1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 175, Short.MAX_VALUE)
        );
        zoomedImage_V1Layout.setVerticalGroup(
            zoomedImage_V1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 171, Short.MAX_VALUE)
        );

        jScrollPane1.setViewportView(statisticsTable_V1);

        sampleCategorySelector.setFocusable(false);

        imageAdjustSelector.setFocusable(false);

        jLabel1.setText("adjust:");
        jLabel1.setFocusable(false);

        clResSelector_V1.setFocusable(false);

        jLabel2.setText("classifier:");
        jLabel2.setFocusable(false);

        fileMenu.setMnemonic('f');
        fileMenu.setText("File");
        fileMenu.setFocusable(false);

        loadPatientsMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        loadPatientsMenuItem.setText("Load patients");
        loadPatientsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadPatientsMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(loadPatientsMenuItem);

        loadRoisMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        loadRoisMenuItem.setText("Load rois");
        loadRoisMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadRoisMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(loadRoisMenuItem);

        loadSampleMenuItem.setText("Load samples");
        loadSampleMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadSampleMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(loadSampleMenuItem);

        saveMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        saveMenuItem.setMnemonic('s');
        saveMenuItem.setText("Save patients");
        saveMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveMenuItem);

        saveRoisMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        saveRoisMenuItem.setText("Save rois");
        saveRoisMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveRoisMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveRoisMenuItem);

        saveSamplesMenuItem.setText("Save samples");
        saveSamplesMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveSamplesMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveSamplesMenuItem);

        exitMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        exitMenuItem.setMnemonic('x');
        exitMenuItem.setText("Exit");
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        editMenu.setMnemonic('e');
        editMenu.setText("Edit");
        editMenu.setFocusable(false);

        detectUsConesLightMenuItem.setText("Detect Us cones (light)");
        detectUsConesLightMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                detectUsConesLightMenuItemActionPerformed(evt);
            }
        });
        editMenu.add(detectUsConesLightMenuItem);

        detectUsConesFullMenuItem.setText("Detect Us cones (full)");
        detectUsConesFullMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                detectUsConesFullMenuItemActionPerformed(evt);
            }
        });
        editMenu.add(detectUsConesFullMenuItem);

        recalrRoiRotationMenuItem.setText("recalcROIRotation");
        editMenu.add(recalrRoiRotationMenuItem);

        menuBar.add(editMenu);

        measurementsMenu.setText("Measurements");

        loadMeasurementsMenuItem.setText("Load measurements");
        loadMeasurementsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadMeasurementsMenuItemActionPerformed(evt);
            }
        });
        measurementsMenu.add(loadMeasurementsMenuItem);

        saveMeasurementsMenuItem.setText("Save measurements");
        saveMeasurementsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveMeasurementsMenuItemActionPerformed(evt);
            }
        });
        measurementsMenu.add(saveMeasurementsMenuItem);

        performPreprocessingMenuItem.setText("Perform preprocessing");
        performPreprocessingMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                performPreprocessingMenuItemActionPerformed(evt);
            }
        });
        measurementsMenu.add(performPreprocessingMenuItem);

        performMeasurementsMenuItem.setText("Perform measurements");
        performMeasurementsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                performMeasurementsMenuItemActionPerformed(evt);
            }
        });
        measurementsMenu.add(performMeasurementsMenuItem);

        removeOutliersMenuItem.setText("Remove outliers");
        removeOutliersMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeOutliersMenuItemActionPerformed(evt);
            }
        });
        measurementsMenu.add(removeOutliersMenuItem);

        menuBar.add(measurementsMenu);

        classificationMenu.setText("Classification");

        trainClassifiersMenuItem.setText("train classifiers");
        trainClassifiersMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                trainClassifiersMenuItemActionPerformed(evt);
            }
        });
        classificationMenu.add(trainClassifiersMenuItem);

        testClassifiersMenuItem.setText("test classifiers");
        testClassifiersMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                testClassifiersMenuItemActionPerformed(evt);
            }
        });
        classificationMenu.add(testClassifiersMenuItem);

        exportWekaInstancesMenuItem.setText("export weka instances");
        exportWekaInstancesMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportWekaInstancesMenuItemActionPerformed(evt);
            }
        });
        classificationMenu.add(exportWekaInstancesMenuItem);

        evaluateClassifiersMenuItem.setText("evaluate all classifiers");
        evaluateClassifiersMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                evaluateClassifiersMenuItemActionPerformed(evt);
            }
        });
        classificationMenu.add(evaluateClassifiersMenuItem);

        menuBar.add(classificationMenu);

        helpMenu.setMnemonic('h');
        helpMenu.setText("Help");
        helpMenu.setFocusable(false);

        contentsMenuItem.setMnemonic('c');
        contentsMenuItem.setText("Contents");
        helpMenu.add(contentsMenuItem);

        aboutMenuItem.setMnemonic('a');
        aboutMenuItem.setText("About");
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 311, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrollableImage_V1, javax.swing.GroupLayout.DEFAULT_SIZE, 646, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 646, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(roiRadPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dispROI_CheckBox, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(zoomedImage_V1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(detailPanel_V1, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dispOutliersCheckBox, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(imageAdjustSelector, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(clResSelector_V1, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(sampleCategorySelector, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(autoROIRotation_CheckBox, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(dispROI_CheckBox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(roiRadPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(zoomedImage_V1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sampleCategorySelector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(autoROIRotation_CheckBox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dispOutliersCheckBox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(imageAdjustSelector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(clResSelector_V1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addGap(0, 24, Short.MAX_VALUE))
                    .addComponent(jScrollPane2)
                    .addComponent(scrollableImage_V1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(detailPanel_V1, javax.swing.GroupLayout.DEFAULT_SIZE, 218, Short.MAX_VALUE)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void loadPatientsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadPatientsMenuItemActionPerformed
        if (vm != null) {
            vm.fv_loadPatients();
        }
    }//GEN-LAST:event_loadPatientsMenuItemActionPerformed

    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        System.exit(0);
    }//GEN-LAST:event_exitMenuItemActionPerformed

    private void loadRoisMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadRoisMenuItemActionPerformed
        if (vm != null) {
            vm.fv_loadRois();
        }
    }//GEN-LAST:event_loadRoisMenuItemActionPerformed

    private void saveRoisMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveRoisMenuItemActionPerformed
        if (vm != null) {
            vm.fv_saveRois();
        }
    }//GEN-LAST:event_saveRoisMenuItemActionPerformed

    private void saveMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveMenuItemActionPerformed
        if (vm != null) {
            vm.fv_savePatients();
        }
    }//GEN-LAST:event_saveMenuItemActionPerformed

    private void detectUsConesLightMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_detectUsConesLightMenuItemActionPerformed
        if (vm != null) {
            vm.fv_detectUsCones(false);
        }
    }//GEN-LAST:event_detectUsConesLightMenuItemActionPerformed

    private void loadSampleMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadSampleMenuItemActionPerformed
        if (vm != null) {
            vm.fv_loadSamples();
        }
    }//GEN-LAST:event_loadSampleMenuItemActionPerformed

    private void saveSamplesMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveSamplesMenuItemActionPerformed
        if (vm != null) {
            vm.fv_saveSamples();
        }
    }//GEN-LAST:event_saveSamplesMenuItemActionPerformed

    private void loadMeasurementsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadMeasurementsMenuItemActionPerformed
        if (vm != null) {
            vm.fv_loadMeasurements();
        }
    }//GEN-LAST:event_loadMeasurementsMenuItemActionPerformed

    private void saveMeasurementsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveMeasurementsMenuItemActionPerformed
        if (vm != null) {
            vm.fv_saveMeasurements();
        }
    }//GEN-LAST:event_saveMeasurementsMenuItemActionPerformed

    private void removeOutliersMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeOutliersMenuItemActionPerformed
        if (vm != null) {
            vm.fv_removeOutliers();
        }
    }//GEN-LAST:event_removeOutliersMenuItemActionPerformed

    private void trainClassifiersMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_trainClassifiersMenuItemActionPerformed
        if (vm != null) {
            vm.fv_trainClassifiers();
        }
    }//GEN-LAST:event_trainClassifiersMenuItemActionPerformed

    private void testClassifiersMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_testClassifiersMenuItemActionPerformed
        if (vm != null) {
            vm.fv_testClassifiers();
        }
    }//GEN-LAST:event_testClassifiersMenuItemActionPerformed

    private void performMeasurementsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_performMeasurementsFullMenuItemActionPerformed
        if (vm != null) {
            vm.fv_performMeasurements();
        }
    }//GEN-LAST:event_performMeasurementsFullMenuItemActionPerformed

    private void exportWekaInstancesMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportWekaInstancesMenuItemActionPerformed
        if (vm != null) {
            vm.fv_exportWekaInstances();
        }
    }//GEN-LAST:event_exportWekaInstancesMenuItemActionPerformed

    private void evaluateClassifiersMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_evaluateClassifiersMenuItemActionPerformed
        if (vm != null) {
            vm.fv_evaluateAllCLassifiers();
        }
    }//GEN-LAST:event_evaluateClassifiersMenuItemActionPerformed

    private void detectUsConesFullMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_detectUsConesFullMenuItemActionPerformed
        if (vm != null) {
            vm.fv_detectUsCones(true);
        }
    }//GEN-LAST:event_detectUsConesFullMenuItemActionPerformed

    private void performPreprocessingMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_performPreprocessingMenuItemActionPerformed
        if (vm != null) {
            vm.fv_performPreprocessing();
        }
    }//GEN-LAST:event_performPreprocessingMenuItemActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JCheckBox autoROIRotation_CheckBox;
    lfa.gui.classifierselector.ClResSelector_V clResSelector_V1;
    private javax.swing.JMenu classificationMenu;
    lfa.gui.advancedtable.AdvancedTable_V colorDataTable_V1;
    lfa.gui.advancedtable.AdvancedTable_V colorDataTable_V2;
    private javax.swing.JMenuItem contentsMenuItem;
    lfa.gui.detailpanel.DetailPanel_V detailPanel_V1;
    private javax.swing.JMenuItem detectUsConesFullMenuItem;
    private javax.swing.JMenuItem detectUsConesLightMenuItem;
    private javax.swing.JCheckBox dispOutliersCheckBox;
    private javax.swing.JCheckBox dispROI_CheckBox;
    private javax.swing.JMenu editMenu;
    private javax.swing.JMenuItem evaluateClassifiersMenuItem;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JMenuItem exportWekaInstancesMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenu helpMenu;
    lfa.gui.enumselector.EnumSelector_V imageAdjustSelector;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTable jTable1;
    private javax.swing.JMenuItem loadMeasurementsMenuItem;
    private javax.swing.JMenuItem loadPatientsMenuItem;
    private javax.swing.JMenuItem loadRoisMenuItem;
    private javax.swing.JMenuItem loadSampleMenuItem;
    private javax.swing.JMenu measurementsMenu;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem performMeasurementsMenuItem;
    private javax.swing.JMenuItem performMeasurementsLightMenuItem;
    private javax.swing.JMenuItem performPreprocessingMenuItem;
    private javax.swing.JMenuItem prepareClassificationMenuItem;
    private javax.swing.JMenuItem recalrRoiRotationMenuItem;
    private javax.swing.JMenuItem removeOutliersMenuItem;
    private javax.swing.ButtonGroup roiRadGroup;
    private javax.swing.JPanel roiRadPanel;
    private javax.swing.JRadioButton roiRad_RadioBtn1;
    private javax.swing.JRadioButton roiRad_RadioBtn2;
    private javax.swing.JRadioButton roiRad_RadioBtn3;
    private javax.swing.JRadioButton roiRad_RadioBtn4;
    private javax.swing.JRadioButton roiRad_RadioBtn5;
    lfa.gui.enumselector.EnumSelector_V sampleCategorySelector;
    private javax.swing.JMenuItem saveMeasurementsMenuItem;
    private javax.swing.JMenuItem saveMenuItem;
    private javax.swing.JMenuItem saveRoisMenuItem;
    private javax.swing.JMenuItem saveSamplesMenuItem;
    lfa.gui.scrollableimage.ScrollableImage_V scrollableImage_V1;
    lfa.gui.roistatisticstable.RoiStatisticsTable_V statisticsTable_V1;
    private javax.swing.JMenuItem testClassifiersMenuItem;
    private javax.swing.JMenuItem trainClassifiersMenuItem;
    lfa.gui.zoomedimage.ZoomedImage_V zoomedImage_V1;
    // End of variables declaration//GEN-END:variables

    //==============================================================================================
    private void dispROIsChanged() {
        roiRadPanel.setEnabled(dispROI_CheckBox.isSelected());
        roiRad_RadioBtn1.setEnabled(dispROI_CheckBox.isSelected());
        roiRad_RadioBtn2.setEnabled(dispROI_CheckBox.isSelected());
        roiRad_RadioBtn3.setEnabled(dispROI_CheckBox.isSelected());
        roiRad_RadioBtn4.setEnabled(dispROI_CheckBox.isSelected());
        roiRad_RadioBtn5.setEnabled(dispROI_CheckBox.isSelected());
        if (vm != null) {
            vm.fv_roiDispModeChanged(dispROI_CheckBox.isSelected());
        }
    }
    
    //==============================================================================================
    private void autoROIRotationChanged() {
        if (vm != null) {
            vm.fv_roiRotationChanged(autoROIRotation_CheckBox.isSelected());
        }
    }
    
    //==============================================================================================
    private void dispOutliersChanged() {
        if (vm != null) {
            vm.fv_dispOutliersChanged(dispOutliersCheckBox.isSelected());
        }
    }
    
    //==============================================================================================
    private void ROIRadChanged() {
        if (vm == null) return;
        List<Integer> possibleRads = new ArrayList<>();
        for (Enumeration<AbstractButton> rButtons = roiRadGroup.getElements(); rButtons.hasMoreElements();) {
            AbstractButton button = rButtons.nextElement();
            String text = button.getText();
            if (!"all".equals(text)) {
                possibleRads.add(Integer.parseInt(text));
            }
        }
        List<Integer> selectedRads = new ArrayList<>();
        for (Enumeration<AbstractButton> rButtons = roiRadGroup.getElements(); rButtons.hasMoreElements();) {
            AbstractButton button = rButtons.nextElement();
            if (button.isSelected()) {
                String text = button.getText();
                if ("all".equals(text)) {
                    selectedRads.addAll(possibleRads);
                } else {
                    selectedRads.add(Integer.parseInt(text));
                }
                break;
            }
        }
        vm.fv_roiRadSelected(selectedRads);
    }
    
}
