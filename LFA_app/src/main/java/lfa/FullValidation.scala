package lfa

import base.ScalaOps
import base.backgroundexecution.{IBackgroundFactory, IBackgroundMonitor, XBackgroundModule, XBackgroundTask}
import base.logging.{ILogger, LoggingModule}
import base.resource.ResourceLoaderModule
import com.google.inject.Guice
import com.typesafe.scalalogging.LazyLogging
import lfa.cl.ClassificationModule
import lfa.colortags.ColorTagModule
import lfa.data.packed.SampleCollection_packed
import lfa.data.{Patient, SampleCategory}
import lfa.datamanager.{DataManagerModule, IDataManager}
import lfa.diagnosis.{DiagnosisDeterminatorModule, IDiagnosisDeterminator}
import lfa.gui.dialog.DialogModule
import lfa.gui.evalresult.EvalResult_Module
import lfa.keyprocessor.KeyProcessor_Module
import lfa.major.{IMajor_VC, IMajor_VM, Major_Module, Major_V}
import lfa.measurement.MeasurementModule
import lfa.roiStatistics.RoiStatistics_Module
import lfa.roilayouteditor.RoiLayout_Module
import lfa.storage.StorageModule
import xubiker.XLearnTools.vote.XVoter

import scala.collection.JavaConverters._
import scala.collection.mutable

object FullValidation extends App with LazyLogging {

  // ----------------------------------------------------------------- initialize environment --------------------------
  VisualStyleManager.applyDefaultSystemVisualStyle()
  private val modules = List(
    new ResourceLoaderModule, new StorageModule, new DialogModule,
    new XBackgroundModule, new ColorTagModule, new DataManagerModule,
    new KeyProcessor_Module, new RoiLayout_Module, new Major_Module,
    new RoiStatistics_Module, new MeasurementModule, new ClassificationModule,
    new DiagnosisDeterminatorModule, new EvalResult_Module, new LoggingModule)
  private val injector = Guice.createInjector(modules.asJava)
  private val majorVM = injector.getInstance(classOf[IMajor_VM])
  private val majorV = injector.getInstance(classOf[Major_V])
  majorVM.setView(majorV)
  majorV.setViewModel(majorVM.asInstanceOf[IMajor_VC])
  majorVM.run()
  private val dataManager = injector.getInstance(classOf[IDataManager])
  private val diagnosisDeterminator = injector.getInstance(classOf[IDiagnosisDeterminator])
  private val xLogger = injector.getInstance(classOf[ILogger])
  // -------------------------------------------------------------------------------------------------------------------

  dataManager.loadPatients(AppConf.validation.patientsPath)
  dataManager.loadRois(AppConf.validation.roisPath)
  dataManager.loadSamples(AppConf.validation.baseSamplePath)

  private val disease = AppConf.diagnostics.disease
  private val score = AppConf.diagnostics.score
  logger.info(s"Full validation with $score configuration. Labels: ${diagnosisDeterminator.getAllLabels(score).mkString(", ")}.")

  private val patients: List[Patient] = dataManager.getPatients.toList.asScala.toList
    .filter(p => dataManager.getSamples.getCategory(p.getId) == SampleCategory.UNUSED)

  private val labels = diagnosisDeterminator.getAllLabels(score).toList

  // ---------- determine the size of train and test sets
  private val patientsByLabel: Map[String, List[Patient]] = patients.groupBy(p => diagnosisDeterminator.getLabel(p, disease, score).get)
  logger.info("class sizes: " + patientsByLabel.map(_._2.size).mkString(" "))
  private val minClassSize = patientsByLabel.map(_._2.size).min
  logger.info(s"class with minimal number of patients contains $minClassSize patients")
  private val trainSizePerClass = (minClassSize.toDouble * 0.66).toInt
  private val testSizePerClass = minClassSize - trainSizePerClass
  logger.info(s"$trainSizePerClass patients per class would be used for training")
  logger.info(s"$testSizePerClass patients per class would be used for testing")

  private var currentSampleName: String = ""
  private val sampleConfidenceMap = mutable.Map.empty[String, (Double, Double)] // sample -> (confidence, accuracy)
  private val classifierConfidenceMap = mutable.Map.empty[(String, String), Double] // (sample, classifier) -> confidence

  private val extendedSamples = mutable.Buffer.empty[String]
  private val extendedTasks = mutable.Buffer.empty[XBackgroundTask]

  // ---------- generate tasks
  private val wizard = injector.getInstance(classOf[IBackgroundFactory]).createWizard(majorV)
  // --- simple samples
  for (_ <- 1 to AppConf.validation.nSamples) {
    wizard.addTask(new XBackgroundTask(ScalaOps.toJavaFunction(_generateApplyAndSaveSample)))
//    wizard.addTask(majorVM.asInstanceOf[IMajor_VC].gt_performMeasurements())
//    wizard.addTask(majorVM.asInstanceOf[IMajor_VC].gt_removeOutliers())
//    wizard.addTask(majorVM.asInstanceOf[IMajor_VC].gt_trainClassifiers())
//    wizard.addTask(majorVM.asInstanceOf[IMajor_VC].gt_testClassifiers())
//    wizard.addTask(majorVM.asInstanceOf[IMajor_VC].gt_evaluateAllCLassifiers())
//    wizard.addTask(new XBackgroundTask(ScalaOps.toJavaFunction(_vote)))
  }
  wizard.addTask(new XBackgroundTask(ScalaOps.toJavaFunction(_printResult)))
  // --- extended samples
//  wizard.addTask(new XBackgroundTask(ScalaOps.toJavaFunction(_generateExtendedTasks)))
//  wizard.addTask(extendedTasks.asJava)
  wizard.addTask(new XBackgroundTask(ScalaOps.toJavaFunction(_finalTask)))
  // ---------- execute tasks
  wizard.start()

  private def _generateApplyAndSaveSample(ctrl: IBackgroundMonitor): AnyRef = {
    // --- reset sample
    patients.foreach(p => dataManager.getSamples.setCategory(p.getId, SampleCategory.UNUSED))
    // --- fill sample
    val trainPids = mutable.Buffer.empty[Int]
    val testPids = mutable.Buffer.empty[Int]
    labels.foreach(lbl => {
      val ps: List[Patient] = scala.util.Random.shuffle(patientsByLabel(lbl))
      val trainPatients = ps.take(trainSizePerClass)
      val testPatients = ps.slice(trainSizePerClass, trainSizePerClass + testSizePerClass)
      trainPatients.foreach(p => dataManager.getSamples.setCategory(p.getId, SampleCategory.TRAIN_SET))
      testPatients.foreach(p => dataManager.getSamples.setCategory(p.getId, SampleCategory.TEST_SET))
      trainPids ++= trainPatients.map(_.getId)
      testPids ++= testPatients.map(_.getId)
    })
    // --- print to log ---
    val train: mutable.Buffer[(Int, String)] = trainPids.zip(trainPids.map(p => diagnosisDeterminator.getLabel(p, disease, score).get))
    val test: mutable.Buffer[(Int, String)] = testPids.zip(testPids.map(p => diagnosisDeterminator.getLabel(p, disease, score).get))
    logger.info("train set: " + train.mkString(" "))
    logger.info("test set: " + test.mkString(" "))
    // --- get sample name
    val samplesPacked = dataManager.getSamples.getPackAction.apply(dataManager.getSamples).asInstanceOf[SampleCollection_packed]
    val sampleName = Math.abs(samplesPacked.hashCode()).toString
    currentSampleName = sampleName
    // --- save sample
    dataManager.saveSamples(AppConf.validation.samplesDir + "\\r_" + sampleName + ".xml")
    xLogger.message(s":::::::::::::::::::: sample $sampleName ::::::::::::::::::::")
    xLogger.message(s"train set (${trainPids.size}): ${trainPids.mkString(", ")}")
    xLogger.message(s"test set (${testPids.size}): ${testPids.mkString(", ")}")
    null
  }

  private def _vote(ctrl: IBackgroundMonitor): AnyRef = {
    val classifiers = dataManager.getClResults.getAllTexClassifierTags.asScala.toList
    xLogger.message(s"${classifiers.size} classifiers")
    val pids = dataManager.getClResults.getAllTexClResults.asScala.map(_.xVectorSysVals(0).toInt).toList.distinct
    var currentSampleWeight = 0.0
    val correctlyClPids = mutable.Buffer.empty[Int]
    for (pid <- pids) {
      val actualLabel = diagnosisDeterminator.getLabel(pid, disease, score).get()
      xLogger.message(s"==================== Patient $pid ($actualLabel) ====================")
      val patientResults = dataManager.getClResults.getAllTexClResults.asScala.filter(_.xVectorSysVals(0).toInt == pid)
      val clVotes = mutable.Buffer.empty[(String, Double)]
      for (cl <- classifiers) {
        val clResults = patientResults.filter(_.tag == cl)
        val clVote = XVoter.vote(clResults.map(_.resLabel).map((_, 1.0))).bestLabelsWithWeights.head
        val correct = actualLabel == clVote._1
        val verdict = if (actualLabel == clVote._1) "OK" else "WRONG"
        val clWeight = (if (correct) clVote._2 else -clVote._2) / pids.size
        val prevClWeight = classifierConfidenceMap.getOrElse((currentSampleName, cl), 0.0)
        classifierConfidenceMap.put((currentSampleName, cl), prevClWeight + clWeight)
        xLogger.message(s"$cl voted for ${clVote._1} with confidence ${clVote._2}. $verdict")
        clVotes += clVote
      }
      val patientVote = XVoter.vote(clVotes).bestLabelsWithWeights.head
      val correct = actualLabel == patientVote._1
      if (correct) correctlyClPids += pid
      val verdict = if (correct) "OK" else "WRONG"
      xLogger.message(s"===== Final vote: ${patientVote._1} with confidence ${patientVote._2}. $verdict =====")
      val patientWeight = if (correct) patientVote._2 else -patientVote._2
      currentSampleWeight += patientWeight / pids.size
    }
    val misClassifiedPids = pids.filter(!correctlyClPids.contains(_))
    sampleConfidenceMap += currentSampleName -> (currentSampleWeight, correctlyClPids.size.toDouble / pids.size)
    xLogger.message("#################### final verdict for sample ####################")
    xLogger.message(s"correctly classified patients: ${correctlyClPids.map(pid => pid.toString + "(" + diagnosisDeterminator.getLabel(pid, disease, score).get + ")").mkString(", ")}")
    xLogger.message(s"misclassified patients: ${misClassifiedPids.map(pid => pid.toString + "(" + diagnosisDeterminator.getLabel(pid, disease, score).get + ")").mkString(", ")}")
    xLogger.message("##################################################################")
    null
  }

  private def _printResult(ctrl: IBackgroundMonitor): AnyRef = {
    // ----- normalize classifier-confidence map -----
    classifierConfidenceMap.foreach(p => {
      classifierConfidenceMap.put(p._1, p._2 / AppConf.validation.nSamples)
    })
    // ----- print sample-confidence map
    xLogger.message(">>>>>>>>>>>>>>>>>>>>-----------------------<<<<<<<<<<<<<<<<<<<<")
    xLogger.message(">>>>>>>>>>>>>>>>>>>> sample-confidence map <<<<<<<<<<<<<<<<<<<<")
    xLogger.message(">>>>>>>>>>>>>>>>>>>>-----------------------<<<<<<<<<<<<<<<<<<<<")
    xLogger.message(s"samples (${sampleConfidenceMap.size}):")
    val sampleConfidenceMapOrdered = sampleConfidenceMap.toList.sortBy(_._2._1)(Ordering[Double].reverse)
    sampleConfidenceMapOrdered.foreach(p => {
      xLogger.message(s"sample ${p._1}: ${p._2._1} (${p._2._2 * 100}% patients classified)")
    })
    // ----- print classifier-confidence map
    xLogger.message(">>>>>>>>>>>>>>>>>>>>---------------------------<<<<<<<<<<<<<<<<<<<<")
    xLogger.message(">>>>>>>>>>>>>>>>>>>> classifier-confidence map <<<<<<<<<<<<<<<<<<<<")
    xLogger.message(">>>>>>>>>>>>>>>>>>>>---------------------------<<<<<<<<<<<<<<<<<<<<")
    val nBestSamples = AppConf.validation.nBestSamples
    val bestSamples = sampleConfidenceMap.toList.sortBy(_._2._1)(Ordering[Double].reverse).map(_._1).take(nBestSamples)
    val t: List[(String, Double)] = bestSamples.flatMap(s => classifierConfidenceMap.filter(i => i._1._1 == s)).map(q => (q._1._2, q._2))
    val classifierConfidence: Map[String, Double] = t.groupBy(_._1).map(p => (p._1, p._2.map(_._2).sum))

    xLogger.message(s"classifiers (${classifierConfidence.size} classifiers for $nBestSamples best samples):")
    classifierConfidence.toList.sortBy(_._2)(Ordering[Double].reverse).foreach(p => {
      xLogger.message(s"${p._1}: ${p._2}")
    })
    null
  }

  private def _generateExtendedTasks(ctrl: IBackgroundMonitor): AnyRef = {
    val sampleConfidenceMapOrdered = sampleConfidenceMap.toList.sortBy(_._2._1)(Ordering[Double].reverse)
    extendedSamples.clear()
    extendedSamples ++= sampleConfidenceMapOrdered.take(AppConf.validation.nBestSamples).map(_._1)
    extendedTasks += new XBackgroundTask(ScalaOps.toJavaFunction(_startExtended))
    for (sample <- extendedSamples) {
      extendedTasks ++= List(
        applyExtendedSampleTask(sample),
        majorVM.asInstanceOf[IMajor_VC].gt_performMeasurements(),
        majorVM.asInstanceOf[IMajor_VC].gt_removeOutliers(),
        majorVM.asInstanceOf[IMajor_VC].gt_trainClassifiers(),
        majorVM.asInstanceOf[IMajor_VC].gt_testClassifiers(),
        majorVM.asInstanceOf[IMajor_VC].gt_evaluateAllCLassifiers(),
        new XBackgroundTask(ScalaOps.toJavaFunction(_vote))
      )
    }
    extendedTasks += new XBackgroundTask(ScalaOps.toJavaFunction(_printExtendedResult))
    null
  }

  private def applyExtendedSampleTask(sampleName: String): XBackgroundTask = {
    def foo(ctrl: IBackgroundMonitor): AnyRef = {
      currentSampleName = sampleName
      xLogger.message(s"******************** extended validation for sample $sampleName ********************")
      dataManager.loadSamples(AppConf.validation.samplesDir + "\\r_" + sampleName + ".xml")
      dataManager.getPatients.toList.asScala
        .filter(p => dataManager.getSamples.getCategory(p.getId) == SampleCategory.UNUSED)
        .foreach(p => dataManager.getSamples.setCategory(p.getId, SampleCategory.TEST_SET))
      val trainPids = dataManager.getPatients.toList.asScala.filter(p => dataManager.getSamples.getCategory(p.getId) == SampleCategory.TRAIN_SET).map(_.getId)
      val testPids = dataManager.getPatients.toList.asScala.filter(p => dataManager.getSamples.getCategory(p.getId) == SampleCategory.TEST_SET).map(_.getId)
      xLogger.message(s"train set (${trainPids.size}): ${trainPids.mkString(", ")}")
      xLogger.message(s"test set (${testPids.size}): ${testPids.mkString(", ")}")
      null
    }
    new XBackgroundTask(ScalaOps.toJavaFunction(foo))
  }

  private def _startExtended(ctrl: IBackgroundMonitor): AnyRef = {
    classifierConfidenceMap.clear()
    sampleConfidenceMap.clear()
    null
  }

  private def _printExtendedResult(ctrl: IBackgroundMonitor): AnyRef = {
    // ----- normalize classifier-confidence map -----
    classifierConfidenceMap.foreach(p => {
      classifierConfidenceMap.put(p._1, p._2 / AppConf.validation.nBestSamples)
    })
    // ----- print sample-confidence map
    xLogger.message(">>>>>>>>>>>>>>>>>>>>--------------------------------<<<<<<<<<<<<<<<<<<<<")
    xLogger.message(">>>>>>>>>>>>>>>>>>>> extended sample-confidence map <<<<<<<<<<<<<<<<<<<<")
    xLogger.message(">>>>>>>>>>>>>>>>>>>>--------------------------------<<<<<<<<<<<<<<<<<<<<")
    xLogger.message(s"samples (${sampleConfidenceMap.size}):")
    val sampleConfidenceMapOrdered = sampleConfidenceMap.toList.sortBy(_._2._1)(Ordering[Double].reverse)
    sampleConfidenceMapOrdered.foreach(p => {
      xLogger.message(s"sample ${p._1}: ${p._2._1} (${p._2._2 * 100}% patients classified)")
    })
    // ----- print classifier-confidence map
    xLogger.message(">>>>>>>>>>>>>>>>>>>>------------------------------------<<<<<<<<<<<<<<<<<<<<")
    xLogger.message(">>>>>>>>>>>>>>>>>>>> extended classifier-confidence map <<<<<<<<<<<<<<<<<<<<")
    xLogger.message(">>>>>>>>>>>>>>>>>>>>------------------------------------<<<<<<<<<<<<<<<<<<<<")
    val sortedSamples = sampleConfidenceMap.toList.sortBy(_._2._1)(Ordering[Double].reverse).map(_._1)
    val t: List[(String, Double)] = sortedSamples.flatMap(s => classifierConfidenceMap.filter(i => i._1._1 == s)).map(q => (q._1._2, q._2))
    val classifierConfidence: Map[String, Double] = t.groupBy(_._1).map(p => (p._1, p._2.map(_._2).sum))
    xLogger.message(s"classifiers (${classifierConfidence.size} classifiers for extended samples):")
    classifierConfidence.toList.sortBy(_._2)(Ordering[Double].reverse).foreach(p => {
      xLogger.message(s"${p._1}: ${p._2}")
    })
    null
  }

  private def _finalTask(ctrl: IBackgroundMonitor): AnyRef = {
    logger.info(":::::::::: full validation is finished ::::::::::")
    xLogger.close()
    null
  }

}