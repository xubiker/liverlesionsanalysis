package lfa.storage;

import java.util.Collection;
import weka.core.Instances;

//**************************************************************************************************
public interface IWekaStorage {
    void exportInstancesToArff(Instances inst);
    void exportInstancesToArff(Instances inst, String fileName);
    void exportInstancesToArffInBatch(Collection<Instances> inst);
}
