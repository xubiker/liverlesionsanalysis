package lfa.storage;

//**************************************************************************************************
public interface IObjectStorage {
    
    <T>T importInstance(Class<T> objClass, String filename) throws Exception;

    <T> void exportInstance(T obj, String filename);

    void deleteInstance(String filename);

    boolean objectExists(String fileName);
}
