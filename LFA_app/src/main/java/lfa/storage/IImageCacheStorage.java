package lfa.storage;

import xubiker.XImageTools.base.XImage;

//**************************************************************************************************
public interface IImageCacheStorage {

    enum FileType {
        PNG(".png"),
        SER(".ser");
        
        private final String value;

        FileType (String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }
    }
    
    XImage importImage(String fileName, FileType fileType);

    boolean exists(String fileName, FileType fileType);

    XImage importImage(String fileName, FileType fileType, long minCreationTime);

    void exportImage(XImage image, String fileName, FileType fileType, boolean reWrite);

}
