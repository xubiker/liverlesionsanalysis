package lfa.storage;

import com.google.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import lfa.gui.dialog.IDialog;
import weka.core.Instances;
import weka.core.converters.ArffSaver;

//**************************************************************************************************
class ArffStorage implements IWekaStorage {

    private final IDialog dialog;
    
    //==============================================================================================
    @Inject
    public ArffStorage(IDialog dialog) {
        this.dialog = dialog;
    }
    
    //==============================================================================================
    @Override
    public void exportInstancesToArff(Instances inst) {
        String path = dialog.saveFile(inst.getClass().toString(), "arff");
        if (path != null) {
            exportInstancesToArff(inst, path);
        } else {
            System.err.println("ArffStorage. exporting failed (null path)");
        }
    }

    //==============================================================================================
    @Override
    public void exportInstancesToArff(Instances inst, String fileName) {
        System.out.println("ArffStorage. performing exporting...");
        try {
            ArffSaver saver = new ArffSaver();
            saver.setInstances(inst);
            saver.setFile(new File(fileName));
            saver.writeBatch();
        } catch (IOException ex) {
            System.err.println("ArffStorage. exporting failed with error: " + ex);
            return;
        }
        System.out.println("ArffStorage. exporting finished");
    }

    //==============================================================================================
    @Override
    public void exportInstancesToArffInBatch(Collection<Instances> inst) {
        String path = dialog.chooseFolder();
        if (path != null) {
            inst.stream().forEach(i -> {
                exportInstancesToArff(i, path + File.separator + i.relationName() + ".arff");
            });
        } else {
            System.err.println("ArffStorage. exporting failed (null path)");
        }
    }
    
}
