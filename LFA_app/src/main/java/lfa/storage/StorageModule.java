package lfa.storage;

import com.google.inject.AbstractModule;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import lfa.packing.PackingModule;

//**************************************************************************************************
public class StorageModule extends AbstractModule {

    @Override
    protected void configure() {
        
        install(new PackingModule());
        
        install(
                new FactoryModuleBuilder()
                .implement(IImageCacheStorage.class, ImageCacheStorage.class)
                .implement(IPackedStorage.class, XMLStorage.class)
                .implement(IWekaStorage.class, ArffStorage.class)
                .implement(IGsonStorage.class, GSONStorage.class)
                .implement(IObjectStorage.class, ObjectStorage.class)
                .build(StorageFactory.class)
        );
        
    }
    
}
