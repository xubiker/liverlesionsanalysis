package lfa.storage;

import lfa.packing.IPackable;

//**************************************************************************************************
public interface IPackedStorage {
    
    IPackable importInstance(Class<?> objClass) throws Exception;
    IPackable importInstance(Class<?> objClass, String filename) throws Exception;
    
    void exportInstance(IPackable obj) throws Exception;
    void exportInstance(IPackable obj, String filename) throws Exception;
}
