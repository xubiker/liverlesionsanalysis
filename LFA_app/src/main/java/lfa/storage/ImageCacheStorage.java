package lfa.storage;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import xubiker.XImageTools.base.XImage;
import xubiker.XImageTools.base.XImageSerializator;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;

//**************************************************************************************************
class ImageCacheStorage implements IImageCacheStorage {
    
    private final String cacheFolderPath;
    
    //==============================================================================================
    @Inject
    public ImageCacheStorage(
            @Assisted("cacheFolderPath") String cacheFolderPath) {
        this.cacheFolderPath = cacheFolderPath;
    }

    //==============================================================================================
    @Override
    public XImage importImage(String fileName, FileType fileType) {
        return importImage(fileName, fileType, Long.MIN_VALUE);
    }

    //==============================================================================================
    @Override
    public synchronized boolean exists(String fileName, FileType fileType) {
        fileName += fileType.toString();
        Path path = Paths.get(cacheFolderPath).resolve(fileName);
        return Files.exists(path) && Files.isRegularFile(path);
    }

    //==============================================================================================
    @Override
    public synchronized XImage importImage(String fileName, FileType fileType, long minCreationTime) {
        try {
            fileName += fileType.toString();
            Path file = Paths.get(cacheFolderPath).resolve(fileName);
            BasicFileAttributes attributes = Files.readAttributes(file, BasicFileAttributes.class);
            long creationTime = attributes.creationTime().toMillis();
            if (creationTime < minCreationTime) {
                return null;
            } else {
                switch (fileType) {
                    case PNG:
                        return importImg(file.toString());
                    case SER:
                        return deserialize(file.toString());
                    default:
                        throw new AssertionError(fileType.name());
                    
                }
            }
        } catch (IOException ex) {
            System.out.println("importing image failed: " + ex);
            return null;
        }
    }

    //==============================================================================================
    private XImage importImg(String path) {
        XImage image = XImage.read(path);
        if (image.isEmpty()) {
            System.err.println("importing image failed");
            return null;
        }
        return image;
    }
    
    //==============================================================================================
    @Override
    public synchronized void exportImage(XImage image, String fileName, FileType fileType, boolean reWrite) {
        fileName += fileType.toString();
        String path = cacheFolderPath + File.separator + fileName;
        File file = new File(path);
        if (!file.exists() || reWrite) {
            switch (fileType) {
                case PNG:
                    XImage.XImageOutput(image).write(path);
                    break;
                case SER:
                    serialize(image, path);
                    break;
                default:
                    throw new AssertionError(fileType.name());
            }
        } else {
            System.out.println("exporting image cancelled");
        }
    }

    //==============================================================================================
    private synchronized void serialize(XImage image, String path) {
        XImageSerializator.serialize(image, path);
    }
    
    //==============================================================================================
    private synchronized XImage deserialize(String path) {
        return XImageSerializator.deserialize(path);
    }

}
