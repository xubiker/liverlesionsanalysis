package lfa.storage;

//**************************************************************************************************
public interface IGsonStorage {

    IGsonConvertible importInstance(Class<?> objClass) throws Exception;
    IGsonConvertible importInstance(Class<?> objClass, String filename) throws Exception;
    
    void exportInstance(IGsonConvertible obj) throws Exception;
    void exportInstance(IGsonConvertible obj, String filename) throws Exception;
    
}
