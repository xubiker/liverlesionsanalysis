package lfa.storage;

import com.google.inject.assistedinject.Assisted;

//**************************************************************************************************
public interface StorageFactory {
    
    IPackedStorage createPackedStorage();
    
    IWekaStorage createWekaStorage();

    IImageCacheStorage createImageCaheStorage(
            @Assisted("cacheFolderPath") String cacheFolderPath
    );
    
    IGsonStorage createGSonStorage(
            @Assisted("prettyPrinting") boolean prettyPrinting
    );
    
    IObjectStorage createObjectStorage(
            @Assisted("cacheFolderPath") String cacheFolderPath
    );
    
}
