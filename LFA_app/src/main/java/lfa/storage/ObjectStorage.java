package lfa.storage;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.nustaq.serialization.FSTObjectInput;
import org.nustaq.serialization.FSTObjectOutput;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

//**************************************************************************************************
class ObjectStorage implements IObjectStorage {
    
    private final String cacheFolderPath;
    private final String extension = ".ser";
    
    //==============================================================================================
    @Inject
    public ObjectStorage(
            @Assisted("cacheFolderPath") String cacheFolderPath
    ) {
        this.cacheFolderPath = cacheFolderPath;
    }

    //==============================================================================================
    @Override
    public synchronized <T> T importInstance(Class<T> objClass, String filename) throws Exception {
        T res = null;
        String fullName = cacheFolderPath + File.separator + filename + extension;
        try (FileInputStream fis = new FileInputStream(fullName);
                FSTObjectInput oin = new FSTObjectInput(fis)) {
                //ObjectInputStream oin = new ObjectInputStream(fis)) {
            res = objClass.cast(oin.readObject());
            oin.close();
            fis.close();
        } catch (Exception ex) {
            System.err.println("import failed: " + ex);
        }
        return res;
    }

    //==============================================================================================
    @Override
    public synchronized <T> void exportInstance(T obj, String filename) {
        System.out.print("exporting object: " + filename + ". ");
        String fullName = cacheFolderPath + File.separator + filename + extension;
        try (FileOutputStream fos = new FileOutputStream(fullName);
                FSTObjectOutput oos = new FSTObjectOutput(fos)) {
            oos.writeObject(obj);
            oos.flush();
            oos.close();
            fos.close();
            System.out.println("Done.");
        } catch (Exception ex) {
            System.err.println("Export failed: " + ex);
        }
    }

    //==============================================================================================
    @Override
    public synchronized void deleteInstance(String filename) {
        System.out.print("deleting object: " + filename + ". ");
        String fullName = cacheFolderPath + File.separator + filename + extension;
        try {
            Files.delete(Paths.get(fullName));
            System.out.println("Done.");
        } catch (Exception ex) {
            System.err.println("Failed: " + ex);
        }
    }

    //==============================================================================================
    @Override
    public boolean objectExists(String filename) {
        String fullName = cacheFolderPath + File.separator + filename + extension;
        File f = new File(fullName);
        return f.exists();
    }
    
}
