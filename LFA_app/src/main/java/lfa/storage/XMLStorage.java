package lfa.storage;

import com.google.inject.Inject;
import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import lfa.gui.dialog.IDialog;
import lfa.packing.IPackable;
import lfa.packing.IPacker;
import lfa.packing.IUnpackable;

//**************************************************************************************************
class XMLStorage implements IPackedStorage {
    
    private IPacker packer;
    private IDialog dialog;
    
    //==============================================================================================
    @Inject
    void setPacker(IPacker packer) {
        this.packer = packer;
    }
    
    //==============================================================================================
    @Inject
    void setDialog(IDialog dialog) {
        this.dialog = dialog;
    }

    //==============================================================================================
    @Override
    public void exportInstance(IPackable obj, String filename) throws Exception {
        System.out.println("XMLStorage. performing exporting...");
        IUnpackable packedObj = packer.pack(obj);
        try {
            File file = new File(filename);
            JAXBContext context = JAXBContext.newInstance(packedObj.getClass());
            Marshaller jaxbMarshaller = context.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(packedObj, file);
        } catch (JAXBException ex) {
            System.err.println("XMLStorage. exporting failed with error: " + ex);
            return;
        }
        System.out.println("XMLStorage. exporting finished");
    }

    //==============================================================================================
    @Override
    public IPackable importInstance(Class<?> objClass, String filename) throws Exception {
        System.out.println("XMLStorage. performing importing...");
        Object unpackT = objClass.newInstance();
        IPackable unpackedInst = (IPackable) unpackT;
        Class<?> packT = unpackedInst.getPackT();
        try {
            File file = new File(filename);
            JAXBContext context = JAXBContext.newInstance(packT);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            Object obj = unmarshaller.unmarshal(file);
            System.out.println("XMLStorage. importing finished");
            IPackable unpackedObj = packer.unpack((IUnpackable) obj);
            return unpackedObj;
        } catch (JAXBException ex) {
            System.err.println("XMLStorage. importing failed with error: " + ex);
            return null;
        }
    }

    //==============================================================================================
    @Override
    public IPackable importInstance(Class<?> objClass) throws Exception {
        String path = dialog.loadFile(objClass.toString(), "xml");
        if (path != null) {
            return importInstance(objClass, path);
        } else {
            System.err.println("XMLStorage. importing failed (null path)");
            return null;
        }
    }

    //==============================================================================================
    @Override
    public void exportInstance(IPackable obj) throws Exception {
        String path = dialog.saveFile(obj.getClass().toString(), "xml");
        if (path != null) {
            exportInstance(obj, path);
        } else {
            System.err.println("XMLStorage. exporting failed (null path)");
        }
    }

}
