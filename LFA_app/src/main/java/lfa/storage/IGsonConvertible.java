package lfa.storage;

import com.google.gson.TypeAdapter;

public interface IGsonConvertible {
    
    Class<? extends TypeAdapter> getGsonTypeAdapter();
    
}
