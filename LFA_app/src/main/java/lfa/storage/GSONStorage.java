package lfa.storage;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import lfa.gui.dialog.IDialog;

//**************************************************************************************************
class GSONStorage implements IGsonStorage {

    @Inject private IDialog dialog;
    private final boolean prettyPrinting;
            
    //==============================================================================================
    @Inject
    public GSONStorage(
            @Assisted("prettyPrinting") boolean prettyPrinting
    ) {
        this.prettyPrinting = prettyPrinting;
    }
    
    //==============================================================================================
    @Override
    public void exportInstance(IGsonConvertible obj, String filename) throws Exception {
        System.out.println("JSONStorage. performing exporting...");

        GsonBuilder gsonBuilder = new GsonBuilder()
        .registerTypeAdapter(obj.getClass(), obj.getGsonTypeAdapter().newInstance());
        if (prettyPrinting) {
            gsonBuilder.setPrettyPrinting();
        }
        Gson gson = gsonBuilder.create();

        File file = new File(filename);
        try (BufferedWriter writer
                = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)))) {
            gson.toJson(obj, writer);
        } catch (Exception ex) {
            System.err.println("JSONStorage. exporting failed with error: " + ex);
            return;
        }
        System.out.println("JSONStorage. exporting finished");
    }

    //==============================================================================================
    @Override
    public IGsonConvertible importInstance(Class<?> objClass, String filename) throws Exception {
        System.out.println("JSONStorage. performing importing...");

        GsonBuilder gsonBuilder = new GsonBuilder()
        .registerTypeAdapter(objClass, ((IGsonConvertible)objClass.newInstance()).getGsonTypeAdapter().newInstance());
        if (prettyPrinting) {
            gsonBuilder.setPrettyPrinting();
        }
        Gson gson = gsonBuilder.create();

        File file = new File(filename);
        try (BufferedReader reader
                = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            IGsonConvertible res = (IGsonConvertible) gson.fromJson(reader, objClass);
            System.out.println("JSONStorage. importing finished");
            return res;
        } catch (Exception ex) {
            System.err.println("JSONStorage. importing failed with error: " + ex);
            return null;
        }
    }

    //==============================================================================================
    @Override
    public IGsonConvertible importInstance(Class<?> objClass) throws Exception {
        String path = dialog.loadFile(objClass.toString(), "json");
        if (path != null) {
            return importInstance(objClass, path);
        } else {
            System.err.println("JSONStorage. importing failed (null path)");
            return null;
        }
    }

    //==============================================================================================
    @Override
    public void exportInstance(IGsonConvertible obj) throws Exception {
        String path = dialog.saveFile(obj.getClass().toString(), "json");
        if (path != null) {
            exportInstance(obj, path);
        } else {
            System.err.println("JSONStorage. exporting failed (null path)");
        }
    }
    
    
}
