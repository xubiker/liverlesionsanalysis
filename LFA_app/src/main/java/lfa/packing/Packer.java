package lfa.packing;

import java.util.function.Function;

//**************************************************************************************************
class Packer implements IPacker {

    //==============================================================================================
    @Override
    public IUnpackable pack(IPackable unpacked) {
        Function<IPackable, IUnpackable> packAction = unpacked.getPackAction();
        IUnpackable packed = packAction.apply(unpacked);
        return packed;
    }

    //==============================================================================================
    @Override
    public IPackable unpack(IUnpackable packed) {
        Function<IUnpackable, IPackable> unpackAction = packed.getUnpackAction();
        IPackable unpacked = unpackAction.apply(packed);
        return unpacked;
    }

}
