package lfa.packing;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

//**************************************************************************************************
public class PackingModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(IPacker.class).to(Packer.class).in(Singleton.class);
    }
    
}
