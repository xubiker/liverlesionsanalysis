package lfa.packing;

//**************************************************************************************************
public interface IPacker {

    public IUnpackable pack (IPackable unpacked);
    public IPackable unpack (IUnpackable packed);
    
}
