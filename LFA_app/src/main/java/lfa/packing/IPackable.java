package lfa.packing;

import java.util.function.Function;

//**************************************************************************************************
public interface IPackable {

    Function<IPackable, IUnpackable> getPackAction();
    Class<?> getPackT();
    
}
