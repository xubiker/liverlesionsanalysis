package lfa.packing;

import java.util.function.Function;

//**************************************************************************************************
public interface IUnpackable {

    Function<IUnpackable, IPackable> getUnpackAction();
    Class<?> getUnpackT();
    
}
