package lfa.roilayouteditor;

import lfa.measurement.ScalaImagePreprocessor;
import base.GraphicsConverter;
import base.backgroundexecution.IBackgroundFactory;
import base.backgroundexecution.IBackgroundWizard;
import base.backgroundexecution.XBackgroundTask;
import base.collections.Pair;
import base.collections.Triplet;
import base.events.*;
import com.google.inject.Inject;
import lfa.image.ImageAdjustMethod;
import lfa.cl.ClResultCollection;
import lfa.cl.ClassificationXEvent;
import lfa.cl.IPrimalOutlierRemover;
import lfa.cl.OutlierRemoverUpdateXEvent;
import lfa.data.*;
import lfa.datamanager.IDataManager;
import lfa.events.*;
import lfa.major.Major_V;
import lfa.painting.IPainter;
import lfa.painting.RoiPaintType;
import scala.Tuple2;
import xubiker.XImageTools.base.XImage;
import xubiker.XImageTools.ultrasound.conedetector.UsConeDetector;
import xubiker.XLearnTools.classify.ClResult;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

//**************************************************************************************************
class RoiLayoutEditor implements IRoiLayoutEditor  {

    private final IBackgroundFactory backgroundFactory;
    private final IDataManager datamanager;
    private final IPainter painter;
    private final ScalaImagePreprocessor imagePreprocessor;
    private IPrimalOutlierRemover outlierRemover;
    private final Major_V majorV;
    
    private Patient activePatient;
    private USnapshot activeSnapshot;
    
    private BufferedImage srcImage, resImage;
    private XImage srcImage_ocv, cachedImage;
    
    private int imageWidth, imageHeight;
    
    private final List<Integer> activeRoiRads = new ArrayList<>();
    private final List<Pair<Roi, Boolean>> activeRois = new ArrayList<>();
    private Roi newRoi = null;
    
    private boolean dispRoisMode, roiRotationMode, dispOutliersMode;
    private ImageAdjustMethod imageAdjustMethod = ImageAdjustMethod.values()[0];
    private boolean roiSelectionMode;
    private String selectedClassifier;

    private final XEventListenerManager xlm = XEventFactory.createListener();
    private final XEventBroadcasterManager xbm = XEventFactory.createBroadcaster();

    //==============================================================================================
    @Inject
    public RoiLayoutEditor(
            IBackgroundFactory backgroundFactory,
            Major_V majorV,
            IDataManager datamanager,
            IPrimalOutlierRemover outlierRemover,
            IPainter painter) {
        
        this.backgroundFactory = backgroundFactory;
        this.majorV = majorV;
        this.datamanager = datamanager;
        this.painter = painter;
        this.outlierRemover = outlierRemover;
        this.imagePreprocessor = new ScalaImagePreprocessor(datamanager);

        xlm.bindXEventHandler(MouseXEvent.class,
                e -> mouseXEventHandler((MouseXEvent) e));
        xlm.bindXEventHandler(RoiXEvent.class,
                e -> roiXEventHandler((XEventExtension) e));
        xlm.bindXEventHandler(ImageAdjustXEvent.class,
                e -> imagePreprocessXEventHandler((ImageAdjustXEvent) e));
        xlm.bindXEventHandler(OutlierRemoverUpdateXEvent.class,
                e -> outlierRemoverXEventHandler((OutlierRemoverUpdateXEvent) e));
        xlm.bindXEventHandler(ClassificationXEvent.ClassifierSelected.class,
                e -> classificationXEventHandler((ClassificationXEvent.ClassifierSelected) e));
    }

    //==============================================================================================
    @Override
    public void update(Patient p, USnapshot s, XImage image) {
        this.srcImage_ocv = image;
        this.srcImage = GraphicsConverter.xImageToBufferedImage(image);
        updateData(p, s);
        fullUpdate();
    }

    //==============================================================================================
    @Override
    public void update() {
        fullUpdate();
    }

    //==============================================================================================
    private void updateData(Patient p, USnapshot s) {
        this.activePatient = p;
        this.activeSnapshot = s;
        this.imageWidth = (srcImage == null) ? -1 : srcImage.getWidth();
        this.imageHeight = (srcImage == null) ? -1 : srcImage.getHeight();
    }
    
    //==============================================================================================
    private void updateActiveRois() {
        this.activeRois.clear();
        RoiCollection roiCollection = datamanager.getRoiCollection();
        if (roiCollection != null && activePatient != null && activeSnapshot != null) {
            List<Roi> rois = roiCollection.getRois(activePatient.getId(), activeSnapshot.getId());
            activeRoiRads.forEach(rad -> {
                activeRois.addAll(
                    rois.stream()
                            .filter(r -> r.getRadius() == rad)
                            .map(r -> new Pair<>(r, false))
                            .collect(Collectors.toList())
                );
            });
        }
    }

    //==============================================================================================
    private void weakUpdate(boolean useCache) {
        this.resImage = processImage(srcImage_ocv, useCache);
        notifyXEventListeners(new PreviewUpdateXEvent(this, resImage));
        notifyXEventListeners(new ZoomedImageUpdateXEvent(this, resImage));
    }
    
    //==============================================================================================
    private void weakUpdateWithoutZoom(boolean useCache) {
        this.resImage = processImage(srcImage_ocv, useCache);
        notifyXEventListeners(new PreviewUpdateXEvent(this, resImage));
    }

    //==============================================================================================
    private void zoomOnlyUpdate(int x, int y) {
        notifyXEventListeners(new ZoomedImageUpdateXEvent(this, resImage, x, y));
    }

    //==============================================================================================
    private void fullUpdate() {
        updateActiveRois();
        weakUpdate(false);
    }
    
    //==============================================================================================
    private void mouseXEventHandler(MouseXEvent e) {
        switch (e.getAction()) {
            //------------------------------------------------------------
            case DRAGGED:
                if (roiSelectionMode) {
                    // do nothing
                } else {
                    placePossibleRoi(e.getX(), e.getY());
                    weakUpdateWithoutZoom(true);
                }
                zoomOnlyUpdate(e.getX(), e.getY());
                break;
            //------------------------------------------------------------
            case MOVED:
                zoomOnlyUpdate(e.getX(), e.getY());
                break;
            //------------------------------------------------------------
            case PRESSED:
                if (roiSelectionMode) {
                    // do nothing
                } else {
                    cancelSelection();
                    placePossibleRoi(e.getX(), e.getY());
                    weakUpdate(true);
                }
                break;
            //------------------------------------------------------------
            case RELEASED:
                if (roiSelectionMode) {
                    selectRois(e.getX(), e.getY());
                } else {
                    placePossibleRoi(e.getX(), e.getY());
                    createNewRoi();
                }
                weakUpdate(false);
                break;
            //------------------------------------------------------------
            default:
                throw new AssertionError(e.getAction().name());
        }
    }
    
    //==============================================================================================
    private void roiXEventHandler(XEventExtension e) {
        //----------------------------------------------------------------
        if (e.getClass() == RoiXEvent.DisplayRoisModeChanged.class) {
            RoiXEvent.DisplayRoisModeChanged event = (RoiXEvent.DisplayRoisModeChanged) e;
            this.dispRoisMode = event.getDispValue();
            weakUpdate(false);
            return;
        }
        //----------------------------------------------------------------
        if (e.getClass() == RoiXEvent.ActiveRadChanged.class) {
            RoiXEvent.ActiveRadChanged event = (RoiXEvent.ActiveRadChanged) e;
            this.activeRoiRads.clear();
            this.activeRoiRads.addAll(event.getActiveRad());
            fullUpdate();
            return;
        }
        //----------------------------------------------------------------
        if (e.getClass() == RoiXEvent.RotationModeChanged.class) {
            RoiXEvent.RotationModeChanged event = (RoiXEvent.RotationModeChanged) e;
            this.roiRotationMode = event.getRotationValue();
            weakUpdate(false);
            return;
        }
        //----------------------------------------------------------------
        if (e.getClass() == RoiXEvent.DispOutliersModeChanged.class) {
            RoiXEvent.DispOutliersModeChanged event = (RoiXEvent.DispOutliersModeChanged) e;
            this.dispOutliersMode = event.getDispOutliersValue();
            weakUpdate(false);
            return;
        }
        //----------------------------------------------------------------
        if (e.getClass() == RoiXEvent.SelectAll.class) {
            roiSelectionMode = true;
            activeRois.stream().forEach(p -> p.setSecond(true));
            weakUpdate(false);
            return;
        }
        //----------------------------------------------------------------
        if (e.getClass() == RoiXEvent.ResetSelection.class) {
            cancelSelection();
            weakUpdate(false);
            return;
        }
        //----------------------------------------------------------------
        if (e.getClass() == RoiXEvent.SelectionModeChanged.class) {
            RoiXEvent.SelectionModeChanged event = (RoiXEvent.SelectionModeChanged) e;
            roiSelectionMode = event.getSelectionMode();
            return;
        }
        //----------------------------------------------------------------
        if (e.getClass() == RoiXEvent.DeleteSelection.class) {
            roiSelectionMode = false;
            deleteSelection();
            fullUpdate();
            return;
        }
        //----------------------------------------------------------------
        if (e.getClass() == RoiXEvent.SelectionSampleChanged.class) {
            modifySelection(((RoiXEvent.SelectionSampleChanged) e).getCategory());
            cancelSelection();
            fullUpdate();
            return;
        }
    }
    
    //==============================================================================================
    private void imagePreprocessXEventHandler(ImageAdjustXEvent e) {
        this.imageAdjustMethod = e.getImageAdjustMethod();
        System.out.println("adjust method changed: " + imageAdjustMethod);
        fullUpdate();
    }
    
    //==============================================================================================
    private void outlierRemoverXEventHandler(OutlierRemoverUpdateXEvent e) {
        fullUpdate();
    }
    
    //==============================================================================================
    private void classificationXEventHandler(ClassificationXEvent.ClassifierSelected e) {
        this.selectedClassifier = e.getSelectedClassifer();
        weakUpdate(false);
    }
    
    //==============================================================================================
    private BufferedImage processImage(XImage srcImage, boolean useCache) {
        if (srcImage == null) return null;
        ClResultCollection clResults = datamanager.getClResults();
        XImage m;
        if (useCache && cachedImage != null) {
            m = cachedImage;
        } else {
            m = srcImage.copy();
            Set<Roi> rois = activeRois.stream().map(Pair::getFirst).collect(Collectors.toSet());
            m = imagePreprocessor.adjust(m, rois, imageAdjustMethod, true);
            if (dispRoisMode) {
                if (roiRotationMode) {
                    m = painter.paintUsCone(m, false, activeSnapshot.getUsConeX(), activeSnapshot.getUsConeY());
                }
                List<Triplet<Roi, RoiPaintType, String>> selected = new ArrayList<>();
                List<Triplet<Roi, RoiPaintType, String>> unselected = new ArrayList<>();
                int pid = activePatient.getId();
                int sid = activeSnapshot.getId();
                activeRois.stream().forEach(p -> {
                    Roi roi = p.getFirst();
                    RoiPaintType rpt = RoiPaintType.NONE;

                    String label = "";
                    if (selectedClassifier != null) {
                        label = clResults.getTexClResult(pid, sid, roi.getId(), selectedClassifier).stream()
                                .map(ClResult::resLabel)
                                .findAny().orElse("");
                    }

                    SampleCategory category = datamanager.getSamples().getCategory(pid, sid, roi.getId());
                    if (category != null) {
                        switch (datamanager.getSamples().getCategory(pid, sid, roi.getId())) {
                            case TRAIN_SET:
                                rpt = RoiPaintType.TRAIN; break;
                            case TEST_SET:
                                rpt = RoiPaintType.TEST; break;
                            case UNUSED:
                                rpt = RoiPaintType.UNUSED; break;
                            case MIXED:
                            case NONE:
                            default:
                                rpt = RoiPaintType.NONE;
                        }
                    }
                    
                    if (dispOutliersMode) {
                        int removedCount = outlierRemover.checkRemovedCount(pid, sid, roi.getId());
                        if (removedCount > 0) {
                            rpt = RoiPaintType.OUTLIER;
                            label = Integer.toString(removedCount);
                        }
                    }
                    Triplet<Roi, RoiPaintType, String> triplet = new Triplet<>(roi, rpt, label);
                    if (p.getSecond()) {
                        selected.add(triplet);
                    } else {
                        unselected.add(triplet);
                    }
                });
                m = painter.paintRois(m, false, selected, roiRotationMode, true);
                m = painter.paintRois(m, false, unselected, roiRotationMode, false);
            }
            cachedImage = m;
        }
        m = painter.paintRoi(m, true, new Triplet<>(newRoi, RoiPaintType.UNUSED, null), roiRotationMode, false);
        return GraphicsConverter.xImageToBufferedImage(m);
    }
    
    //==============================================================================================
    private void cancelSelection() {
        roiSelectionMode = false;
        activeRois.stream().forEach(p -> p.setSecond(false));
    }
    
    //==============================================================================================
    private void selectRois(int x, int y) {
        activeRois.stream().forEach(p -> {
            if (coordIsInsideRoi(x, y, p.getFirst())) {
                p.setSecond(!p.getSecond());
            }
        });
    }
    
    //==============================================================================================
    private boolean coordIsInsideRoi (int x, int y, Roi r) {
        int left = r.getCenterX() - r.getRadius();
        int right = r.getCenterX() + r.getRadius();
        int up = r.getCenterY() - r.getRadius();
        int down = r.getCenterY() + r.getRadius();
        return (x > left) && (x < right) && (y > up) && (y < down);
    }
    
    //==============================================================================================
    private void placePossibleRoi(int x, int y) {
        if (activeRoiRads.size() != 1 || activePatient == null || activeSnapshot == null) {
            newRoi = null;
            return;
        }
        int activeRoiRad = activeRoiRads.iterator().next();
        int left = x - activeRoiRad;
        int up = y - activeRoiRad;
        int width = 2 * activeRoiRad + 1;
        int height = 2 * activeRoiRad + 1;
        if ((left >= 0) && (up >= 0) && (left + width < imageWidth) && (up + height < imageHeight)) {
            newRoi = new Roi();
            newRoi.setCenterX(x);
            newRoi.setCenterY(y);
            newRoi.setRadius(activeRoiRad);
            double a = calculateRotation(newRoi, activeSnapshot.getUsConeX(), activeSnapshot.getUsConeY());
            newRoi.setAlpha(-a);
        } else {
            newRoi = null;
        }
    }
    
    //==============================================================================================
    private void createNewRoi() {
        if (newRoi == null || activePatient == null || activeSnapshot == null) {
            return;
        }
        try {
            Roi r = new Roi(newRoi);
            newRoi = null;
            r.setPatientId(activePatient.getId());
            r.setSnapshotId(activeSnapshot.getId());
            datamanager.getRoiCollection().addRoi(r);
            datamanager.getSamples().addCategory(activePatient.getId(), activeSnapshot.getId(), r.getId(), SampleCategory.UNUSED);
            activeRois.add(new Pair<>(r, false));
        } catch (Exception ex) {
            System.err.println(ex);
        }
    }
    
    //==============================================================================================
    private void deleteSelection() {
        int pid = activePatient.getId();
        int sid = activeSnapshot.getId();
        List<Integer> ridList = activeRois.stream()
                .filter(p -> p.getSecond() == true)
                .map(p -> p.getFirst().getId())
                .collect(Collectors.toList());
        if (!ridList.isEmpty()) {
            try {
                datamanager.getSamples().removeCategories(pid, sid, ridList);
            } catch (Exception ex) {
                Logger.getLogger(RoiLayoutEditor.class.getName()).log(Level.SEVERE, null, ex);
            }
            datamanager.getRoiCollection().removeRoiById(pid, sid, ridList);
        }
    }
    
    //==============================================================================================
    private void modifySelection(SampleCategory newCategory) {
        List<Pair<Integer, SampleCategory>> rList = activeRois.stream()
                .filter(p -> p.getSecond() == true)
                .map(p -> new Pair<>(p.getFirst().getId(), newCategory))
                .collect(Collectors.toList());
        try {
            datamanager.getSamples().setCategory(activePatient.getId(), activeSnapshot.getId(), rList);
        } catch (Exception ex) {
            Logger.getLogger(RoiLayoutEditor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //==============================================================================================
    private static double calculateRotation(Roi r, int x, int y) {
        if ((x == 0) && (y == 0)) {
            return 0;
        }
        double dx = r.getCenterX() - x;
        double dy = r.getCenterY() - y;
        double alpha = Math.atan(dx / dy);
        return alpha;
    }    
    
    //==============================================================================================
    @Override
    public void detectUsCones(boolean forceRecalculate) {
        XBackgroundTask task = new XBackgroundTask(controller -> {
            int numOfUsImages = datamanager.getPatients().toList().stream()
                    .mapToInt(p -> p.getUltrasound().toList().size())
                    .sum();
            int i = 0;
            System.out.println("Detecting US cones...");
            for (Patient p : datamanager.getPatients().toList()) {
                for (USnapshot s : p.getUltrasound().toList()) {
                    if (controller.isWorkerCancelled()) return null;
                    if ((s.getUsConeX() == 0) && (s.getUsConeY() == 0) || forceRecalculate) {
                        try {
                            XImage image = datamanager.loadImage(p, s, true);
                            UsConeDetector detector = new UsConeDetector();
                            Tuple2<Object, Object> detectRes = detector.apply(image);
                            Pair<Integer, Integer> usConeCenter = new Pair<>((int)detectRes._1(), (int)detectRes._2());
                            s.setUsConeX(usConeCenter.getFirst());
                            s.setUsConeY(usConeCenter.getSecond());
                        } catch (Exception ex) {
                            System.out.println("exception: " + ex);
                        }
                    }
                    i++;
                    int progress = 100 * i / numOfUsImages;
                    controller.updateProgress(progress, "Detecting US cones...", progress + "%");
                }
            }
            System.out.println("Detecting US cones completed");
            return null;
        });
        IBackgroundWizard wizard = backgroundFactory.createWizard(majorV);
        wizard.addTaskAndStart(task);
    }
    
    //==============================================================================================
    @Override
    public void registerXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        xbm.registerXEventListener(event_t, listener);
    }

    //==============================================================================================
    @Override
    public void unregisterXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        xbm.unregisterXEventListener(event_t, listener);
    }
    
    //==============================================================================================
    private void notifyXEventListeners(XEvent event) {
        xbm.notifyXEventListeners(event);
    }

    //==============================================================================================
    @Override
    public void eventHappened(XEvent event) {
        xlm.eventHappened(event);
    }
    
}
