package lfa.roilayouteditor;

import com.google.inject.AbstractModule;
import lfa.painting.Painting_Module;

//**************************************************************************************************
public class RoiLayout_Module extends AbstractModule {

    @Override
    protected void configure() {
        bind(IRoiLayoutEditor.class).to(RoiLayoutEditor.class);
        
        install(new Painting_Module());
    }
    
}
