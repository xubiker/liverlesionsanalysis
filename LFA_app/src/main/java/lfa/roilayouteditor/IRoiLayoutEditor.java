package lfa.roilayouteditor;

import base.events.XEventBroadcaster;
import base.events.XEventListener;
import lfa.cl.IPrimalOutlierRemover;
import lfa.data.Patient;
import lfa.data.USnapshot;
import xubiker.XImageTools.base.XImage;

import java.awt.image.BufferedImage;

//**************************************************************************************************
public interface IRoiLayoutEditor extends XEventListener, XEventBroadcaster {

    void update(Patient p, USnapshot s, XImage image);
    void update();

    void detectUsCones(boolean forceRecalculate);
}
