package lfa.gui.dialog;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

//**************************************************************************************************
class Dialog implements IDialog {

    //==============================================================================================
    @Override
    public String loadFile(String objectLabel, String extension) {
        JFileChooser fileopen = new JFileChooser();
        fileopen.setMultiSelectionEnabled(false);
        fileopen.setCurrentDirectory(new File("."));
        FileNameExtensionFilter filter = new FileNameExtensionFilter("." + extension + " files", extension);
        fileopen.setFileFilter(filter);
        int ret = fileopen.showDialog(null, "Load " + objectLabel);
        if (ret == JFileChooser.APPROVE_OPTION) {
            File f = fileopen.getSelectedFile();
            return f.getAbsolutePath();
        }
        return null;
    }                                                  

    //==============================================================================================
    @Override
    public String saveFile(String objectLabel, String extension) {
        JFileChooser filesave = new JFileChooser();
        filesave.setMultiSelectionEnabled(false);
        filesave.setCurrentDirectory(new File("."));
        FileNameExtensionFilter filter = new FileNameExtensionFilter("." + extension + " files", extension);
        filesave.setFileFilter(filter);
        int ret = filesave.showDialog(null, "Save " + objectLabel);
        if (ret == JFileChooser.APPROVE_OPTION) {
            File f = filesave.getSelectedFile();
            String path = f.getAbsolutePath();
            if (!path.endsWith("." + extension)) {
                path += "." + extension;
            }
            return path;
        }
        return null;
    }

    //==============================================================================================
    @Override
    public String chooseFolder() {
        JFileChooser chooser = new JFileChooser();
        chooser.setMultiSelectionEnabled(false);
        chooser.setCurrentDirectory(new File("."));
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);
        
        int ret = chooser.showDialog(null, "choose folder");
        if (ret == JFileChooser.APPROVE_OPTION) {
            File f = chooser.getSelectedFile();
            String path = f.getAbsolutePath();
            return path;
        }
        return null;
    }
    
}
