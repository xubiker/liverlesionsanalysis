package lfa.gui.dialog;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

public class DialogModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(IDialog.class).to(Dialog.class).in(Singleton.class);
    }
    
}
