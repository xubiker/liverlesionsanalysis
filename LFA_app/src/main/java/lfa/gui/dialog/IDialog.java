package lfa.gui.dialog;

//**************************************************************************************************
public interface IDialog {

    String loadFile(String objectLabel, String extension);
    
    String saveFile(String objectLabel, String extension);
    
    String chooseFolder();
    
}
