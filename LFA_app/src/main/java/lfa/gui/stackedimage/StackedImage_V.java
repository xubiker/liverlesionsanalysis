package lfa.gui.stackedimage;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

//**************************************************************************************************
public class StackedImage_V extends JPanel {
    
    private IStackedImage_VC vm;
    private final ScrollablePic picture;
    
    //==============================================================================================
    public StackedImage_V() {
        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        this.picture = new ScrollablePic();
        JScrollPane pictureScrollPane = new JScrollPane(picture);
        //pictureScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        add(pictureScrollPane);
    }
    
    //==============================================================================================
    public void setViewModel(IStackedImage_VC vm) {
        this.vm = vm;
        this.picture.setViewModel(vm);
    }

    //==============================================================================================
    public void updateImage(ImageIcon image) {
        
        picture.updateImage(image);
    }
    
}
