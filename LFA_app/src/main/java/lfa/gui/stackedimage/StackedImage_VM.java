package lfa.gui.stackedimage;

import base.collections.Pair;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.List;
import javax.swing.ImageIcon;

//**************************************************************************************************
public class StackedImage_VM implements IStackedImage_VM, IStackedImage_VC {

    private StackedImage_V view;
    private List<Pair<BufferedImage, String>> images;
    private int stackPos = 0;
    private boolean dispTextOnImages = true;
    
    //==============================================================================================
    @Override
    public void setView(StackedImage_V view) {
        this.view = view;
        view.setViewModel(this);
    }

    //==============================================================================================
    private void updateView(Pair<BufferedImage, String> i) {
        if (view != null) {
            ImageIcon ii = null;
            if (i != null) {
                ii = (dispTextOnImages)
                        ? new ImageIcon(placeText(i.getFirst(), i.getSecond()))
                        : new ImageIcon(i.getFirst());
            }
            view.updateImage(ii);
        }
    }
    
    //==============================================================================================
    @Override
    public void updateImages(List<Pair<BufferedImage, String>> images) {
        if (images == null || images.isEmpty()) {
            this.images.clear();
            this.stackPos = 0;
            updateView(null);
        } else {
            this.images = images;
            stackPos = (stackPos < images.size()) ? stackPos : 0;
            updateView(images.get(stackPos));
        }
    }

    //==============================================================================================
    @Override
    public void updateImage(Pair<BufferedImage, String> image, int stackPos) throws Exception {
        if (stackPos < 0 || stackPos >= images.size()) {
            throw new Exception("invalid input");
        }
        images.set(stackPos, image);
        if (this.stackPos == stackPos) {
            updateView(image);
        }
    }

    //==============================================================================================
    @Override
    public void switchStackPosition(int stackPos) throws Exception {
        if (stackPos < 0 || stackPos >= images.size()) {
            throw new Exception("invalid input");
        }
        updateView(images.get(stackPos));
    }

    //==============================================================================================
    @Override
    public void fv_switchStack() {
        stackPos = (stackPos + 1) % images.size();
        updateView(images.get(stackPos));
    }

    //==============================================================================================
    private BufferedImage placeText(BufferedImage i, String text) {
        BufferedImage img = new BufferedImage(i.getWidth(), i.getHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = img.createGraphics();
        g2d.drawImage(i, 0, 0, null);
        g2d.setPaint(Color.red);
        g2d.setFont(new Font("Serif", Font.BOLD, 20));
        FontMetrics fm = g2d.getFontMetrics();
        int x = 10;//img.getWidth() - fm.stringWidth(text) - 5;
        int y = fm.getHeight();
        g2d.drawString(text, x, y);
        g2d.dispose();
        return img;
    }

}
