package lfa.gui.stackedimage;

import com.google.inject.AbstractModule;

//**************************************************************************************************
public class StackedImage_Module extends AbstractModule {

    @Override
    protected void configure() {
        bind(IStackedImage_VM.class).to(StackedImage_VM.class);
    }
    
}
