package lfa.gui.stackedimage;

import base.collections.Pair;
import java.awt.image.BufferedImage;
import java.util.List;

//**************************************************************************************************
public interface IStackedImage_VM {

    void setView(StackedImage_V view);
    void updateImages(List<Pair<BufferedImage, String>> images);
    void updateImage(Pair<BufferedImage, String> image, int stackPos) throws Exception;
    void switchStackPosition(int stackPos) throws Exception;

}
