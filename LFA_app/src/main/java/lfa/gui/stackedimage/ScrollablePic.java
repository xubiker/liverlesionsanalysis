package lfa.gui.stackedimage;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

//**************************************************************************************************
public class ScrollablePic extends JLabel implements MouseListener {
    
    private IStackedImage_VC vm;
    private boolean dispPicture = false;
    
    //==============================================================================================
    public ScrollablePic() {
        setAutoscrolls(true);
        initializeListeners();
    }
    
    //==============================================================================================
    private void initializeListeners() {
        addMouseListener(this);
    }

    //==============================================================================================
    public void setViewModel(IStackedImage_VC vm) {
        this.vm = vm;
    }
    
    //==============================================================================================
    public void updateImage (ImageIcon icon) {
        if (icon == null) {
            super.setIcon(null);
            dispPicture = false;
            setText("No image");
            setHorizontalAlignment(CENTER);
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            //setOpaque(false);
            setForeground(Color.black);
        } else {
            super.setIcon(icon);
            setText("");
            setHorizontalAlignment(LEFT);
            setVerticalAlignment(TOP);
            //setAlignmentX(LEFT_ALIGNMENT);
            //setAlignmentY(LEFT_ALIGNMENT);
            setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
            dispPicture = true;
        }
    }

    //==============================================================================================
    @Override
    public Dimension getPreferredSize() {
        if (!dispPicture) {
            return new Dimension(100, 100);
        } else {
            return super.getPreferredSize();
        }
    }

    //==============================================================================================
    @Override
    public void mouseClicked(MouseEvent e) {}

    //==============================================================================================
    @Override
    public void mousePressed(MouseEvent e) {}

    //==============================================================================================
    @Override
    public void mouseReleased(MouseEvent e) {
        if (e.getButton() == 3) {
            vm.fv_switchStack();
        }
    }

    //==============================================================================================
    @Override
    public void mouseEntered(MouseEvent e) {}

    //==============================================================================================
    @Override
    public void mouseExited(MouseEvent e) {}

}