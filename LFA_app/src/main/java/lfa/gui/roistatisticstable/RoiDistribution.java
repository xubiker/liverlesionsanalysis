package lfa.gui.roistatisticstable;

import lfa.data.SampleCategory;

//**************************************************************************************************
public class RoiDistribution {

    private final int[] d;
    
    //==============================================================================================
    public RoiDistribution() {
        int classes = SampleCategory.values().length;
        d = new int[classes];
    }
    
    //==============================================================================================
    public void change(SampleCategory cc, int count) {
        int index = SampleCategory.valueOf(cc.name()).ordinal();
        d[index] += count;
    }
    
    //==============================================================================================
    @Override
    public String toString() {
        String s = "";
        int classesCnt = d.length;
        int sum = 0;
        for (int i = 0; i < classesCnt - 1; i++) {
            s += d[i] + " | ";
            sum += d[i];
        }
        s += d[classesCnt-1] + " || ";
        sum += d[classesCnt-1];
        s += sum;
        return s;
    }
    
}
