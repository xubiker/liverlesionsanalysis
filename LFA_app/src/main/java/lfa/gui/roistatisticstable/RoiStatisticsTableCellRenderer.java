package lfa.gui.roistatisticstable;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

//**************************************************************************************************
public class RoiStatisticsTableCellRenderer extends DefaultTableCellRenderer {

    private final Color color;
    private final Font font;
    
    public RoiStatisticsTableCellRenderer(Color color, Font font) {
        this.color = color;
        this.font = font;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component cr = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        ((JLabel)cr).setHorizontalAlignment(SwingConstants.CENTER);
        if (row == 0 || column == 0) {
            ((JLabel)cr).setFont(font);
            cr.setBackground(color);
        } else {
            cr.setBackground(table.getBackground());
        }
        return cr;
    }

}
