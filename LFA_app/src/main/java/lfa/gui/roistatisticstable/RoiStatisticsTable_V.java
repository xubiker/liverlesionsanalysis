package lfa.gui.roistatisticstable;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Collections;
import javax.swing.table.DefaultTableModel;

//**************************************************************************************************
public class RoiStatisticsTable_V extends javax.swing.JTable {

    private IRoiStatisticsTable_VM vm;
    private final int defRowSize = 3, defColSize = 4;

    //==============================================================================================
    public RoiStatisticsTable_V() {
        DefaultTableModel model = (DefaultTableModel) getModel();
        model.setRowCount(defRowSize);
        model.setColumnCount(defColSize);
//        setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        setEnabled(false);
        setFocusable(false);
        setTableHeader(null);
        setPreferredScrollableViewportSize(new Dimension(200,100));
    }
    
    //==============================================================================================
    public void setVM(IRoiStatisticsTable_VM vm) {
        this.vm = vm;
    }

    //==============================================================================================
    public void setHeaders(String[] rowHeaders, String[] colHeaders) {
        DefaultTableModel model = (DefaultTableModel) getModel();
        model.setRowCount(rowHeaders.length + 1);
        model.setColumnCount(colHeaders.length + 1);
        for (int i = 0; i < rowHeaders.length; i++) {
            model.setValueAt(rowHeaders[i], i + 1, 0);
        }
        for (int i = 0; i < colHeaders.length; i++) {
            model.setValueAt(colHeaders[i], 0, i + 1);
        }
        RoiStatisticsTableCellRenderer cellRenderer = new RoiStatisticsTableCellRenderer(
                new Color(200, 255, 255),
                new Font("Consolas Unicode MS", Font.BOLD, 14)
        );
        Collections.list(getColumnModel().getColumns()).forEach(c -> c.setCellRenderer(cellRenderer));

        Dimension size = getParent().getSize();
        setRowHeight(size.height / getRowCount());
    }
    
    //==============================================================================================
    public void updateDistributionData(int row, int column, RoiDistribution d) {
        DefaultTableModel model = (DefaultTableModel) getModel();
        model.setValueAt(d.toString(), row + 1, column + 1);
    }

    //==============================================================================================
    public void updateDistributionData(RoiDistribution[][] dd) {
        DefaultTableModel model = (DefaultTableModel) getModel();
        int cols = model.getColumnCount();
        int rows = model.getRowCount();
        if (dd != null) {
            if (dd.length != (rows - 1) || dd[0].length != (cols - 1)) {
                System.out.println("OOOOOOps");
                return;
            }
            for (int i = 0; i < rows - 1; i++) {
                for (int j = 0; j < cols - 1; j++) {
                    model.setValueAt(dd[i][j], i + 1, j + 1);
                }
            }
        } else {
            for (int i = 0; i < rows - 1; i++) {
                for (int j = 0; j < cols - 1; j++) {
                    model.setValueAt(new RoiDistribution(), i + 1, j + 1);
                }
            }
        }
    }
}
