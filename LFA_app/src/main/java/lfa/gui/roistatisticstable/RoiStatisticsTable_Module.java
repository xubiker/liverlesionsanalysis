package lfa.gui.roistatisticstable;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

//**************************************************************************************************
public class RoiStatisticsTable_Module extends AbstractModule {

    @Override
    protected void configure() {
        bind(IRoiStatisticsTable_VM.class).to(RoiStatisticsTable_VM.class).in(Singleton.class);
    }
    
}
