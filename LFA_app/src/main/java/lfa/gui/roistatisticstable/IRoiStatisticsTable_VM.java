package lfa.gui.roistatisticstable;

import base.events.XEventListener;

//**************************************************************************************************
public interface IRoiStatisticsTable_VM extends XEventListener {
    
    void setView(RoiStatisticsTable_V view);
    
}
