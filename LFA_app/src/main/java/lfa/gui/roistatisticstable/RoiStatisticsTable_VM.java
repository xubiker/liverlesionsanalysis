package lfa.gui.roistatisticstable;

import lfa.diagnosis.FIBROSIS$;
import lfa.roiStatistics.RoiStatisticsChangedXEvent;
import base.events.XEvent;
import base.events.XEventFactory;
import base.events.XEventListenerManager;
import com.google.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lfa.data.LiverCutType;
import lfa.events.RoiXEvent;
import lfa.roiStatistics.IRoiStatisticsManager;

//**************************************************************************************************
class RoiStatisticsTable_VM implements IRoiStatisticsTable_VM {

    private final Map<Integer, RoiDistribution[][]> rois;
    private final List<Integer> activeRads = new ArrayList<>();
    private RoiStatisticsTable_V view;

    private final IRoiStatisticsManager roiStatisticsManager;

    private final XEventListenerManager xlm = XEventFactory.createListener();
    
    //==============================================================================================
    @Inject
    public RoiStatisticsTable_VM (
            IRoiStatisticsManager roiStatisticsManager
    ) {
        rois = new HashMap<>();
        this.roiStatisticsManager = roiStatisticsManager;
    }
    
    //==============================================================================================
    @Override
    public void setView(RoiStatisticsTable_V view) {
        this.view = view;
        String[] fibrosisLabels = FIBROSIS$.MODULE$.primaryScore().possibleLabeledValues();
        view.setHeaders(LiverCutType.stringValues(), fibrosisLabels);
    }
    
    //==============================================================================================
    private void updateView(int fIdx, LiverCutType c, RoiDistribution d) {
        int rowIdx = c.getIntValue();
        int colIdx = fIdx;
        view.updateDistributionData(rowIdx, colIdx, d);
    }
    
    //==============================================================================================
    private void fullUpdate() {
        if (activeRads.size() == 1) {
            int rad = activeRads.iterator().next();
            RoiDistribution[][] allStatistics = roiStatisticsManager.getAllStatistics(rad);
            rois.put(rad, allStatistics);
            if (view != null) {
                view.updateDistributionData(allStatistics);
            }
        }
    }

    //==============================================================================================
    @Override
    public void eventHappened(XEvent event) {
        if (event.getClass() == RoiXEvent.ActiveRadChanged.class) {
            activeRads.clear();
            activeRads.addAll(((RoiXEvent.ActiveRadChanged)event).getActiveRad());
            fullUpdate();
        } else if (event.getClass() == RoiStatisticsChangedXEvent.class) {
            RoiStatisticsChangedXEvent e = (RoiStatisticsChangedXEvent) event;
            if (view != null && activeRads.contains(e.getRadius())) {
                updateView(e.getDiseaseStage(), e.getCutType(), e.getDistribution());
            }
        }
    }
    
}
