package lfa.gui.enumselector;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import lfa.image.ImageAdjustMethod;
import lfa.data.SampleCategory;
import lfa.events.SampleCategoryModifiedXEvent;
import lfa.events.ImageAdjustXEvent;

//**************************************************************************************************
public class EnumSelector_Module extends AbstractModule {

    //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    static class SampleSelectorVM extends EnumSelector_VM<SampleCategory> {
        @Override
        public void itemSelected(SampleCategory item) {
            super.itemSelected(item);
            notifyXEventListeners(new SampleCategoryModifiedXEvent(this, item));
        }
    }
    
    //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    static class ImageAdjustSelectorVM extends EnumSelector_VM<ImageAdjustMethod> {
        @Override
        public void itemSelected(ImageAdjustMethod item) {
            super.itemSelected(item);
            notifyXEventListeners(new ImageAdjustXEvent(this, item));
        }
    }

    //==============================================================================================
    @Override
    protected void configure() {
        bind(new TypeLiteral<IEnumSelector_VM<SampleCategory>>(){}).to(SampleSelectorVM.class);
        bind(new TypeLiteral<IEnumSelector_VM<ImageAdjustMethod>>(){}).to(ImageAdjustSelectorVM.class);
    }
    
}
