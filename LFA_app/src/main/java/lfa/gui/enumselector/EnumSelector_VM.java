package lfa.gui.enumselector;

import base.events.XEvent;
import base.events.XEventBroadcasterManager;
import base.events.XEventFactory;
import base.events.XEventListener;
import base.events.XEventListenerManager;

//**************************************************************************************************
abstract class EnumSelector_VM<T extends Enum<T>> implements IEnumSelector_VM<T>, IEnumSelector_VC<T> {

    private EnumSelector_V view;
    
    private final XEventListenerManager xlm = XEventFactory.createListener();
    private final XEventBroadcasterManager xbm = XEventFactory.createBroadcaster();

    //==============================================================================================
    public EnumSelector_VM() {
    }
    
    //==============================================================================================
    @Override
    public void setView(EnumSelector_V view) {
        this.view = view;
    }

    //==============================================================================================
    @Override
    public void itemSelected(T item) {
    }

    //==============================================================================================
    @Override
    public void setItem(T item) {
        if (view == null) return;
        if (item == null) return; 
        view.setSelection(item);
    }
    
    //==============================================================================================
    @Override
    public void eventHappened(XEvent event) {
        xlm.eventHappened(event);
    }

    //==============================================================================================
    @Override
    public void registerXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        xbm.registerXEventListener(event_t, listener);
    }

    //==============================================================================================
    @Override
    public void unregisterXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        xbm.unregisterXEventListener(event_t, listener);
    }
    
    //==============================================================================================
    protected void notifyXEventListeners(XEvent e) {
        xbm.notifyXEventListeners(e);
    }
}
