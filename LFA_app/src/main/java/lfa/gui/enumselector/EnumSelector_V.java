package lfa.gui.enumselector;

import javax.swing.JComboBox;

//**************************************************************************************************
public final class EnumSelector_V<T extends Enum<T>> extends JComboBox<T> {
    
    private IEnumSelector_VC vm;
    private T oldVal;
    private boolean forceSelect = false;
    
    public EnumSelector_V(Class<T> type) {
        super(type.getEnumConstants());
        oldVal = (T) getSelectedItem();
        super.addItemListener(e -> itemListener());
    }
    
    //==============================================================================================
    public EnumSelector_V() {
        super();
    }
    
    //==============================================================================================
    public void setViewModel(IEnumSelector_VC vm) {
        this.vm = vm;
    }
    
    //==============================================================================================
    public void setSelection(T value) {
        forceSelect = true;
        setSelectedItem(value);
        forceSelect = false;
    }
    
    //==============================================================================================
    private void itemListener() {
        if (vm != null) {
            T newVal = (T) getSelectedItem();
            if (newVal != oldVal && !forceSelect) {
                forceSelect = false;
                vm.itemSelected(newVal);
            }
            oldVal = newVal;
        }
    }
    
}
