package lfa.gui.enumselector;

//**************************************************************************************************
public interface IEnumSelector_VC<T extends Enum<T>> {

    public void itemSelected(T value);

}
