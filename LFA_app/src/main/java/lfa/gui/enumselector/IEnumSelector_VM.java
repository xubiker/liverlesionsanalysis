package lfa.gui.enumselector;

import base.events.XEventBroadcaster;
import base.events.XEventListener;

//**************************************************************************************************
public interface IEnumSelector_VM<T extends Enum<T>> extends XEventListener, XEventBroadcaster {
    
    public void setView(EnumSelector_V<T> view);
    
    public void setItem(T value);
    
}
