package lfa.gui.evalresult;

import base.collections.Pair;
import scala.tools.reflect.Eval;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListModel;

//**************************************************************************************************
public class EvalResult_V extends javax.swing.JFrame {

    private final ArrayList<Pair<String, String>> data = new ArrayList<>();
    
    public EvalResult_V() {
        initComponents();
        setLocationRelativeTo(null);
        classifiersList.addListSelectionListener(e -> classifierSelected());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        classifiersList = new javax.swing.JList();
        jScrollPane1 = new javax.swing.JScrollPane();
        EvalResultArea = new javax.swing.JTextArea();

        setTitle("Evaluation Results");
        setAlwaysOnTop(false);
        setName("Evaluation Results"); // NOI18N
        setType(java.awt.Window.Type.POPUP);

        jScrollPane2.setViewportView(classifiersList);

        jSplitPane1.setLeftComponent(jScrollPane2);

        EvalResultArea.setColumns(20);
        EvalResultArea.setRows(5);
        EvalResultArea.setFont(new Font(EvalResultArea.getFont().getName(), EvalResultArea.getFont().getStyle(), EvalResultArea.getFont().getSize() * 2));
        jScrollPane1.setViewportView(EvalResultArea);

        jSplitPane1.setRightComponent(jScrollPane1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 700, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 700, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void setData(List<Pair<String, String>> data) {
        this.data.clear();
        this.data.addAll(data);
        DefaultListModel listModel = new DefaultListModel();
        data.stream().forEach(p -> listModel.addElement(p.getFirst()));
        classifiersList.setModel(listModel);
        if (!data.isEmpty()) {
            classifiersList.setSelectedIndex(0);
        }
    }
    
    private void classifierSelected() {
        int idx = classifiersList.getSelectedIndex();
        if (idx != -1) {
            String evaluationResult = data.get(idx).getSecond();
            EvalResultArea.setText(evaluationResult);
        }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea EvalResultArea;
    private javax.swing.JList classifiersList;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane1;
    // End of variables declaration//GEN-END:variables
}
