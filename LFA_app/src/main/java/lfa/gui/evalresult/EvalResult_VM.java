package lfa.gui.evalresult;

import base.collections.Pair;
import java.util.ArrayList;
import java.util.List;

//**************************************************************************************************
class EvalResult_VM implements IEvalResult_VM {

    private final List<Pair<String, String>> list = new ArrayList<>();
    EvalResult_V view = new EvalResult_V();
    
    //==============================================================================================
    @Override
    public void addEvalResultData(List<Pair<String, String>> evalResult) {
        list.addAll(evalResult);
    }

    //==============================================================================================
    @Override
    public void clearAllData() {
        list.clear();
    }

    //==============================================================================================
    @Override
    public void display() {
        view.setData(list);
        view.setVisible(true);
    }
    
}
