package lfa.gui.evalresult;

import base.collections.Pair;
import java.util.List;

//**************************************************************************************************
public interface IEvalResult_VM {
    
    void addEvalResultData(List<Pair<String, String>> evalResult);
    void clearAllData();
    void display();
    
}
