package lfa.gui.evalresult;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

//**************************************************************************************************
public class EvalResult_Module extends AbstractModule {

    @Override
    protected void configure() {
        bind(IEvalResult_VM.class).to(EvalResult_VM.class).in(Singleton.class);
    }
    
}
