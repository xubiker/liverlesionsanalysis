package lfa.gui.advancedtable;

import base.collections.Pair;
import java.awt.Color;
import java.awt.Component;
import java.util.Map;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;

//**************************************************************************************************
class AdvancedTableCellRenderer extends DefaultTableCellRenderer {

    private final Map<Pair<Integer, Integer>, Color> cellColorMap;
    private final boolean makeSelectionDarker;

    //==============================================================================================
    public AdvancedTableCellRenderer(Map<Pair<Integer, Integer>, Color> cellColorMap, boolean makeSelectionDarker) {
        this.cellColorMap = cellColorMap;
        this.makeSelectionDarker = makeSelectionDarker;
    }

    //==============================================================================================
    private Color getOppositeColor(Color c) {
        return new Color(
                255 - c.getRed(),
                255 - c.getGreen(),
                255 - c.getBlue()
        );
    }
    
    //==============================================================================================
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component cr = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        Color color = cellColorMap.get(new Pair<>(row, column));
        JTableHeader header = table.getTableHeader();
        if (isSelected) {
            if (color != null) {
                cr.setBackground(makeSelectionDarker ? color.darker() : color);
                //cr.setForeground(getOppositeColor(color));
            } else {
                cr.setForeground(table.getSelectionForeground());
                cr.setBackground(table.getSelectionBackground());
            }
        } else {
            if (color != null) {
                cr.setBackground(color);
            } else {
                cr.setBackground(table.getBackground());
                cr.setForeground(table.getForeground());
            }
        }
        return cr;
    }
}
