package lfa.gui.advancedtable;

import base.events.XEventBroadcaster;
import java.awt.Color;
import java.util.Collection;
import java.util.Map;
import lfa.colortags.IColorTagManager;

//**************************************************************************************************
public interface IAdvancedTable_VM extends XEventBroadcaster {
 
    void setView(AdvancedTable_V view);

    void setColorTagManager(IColorTagManager colorTagManager);
    
    void setItems(Collection<? extends IAdvancedTableSuitable> items, boolean saveLastPosition);
    
    void updateItem(int position, IAdvancedTableSuitable item);
    void updateSelectedItem(IAdvancedTableSuitable item);
    void updateCellColorMap();
    
    void setHeadersFromInstance(IAdvancedTableSuitable item);
    void setColorMap(Map<Integer, Color> colorMap);
    
    void moveSelection(int shift);
    
}
