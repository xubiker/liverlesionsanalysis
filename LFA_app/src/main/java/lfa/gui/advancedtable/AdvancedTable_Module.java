package lfa.gui.advancedtable;

import com.google.inject.AbstractModule;

//**************************************************************************************************
public class AdvancedTable_Module extends AbstractModule {

    @Override
    protected void configure() {
        bind(IAdvancedTable_VM.class).to(AdvancedTable_VM.class);
    }
    
}
