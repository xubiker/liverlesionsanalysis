package lfa.gui.advancedtable;

import base.events.XEvent;
import base.events.XEventBroadcaster;

//**************************************************************************************************
public class TableItemSelectedXEvent extends XEvent {
    
    private final IAdvancedTableSuitable selectedItem;
    
    //==============================================================================================
    public TableItemSelectedXEvent(XEventBroadcaster sender, IAdvancedTableSuitable selectedItem) {
        super(sender);
        this.selectedItem = selectedItem;
    }
    
    //==============================================================================================
    public IAdvancedTableSuitable getSelectedItem() {
        return selectedItem;
    }
    
}
