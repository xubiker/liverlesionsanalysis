package lfa.gui.advancedtable;

import java.util.List;

//**************************************************************************************************
public interface IAdvancedTableSuitable {
    
    List<String> getLabels();
    List<String> getValues();
    List<Integer> getColorTags();
    
}
