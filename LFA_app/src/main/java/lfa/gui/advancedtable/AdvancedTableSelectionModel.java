package lfa.gui.advancedtable;

import javax.swing.DefaultListSelectionModel;
import javax.swing.ListSelectionModel;

//**************************************************************************************************
class AdvancedTableSelectionModel extends DefaultListSelectionModel {

    public AdvancedTableSelectionModel () {
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    @Override
    public void clearSelection() {
    }

    @Override
    public void removeSelectionInterval(int index0, int index1) {
    }

}