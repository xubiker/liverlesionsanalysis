package lfa.gui.advancedtable;

import base.collections.Pair;

import java.awt.*;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

//**************************************************************************************************
public class AdvancedTable_V extends JTable {

    private IAdvancedTable_VC vm;
    private Map<Pair<Integer, Integer>, Color> cellColorMap = new HashMap<>();
    private boolean makeCellDarkerOnSelection;
    private int lastSelectedRow = -1;
    
    //==============================================================================================
    public AdvancedTable_V() {
        initialize();
    }

    //==============================================================================================
    private void initialize() {
        this.setSelectionModel(new AdvancedTableSelectionModel());
        DefaultTableModel m = (DefaultTableModel) super.getModel();
        m.setRowCount(0);
        this.setFont(new Font("Arial", Font.PLAIN, 12));
    }
    
    //==============================================================================================
    public void setViewModel(IAdvancedTable_VC vm) {
        this.vm = vm;
    }
    
    //==============================================================================================
    void setHeadersFromInstance(IAdvancedTableSuitable item) {
        if (item == null) return;
        DefaultTableModel m = (DefaultTableModel) super.getModel();
        List<String> labels = item.getLabels();
        m.setColumnCount(labels.size());
        m.setColumnIdentifiers(labels.toArray());
    }
    
    //==============================================================================================
    void setContent(List<IAdvancedTableSuitable> items, boolean saveLastSelectPos) throws Exception {
        DefaultTableModel m = (DefaultTableModel) super.getModel();
        if (items == null || items.isEmpty()) {
            m.setRowCount(0);
            return;
        }
        List<String> labels = items.iterator().next().getLabels();
        int columnsCnt = labels.size();
        m.setColumnCount(columnsCnt);
        m.setRowCount(items.size());
        m.setColumnIdentifiers(labels.toArray());
        for (int i = 0; i < items.size(); i++) {
            IAdvancedTableSuitable item = items.get(i);
            List<String> values = item.getValues();
            if (values.size() != columnsCnt) {
                throw new Exception("AdvancedTable_V. Inappropiate num of columns in item " + i);
            }
            for (int j = 0; j < values.size(); j++) {
                String value = values.get(j);
                m.setValueAt(value, i, j);
            }
        }
        updateCellRenderer();
        if (saveLastSelectPos && lastSelectedRow != -1) {
            int newRow = lastSelectedRow;
            lastSelectedRow = -1;
            setRowSelectionInterval(newRow, newRow);
        } else {
            lastSelectedRow = -1;
            setRowSelectionInterval(0, 0);
        }
    }
    
    //==============================================================================================
    void updateContent(int pos, IAdvancedTableSuitable item) throws Exception {
        DefaultTableModel m = (DefaultTableModel) super.getModel();
        if (item == null || pos < 0 || pos >= getRowCount()) {
            throw new Exception("AdvancedTable_V. Update content - invalid params");
        }
        List<String> values = item.getValues();
        if (values.size() != getColumnCount()) {
            throw new Exception("AvdancedTable_V. Inappropiate num of columns");
        }
        for (int j = 0; j < values.size(); j++) {
            String value = values.get(j);
            m.setValueAt(value, pos, j);
        }
        updateCellRenderer();
    }
    
    //==============================================================================================
    public void updateCellColorMap(Map<Pair<Integer, Integer>, Color> cellColorMap, boolean makeCellDarkerOnSelection) {
        this.makeCellDarkerOnSelection = makeCellDarkerOnSelection;
        this.cellColorMap = cellColorMap;
        updateCellRenderer();
    }
    
    //==============================================================================================
    public void updateCellRenderer() {
        AdvancedTableCellRenderer renderer = new AdvancedTableCellRenderer(cellColorMap, makeCellDarkerOnSelection);
        for (Enumeration<TableColumn> e = getColumnModel().getColumns(); e.hasMoreElements();) {
            e.nextElement().setCellRenderer(renderer);
        }
        updateUI();
    }
    
    //==============================================================================================
    public void moveSelection(int shift) {
        int curRow = getSelectedRow();
        if (curRow == -1) return;
        int newRow = curRow + shift;
        newRow = Math.max(newRow, 0);
        newRow = Math.min(newRow, getRowCount()-1);
        setRowSelectionInterval(newRow, newRow);
        scrollRectToVisible(new Rectangle(getCellRect(newRow, 0, true)));
    }
    
    //==============================================================================================
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (!e.getValueIsAdjusting()) {
            if (getSelectedRow() != lastSelectedRow) {
                lastSelectedRow = getSelectedRow();
                super.valueChanged(e);
                if (vm != null && lastSelectedRow != -1) {
                    vm.vc_itemSelected(lastSelectedRow);
                }
            }
        }
    }
    
}