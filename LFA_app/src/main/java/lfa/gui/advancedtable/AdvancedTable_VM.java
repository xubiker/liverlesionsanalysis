package lfa.gui.advancedtable;

import base.collections.Pair;
import base.events.XEvent;
import base.events.XEventBroadcasterManager;
import base.events.XEventFactory;
import base.events.XEventListener;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import lfa.colortags.IColorTagManager;

//**************************************************************************************************
class AdvancedTable_VM implements IAdvancedTable_VM, IAdvancedTable_VC {

    private AdvancedTable_V view;
    private IColorTagManager colorTagManager;
    private final List<IAdvancedTableSuitable> tableItems = new ArrayList<>();
    private final Map<Integer, Color> colorMap = new HashMap<>();
    private final XEventBroadcasterManager xbm = XEventFactory.createBroadcaster();

    //==============================================================================================
    @Override
    public void setView(AdvancedTable_V view) {
        this.view = view;
    }

    //==============================================================================================
    @Override
    public void setColorTagManager(IColorTagManager colorTagManager) {
        this.colorTagManager = colorTagManager;
    }
    
    //==============================================================================================
    @Override
    public void setHeadersFromInstance(IAdvancedTableSuitable item) {
        if (view != null) {
            view.setHeadersFromInstance(item);
        }
    }
    
    //==============================================================================================
    @Override
    public void setItems(Collection<? extends IAdvancedTableSuitable> items, boolean saveLastPosition) {
        tableItems.clear();
        tableItems.addAll(items);
        updateCellColorMap();
        if (view != null) {
            try {
                view.setContent(tableItems, saveLastPosition);
            } catch (Exception ex) {
                Logger.getLogger(AdvancedTable_VM.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    //==============================================================================================
    @Override
    public void updateItem(int position, IAdvancedTableSuitable item) {
        try {
            tableItems.set(position, item);
            updateCellColorMap();
            if (view != null) {
                view.updateContent(position, item);
            }
        } catch (Exception ex) {
            System.err.println("AdvancedTable_VM: " + ex);
        }
    }

    //==============================================================================================
    @Override
    public void updateSelectedItem(IAdvancedTableSuitable item) {
        int selectedRow = view.getSelectedRow();
        if (selectedRow != -1) {
            updateItem(selectedRow, item);
        }
    }
    
    //==============================================================================================
    @Override
    public void updateCellColorMap() {
        Map<Pair<Integer, Integer>, Color> cellColorMap = new HashMap<>();
        for (int i = 0; i < tableItems.size(); i++) {
            IAdvancedTableSuitable item = tableItems.get(i);
            List<Integer> colorTags = colorTagManager.getColorTags(item);
            for (int j = 0; j < colorTags.size(); j++) {
                Integer tag = colorTags.get(j);
                Color color = colorMap.get(tag);
                cellColorMap.put(new Pair<>(i,j), color);
            }
        }
        if (view != null) {
            view.updateCellColorMap(cellColorMap, true);
        }
    }
    
    //==============================================================================================
    @Override
    public void setColorMap(Map<Integer, Color> colorMap) {
        this.colorMap.clear();
        this.colorMap.putAll(colorMap);
        updateCellColorMap();
    }
    
    //==============================================================================================
    @Override
    public void moveSelection(int shift) {
        if (view == null) return;
        view.moveSelection(shift);
    }
    
    //==============================================================================================
    @Override
    public void vc_itemSelected(int index) {
        IAdvancedTableSuitable selectedItem = tableItems.get(index);
        notifyXEventListeners(new TableItemSelectedXEvent(this, selectedItem));
    }
    
    //==============================================================================================
    @Override
    public void registerXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        xbm.registerXEventListener(event_t, listener);
    }

    //==============================================================================================
    @Override
    public void unregisterXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        xbm.unregisterXEventListener(event_t, listener);
    }

    //==============================================================================================
    private void notifyXEventListeners(XEvent e) {
        xbm.notifyXEventListeners(e);
    }
    
}
