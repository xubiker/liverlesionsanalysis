package lfa.gui.detailpanel;

//**************************************************************************************************
public interface IDetailInfoProvider {

    String getDetailInfo();

}
