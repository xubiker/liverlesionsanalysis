package lfa.gui.detailpanel;

import com.google.inject.AbstractModule;

//**************************************************************************************************
public class DetailPanel_Module extends AbstractModule {

    @Override
    protected void configure() {
        bind(IDetailPanel_VM.class).to(DetailPanel_VM.class);
    }
    
}
