package lfa.gui.detailpanel;

//**************************************************************************************************
public interface IDetailPanel_VM {
    
    void setView(DetailPanel_V view);
    
    void updateData(IDetailInfoProvider detailInfoProvider);
    
}
