package lfa.gui.detailpanel;

//**************************************************************************************************
class DetailPanel_VM implements IDetailPanel_VM {

    private DetailPanel_V view;
    
    //==============================================================================================
    @Override
    public void setView(DetailPanel_V view) {
        this.view = view;
    }

    //==============================================================================================
    @Override
    public void updateData(IDetailInfoProvider detailInfoProvider) {
        if (view == null) return;
        if (detailInfoProvider == null) {
            view.updateData("");
        } else {
            view.updateData(detailInfoProvider.getDetailInfo());
        }
    }
    
    
}
