package lfa.gui.scrollableimage;

import base.events.XEventBroadcaster;
import base.events.XEventListener;
import java.awt.image.BufferedImage;

//**************************************************************************************************
public interface IScrollableImage_VM extends XEventListener, XEventBroadcaster {

    void setView(ScrollableImage_V view);

    void updateImage(BufferedImage image);
    
    void setProcessMouseEventsFlg(boolean processMouseEventsFlg);
    
}
