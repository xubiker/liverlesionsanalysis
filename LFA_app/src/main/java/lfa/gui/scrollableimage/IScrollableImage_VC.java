package lfa.gui.scrollableimage;

//**************************************************************************************************
public interface IScrollableImage_VC {
 
    void fv_mouseDragged(int x, int y);
    void fv_mouseMoved(int x, int y);
    void fv_mousePressed(int x, int y);
    void fv_mouseReleased(int x, int y);
    
}
