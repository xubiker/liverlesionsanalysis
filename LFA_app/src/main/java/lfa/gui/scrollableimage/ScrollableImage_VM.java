package lfa.gui.scrollableimage;

import base.events.XEvent;
import base.events.XEventBroadcasterManager;
import base.events.XEventFactory;
import base.events.XEventListener;
import base.events.XEventListenerManager;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;
import lfa.events.MouseXEvent;
import lfa.events.PreviewUpdateXEvent;

//**************************************************************************************************
class ScrollableImage_VM implements IScrollableImage_VM, IScrollableImage_VC {

    private ScrollableImage_V view;
    private final XEventListenerManager xlm = XEventFactory.createListener();
    private final XEventBroadcasterManager xbm = XEventFactory.createBroadcaster();

    //==============================================================================================
    public ScrollableImage_VM() {
        xlm.bindXEventHandler(PreviewUpdateXEvent.class,
                e -> previewUpdateXEventHandler((PreviewUpdateXEvent) e));
    }
    
    //==============================================================================================
    @Override
    public void setView(ScrollableImage_V view) {
        this.view = view;
    }

    //==============================================================================================
    @Override
    public void setProcessMouseEventsFlg(boolean processMouseEventsFlg) {
        if (view != null) {
            view.setProcessMouseEventsFlg(processMouseEventsFlg);
        }
    }
    
    //==============================================================================================
    private void previewUpdateXEventHandler(PreviewUpdateXEvent event) {
        updateImage(event.getImage());
    }
    
    //==============================================================================================
    @Override
    public void updateImage(BufferedImage image) {
        if (view != null) {
            if (image == null) {
                view.updateImage(null);
            } else {
                view.updateImage(new ImageIcon(image));
            }
        }
    }

    //==============================================================================================
    @Override
    public void fv_mouseDragged(int x, int y) {
        notifyXEventListeners(new MouseXEvent(this, x, y, MouseXEvent.MouseAction.DRAGGED));
    }

    //==============================================================================================
    @Override
    public void fv_mouseMoved(int x, int y) {
        notifyXEventListeners(new MouseXEvent(this, x, y, MouseXEvent.MouseAction.MOVED));
    }

    //==============================================================================================
    @Override
    public void fv_mousePressed(int x, int y) {
        notifyXEventListeners(new MouseXEvent(this, x, y, MouseXEvent.MouseAction.PRESSED));
    }

    //==============================================================================================
    @Override
    public void fv_mouseReleased(int x, int y) {
        notifyXEventListeners(new MouseXEvent(this, x, y, MouseXEvent.MouseAction.RELEASED));
    }

    //==============================================================================================
    @Override
    public void registerXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        xbm.registerXEventListener(event_t, listener);
    }

    //==============================================================================================
    @Override
    public void unregisterXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        xbm.unregisterXEventListener(event_t, listener);
    }
    
    //==============================================================================================
    private void notifyXEventListeners(XEvent event) {
        xbm.notifyXEventListeners(event);
    }

    //==============================================================================================
    @Override
    public void eventHappened(XEvent event) {
        xlm.eventHappened(event);
    }

}
