package lfa.gui.scrollableimage;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

//**************************************************************************************************
public class ScrollableImage_V extends JPanel {
    
    private IScrollableImage_VC vm;
    private boolean processMouseEvents = true;
    private final ScrollablePic picture;
    
    //==============================================================================================
    public ScrollableImage_V() {
        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        this.picture = new ScrollablePic(processMouseEvents);
        JScrollPane pictureScrollPane = new JScrollPane(picture);
        add(pictureScrollPane);
    }
    
    //==============================================================================================
    public void setViewModel(IScrollableImage_VC vm) {
        this.vm = vm;
        this.picture.setViewModel(vm);
    }

    //==============================================================================================
    public void setProcessMouseEventsFlg(boolean processMouseEventsFlg) {
        this.processMouseEvents = processMouseEventsFlg;
        this.picture.setProcessMouseEventsFlg(processMouseEventsFlg);
    }
    
    //==============================================================================================
    public void updateImage(ImageIcon image) {
        picture.updateImage(image);
    }
    
}
