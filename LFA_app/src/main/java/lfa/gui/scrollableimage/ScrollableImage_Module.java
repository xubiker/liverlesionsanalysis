package lfa.gui.scrollableimage;

import com.google.inject.AbstractModule;

//**************************************************************************************************
public class ScrollableImage_Module extends AbstractModule {

    @Override
    protected void configure() {
        bind(IScrollableImage_VM.class).to(ScrollableImage_VM.class);
    }
    
}
