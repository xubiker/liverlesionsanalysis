package lfa.gui.scrollableimage;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

//**************************************************************************************************
public class ScrollablePic extends JLabel implements MouseMotionListener, MouseListener {
    
    private IScrollableImage_VC vm;
    
    private boolean dispPicture = false;
    private boolean processMouseEvents;
    private ImageIcon icon;
    
    //==============================================================================================
    public ScrollablePic(boolean registerMouseEvents) {
        this.processMouseEvents = registerMouseEvents;
        setAutoscrolls(true);
        initializeListeners();
    }
    
    //==============================================================================================
    private void initializeListeners() {
        addMouseMotionListener(this);
        addMouseListener(this);
    }

    //==============================================================================================
    public void setViewModel(IScrollableImage_VC vm) {
        this.vm = vm;
    }
    
    //==============================================================================================
    public void setProcessMouseEventsFlg(boolean processMouseEventsFlg) {
        this.processMouseEvents = processMouseEventsFlg;
    }
    
    //==============================================================================================
    public void updateImage (ImageIcon icon) {
        this.icon = icon;
        if (icon == null) {
            super.setIcon(null);
            dispPicture = false;
            setText("No image");
            setHorizontalAlignment(CENTER);
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            //setOpaque(false);
            setForeground(Color.black);
        } else {
            super.setIcon(icon);
            setText("");
            setHorizontalAlignment(CENTER);
            setVerticalAlignment(CENTER);
            setAlignmentX(CENTER_ALIGNMENT);
            setAlignmentY(CENTER_ALIGNMENT);
            setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
            dispPicture = true;
        }
    }

    //==============================================================================================
    @Override
    public Dimension getPreferredSize() {
        if (!dispPicture) {
            return new Dimension(100, 100);
        } else {
            return super.getPreferredSize();
        }
    }

    //==============================================================================================
    @Override
    public void mouseDragged(MouseEvent e) {
        if (!processMouseEvents || !dispPicture || vm == null) {
            return;
        }
        scrollRectToVisible(new Rectangle(e.getX(), e.getY(), 1, 1));
        int[] coords = recalcXYposition(e.getX(), e.getY());
        vm.fv_mouseDragged(coords[0], coords[1]);
    }

    //==============================================================================================
    @Override
    public void mouseMoved(MouseEvent e) {
        if (!processMouseEvents || !dispPicture || vm == null) {
            return;
        }
        int[] coords = recalcXYposition(e.getX(), e.getY());
        vm.fv_mouseMoved(coords[0], coords[1]);
    }

    //==============================================================================================
    @Override
    public void mouseClicked(MouseEvent e) { }

    //==============================================================================================
    @Override
    public void mousePressed(MouseEvent e) {
        if (!processMouseEvents || !dispPicture || vm == null) {
            return;
        }
        int[] coords = recalcXYposition(e.getX(), e.getY());
        vm.fv_mousePressed(coords[0], coords[1]);
    }

    //==============================================================================================
    @Override
    public void mouseReleased(MouseEvent e) {
        if (!processMouseEvents || !dispPicture || vm == null) {
            return;
        }
        int[] coords = recalcXYposition(e.getX(), e.getY());
        vm.fv_mouseReleased(coords[0], coords[1]);
    }

    //==============================================================================================
    @Override
    public void mouseEntered(MouseEvent e) { }

    //==============================================================================================
    @Override
    public void mouseExited(MouseEvent e) { }

    //==============================================================================================
    private int[] recalcXYposition (int x, int y) {
        int[] res = new int[2];
        int borderW = (getWidth() - icon.getIconWidth()) / 2;
        int borderH = (getHeight() - icon.getIconHeight()) / 2;
        borderH = (borderH < 0) ? 0 : borderH;
        borderW = (borderW < 0) ? 0 : borderW;
        res[0] = x - borderW;
        res[1] = y - borderH;
        return res;
    }

}