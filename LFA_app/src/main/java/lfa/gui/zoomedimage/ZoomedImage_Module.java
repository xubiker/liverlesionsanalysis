package lfa.gui.zoomedimage;

import com.google.inject.AbstractModule;

//**************************************************************************************************
public class ZoomedImage_Module extends AbstractModule {

    @Override
    protected void configure() {
        bind(IZoomedImage_VM.class).to(ZoomedImage_VM.class);
    }
    
}
