package lfa.gui.zoomedimage;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

//**************************************************************************************************
public class ZoomedImage_V extends javax.swing.JPanel {

    private BufferedImage image;
    
    //==============================================================================================
    public ZoomedImage_V() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        setBorder(javax.swing.BorderFactory.createEtchedBorder());
        setCursor(new java.awt.Cursor(java.awt.Cursor.CROSSHAIR_CURSOR));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 396, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 296, Short.MAX_VALUE)
        );
    }// </editor-fold>                        

    //==============================================================================================
    public void updateImage(BufferedImage image) {
        this.image = image;
        repaint();
    }
    
    //==============================================================================================
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, null);
    }

    // Variables declaration - do not modify                     
    // End of variables declaration                   
}
