package lfa.gui.zoomedimage;

import base.GraphicsConverter;
import base.events.XEvent;
import base.events.XEventFactory;
import base.events.XEventListenerManager;
import java.awt.image.BufferedImage;
import static java.lang.Integer.*;
import lfa.events.ZoomedImageUpdateXEvent;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;

//**************************************************************************************************
class ZoomedImage_VM implements IZoomedImage_VM {

    private ZoomedImage_V view;
    private double zoomFactor;
    private int ocv_inter;
    private int lastX = -1, lastY = -1;
    private final XEventListenerManager xlm = XEventFactory.createListener();
    
    //==============================================================================================
    public ZoomedImage_VM() {
        this.zoomFactor = 1;
        this.ocv_inter = Imgproc.INTER_CUBIC;
        
        xlm.bindXEventHandler(ZoomedImageUpdateXEvent.class,
                e -> zoomedImageUpdateXEventHandler((ZoomedImageUpdateXEvent) e));
    }
    
    //==============================================================================================
    @Override
    public void setView(ZoomedImage_V view) {
        this.view = view;
    }

    //==============================================================================================
    @Override
    public void setZoomFactor(double zoomFactor) {
        this.zoomFactor = zoomFactor;
    }

    //==============================================================================================
    @Override
    public void setInterpolation(Interpolation interpolation) {
        switch(interpolation) {
            case LINEAR:
                ocv_inter = Imgproc.INTER_LINEAR;
                break;
            case CUBIC:
                ocv_inter = Imgproc.INTER_CUBIC;
                break;
            case LANCZOS4:
                ocv_inter = Imgproc.INTER_LANCZOS4;
                break;
            default:
                throw new AssertionError(interpolation.name());
        }
    }

    //==============================================================================================
    @Override
    public void updateImage(BufferedImage image, int x, int y) {
        if (view == null) return;
        if (image == null || x == -1 || y == -1) {
            view.updateImage(null);
            return;
        }
        Mat m = GraphicsConverter.bufferedImageToMat(image);
        int radW = view.getWidth() / 2;
        int radH = view.getHeight() / 2;
        int srcRadW = (int) (1. / zoomFactor * radW);
        int srcRadH = (int) (1. / zoomFactor * radH);
        int left = max(0, x - srcRadW);
        int right = min(m.width(), x + srcRadW);
        int up = max(0, y - srcRadH);
        int down = min(m.height(), y + srcRadH);
        if ((right - left > 0) && (down - up > 0)) {
            Rect roi = new Rect(left, up, right - left, down - up);
            Mat submat = m.submat(roi);
            Mat submatR = new Mat();
            Imgproc.resize(submat, submatR, new Size(2 * radW, 2 * radH), 0, 0, ocv_inter);
            BufferedImage img = GraphicsConverter.matToBufferedImage(submatR);
            view.updateImage(img);
        }
    }
    
    //==============================================================================================
    private void zoomedImageUpdateXEventHandler(ZoomedImageUpdateXEvent e) {
        if (e.getX() == -1 || e.getY() == -1) {
            updateImage(e.getImage(), lastX, lastY);
        } else {
            updateImage(e.getImage(), e.getX(), e.getY());
            this.lastX = e.getX();
            this.lastY = e.getY();
        }
    }

    //==============================================================================================
    @Override
    public void eventHappened(XEvent event) {
        xlm.eventHappened(event);
    }
    
}
