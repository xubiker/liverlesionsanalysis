package lfa.gui.zoomedimage;

import base.events.XEventListener;
import java.awt.image.BufferedImage;

//**************************************************************************************************
public interface IZoomedImage_VM extends XEventListener {

    public enum Interpolation {
        LINEAR,
        CUBIC,
        LANCZOS4
    }
    
    void setView(ZoomedImage_V view);
    
    void setZoomFactor(double zoomFactor);
    void setInterpolation(Interpolation interpolation);
    void updateImage(BufferedImage image, int x, int y);

}
