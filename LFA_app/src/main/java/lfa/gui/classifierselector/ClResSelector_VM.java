package lfa.gui.classifierselector;

import base.collections.Pair;
import base.events.XEvent;
import base.events.XEventBroadcasterManager;
import base.events.XEventFactory;
import base.events.XEventListener;
import base.events.XEventListenerManager;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import lfa.cl.ClassificationXEvent;

//**************************************************************************************************
class ClResSelector_VM implements IClResSelector_VM, IClResSelector_VC {

    private ClResSelector_V view;

    private final List<Pair<String, String>> availableCls = new ArrayList<>();
    
    private final XEventListenerManager xlm = XEventFactory.createListener();
    private final XEventBroadcasterManager xbm = XEventFactory.createBroadcaster();
    
    //==============================================================================================
    public ClResSelector_VM() {
        xlm.bindXEventHandler(ClassificationXEvent.ClassifiersUpdated.class,
                e -> classifiersUpdatedXEventHandler((ClassificationXEvent.ClassifiersUpdated) e));
    }
    
    //==============================================================================================
    @Override
    public void setView(ClResSelector_V view) {
        this.view = view;
    }

    //==============================================================================================
    @Override
    public void infoTagSelected(String value) {
        String selectedClassifier =availableCls.stream()
                .filter(clp -> clp.getFirst().equals(value))
                .map(Pair::getSecond)
                .findAny().orElse("");
        notifyXEventListeners(new ClassificationXEvent.ClassifierSelected(this, selectedClassifier));
    }

    //==============================================================================================
    @Override
    public void updatePossibleClassifiers(List<String> classifiers) {
        availableCls.clear();
        availableCls.addAll(
        IntStream.range(0, classifiers.size())
                .mapToObj(i -> {
                    String shortString = i + ": " + classifiers.get(i).substring(0, 11) + "..";
                    return new Pair<>(shortString, classifiers.get(i));
                }).collect(Collectors.toList())
        );
        view.setItems(availableCls.stream().map(Pair::getFirst).collect(Collectors.toList()));
    }
    
    //==============================================================================================
    private void classifiersUpdatedXEventHandler(ClassificationXEvent.ClassifiersUpdated e) {
        updatePossibleClassifiers(e.getClassifiers());
    }

    //==============================================================================================
    @Override
    public void registerXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        xbm.registerXEventListener(event_t, listener);
    }

    //==============================================================================================
    @Override
    public void unregisterXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        xbm.unregisterXEventListener(event_t, listener);
    }
    
    //==============================================================================================
    private void notifyXEventListeners(XEvent e) {
        xbm.notifyXEventListeners(e);
    }

    //==============================================================================================
    @Override
    public void eventHappened(XEvent event) {
        xlm.eventHappened(event);
    }

}
