package lfa.gui.classifierselector;

import com.google.inject.AbstractModule;

//**************************************************************************************************
public class ClResSelector_Module extends AbstractModule {

    @Override
    protected void configure() {
        bind(IClResSelector_VM.class).to(ClResSelector_VM.class);
    }
    
}
