package lfa.gui.classifierselector;

import base.events.XEventBroadcaster;
import base.events.XEventListener;
import java.util.List;

//**************************************************************************************************
public interface IClResSelector_VM extends XEventBroadcaster, XEventListener {
    
    public void setView(ClResSelector_V view);

    public void updatePossibleClassifiers(List<String> classifiers);
    
}
