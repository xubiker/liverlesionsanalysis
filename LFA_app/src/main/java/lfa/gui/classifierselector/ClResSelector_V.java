package lfa.gui.classifierselector;

import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

//**************************************************************************************************
public class ClResSelector_V extends JComboBox<String> {
    
    private IClResSelector_VC vm;
    private String oldVal;
    private static final String defaultVal = "-none-";
    
    //==============================================================================================
    public ClResSelector_V() {
        super(new String[]{defaultVal});
        super.addItemListener(e -> itemListener());
    }
    
    //==============================================================================================
    public void setItems(List<String> content) {
        content.add(0, defaultVal);
        String[] items = content.toArray(new String[content.size()]);
        super.setModel(new DefaultComboBoxModel<>(items));
        vm.infoTagSelected(defaultVal);
    }
    
    //==============================================================================================
    public void setViewModel(IClResSelector_VC vm) {
        this.vm = vm;
    }
    
    //==============================================================================================
    private void itemListener() {
        if (vm != null) {
            String newVal = (String) getSelectedItem();
            if (!newVal.equals(oldVal)) {
                vm.infoTagSelected(newVal);
            }
            oldVal = newVal;
        }
    }
    
}
