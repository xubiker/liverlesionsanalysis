
package lfa.keyprocessor;

import base.events.XEventBroadcaster;
import javax.swing.ActionMap;
import javax.swing.InputMap;

//**************************************************************************************************
public interface IKeyProcessor extends XEventBroadcaster {

    void setActionKeyMaps(InputMap inputMap, ActionMap actionMap);
    
}
