package lfa.keyprocessor;

import com.google.inject.AbstractModule;

//==================================================================================================
public class KeyProcessor_Module extends AbstractModule {

    @Override
    protected void configure() {
        bind(IKeyProcessor.class).to(KeyProcessor.class);
    }
    
}
