package lfa.keyprocessor;

import base.events.XEvent;
import base.events.XEventBroadcasterManager;
import base.events.XEventFactory;
import base.events.XEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.KeyStroke;
import lfa.data.SampleCategory;
import lfa.data.LiverCutType;
import lfa.events.PatientSwitchXEvent;
import lfa.events.RoiXEvent;
import lfa.events.SnapshotModifiedXEvent;
import lfa.events.SnapshotSwitchXEvent;

//**************************************************************************************************
public class KeyProcessor implements IKeyProcessor{

    private InputMap inputMap;
    private ActionMap actionMap;
    private final Map<String, Boolean> isPressed = new HashMap<>();
    private final XEventBroadcasterManager xbm = XEventFactory.createBroadcaster();

    //**********************************************************************************************
    public class VK_Action extends AbstractAction {
        private final String type;
        private final Consumer<String> action;
        
        public VK_Action(String type, Consumer<String> action) {
            this.type = type;
            this.action = action;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            action.accept(type);
        }
    }

    //==============================================================================================
    public KeyProcessor() { }
    
    //==============================================================================================
    @Override
    public void setActionKeyMaps(InputMap inputMap, ActionMap actionMap) {
        this.inputMap = inputMap;
        this.actionMap = actionMap;
        configure();
    }
    
    //==============================================================================================
    private void addKeyStroke(KeyStroke keyStroke, String label, boolean pressed) {
        inputMap.put(keyStroke, keyStroke.hashCode());
        if (pressed) {
            actionMap.put(keyStroke.hashCode(), new VK_Action(label, str -> pressed(str)));
        } else {
            actionMap.put(keyStroke.hashCode(), new VK_Action(label, str -> released(str)));
        }
        isPressed.put(label, false);
    }
    
    //==============================================================================================
    private void configure() {
        addKeyStroke(KeyStroke.getKeyStroke("pressed UP"), "UP", true);
        addKeyStroke(KeyStroke.getKeyStroke("pressed DOWN"), "DOWN", true);
        addKeyStroke(KeyStroke.getKeyStroke("pressed LEFT"), "LEFT", true);
        addKeyStroke(KeyStroke.getKeyStroke("pressed RIGHT"), "RIGHT", true);
       
        addKeyStroke(KeyStroke.getKeyStroke("pressed PAGE_UP"), "PAGE_UP", true);
        addKeyStroke(KeyStroke.getKeyStroke("pressed PAGE_DOWN"), "PAGE_DOWN", true);

        addKeyStroke(KeyStroke.getKeyStroke("pressed HOME"), "HOME", true);
        addKeyStroke(KeyStroke.getKeyStroke("pressed END"), "END", true);

        addKeyStroke(KeyStroke.getKeyStroke("pressed C"), "C", true);
        addKeyStroke(KeyStroke.getKeyStroke("released C"), "C", false);
        addKeyStroke(KeyStroke.getKeyStroke("pressed DELETE"), "DELETE", true);
        addKeyStroke(KeyStroke.getKeyStroke("released DELETE"), "DELETE", false);
        addKeyStroke(KeyStroke.getKeyStroke("pressed ESCAPE"), "ESCAPE", true);
        addKeyStroke(KeyStroke.getKeyStroke("released ESCAPE"), "ESCAPE", false);
        addKeyStroke(KeyStroke.getKeyStroke("pressed 0"), "0", true);
        addKeyStroke(KeyStroke.getKeyStroke("released 0"), "0", false);
        addKeyStroke(KeyStroke.getKeyStroke("pressed 1"), "1", true);
        addKeyStroke(KeyStroke.getKeyStroke("released 1"), "1", false);
        addKeyStroke(KeyStroke.getKeyStroke("pressed 2"), "2", true);
        addKeyStroke(KeyStroke.getKeyStroke("released 2"), "2", false);
        addKeyStroke(KeyStroke.getKeyStroke("pressed 3"), "3", true);
        addKeyStroke(KeyStroke.getKeyStroke("released 3"), "3", false);
        addKeyStroke(KeyStroke.getKeyStroke("pressed 4"), "4", true);
        addKeyStroke(KeyStroke.getKeyStroke("released 4"), "4", false);
        addKeyStroke(KeyStroke.getKeyStroke("control pressed A"), "CTRL+A", true);
        addKeyStroke(KeyStroke.getKeyStroke("control released A"), "CTRL+A", false);
        addKeyStroke(KeyStroke.getKeyStroke(KeyEvent.VK_SHIFT, KeyEvent.SHIFT_DOWN_MASK, false), "SHIFT", true);
        addKeyStroke(KeyStroke.getKeyStroke(KeyEvent.VK_SHIFT, 0, true), "SHIFT", false);
    }

    //==============================================================================================
    private void pressed(String key) {
        Boolean wasPressed = isPressed.get(key);
        isPressed.put(key, true);
        switch(key) {
            case "C":
                break;
            case "UP":
                notifyXEventListeners(new PatientSwitchXEvent(this, PatientSwitchXEvent.SwitchDirection.PREV));
                break;
            case "DOWN":
                notifyXEventListeners(new PatientSwitchXEvent(this, PatientSwitchXEvent.SwitchDirection.NEXT));
                break;
            case "LEFT":
                notifyXEventListeners(new SnapshotSwitchXEvent(this, SnapshotSwitchXEvent.SwitchDirection.PREV));
                break;
            case "RIGHT":
                notifyXEventListeners(new SnapshotSwitchXEvent(this, SnapshotSwitchXEvent.SwitchDirection.NEXT));
                break;
            case "HOME":
                notifyXEventListeners(new PatientSwitchXEvent(this, PatientSwitchXEvent.SwitchDirection.FIRST));
                break;
            case "END":
                notifyXEventListeners(new PatientSwitchXEvent(this, PatientSwitchXEvent.SwitchDirection.LAST));
                break;
            case "PAGE_DOWN":
                notifyXEventListeners(new PatientSwitchXEvent(this, PatientSwitchXEvent.SwitchDirection.NEXT_5));
                break;
            case "PAGE_UP":
                notifyXEventListeners(new PatientSwitchXEvent(this, PatientSwitchXEvent.SwitchDirection.PREV_5));
                break;

            case "0":
                if (isPressed.get("C")) {
                    notifyXEventListeners(new SnapshotModifiedXEvent.CutTypeChanged(this, LiverCutType.UNUSED));
                } else {
                    notifyXEventListeners(new RoiXEvent.SelectionSampleChanged(this, SampleCategory.UNUSED));
                }
                break;
            case "1":
                if (isPressed.get("C")) {
                    notifyXEventListeners(new SnapshotModifiedXEvent.CutTypeChanged(this, LiverCutType.CUT1));
                } else {
                    notifyXEventListeners(new RoiXEvent.SelectionSampleChanged(this, SampleCategory.TRAIN_SET));
                }
                break;
            case "2":
                if (isPressed.get("C")) {
                    notifyXEventListeners(new SnapshotModifiedXEvent.CutTypeChanged(this, LiverCutType.CUT2));
                } else {
                    notifyXEventListeners(new RoiXEvent.SelectionSampleChanged(this, SampleCategory.TEST_SET));
                }
                break;
            case "3":
                if (isPressed.get("C")) {
                    notifyXEventListeners(new SnapshotModifiedXEvent.CutTypeChanged(this, LiverCutType.CUT3));
                }
                break;
            case "4":
                if (isPressed.get("C")) {
                    notifyXEventListeners(new SnapshotModifiedXEvent.CutTypeChanged(this, LiverCutType.CUT4));
                }
                break;
            case "CTRL+A":
                if (Objects.equals(wasPressed, Boolean.FALSE)) {
                    notifyXEventListeners(new RoiXEvent.SelectAll(this));
                }
                break;
            case "ESCAPE":
                if (Objects.equals(wasPressed, Boolean.FALSE)) {
                    notifyXEventListeners(new RoiXEvent.ResetSelection(this));
                }
                break;
            case "SHIFT":
                if (Objects.equals(wasPressed, Boolean.FALSE)) {
                    notifyXEventListeners(new RoiXEvent.SelectionModeChanged(this, true));
                }
                break;
            case "DELETE":
                if (Objects.equals(wasPressed, Boolean.FALSE)) {
                    notifyXEventListeners(new RoiXEvent.DeleteSelection(this));
                }
                break;
        }
        
    }
    
    //==============================================================================================
    private void released(String key) {
        isPressed.put(key, false);
        if ("SHIFT".equals(key)) {
            notifyXEventListeners(new RoiXEvent.SelectionModeChanged(this, false));
        }
    }

    //==============================================================================================
    @Override
    public void registerXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        xbm.registerXEventListener(event_t, listener);
    }

    //==============================================================================================
    @Override
    public void unregisterXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        xbm.unregisterXEventListener(event_t, listener);
    }

    //==============================================================================================
    private void notifyXEventListeners(XEvent event) {
        xbm.notifyXEventListeners(event);
    }

}
