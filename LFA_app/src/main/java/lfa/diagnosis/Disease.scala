package lfa.diagnosis

sealed trait Disease {
  def possibleScores: List[Score]
  def primaryScore: Score = possibleScores.head
}

case object FIBROSIS extends Disease {
  def possibleScores: List[Score] = List[Score](METAVIR, METAVIR_RED1, METAVIR_RED2, ISHAK, CIRRH_SCORE, HEALTH_SCORE, HEALTH_SCORE_2)
}

case object STEATOSIS extends Disease {
  def possibleScores: List[Score] = List[Score](STEATOSIS_SCORE)
  override def toString = "STEATOSIS"
}

object Disease {
  def apply(s: String): Option[Disease] = {
    s match {
      case "FIBROSIS" => Some(FIBROSIS)
      case "STEATOSIS" => Some(STEATOSIS)
      case _ => None
    }
  }
}
