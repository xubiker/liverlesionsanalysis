package lfa.diagnosis;

import lfa.data.Patient;
import lfa.measurement.TexID;

import java.util.Optional;

public interface IDiagnosisDeterminator {

    Optional<String> getLabel(Patient p, Disease disease, Score score);
    Optional<String> getLabel(int pid, Disease disease, Score score);
    Optional<String> getLabel(TexID tid, Disease disease, Score score);
    String[] getAllLabels(Score score);
}
