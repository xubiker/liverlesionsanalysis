package lfa.diagnosis

import java.util.Optional

import com.google.inject.Inject
import lfa.data.Patient
import lfa.datamanager.IDataManager
import lfa.measurement.TexID

class DiagnosisDeterminator @Inject()(dataManager: IDataManager) extends IDiagnosisDeterminator {

  override def getLabel(p: Patient, disease: Disease, score: Score): Optional[String] = {
    val diagnosis = p.getMultiDiagnosis.getDiagnosis(disease)
    if (diagnosis.isEmpty) return Optional.empty()
    val sc = diagnosis.get.jToScore(score)
    if (sc.isEmpty)
      Optional.empty()
    else
      Optional.of(sc.get.toShortString)
  }

  override def getLabel(pid: Int, disease: Disease, score: Score): Optional[String] = {
    val p = dataManager.getPatients.toMap.get(pid)
    if (p == null) Optional.empty() else getLabel(p, disease, score)
  }

  override def getLabel(tid: TexID, disease: Disease, score: Score): Optional[String] =
    getLabel(tid.pid, disease, score)

  override def getAllLabels(score: Score): Array[String] = score.possibleLabeledValues
}
