package lfa.diagnosis

import java.util.function.Function

import lfa.data.packed.MultiDiagnosis_packed
import lfa.packing.{IPackable, IUnpackable}

import scala.collection.JavaConverters._

case class MultiDiagnosis(diagnosis: Diagnosis*) extends IPackable {

  import DiagnosisOps._

  def this (diagnosis: List[Diagnosis]) = this(diagnosis:_*)

  // specially for java-world
  def this (diagnosis: java.util.Collection[Diagnosis]) = this(diagnosis.asScala.toSeq:_*)

  private val diagnosisMap: Map[(Disease, Score), Int] = diagnosis
    .flatMap(d => d.disease.possibleScores.map(s => d.toScore(s)))
    .flatten.map(d => (d.disease, d.score) -> d.stage).toMap

  def getDiagnosis(disease: Disease, score: Score): Option[Diagnosis] = {
    diagnosisMap.get((disease, score)).flatMap(stage => Some(Diagnosis(disease, score, stage)))
  }

  def getDiagnosis(disease: Disease): Option[Diagnosis] = {
    getDiagnosis(disease, disease.primaryScore)
  }

  def getStage(disease: Disease, score: Score): Option[Int] = {
    diagnosisMap.get((disease, score))
  }

  def getStageWithEq(disease: Disease, score: Score): Int = {
    assert(disease.possibleScores.contains(score))
    diagnosisMap.getOrElse((disease, score), score.minStage)
  }

  def getPrimaryDiagnoses: java.lang.Iterable[Diagnosis] = {
    diagnosisMap.filter(_._1._2.isPrimary).toSeq.map(q => Diagnosis(q._1._1, q._1._2, q._2)).asJava
  }

  override def getPackAction: Function[IPackable, IUnpackable] = new Function[IPackable, IUnpackable] {
    override def apply(input: IPackable) = new MultiDiagnosis_packed(input.asInstanceOf[MultiDiagnosis])
  }

  override def getPackT = classOf[MultiDiagnosis_packed]

}
