package lfa.diagnosis;

import com.google.inject.AbstractModule;

public class DiagnosisDeterminatorModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(IDiagnosisDeterminator.class).to(DiagnosisDeterminator.class).asEagerSingleton();
    }

}
