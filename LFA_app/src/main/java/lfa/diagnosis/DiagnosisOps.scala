package lfa.diagnosis

object DiagnosisOps {

  implicit class DiagnosisOps(d: Diagnosis) {

    private val METAVIR_TO_CIRHH_SCORE = Map(
      0 -> 0, 1 -> 0, 2 -> 0, 3 -> 1, 4 -> 1
    )
    private val METAVIR_TO_HEALTH_SCORE = Map(
      0 -> 0, 1 -> 1, 2 -> 1, 3 -> 1, 4 -> 1
    )
    private val METAVIR_TO_HEALTH_SCORE_2 = Map(
      0 -> 0, 1 -> 0, 2 -> 1, 3 -> 1, 4 -> 1
    )
    private val METAVIR_TO_METAVIR_RED1 = Map(
      0 -> 0, 1 -> 1, 2 -> 2, 3 -> 3, 4 -> 3
    )
    private val METAVIR_TO_METAVIR_RED2 = Map(
      0 -> 0, 1 -> 0, 2 -> 1, 3 -> 2, 4 -> 2
    )
    val ISHAK_TO_METAVIR = Map(
      0 -> 0, 1 -> 0, 2 -> 0, 3 -> 0, 4 -> 0, 5 -> 0, 6 -> 0
    )

    private val mapOfMaps = Map(
      (METAVIR, METAVIR_RED1) -> METAVIR_TO_METAVIR_RED1,
      (METAVIR, METAVIR_RED2) -> METAVIR_TO_METAVIR_RED2,
      (METAVIR, CIRRH_SCORE) -> METAVIR_TO_CIRHH_SCORE,
      (METAVIR, HEALTH_SCORE) -> METAVIR_TO_HEALTH_SCORE,
      (METAVIR, HEALTH_SCORE_2) -> METAVIR_TO_HEALTH_SCORE_2,
      (ISHAK, METAVIR) -> ISHAK_TO_METAVIR
    )

    def toScore(otherScore: Score): Option[Diagnosis] = {
      if (!d.disease.possibleScores.contains(otherScore)) return None
      if (otherScore == d.score) return Some(d)
      val stageMap: Map[Int, Int] = mapOfMaps.getOrElse(d.score -> otherScore, Map.empty[Int, Int])
      stageMap.get(d.stage).map(Diagnosis(d.disease, otherScore, _))
    }

    def toScore(otherScore: String): Option[Diagnosis] = {
      val otherScoreParsed = Score(otherScore)
      if (otherScoreParsed.nonEmpty) toScore(otherScoreParsed.get) else None
    }

    def isHealthy: Boolean = {
      d.stage <= 0
    }
  }

}
