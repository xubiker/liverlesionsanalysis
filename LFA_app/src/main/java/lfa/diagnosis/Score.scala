package lfa.diagnosis

abstract sealed class Score(disease: Disease, val label: String, val minStage: Int, val maxStage: Int, val isPrimary: Boolean) extends Product with Serializable {
  def possibleLabeledValues: Array[String] = {
    (minStage to maxStage).map(label + _).toArray
  }
}

case object METAVIR extends Score(FIBROSIS, "F", 0, 4, true)

case object METAVIR_RED1 extends Score(FIBROSIS, "R", 0, 3, false)

case object METAVIR_RED2 extends Score(FIBROSIS, "Q", 0, 2, false)

case object ISHAK extends Score(FIBROSIS, "I", 0, 6, false)

case object CIRRH_SCORE extends Score(FIBROSIS, "C", 0, 1, true)

case object HEALTH_SCORE extends Score(FIBROSIS, "H", 0, 1, false)

case object HEALTH_SCORE_2 extends Score(FIBROSIS, "L", 0, 1, false)

case object STEATOSIS_SCORE extends Score(STEATOSIS, "S", 0, 1, true)


object Score {
  def apply(name: String): Option[Score] = {
    name match {
      case "METAVIR" => Some(METAVIR)
      case "METAVIR_RED1" => Some(METAVIR_RED1)
      case "ISHAK" => Some(ISHAK)
      case "CIRRH_SCORE" => Some(CIRRH_SCORE)
      case "HEALTH_SCORE" => Some(HEALTH_SCORE)
      case "HEALTH_SCORE_2" => Some(HEALTH_SCORE_2)
      case _ => None
    }
  }
}
