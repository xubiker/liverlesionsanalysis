package lfa.diagnosis

import java.util.function.Function

import lfa.data.packed.Diagnosis_packed
import lfa.packing.{IPackable, IUnpackable}

case class Diagnosis(disease: Disease, score: Score, stage: Int) extends IPackable {

  assert(stage >= score.minStage && stage <= score.maxStage)

  override def toString: String = s"$disease: ${score.label}$stage"
  def toShortString: String = s"${score.label}$stage"

  // specially for java-world
  def jToScore(otherSCore: Score): Option[Diagnosis] = {
    import DiagnosisOps._
    this.toScore(otherSCore)
  }

  override def getPackAction: Function[IPackable, IUnpackable] = new Function[IPackable, IUnpackable] {
    override def apply(input: IPackable): IUnpackable = new Diagnosis_packed(input.asInstanceOf[Diagnosis])
  }

  override def getPackT = classOf[Diagnosis_packed]
}

case object Diagnosis {
  def apply(disease: String, score: String, stage: Int): Option[Diagnosis] = {
    val d = Disease.apply(disease)
    val s = Score.apply(score)
    if (d.isEmpty || s.isEmpty) None else Some(Diagnosis(d.get, s.get, stage))
  }
}

