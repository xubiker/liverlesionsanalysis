package lfa.roiStatistics;

import base.collections.Pair;
import base.collections.Triplet;
import base.events.XEvent;
import base.events.XEventBroadcasterManager;
import base.events.XEventExtension;
import base.events.XEventFactory;
import base.events.XEventListener;
import base.events.XEventListenerManager;
import com.google.inject.Inject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lfa.diagnosis.FIBROSIS$;
import lfa.data.*;
import lfa.datamanager.IDataManager;
import lfa.datamanager.DataManagerXEvent;
import lfa.events.CollectionUpdateXEvent;
import lfa.gui.roistatisticstable.RoiDistribution;

//**************************************************************************************************
class RoiStatisticsManager implements IRoiStatisticsManager {

    private final IDataManager dataManager;
    
    private final int cutCnt, fibrosisCnt;
    private final Map<Integer, RoiDistribution[][]> map = new HashMap<>();
    private final XEventListenerManager xlm = XEventFactory.createListener();
    private final XEventBroadcasterManager xbm = XEventFactory.createBroadcaster();
    
    //==============================================================================================
    @Inject
    public RoiStatisticsManager(IDataManager dataManager) {
        this.dataManager = dataManager;
        cutCnt = LiverCutType.values().length;
        fibrosisCnt = FIBROSIS$.MODULE$.primaryScore().possibleLabeledValues().length;
        
        xlm.bindXEventHandler(DataManagerXEvent.class, e -> dataManagerXEventHandler((DataManagerXEvent) e));
        xlm.bindXEventHandler(CollectionUpdateXEvent.class, e -> collectionUpdateXEventHandler((XEventExtension) e));
    }
    
    //==============================================================================================
    private void dataManagerXEventHandler(DataManagerXEvent e) {
        switch(e.getUpdatedDataType()) {
            case PATIENTS:
                break;
            case ROIS:
                updateRoiStatistics();
                break;
            case SAMPLES:
                dataManager.getSamples().unregisterXEventListener(CollectionUpdateXEvent.class, this);
                dataManager.getSamples().registerXEventListener(CollectionUpdateXEvent.class, this);
                updateRoiStatistics();
                break;
            case MEASUREMENTS:
                // do nothing
                break;
            default:
                throw new AssertionError(e.getUpdatedDataType().name());
        }
    }
    
    //==============================================================================================
    private void collectionUpdateXEventHandler(XEventExtension ext) {
        if (ext.getClass() == CollectionUpdateXEvent.ElementsAddedWithInfo.class) {
            CollectionUpdateXEvent.ElementsAddedWithInfo e = (CollectionUpdateXEvent.ElementsAddedWithInfo) ext;
            if (e.getDataType() != SampleCategory.class) return;
            ((List<Pair<SampleCategory, CollectionUpdateXEvent.AdditionalInfoPSR>>)e.getElementsWithInfo())
            .stream().forEach(i -> {
                CollectionUpdateXEvent.AdditionalInfoPSR id = i.getSecond();
                Roi roi = dataManager.getRoiCollection().getRoi(id.getPid(), id.getSid(), id.getRid());
                addRoiStatistics(roi, false);
            });
            forceUpdateAll();
        } else if (ext.getClass() == CollectionUpdateXEvent.ElementsRemovedWithInfo.class) {
            CollectionUpdateXEvent.ElementsRemovedWithInfo e = (CollectionUpdateXEvent.ElementsRemovedWithInfo) ext;
            if (e.getDataType() != SampleCategory.class) return;
            ((List<Pair<SampleCategory, CollectionUpdateXEvent.AdditionalInfoPSR>>)e.getElementsWithInfo())
            .stream().forEach(i -> {
                SampleCategory oldCc = i.getFirst();
                CollectionUpdateXEvent.AdditionalInfoPSR id = i.getSecond();
                Roi roi = dataManager.getRoiCollection().getRoi(id.getPid(), id.getSid(), id.getRid());
                removeRoiStatistics(roi, oldCc, false);
            });
            forceUpdateAll();
        } else if (ext.getClass() == CollectionUpdateXEvent.ElementsModifiedWithInfo.class) {
            CollectionUpdateXEvent.ElementsModifiedWithInfo e = (CollectionUpdateXEvent.ElementsModifiedWithInfo) ext;
            if (e.getDataType() != SampleCategory.class) return;
            ((List<Triplet<SampleCategory, SampleCategory, CollectionUpdateXEvent.AdditionalInfoPSR>>)e.getElementsWithInfo())
            .stream().forEach(i -> {
                CollectionUpdateXEvent.AdditionalInfoPSR id = i.getThird();
                Roi roi = dataManager.getRoiCollection().getRoi(id.getPid(), id.getSid(), id.getRid());
                modifyRoiStatistics(roi, i.getFirst(), i.getSecond(), false);
            });
            forceUpdateAll();
        }
    }
    
    //==============================================================================================
    private void updateRoiStatistics() {
        resetAll();
        RoiCollection rois = dataManager.getRoiCollection();
        PatientCollection patients = dataManager.getPatients();
        SampleCollection samples = dataManager.getSamples();
        if (rois != null && patients != null && samples != null) {
            rois.getRois().stream().forEach(r -> {
                Patient p = patients.toMap().get(r.getPatientId());
                USnapshot s = p.getUltrasound().toMap().get(r.getSnapshotId());
                SampleCategory c = samples.getCategory(p.getId(), s.getId(), r.getId());
                if (c != null) {
                    addRoi(r.getRadius(), p.getFibrosisStage(), s.getCutType(), c, false);
                }
            });
            forceUpdateAll();
        }
    }
    
    //==============================================================================================
    private void addRoiStatistics(Roi roi, boolean update) {
        int radius = roi.getRadius();
        Patient p = dataManager.getPatients().toMap().get(roi.getPatientId());
        USnapshot s = p.getUltrasound().toMap().get(roi.getSnapshotId());
        SampleCategory c = dataManager.getSamples().getCategory(p.getId(), s.getId(), roi.getId());
        addRoi(radius, p.getFibrosisStage(), s.getCutType(), c, update);
    }
    
    //==============================================================================================
    private void removeRoiStatistics(Roi roi, SampleCategory cc, boolean update) {
        if (roi == null) {
            System.err.println("Rremoving roi statistics error: null roi");
            return;
        }
        int radius = roi.getRadius();
        Patient p = dataManager.getPatients().toMap().get(roi.getPatientId());
        USnapshot s = p.getUltrasound().toMap().get(roi.getSnapshotId());
        deleteRoi(radius, p.getFibrosisStage(), s.getCutType(), cc, update);
    }
    
    //==============================================================================================
    private void modifyRoiStatistics(Roi roi, SampleCategory oldVal, SampleCategory newVal, boolean update) {
        int radius = roi.getRadius();
        Patient p = dataManager.getPatients().toMap().get(roi.getPatientId());
        USnapshot s = p.getUltrasound().toMap().get(roi.getSnapshotId());
        deleteRoi(radius, p.getFibrosisStage(), s.getCutType(), oldVal, update);
        addRoi(radius, p.getFibrosisStage(), s.getCutType(), newVal, update);
    }

    //==============================================================================================
    @Override
    public void addRoi (int radius, int diseaseStage, LiverCutType cut, SampleCategory cc, boolean update) {
        if (map.get(radius) == null) {
            RoiDistribution[][] d = new RoiDistribution[cutCnt][fibrosisCnt];
            for (int  i = 0 ; i < cutCnt; i++) {
                for (int j = 0; j < fibrosisCnt; j++) {
                    d[i][j] = new RoiDistribution();
                }
            }
            map.put(radius, d);
        }
        RoiDistribution[][] dd = map.get(radius);
        int cutIndex = LiverCutType.valueOf(cut.name()).ordinal();
        RoiDistribution d = dd[cutIndex][diseaseStage];
        d.change(cc, 1);
        if (update) {
            notifyXEventListeners(new RoiStatisticsChangedXEvent(this, radius, diseaseStage, cut, d));
        }
    }
    
    //==============================================================================================
    @Override
    public void deleteRoi (int radius, int diseaseStage, LiverCutType cut, SampleCategory cc, boolean update) {
        RoiDistribution[][] dd = map.get(radius);
        if (dd == null) {
            System.err.println("RoiStatisticsManager: null distribution array while deleting roi");
            return;
        }
        int cutIndex = LiverCutType.valueOf(cut.name()).ordinal();
        RoiDistribution d = dd[cutIndex][diseaseStage];
        d.change(cc, -1);
        if (update) {
            notifyXEventListeners(new RoiStatisticsChangedXEvent(this, radius, diseaseStage, cut, d));
        }
    }
    
    //==============================================================================================
    @Override
    public void resetAll() {
        map.clear();
    }
    
    //==============================================================================================
    @Override
    public void forceUpdateAll() {
        map.entrySet().stream().forEach(s -> {
            int rad = s.getKey();
            RoiDistribution[][] dd = s.getValue();
            for (int  i = 0; i < cutCnt; i++) {
                for (int j = 0; j < fibrosisCnt; j++) {
                    RoiDistribution d = dd[i][j];
                    notifyXEventListeners(
                        new RoiStatisticsChangedXEvent(this, rad, j, LiverCutType.values()[i], d)
                    );
                }
            }
        });
    }

    //==============================================================================================
    @Override
    public RoiDistribution[][] getAllStatistics(int radius) {
        return map.get(radius);
    }

    //==============================================================================================
    @Override
    public void registerXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        xbm.registerXEventListener(event_t, listener);
    }

    //==============================================================================================
    @Override
    public void unregisterXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        xbm.unregisterXEventListener(event_t, listener);
    }
    
    //==============================================================================================
    public void notifyXEventListeners(XEvent xevent) {
        xbm.notifyXEventListeners(xevent);
    }

    //==============================================================================================
    @Override
    public void eventHappened(XEvent event) {
        xlm.eventHappened(event);
    }

}
