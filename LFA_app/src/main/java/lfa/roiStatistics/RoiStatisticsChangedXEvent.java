package lfa.roiStatistics;

import base.events.XEvent;
import base.events.XEventBroadcaster;
import lfa.data.LiverCutType;
import lfa.gui.roistatisticstable.RoiDistribution;

//**************************************************************************************************
public class RoiStatisticsChangedXEvent extends XEvent {
    
    private final int diseaseStage;
    private final LiverCutType cutType;
    private final RoiDistribution distribution;
    private final int radius;
    
    //==============================================================================================
    public RoiStatisticsChangedXEvent(XEventBroadcaster sender, int r, int diseaseStage, LiverCutType c, RoiDistribution d) {
        super(sender);
        this.radius = r;
        this.diseaseStage = diseaseStage;
        this.cutType = c;
        this.distribution = d;
    }
    
    //==============================================================================================
    public LiverCutType getCutType() {
        return cutType;
    }

    //==============================================================================================
    public int getDiseaseStage() {
        return diseaseStage;
    }
    
    //==============================================================================================
    public RoiDistribution getDistribution() {
        return distribution;
    }
    
    //==============================================================================================
    public int getRadius() {
        return radius;
    }
    
}
