package lfa.roiStatistics;

import base.events.XEventBroadcaster;
import base.events.XEventListener;
import lfa.data.SampleCategory;
import lfa.data.LiverCutType;
import lfa.gui.roistatisticstable.RoiDistribution;

//**************************************************************************************************
public interface IRoiStatisticsManager extends XEventBroadcaster, XEventListener {
    
    void addRoi (int radius, int diseaseStage, LiverCutType cut, SampleCategory cc, boolean update);
    void deleteRoi (int radius, int diseaseStage, LiverCutType cut, SampleCategory cc, boolean update);
    void resetAll();
    void forceUpdateAll();
    RoiDistribution[][] getAllStatistics(int radius);
}