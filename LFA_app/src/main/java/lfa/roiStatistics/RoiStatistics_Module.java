package lfa.roiStatistics;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

//**************************************************************************************************
public class RoiStatistics_Module extends AbstractModule {

    @Override
    protected void configure() {
        bind(IRoiStatisticsManager.class).to(RoiStatisticsManager.class).in(Singleton.class);
    }
    
}
