package lfa

import java.util

import base.collections.Triplet
import lfa.data.{LiverCutType, SampleCategory}
import lfa.image.{ImageAdjustMethod => IAM, ImagePreprocessMethod => IPM}
import lfa.measurement.RoiTexturalProcessor
import lfa.storage.IImageCacheStorage
import xubiker.XLearnTools.attributeselection.XAttributeSelectionModel
import xubiker.XLearnTools.balance.XDataBalancingModel
import xubiker.XLearnTools.base.XAttributeLabels
import xubiker.XLearnTools.classify.{ComposedClassifierModel, XClassifierModel}
import xubiker.XLearnTools.normalize.XDataNormalizeModel
import xubiker.XLearnTools.outliers.XOutlierRemoveModel

import scala.collection.JavaConverters._

case object AppConf {

  case object performance {
    val numOfActors = 4
    val deleteClassifierCacheAfterUsage = true
  }

  case object cache {
    val imageCachePath = "..\\cache\\images\\"
    val objectCachePath = "..\\cache\\objects\\"
    val imageCacheType = IImageCacheStorage.FileType.SER
    val gsonPrettyPrinting = false
  }

  val iams: List[IAM] = List(
//          IAM.ORIGINAL//,
//    IAM.MINMAX//,
//          IAM.MEAN
          IAM.MEANSTD
  )

  val ipms: List[IPM] = List(
    IPM.None//,
//    IPM.ERF
    //    IPM.SradMne,
    //    IPM.SradMneDer,
    //    IPM.ErfSradMne,
//        IPM.ErfSradMneDer
    //    IPM.TvReg
  )

  val rads: List[Int] = List(20)
  val cuts: List[LiverCutType] = List(LiverCutType.CUT3)
  val quantLevels: List[Int] = List(256)
  val removeOutliers: List[Boolean] = List(false)

  val texParams: List[(Int, IPM, IAM)] = iams.flatMap(a => ipms.flatMap(p => quantLevels.map(q => (q, p, a))))

  val texClassifierModels = List(
    //XClassifierModel.RANDOM_FOREST("-I", "300", "-K", "10"),
    XClassifierModel.RANDOM_FOREST("-I", "500", "-K", "10")
    //XClassifierModel.MULTILAYER_PERCEPTRON("-L", "0.3", "-M", "0.2", "-N", "1000", "-V", "0",  "-S", "0", "-E", "20", "-H", "a")
    //XClassifierModel.K_STAR("-B", "20", "-M", "a")
  )

  val composedClassifiers: List[ComposedClassifierModel] = texClassifierModels.flatMap(model => List(
    ComposedClassifierModel(model, Some(XDataNormalizeModel.STD), Some(XAttributeSelectionModel.CFS()), Some(XDataBalancingModel.SmoteBalancer(2, 3)))
    //ComposedClassifierModel(model, Some(XDataNormalizeModel.STD), Some(XAttributeSelectionModel.CFS()), None)
  ))

  val radsJ: java.util.List[Integer] = rads.map(new Integer(_)).asJava
  val texParamsJ: util.List[Triplet[Integer, IPM, IAM]] = texParams.map(t => new Triplet[Integer, IPM, IAM](new Integer(t._1), t._2, t._3)).asJava
  val preprocessCategoriesJ: java.util.List[SampleCategory] = List(SampleCategory.TRAIN_SET, SampleCategory.TEST_SET, SampleCategory.UNUSED).asJava

  val outlierRemoverModel: XOutlierRemoveModel.I = XOutlierRemoveModel.SUM_EXTREMUS_PENALTY(0.35)

  object diagnostics {
    val disease = diagnosis.FIBROSIS
    val score = diagnosis.HEALTH_SCORE_2
  }

  object texture {
    val use_laws: Boolean = true
    val use_glcm: Boolean = false
    val use_glrlm: Boolean = true
    val use_wavelet: Boolean = true
  }

  val removeOutliersFromTestSet = false

  def texFeatureAttributes: XAttributeLabels = RoiTexturalProcessor.attributes

  object manualRun {
    val patientsPath = Some("..\\Data\\Patients\\patients3.xml")
//    val roisPath = Some("..\\Layouts\\x\\rois2.xml")
//    val samplePath = Some("..\\Layouts\\x\\s_2_1.xml")
    val roisPath = Some("..\\Layouts\\fibrosis_full_validation\\rois.xml")
    val samplePath = Some("..\\Layouts\\fibrosis_full_validation\\base_sample.xml")
  }

  // --- full validation parameters ---
  object validation {
    val nSamples = 1
    val nBestSamples = 0
    val patientsPath = "..\\Data\\Patients\\patients3.xml"
    val roisPath = "..\\Layouts\\fibrosis_full_validation\\rois.xml"
    val baseSamplePath = "..\\Layouts\\fibrosis_full_validation\\base_sample.xml"
    val samplesDir: String = System.getProperty("user.dir") + "\\..\\Layouts\\fibrosis_full_validation\\"
  }

  object export {
    val roisExportDir = "..\\Layouts\\export\\"
    val exportAsImages = true
  }
}