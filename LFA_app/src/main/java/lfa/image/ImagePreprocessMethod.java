package lfa.image;

//**************************************************************************************************
public enum ImagePreprocessMethod {
    None,
    ERF,
    SradMne,
    SradMneDer,
    ErfSradMne,
    ErfSradMneDer,
}
