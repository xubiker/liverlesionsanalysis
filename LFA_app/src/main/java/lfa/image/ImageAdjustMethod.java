package lfa.image;

//**************************************************************************************************
public enum ImageAdjustMethod {
    ORIGINAL,
    MINMAX,
    MEAN,
    MEANSTD
}
