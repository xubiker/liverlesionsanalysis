package lfa.datamanager;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

//**************************************************************************************************
public class DataManagerModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(IDataManager.class).to(DataManager.class).in(Singleton.class);
    }
    
}
