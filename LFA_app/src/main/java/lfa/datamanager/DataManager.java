package lfa.datamanager;

import base.events.*;
import com.google.inject.Inject;
import lfa.AppConf;
import lfa.AppConf$;
import lfa.cl.ClResultCollection;
import lfa.data.*;
import lfa.events.CollectionUpdateXEvent;
import lfa.measurement.XVectorCollection;
import lfa.packing.IPackable;
import lfa.storage.*;
import weka.core.Instances;
import xubiker.XImageTools.base.XImage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

//**************************************************************************************************
class DataManager implements IDataManager {
    
    private final IPackedStorage packedStorage;
    private final IWekaStorage wekaStorage;
    private final IImageCacheStorage imageCacheStorage;
    private final IGsonStorage gsonStorage;
    private final IObjectStorage objectStorage;
    
    private final XEventListenerManager xlm = XEventFactory.createListener();
    private final XEventBroadcasterManager xbm = XEventFactory.createBroadcaster();
    
    private PatientCollection patientCollection;
    private RoiCollection roiCollection;
    private SampleCollection sampleCollection;
    private XVectorCollection measurementCollection;
    private ClResultCollection clResultCollection;

    //==============================================================================================
    @Inject
    public DataManager(StorageFactory storageFactory) {
        packedStorage = storageFactory.createPackedStorage();
        wekaStorage = storageFactory.createWekaStorage();
        imageCacheStorage = storageFactory.createImageCaheStorage(AppConf.cache$.MODULE$.imageCachePath());
        objectStorage = storageFactory.createObjectStorage(AppConf.cache$.MODULE$.objectCachePath());
        gsonStorage = storageFactory.createGSonStorage(AppConf.cache$.MODULE$.gsonPrettyPrinting());
    }
    
    //==============================================================================================
    @Override
    public PatientCollection getPatients() {
        if (patientCollection == null) {
            System.out.println("DataManager: patientCollection not found, creating new instance");
            this.patientCollection = new PatientCollection();
            initializePatientCollection();
        }
        return patientCollection;
    }

    //==============================================================================================
    @Override
    public RoiCollection getRoiCollection() {
        if (roiCollection == null) {
            System.out.println("DataManager: roiCollection not found, creating new instance");
            this.roiCollection = new RoiCollection();
            initializeRoiCollection();
        }
        return roiCollection;
    }

    //==============================================================================================
    @Override
    public SampleCollection getSamples() {
        if (sampleCollection == null) {
            System.out.println("DataManager: sampleCollection not found, creating new instance");
            this.sampleCollection = new SampleCollection();
            initializeSampleCollection();
        }
        return sampleCollection;
    }
    
    //==============================================================================================
    @Override
    public XVectorCollection getMeasurements() {
        if (measurementCollection == null) {
            System.out.println("DataManager: measurementCollection not found, creating new instance");
            this.measurementCollection = new XVectorCollection();
            initializeMeasurementCollection();
        }
        return measurementCollection;
    }

    //==============================================================================================
    @Override
    public ClResultCollection getClResults() {
        if (clResultCollection == null) {
            System.out.println("DataManager: clResultsCollection not found, creating new instance");
            this.clResultCollection = new ClResultCollection();
            initializeClResultsCollection();
        }
        return clResultCollection;
    }

    //==============================================================================================
    @Override
    public void loadPatients(String path) {
        System.out.println("DataManager. loading patients...");
        try {
            IPackable importRes = (path == null)
                    ? packedStorage.importInstance(PatientCollection.class)
                    : packedStorage.importInstance(PatientCollection.class, path);
            if (importRes != null) {
                this.patientCollection = (PatientCollection) importRes;
                initializePatientCollection();
                System.out.println("DataManager. loading finished successfully");
                notifyXEventListeners(new DataManagerXEvent(this, DataManagerXEvent.DATA_UPDATED.PATIENTS));
            } else {
                System.err.println("DataManager. loading patients failed (null import)");
            }
        } catch (Exception ex) {
            System.err.println("DataManager. loading patients failed: " + ex);
        }
    }

    //==============================================================================================
    @Override
    public void loadRois(String path) {
        System.out.println("DataManager. loading rois...");
        try {
            IPackable importRes;
            importRes = (path == null)
                    ? packedStorage.importInstance(RoiCollection.class)
                    : packedStorage.importInstance(RoiCollection.class, path);
            if (importRes != null) {
                this.roiCollection = (RoiCollection) importRes;
                initializeRoiCollection();
                System.out.println("DataManager. loading finished successfully");
                notifyXEventListeners(new DataManagerXEvent(this, DataManagerXEvent.DATA_UPDATED.ROIS));
            } else {
                System.err.println("DataManager. loading rois failed (null import)");
            }
        } catch (Exception ex) {
            System.err.println("DataManager. loading rois failed: " + ex);
            Logger.getLogger(DataManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    //==============================================================================================
    @Override
    public void loadSamples(String path) {
        System.out.println("DataManager. loading samples...");
        try {
            IPackable importRes = (path == null)
                    ? packedStorage.importInstance(SampleCollection.class)
                    : packedStorage.importInstance(SampleCollection.class, path);
            if (importRes != null) {
                this.sampleCollection = (SampleCollection) importRes;
                initializeSampleCollection();
                System.out.println("DataManager. loading finished successfully");
                notifyXEventListeners(new DataManagerXEvent(this, DataManagerXEvent.DATA_UPDATED.SAMPLES));
            } else {
                System.err.println("DataManager. loading samples failed (null import)");
            }
        } catch (Exception ex) {
            System.err.println("DataManager. loading samples failed: " + ex);
        }
    }
    
    //==============================================================================================
    @Override
    public void loadMeasurements(String path) {
        System.out.println("DataManager. loading measurements...");
        try {
            IGsonConvertible importRes = (path == null)
                    ? gsonStorage.importInstance(XVectorCollection.class)
                    : gsonStorage.importInstance(XVectorCollection.class, path);
            if (importRes != null) {
                this.measurementCollection = (XVectorCollection) importRes;
                initializeMeasurementCollection();
                System.out.println("DataManager. loading finished successfully");
                notifyXEventListeners(new DataManagerXEvent(this, DataManagerXEvent.DATA_UPDATED.MEASUREMENTS));
            } else {
                System.err.println("DataManager. loading measurements failed (null import)");
            }
        } catch (Exception ex) {
            System.err.println("DataManager. loading measurements failed: " + ex);
        }
    }

    //==============================================================================================
    @Override
    public void loadPatients() {
        loadPatients(null);
    }
    
    //==============================================================================================
    @Override
    public void loadRois() {
        loadRois(null);
    }

    //==============================================================================================
    @Override
    public void loadSamples() {
        loadSamples(null);
    }
    
    //==============================================================================================
    @Override
    public void loadMeasurements() {
        loadMeasurements(null);
    }
    
    //==============================================================================================
    @Override
    public void savePatients() {
        System.out.println("DataManager. saving patients...");
        try {
            packedStorage.exportInstance(patientCollection);
            System.out.println("DataManager. saving finished successfully");
        } catch (Exception ex) {
            System.err.println("DataManager. saving failed: " + ex);
        }
    }

    //==============================================================================================
    @Override
    public void saveRois() {
        System.out.println("DataManager. saving rois...");
        try {
            packedStorage.exportInstance(roiCollection);
            System.out.println("DataManager. saving finished successfully");
        } catch (Exception ex) {
            System.err.println("DataManager. saving failed: " + ex);
        }
    }

    //==============================================================================================
    @Override
    public void saveSamples() {
        System.out.println("DataManager. saving samples...");
        try {
            packedStorage.exportInstance(sampleCollection);
            System.out.println("DataManager. saving finished successfully");
        } catch (Exception ex) {
            System.err.println("DataManager. saving failed: " + ex);
        }
    }

    //==============================================================================================
    @Override
    public void saveSamples(String path) {
        System.out.println("DataManager. saving samples...");
        try {
            packedStorage.exportInstance(sampleCollection, path);
            System.out.println("DataManager. saving finished successfully");
        } catch (Exception ex) {
            System.err.println("DataManager. saving failed: " + ex);
        }
    }

    //==============================================================================================
    @Override
    public void saveMeasurements() {
        System.out.println("DataManager. saving measurements...");
        try {
            gsonStorage.exportInstance(measurementCollection);
//            packedStorage.exportInstance(measurementCollection);
            System.out.println("DataManager. saving finished successfully");
        } catch (Exception ex) {
            System.err.println("DataManager. saving failed: " + ex);
        }
    }

    //==============================================================================================
    @Override
    public Collection<Roi> getRois(int patientId, int snapshotId) {
        List<Roi> roisList = new ArrayList<>();
        if (roiCollection != null) {
            roisList.addAll(roiCollection.getRois(patientId, snapshotId));
        }
        return roisList;
    }
    
    //==============================================================================================
    @Override
    public void createAndFillSamples(SampleCategory desiredCategory) {
        System.out.println("DataManager. creating new samples for all rois");
        this.getSamples().removeAllCategories();
        this.getRoiCollection().getRois().forEach(r -> {
            int pid = r.getPatientId();
            int sid = r.getSnapshotId();
            int rid = r.getId();
            try {
                sampleCollection.addCategory(pid, sid, rid, desiredCategory);
            } catch (Exception ex) {
                Logger.getLogger(DataManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }
    
    //==============================================================================================
    private void initializePatientCollection() {
        System.out.println("initialize patients");
        if (patientCollection == null) return;
        patientCollection.registerXEventListener(CollectionUpdateXEvent.class, this);
        xlm.bindXEventHandler(CollectionUpdateXEvent.class,
                this::notifyXEventListeners);
    }
    
//==============================================================================================
    private void initializeRoiCollection() {
        if (roiCollection == null) return;
        roiCollection.registerXEventListener(CollectionUpdateXEvent.class, this);
        xlm.bindXEventHandler(CollectionUpdateXEvent.class,
                this::notifyXEventListeners);
    }

    //==============================================================================================
    private void initializeSampleCollection() {
        if (sampleCollection == null) return;
        sampleCollection.registerXEventListener(CollectionUpdateXEvent.class, this);
        xlm.bindXEventHandler(CollectionUpdateXEvent.class,
                this::notifyXEventListeners);
    }
    
    //==============================================================================================
    private void initializeMeasurementCollection() {
        // ===
    }
    
    //==============================================================================================
    private void initializeClResultsCollection() {
        // ===
    }

    //==============================================================================================
    @Override
    public XImage loadImage(Patient p, USnapshot s, boolean loadGray) {
        if (p == null || s == null) {
            return null;
        }
        String imagePath = patientCollection.getPath() + p.getPath() + s.getFileName();
        if (loadGray) {
            return XImage.readGray(imagePath);
        } else {
            return XImage.read(imagePath);
        }
    }
    
    //==============================================================================================
    @Override
    public XImage loadImage(int pid, int sid, boolean loadGray) {
        Patient patient = patientCollection.toMap().get(pid);
        if (patient == null) {
            return null;
        }
        USnapshot snapshot = patient.getUltrasound().toMap().get(sid);
        if (snapshot == null) {
            return null;
        }
        return loadImage(patient, snapshot, loadGray);
    }

    //==============================================================================================
    @Override
    public void exportWekaInstances(Instances instances) {
        System.out.println("DataManager. saving weka instances...");
        wekaStorage.exportInstancesToArff(instances);
        System.out.println("DataManager. saving finished successfully");
    }
    
    //==============================================================================================
    @Override
    public void exportWekaInstances(Instances instances, String path) {
        System.out.println("DataManager. saving weka instances...");
        wekaStorage.exportInstancesToArff(instances, path);
        System.out.println("DataManager. saving finished successfully");
    }

    //==============================================================================================
    @Override
    public void exportBatchWekaInstances(Collection<Instances> instances) {
        System.out.println("DataManager. saving weka instances in batch mode...");
        wekaStorage.exportInstancesToArffInBatch(instances);
        System.out.println("DataManager. saving finished successfully");
    }

    //==============================================================================================
    @Override
    public void registerXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        xbm.registerXEventListener(event_t, listener);
    }

    //==============================================================================================
    @Override
    public void unregisterXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        xbm.unregisterXEventListener(event_t, listener);
    }

    //==============================================================================================
    private void notifyXEventListeners(XEvent event) {
        xbm.notifyXEventListeners(event);
    }

    //==============================================================================================
    @Override
    public void eventHappened(XEvent event) {
        xlm.eventHappened(event);
    }

    //==============================================================================================
    @Override
    public IImageCacheStorage getImageCacheStorage() {
        return imageCacheStorage;
    }

    //==============================================================================================
    @Override
    public IObjectStorage getObjectStorage() {
        return objectStorage;
    }

}
