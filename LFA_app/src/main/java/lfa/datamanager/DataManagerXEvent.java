package lfa.datamanager;

import base.events.XEvent;
import base.events.XEventBroadcaster;

//**************************************************************************************************
public class DataManagerXEvent extends XEvent {
    
    public static enum DATA_UPDATED {
        PATIENTS,
        ROIS,
        SAMPLES,
        MEASUREMENTS,
    }
    
    private final DATA_UPDATED updatedDataType;
    
    public DataManagerXEvent(XEventBroadcaster sender, DATA_UPDATED updatedDataType) {
        super(sender);
        this.updatedDataType = updatedDataType;
    }
    
    public DATA_UPDATED getUpdatedDataType() {
        return updatedDataType;
    }
    
}
