package lfa.datamanager;

import base.events.XEventBroadcaster;
import base.events.XEventListener;
import lfa.cl.ClResultCollection;
import lfa.data.*;
import lfa.measurement.XVectorCollection;
import lfa.storage.IImageCacheStorage;
import lfa.storage.IObjectStorage;
import weka.core.Instances;
import xubiker.XImageTools.base.XImage;

import java.util.Collection;

//**************************************************************************************************
public interface IDataManager extends XEventListener, XEventBroadcaster {
    
    PatientCollection getPatients();
    RoiCollection getRoiCollection();
    SampleCollection getSamples();
    XVectorCollection getMeasurements();
    ClResultCollection getClResults();

    XImage loadImage(Patient p, USnapshot s, boolean loadGray);
    XImage loadImage(int pid, int sid, boolean loadGray);

    Collection<Roi> getRois(int patientId, int snapshotId);
    
    void loadPatients();
    void loadRois();
    void loadSamples();
    void loadMeasurements();

    void loadPatients(String path);
    void loadRois(String path);
    void loadSamples(String path);
    void loadMeasurements(String paths);
    
    void savePatients();
    void saveRois();
    void saveSamples();
    void saveSamples(String path);
    void saveMeasurements();
    
    void createAndFillSamples(SampleCategory desiredCategory);
    
    void exportWekaInstances(Instances instances);
    void exportWekaInstances(Instances instances, String path);
    void exportBatchWekaInstances(Collection<Instances> instances);
    
    IImageCacheStorage getImageCacheStorage();

    IObjectStorage getObjectStorage();

}
