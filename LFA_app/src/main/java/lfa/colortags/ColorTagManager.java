package lfa.colortags;

import base.collections.Pair;
import base.events.XEvent;
import base.events.XEventExtension;
import base.events.XEventFactory;
import base.events.XEventListenerManager;
import com.google.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lfa.data.SampleCategory;
import lfa.data.Patient;
import lfa.data.USnapshot;
import lfa.datamanager.IDataManager;
import lfa.events.RoiXEvent;
import lfa.gui.advancedtable.IAdvancedTableSuitable;

//**************************************************************************************************
class ColorTagManager implements IColorTagManager {
    
    private final IDataManager datamanager;
    private final XEventListenerManager xlm = XEventFactory.createListener();
    //private final List<Integer> activeRoiRads = new ArrayList<>();
    
    //==============================================================================================
    @Inject
    public ColorTagManager(IDataManager datamanager) {
        this.datamanager = datamanager;
//        xlm.bindXEventHandler(RoiXEvent.class, e -> roiXEventHandler((XEventExtension) e));
    }
    
    //==============================================================================================
//    private void roiXEventHandler(XEventExtension e) {
//        if (e.getClass() == RoiXEvent.ActiveRadChanged.class) {
//            RoiXEvent.ActiveRadChanged event = (RoiXEvent.ActiveRadChanged) e;
//            activeRoiRads.clear();
//            activeRoiRads.addAll(event.getActiveRad());
//        }
//    }
    
    //==============================================================================================
    @Override
    public List<Integer> getColorTags(IAdvancedTableSuitable item) {
        if (item.getClass() == Patient.class) {
            Patient patient = (Patient) item;
            SampleCategory cc = datamanager.getSamples().getCategory(patient.getId());
            List<Integer> colorTags = patient.getColorTags().stream()
                    .map(tag -> ColorTagConverter.convert(new Pair<>("fibrosis", tag)))
                    .collect(Collectors.toList());
            if (cc != null) {
                colorTags.set(0, ColorTagConverter.convert(new Pair<>("cc", cc.ordinal())));
            } else {
                colorTags.set(0, -1);
            }
            colorTags.set(1, -1);
            colorTags.set(3, -1);
            return colorTags;
        }
        if (item.getClass() == USnapshot.class) {
            USnapshot s = (USnapshot) item;
            List<Integer> colorTags = s.getColorTags();
            colorTags.set(0, ColorTagConverter.convert(new Pair<>("cut", s.getCutType().ordinal())));
            colorTags.set(1, ColorTagConverter.convert(new Pair<>("cut", s.getCutType().ordinal())));
            return colorTags;
        }
        return item.getColorTags();
    }

    //==============================================================================================
    @Override
    public void eventHappened(XEvent event) {
        xlm.eventHappened(event);
    }
    
}
