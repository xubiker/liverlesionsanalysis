package lfa.colortags;

import base.collections.Pair;
import java.util.HashMap;
import java.util.Map;

public class ColorTagConverter {

    private static final int k = 1000;
    private static final Map<Integer, String> map = new HashMap<>();
    
    public static int convert(Pair<String, Integer> p) {
        String label = p.getFirst();
        map.put(label.hashCode(), label);
        return label.hashCode() * k + p.getSecond();
    }
    
    public static Pair<String, Integer> convert(int tag) {
        return new Pair<>(map.get(tag / k), tag % k);
    }
    
}
