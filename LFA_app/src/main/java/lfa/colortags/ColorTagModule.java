package lfa.colortags;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

//**************************************************************************************************
public class ColorTagModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(IColorTagManager.class).to(ColorTagManager.class).in(Singleton.class);
    }
    
}
