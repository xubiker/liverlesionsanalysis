package lfa.colortags;

import base.events.XEventListener;
import java.util.List;
import lfa.gui.advancedtable.IAdvancedTableSuitable;

//**************************************************************************************************
public interface IColorTagManager extends XEventListener {

    List<Integer> getColorTags(IAdvancedTableSuitable item);
    
}
