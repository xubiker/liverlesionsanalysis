package lfa.painting;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

//**************************************************************************************************
public class Painting_Module extends AbstractModule {

    @Override
    protected void configure() {
        bind(IPainter.class).to(Painter.class).in(Singleton.class);
    }
    
}
