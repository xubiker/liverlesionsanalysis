package lfa.painting;

import base.collections.Triplet;
import lfa.data.Roi;
import org.opencv.core.Core;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import xubiker.XImageTools.base.XImage;

import java.util.*;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static org.opencv.imgproc.Imgproc.line;
import static org.opencv.imgproc.Imgproc.putText;

//**************************************************************************************************
class Painter implements IPainter {
    
    private final Map<RoiPaintType, Scalar> colorMap = new HashMap<>();
    
    //==============================================================================================
    public Painter() {
        colorMap.put(RoiPaintType.TRAIN, new Scalar(0, 255, 0));
        colorMap.put(RoiPaintType.TEST, new Scalar(0, 0, 255));
        colorMap.put(RoiPaintType.UNUSED, new Scalar(0, 255, 255));
        colorMap.put(RoiPaintType.NONE, new Scalar(255, 255, 255));
        colorMap.put(RoiPaintType.OUTLIER, new Scalar(255, 0, 159));
    }
    
    //==============================================================================================
    @Override
    public XImage paintRois(XImage srcImage, boolean makeCopy, Collection<Triplet<Roi, RoiPaintType, String>> rois, boolean rotation, boolean selected) {
        XImage resImage = (makeCopy) ? srcImage.copy() : srcImage;
        rois.forEach(t -> {
            if (t != null && t.getFirst() != null && t.getSecond() != null) {
                Roi roi = t.getFirst();
                Point roiCenter = new Point(roi.getCenterX(), roi.getCenterY());
                double roiRad = roi.getRadius();
                double roiAlpha = (rotation) ? roi.getAlpha() : 0;

                Point lu = addPoints(roiCenter, rotatePoint(new Point(-roiRad, -roiRad), roiAlpha));
                Point ru = addPoints(roiCenter, rotatePoint(new Point(roiRad, -roiRad), roiAlpha));
                Point ld = addPoints(roiCenter, rotatePoint(new Point(-roiRad, roiRad), roiAlpha));
                Point rd = addPoints(roiCenter, rotatePoint(new Point(roiRad, roiRad), roiAlpha));

                Scalar color = (selected) ? new Scalar(255, 255, 0) : colorMap.get(t.getSecond());
                line(resImage.cvData(), lu, ru, color);
                line(resImage.cvData(), ru, rd, color);
                line(resImage.cvData(), rd, ld, color);
                line(resImage.cvData(), ld, lu, color);

                String label = t.getThird();
                if (label != null && !"".equals(label)) {
                    Point p = new Point(ld.x + (roiCenter.x - ld.x) / 2, roiCenter.y);
                    putText(resImage.cvData(), label, p, Core.FONT_HERSHEY_SIMPLEX, 0.4, new Scalar(255,255,255));
                }
            }
        });
        
        return resImage;
    }

    //==============================================================================================
    @Override
    public XImage paintRoi(XImage srcImage, boolean makeCopy,
            Triplet<Roi, RoiPaintType, String> roi, boolean rotation, boolean selected) {
        return paintRois(srcImage, makeCopy, Collections.singletonList(roi), rotation, selected);
    }
    
    //==============================================================================================
    @Override
    public XImage paintUsCone(XImage srcImage, boolean makeCopy, int usConeCenterX, int usConeCenterY) {
        XImage resImage = (makeCopy) ? srcImage.copy() : srcImage;
        line(resImage.cvData(), new Point(usConeCenterX, 0), new Point(usConeCenterX, 100), new Scalar(255, 255, 255));
        return resImage;
    }
    
    //==============================================================================================
    private static Point rotatePoint(Point p, double alpha) {
        double s = sin(alpha);
        double c = cos(alpha);
        return new Point(p.x * c - p.y * s, p.x * s + p.y * c);
    }

    //==============================================================================================
    private static Point addPoints(Point p1, Point p2) {
        return new Point(p1.x + p2.x, p1.y + p2.y);
    }

}
