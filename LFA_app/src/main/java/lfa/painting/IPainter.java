package lfa.painting;

import base.collections.Triplet;
import java.util.Collection;
import lfa.data.Roi;
import xubiker.XImageTools.base.XImage;

//**************************************************************************************************
public interface IPainter {

    XImage paintRoi(XImage srcImage, boolean makeCopy,
            Triplet<Roi, RoiPaintType, String> roi, boolean rotation, boolean selected);
    XImage paintRois(XImage srcImage, boolean makeCopy,
            Collection<Triplet<Roi, RoiPaintType, String>> rois, boolean rotation, boolean selected);
    XImage paintUsCone(XImage srcImage, boolean makeCopy, int usConeCenterX, int usConeCenterY);

}
