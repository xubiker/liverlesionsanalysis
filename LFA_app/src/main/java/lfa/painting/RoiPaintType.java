package lfa.painting;

public enum RoiPaintType {
    
    OUTLIER,
    TRAIN,
    TEST,
    UNUSED,
    NONE
    
}
