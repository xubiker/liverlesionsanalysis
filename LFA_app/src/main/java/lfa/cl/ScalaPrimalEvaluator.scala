package lfa.cl
import base.ScalaOps
import base.backgroundexecution.{IBackgroundFactory, IBackgroundMonitor, XBackgroundTask}
import com.google.inject.Inject
import com.typesafe.scalalogging.LazyLogging
import lfa.AppConf
import lfa.datamanager.IDataManager
import lfa.diagnosis.IDiagnosisDeterminator
import lfa.gui.evalresult.IEvalResult_VM
import lfa.major.Major_V
import lfa.measurement.TexID
import xubiker.XLearnTools.classify.ClResult
import xubiker.XLearnTools.evaluation.{EvaluationResult, XEvaluator}

class ScalaPrimalEvaluator @Inject()(
                                      dataManager: IDataManager,
                                      diagnosisDeterminator: IDiagnosisDeterminator,
                                      backgroundFactory: IBackgroundFactory,
                                      evalResultVM: IEvalResult_VM,
                                      majorV: Major_V
                                    ) extends IPrimalEvaluator with LazyLogging {

  import scala.collection.JavaConverters._

  private val disease = AppConf.diagnostics.disease
  private val score = AppConf.diagnostics.score
  private val evaluator = XEvaluator(diagnosisDeterminator.getAllLabels(score))

  private def _evaluate(controller: IBackgroundMonitor): AnyRef = {
    logger.info("evaluating classifiers")
    val clResultsByClassifier: Map[String, Iterable[ClResult]] = dataManager.getClResults.getAllTexClResults.asScala.groupBy(_.tag)
    val evalResultsByClassifier = clResultsByClassifier.map(pair => {
      val classifierTag = pair._1
      val results = pair._2
      val evalData = results.map(res => {
        val realLabel = diagnosisDeterminator.getLabel(new TexID(res.xVectorSysVals).pid, disease, score)
        assert(realLabel.isPresent)
        (realLabel.get, res)
      })
      val evalRes = evaluator.evaluate(evalData)
      (classifierTag, evalRes)
    })
    analyzeEvaluationResults(evalResultsByClassifier)
    pushToEvalResultVM(evalResultsByClassifier)
    logger.info("evaluating finished")
    null
  }

  private def pushToEvalResultVM(evalResultsByClassifier: Map[String, EvaluationResult]): Unit = {
    val filteredAndSortedEvaluations = evalResultsByClassifier.toList
      .filter(_._2.getMetric("fAVG", "ACC").get > 0.5)
      .sortBy(_._2.getMetric("fAVG", "AUC"))(Ordering[Option[Double]].reverse)
    evalResultVM.clearAllData()
    val res = filteredAndSortedEvaluations.map(r => new base.collections.Pair[String, String](r._1, r._2.toString)).asJava
    evalResultVM.addEvalResultData(res)
    evalResultVM.display()
  }

  private def analyzeEvaluationResults(clResultsByClassifier: Map[String, EvaluationResult]): Unit = {
    // TODO: function has to print some conclusion based on the made evaluations
  }

  override def evaluateAllClassifiers(): Unit =
    backgroundFactory.createWizard(majorV).addTaskAndStart(gt_evaluateAllClassifiers())

  override def gt_evaluateAllClassifiers(): XBackgroundTask = new XBackgroundTask(ScalaOps.toJavaFunction(_evaluate))
}
