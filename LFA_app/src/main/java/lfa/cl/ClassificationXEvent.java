package lfa.cl;

import base.events.XEvent;
import base.events.XEventBroadcaster;
import base.events.XEventExtension;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

//**************************************************************************************************
public class ClassificationXEvent extends XEvent {
    
    //**********************************************************************************************
    public ClassificationXEvent(XEventBroadcaster sender) {
        super(sender);
    }
    
    //**********************************************************************************************
    public static class ClassifiersUpdated extends XEventExtension {
       
        private final List<String> availableClassifiers = new ArrayList<>();

        public ClassifiersUpdated(XEventBroadcaster sender, Collection<String> availableClassifiers) {
            super(sender, ClassifiersUpdated.class);
            this.availableClassifiers.clear();
            this.availableClassifiers.addAll(availableClassifiers);
        }
        
        public List<String> getClassifiers() {
            return availableClassifiers;
        }
    }
    
    //**********************************************************************************************
    public static class ClassifierSelected extends XEventExtension {
        
        private final String selectedClassifier;
        
        public ClassifierSelected(XEventBroadcaster sender, String selectedClassifier) {
            super(sender, ClassifierSelected.class);
            this.selectedClassifier = selectedClassifier;
        }
        
        public String getSelectedClassifer() {
            return selectedClassifier;
        }
        
    }
}
