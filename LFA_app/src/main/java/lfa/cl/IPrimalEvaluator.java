package lfa.cl;

import base.backgroundexecution.XBackgroundTask;

//**************************************************************************************************
public interface IPrimalEvaluator {

    void evaluateAllClassifiers();
    XBackgroundTask gt_evaluateAllClassifiers();

}
