package lfa.cl

import base.ScalaOps
import base.backgroundexecution.{IBackgroundFactory, IBackgroundMonitor, XBackgroundTask}
import base.events.{XEvent, XEventFactory, XEventListener}
import base.time.TimeManager
import com.google.inject.Inject
import com.typesafe.scalalogging.LazyLogging
import lfa.AppConf
import lfa.data.{LiverCutType, SampleCategory}
import lfa.datamanager.IDataManager
import lfa.diagnosis.DiagnosisDeterminator
import lfa.image.{ImageAdjustMethod => IAM, ImagePreprocessMethod => IPM}
import lfa.major.Major_V
import lfa.measurement.TexID
import xubiker.XLearnTools.base.{XAttributeLabels, XVector}
import xubiker.XLearnTools.classify._

import scala.collection.immutable.Iterable
import scala.collection.mutable

class ScalaPrimalClassifier @Inject()(
                                       dataManager: IDataManager,
                                       backgroundFactory: IBackgroundFactory,
                                       majorV: Major_V,
                                       primalOutlierRemover: IPrimalOutlierRemover
                                     ) extends IPrimalClassifier with LazyLogging {

  case class ClGroup(rad: Integer, cut: LiverCutType, texParam: (Int, IPM, IAM), removeOutliers: Boolean) {
    override def toString: String = {
      val s = s"R${rad}_${cut}_q${texParam._1}_${texParam._2}_${texParam._3}"
      if (removeOutliers) s + "_RO" else s
    }
  }

  private val xbm = XEventFactory.createBroadcaster

  private val classLabels = new DiagnosisDeterminator(dataManager).getAllLabels(AppConf.diagnostics.score).toList

  private val attributes: XAttributeLabels = AppConf.texFeatureAttributes

  private val clGroups: Iterable[ClGroup] =
    AppConf.rads.flatMap(r =>
      AppConf.cuts.flatMap(c =>
        AppConf.texParams.flatMap(tP =>
          AppConf.removeOutliers.map(ro =>
            ClGroup(r, c, tP, ro)
          )
        )
      )
    )

  private val texClassifiers: Map[ClGroup, Iterable[ComposedClassifierModel]] = clGroups.map(p =>
    p -> AppConf.composedClassifiers
  ).toMap

  private val numOfClassifiers = clGroups.size * texClassifiers.size

  private var texClassifierShadows: Option[Map[ClGroup, Iterable[String]]] = None

  /** train all classifiers and store shadows of trained classifiers */
  private def _train(controller: IBackgroundMonitor): AnyRef = {
    texClassifierShadows = None
    val classifierShadows = mutable.Map.empty[ClGroup, Iterable[String]]
    controller.updateProgress(0, "Training classifiers...", "0%")
    for (entry <- texClassifiers) {
      val clGroup = entry._1
      println(clGroup)
      val classifierModels = entry._2
      println(classifierModels.mkString("\n"))
      val data = getXVectors(clGroup.cut, clGroup.rad, clGroup.texParam._1, clGroup.texParam._2, clGroup.texParam._3, clGroup.removeOutliers, SampleCategory.TRAIN_SET)
      println(data.size)
      if (!controller.isWorkerCancelled) {
        val classifiers = classifierModels.map(conf => ComposedClassifier(conf, classLabels, attributes))
        val shadows = classifiers.map(cl => exportClassifier(cl.train(completeVectors(data))))
        classifierShadows += clGroup -> shadows
        System.gc()
        controller.updateProgressInc(100.0 / numOfClassifiers)
      }
    }
    texClassifierShadows = Some(classifierShadows.toMap)
    null
  }

  /** test all classifiers and from the stored shadows */
  private def _test(controller: IBackgroundMonitor): AnyRef = {
    if (texClassifierShadows.isEmpty) {
      logger.warn("primal classifier is not trained")
    } else {
      val startTime = TimeManager.getTimeInMillis
      controller.updateProgress(0, "Testing classifiers...", "0%")
      val clResCollection = dataManager.getClResults
      clResCollection.clearAll()
      for (entry <- texClassifierShadows.get) {
        val clGroup = entry._1
        val shadows = entry._2
        val data = getXVectors(clGroup.cut, clGroup.rad, clGroup.texParam._1, clGroup.texParam._2, clGroup.texParam._3,
          AppConf.removeOutliersFromTestSet, SampleCategory.TEST_SET)
        for (shadow <- shadows) {
          if (!controller.isWorkerCancelled) {
            val classifier = importClassifier(shadow)
            data.map(v => classifier.test(v, overridedTag = clGroup.toString + "_" + classifier.toShallowShadow))
              .foreach(clResCollection.addTexClResult)
            if (AppConf.performance.deleteClassifierCacheAfterUsage) cleanClassifier(shadow)
            controller.updateProgressInc(100.0 / numOfClassifiers)
          }
        }
      }
      clResCollection.getAllTexClassifierTags
      logger.info("Testing primal classifier finished in " + TimeManager.getTimeInSecondsSince(startTime))
      notifyXEventListeners(new ClassificationXEvent.ClassifiersUpdated(this, clResCollection.getAllTexClassifierTags))
    }
    null
  }

  override def train(): Unit = backgroundFactory.createWizard(majorV).addTaskAndStart(gt_train())

  override def test(): Unit = backgroundFactory.createWizard(majorV).addTaskAndStart(gt_test())

  override def gt_train(): XBackgroundTask = new XBackgroundTask(ScalaOps.toJavaFunction(_train))

  override def gt_test(): XBackgroundTask = new XBackgroundTask(ScalaOps.toJavaFunction(_test))

  override def registerXEventListener(event_t: Class[_ <: XEvent], listener: XEventListener): Unit =
    xbm.registerXEventListener(event_t, listener)

  override def unregisterXEventListener(event_t: Class[_ <: XEvent], listener: XEventListener): Unit =
    xbm.unregisterXEventListener(event_t, listener)

  private def notifyXEventListeners(event: XEvent): Unit = xbm.notifyXEventListeners(event)

  private def completeVectors(vectors: Iterable[XVector]): Iterable[(XVector, String)] = {
    val diagnosisDeterminator = new DiagnosisDeterminator(dataManager)
    vectors.map(v => (v, diagnosisDeterminator.getLabel(new TexID(v.sysVals), AppConf.diagnostics.disease, AppConf.diagnostics.score).get))
  }

  private def getXVectors(cut: LiverCutType, rad: Int, quantLevels: Int, ipm: IPM, iam: IAM,
                          removeOutliers: Boolean = false, sc: SampleCategory): Iterable[XVector] = {
    import collection.JavaConverters._
    val data = dataManager.getMeasurements
    val samples = dataManager.getSamples
    val vectors = data.getXVectorsKeysAsList.asScala.toList
      .filter(key => key.cut == cut && key.quant == quantLevels
        && key.ipm == ipm && key.iam == iam
        && samples.getCategory(key.pid, key.sid, key.rid) == sc)
      .map(key => data.getXVector(key).get())
    if (removeOutliers)
      vectors.filter(!primalOutlierRemover.checkRemoved(_))
    else
      vectors
  }

  private def exportClassifier(classifier: ComposedClassifier): String = {
    val compressedName = classifier.toDeepShadow.hashCode.toString
    dataManager.getObjectStorage.exportInstance(classifier, compressedName)
    compressedName
  }

  private def importClassifier(shadow: String): ComposedClassifier = {
    dataManager.getObjectStorage.importInstance(classOf[ComposedClassifier], shadow)
  }

  private def cleanClassifier(shadow: String): Unit = dataManager.getObjectStorage.deleteInstance(shadow)

  override def getAllClassifierShadows: java.lang.Iterable[String] = {
    import scala.collection.JavaConverters._
    if (texClassifierShadows.nonEmpty) {
      val q = texClassifierShadows.get
      val map: Iterable[String] = q.flatMap(p => p._2)
      map.asJava
    } else {
      List.empty[String].asJava
    }
  }
}