package lfa.cl;

import base.events.XEvent;
import base.events.XEventBroadcaster;

//**************************************************************************************************
public class OutlierRemoverUpdateXEvent extends XEvent {

    public OutlierRemoverUpdateXEvent(XEventBroadcaster sender) {
        super(sender);
    }

}
