package lfa.cl;

import com.google.inject.AbstractModule;

//**************************************************************************************************
public class ClassificationModule extends AbstractModule {

    //==============================================================================================
    @Override
    protected void configure() {
        bind(IPrimalClassifier.class).to(ScalaPrimalClassifier.class).asEagerSingleton();
        bind(IPrimalEvaluator.class).to(ScalaPrimalEvaluator.class).asEagerSingleton();
        bind(IPrimalOutlierRemover.class).to(ScalaPrimalOutlierRemover.class).asEagerSingleton();
    }
}
