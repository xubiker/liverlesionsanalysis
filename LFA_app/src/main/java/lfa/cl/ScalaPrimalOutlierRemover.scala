package lfa.cl
import java.util

import base.ScalaOps
import base.backgroundexecution.{IBackgroundFactory, IBackgroundMonitor, XBackgroundTask}
import base.events.{XEvent, XEventFactory, XEventListener}
import base.time.TimeManager
import com.google.inject.Inject
import com.typesafe.scalalogging.LazyLogging
import lfa.AppConf
import lfa.datamanager.IDataManager
import lfa.image.{ImageAdjustMethod => IAM, ImagePreprocessMethod => IPM}
import lfa.major.Major_V
import lfa.measurement.TexID
import xubiker.XLearnTools.base.XVector
import xubiker.XLearnTools.outliers.XOutlierRemover

import scala.collection.mutable

class ScalaPrimalOutlierRemover @Inject()(dataManager: IDataManager,
                                backgroundFactory: IBackgroundFactory,
                                majorV: Major_V
                               ) extends IPrimalOutlierRemover with LazyLogging {

  case class TParams(q: Int, ipm: IPM, iam: IAM) {}

  private val xbm = XEventFactory.createBroadcaster

  private val removed = mutable.Set.empty[TexID]

  override def checkRemoved(measurement: XVector): Boolean =
    removed.contains(new TexID(measurement.sysVals))

  override def checkRemoved(pid: Int, sid: Int, rid: Int): util.List[TexID] = {
    import collection.JavaConverters._
    removed.filter(i => i.pid == pid && i.sid == sid && i.rid == rid).toList.asJava
  }

  override def checkRemovedCount(pid: Int, sid: Int, rid: Int): Int =
    removed.count(i => i.pid == pid && i.sid == sid && i.rid == rid)

  private def _process(tp: TParams, rad: Int, controller: IBackgroundMonitor, totalNumOfVectors: Int) = {
    import scala.collection.JavaConversions._
    val measurements = dataManager.getMeasurements
    val removeModel = AppConf.outlierRemoverModel
    dataManager.getPatients.toList.toList.par.foreach(p => {
      val pid = p.getId
      val numOfProcessedVectors = p.getUltrasound.toList.toList.map(s => {
        val sid = s.getId
        val vectors: List[XVector] = measurements.getXVectorsKeysAsList.toList
          .filter(k => k.pid == pid && k.sid == sid && k.rad == rad && k.quant == tp.q && k.ipm == tp.ipm && k.iam == tp.iam)
          .map(key => measurements.getXVector(key).get)
        val removedVectors: Iterable[XVector] = if (vectors.nonEmpty) XOutlierRemover.remove(removeModel, vectors)._2 else List.empty[XVector]
        removedVectors.foreach(v => removed += new TexID(v.sysVals))
        vectors.length
      }).sum
      controller.updateProgressInc(100.0 * numOfProcessedVectors / totalNumOfVectors)
    })
  }

  private def _processAll(controller: IBackgroundMonitor): AnyRef = {
    import scala.collection.JavaConversions._
    val totalNumOfVectors = dataManager.getMeasurements.size()
    val rads: List[Int] = dataManager.getRoiCollection.getRois.toList.map(_.getRadius).distinct
    val tParams: List[TParams] = dataManager.getMeasurements.getXVectorsKeysAsList.toList.map(key => TParams(key.quant, key.ipm, key.iam)).distinct
    logger.info(s"Removing outliers. Num of vectors: $totalNumOfVectors")
    controller.updateProgress(0, "Removing outliers...", "0%")
    val timeInMillis = TimeManager.getTimeInMillis
    rads.foreach(rad => tParams.foreach(tp => _process(tp, rad, controller, totalNumOfVectors)))
    val t = TimeManager.convertMillisToSeconds(TimeManager.getTimeInMillis - timeInMillis)
    logger.info(s"done in $t seconds")
    logger.info(s"totally removed ${removed.size} from $totalNumOfVectors elements")
    xbm.notifyXEventListeners(new OutlierRemoverUpdateXEvent(this))
    null
  }

  override def processAll(): Unit =
    backgroundFactory.createWizard(majorV).addTaskAndStart(gt_processAll)

  override def gt_processAll: XBackgroundTask = new XBackgroundTask(ScalaOps.toJavaFunction(_processAll))

  override def registerXEventListener(event_t: Class[_ <: XEvent], listener: XEventListener): Unit = xbm.registerXEventListener(event_t, listener)

  override def unregisterXEventListener(event_t: Class[_ <: XEvent], listener: XEventListener): Unit = xbm.unregisterXEventListener(event_t, listener)

}
