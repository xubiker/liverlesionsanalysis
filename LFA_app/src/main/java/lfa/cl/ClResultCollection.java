package lfa.cl;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import lfa.measurement.TexID;
import xubiker.XLearnTools.classify.ClResult;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

//**************************************************************************************************
public class ClResultCollection {

    private final Table<String, TexID, ClResult> texTable = HashBasedTable.create();

    public void clearAll() {
        texTable.clear();
    }

    public void addTexClResult(ClResult result) throws Exception {
        texTable.put(result.tag(), new TexID(result.xVectorSysVals()), result);
    }

    public List<ClResult> getTexClResult(String classifierTag) {
        return texTable.row(classifierTag).values().stream().collect(Collectors.toList());
    }

    public List<ClResult> getTexClResult(int pid, int sid, int rid, String classifierTag) {
        Map<TexID, ClResult> row = texTable.row(classifierTag);
        return row.keySet().stream()
                .filter(key -> key.pid() == pid && key.sid() == sid && key.rid() == rid)
                .map(row::get)
                .filter(v -> v.tag().equals(classifierTag))
                .collect(Collectors.toList());
    }

    public Collection<ClResult> getAllTexClResults() {
        return texTable.values();
    }

    public List<String> getAllTexClassifierTags() {
        return texTable.rowKeySet().stream().collect(Collectors.toList());
    }

}
