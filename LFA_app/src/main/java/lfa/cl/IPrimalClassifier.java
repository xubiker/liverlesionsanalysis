package lfa.cl;

import base.backgroundexecution.XBackgroundTask;
import base.events.XEventBroadcaster;

//**************************************************************************************************
public interface IPrimalClassifier extends XEventBroadcaster {

    void train();
    void test();
    XBackgroundTask gt_train();
    XBackgroundTask gt_test();
    Iterable<String> getAllClassifierShadows();

//    void exportWekaInstances();

//    boolean isTested();
}
