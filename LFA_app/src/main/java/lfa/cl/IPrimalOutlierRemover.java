package lfa.cl;

import base.backgroundexecution.XBackgroundTask;
import base.events.XEventBroadcaster;
import java.util.List;

import lfa.measurement.TexID;
import xubiker.XLearnTools.base.XVector;

//**************************************************************************************************
public interface IPrimalOutlierRemover extends XEventBroadcaster {

    void processAll();
    XBackgroundTask gt_processAll();

    List<TexID> checkRemoved(int pid, int sid, int rid);
    boolean checkRemoved(XVector measurement);
    int checkRemovedCount(int pid, int sid, int rid);
}
