package lfa.measurement;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import xubiker.XLearnTools.base.XVector;

import java.io.IOException;

public class XVector_TA extends TypeAdapter<XVector> {

    @Override
    public void write(JsonWriter out, XVector v) throws IOException {
        out.beginObject();
        out.name("tag").value(v.tag());
        out.name("numOfSysVals").value(v.sysVals().length);
        out.name("sysVals").beginArray();
        for (String f : v.sysVals()) {
            out.value(f);
        }
        out.endArray();
        out.name("numOfVals").value(v.vals().length);
        out.name("vals").beginArray();
        for (double val : v.vals()) {
            out.value(val);
        }
        out.endArray();
        out.endObject();
    }

    @Override
    public XVector read(JsonReader in) throws IOException {

        in.beginObject();
        if (!in.nextName().equals("tag")) throw new IOException("Parse error");
        String tag = in.nextString();
        if (!in.nextName().equals("numOfSysVals")) throw new IOException("Parse error");
        int numOfSysVals = in.nextInt();
        String[] sysVals = new String[numOfSysVals];
        if (!in.nextName().equals("sysVals")) throw new IOException("Parse error");
        in.beginArray();
        for (int i = 0; i < numOfSysVals; i++) {
            sysVals[i] = in.nextString();
        }
        in.endArray();

        if (!in.nextName().equals("numOfVals")) throw new IOException("Parse error");
        int numOfVals = in.nextInt();
        if (!in.nextName().equals("vals")) throw new IOException("Parse error");
        double[] vals = new double[numOfVals];
        in.beginArray();
        for (int i = 0; i < numOfVals; i++) {
            vals[i] = in.nextDouble();
        }
        in.endArray();

        in.endObject();

        return new XVector(sysVals, vals, tag);
    }
}
