package lfa.measurement;

import base.backgroundexecution.XBackgroundTask;

//**************************************************************************************************
public interface IMeasurementProcessor {

    void performPreprocessing();
    
    void performMeasurements();

    XBackgroundTask gt_performMeasurements();

}
