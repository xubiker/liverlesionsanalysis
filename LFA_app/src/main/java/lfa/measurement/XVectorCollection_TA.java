package lfa.measurement;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import lfa.AppConf;
import xubiker.XLearnTools.base.XVector;

import java.io.IOException;

public class XVectorCollection_TA extends TypeAdapter<XVectorCollection> {

    private final XVector_TA xVector_TA = new XVector_TA();

    @Override
    public XVectorCollection read(JsonReader in) throws IOException {
        in.beginObject();
        if (!in.nextName().equals("numOfAttributes")) throw new IOException("Parse error");
        int numOfAttributes = in.nextInt();
        if (!in.nextName().equals("attributes")) throw new IOException("Parse error");
        String[] attributes = new String[numOfAttributes];
        in.beginArray();
        for (int i = 0; i < numOfAttributes; i++) {
            attributes[i] = in.nextString();
        }
        in.endArray();
        if (!in.nextName().equals("size")) throw new IOException("Parse error");
        int size = in.nextInt();
        if (!in.nextName().equals("measurements")) throw new IOException("Parse error");
        XVectorCollection coll = new XVectorCollection(size);
        in.beginArray();
        for (int i = 0; i < size; i++) {
            XVector m = xVector_TA.read(in);
            coll.addXVector(m);
        }
        in.endArray();
        in.endObject();
        //coll.setAttributes(new XAttributeLabels(attributes));
        return coll;
    }

    @Override
    public void write(JsonWriter out, XVectorCollection coll) throws IOException {
        out.beginObject();
        out.name("numOfAttributes").value(AppConf.texFeatureAttributes().length());
        out.name("attributes").beginArray();
        for (String attr: AppConf.texFeatureAttributes().asJavaList()) {
            out.value(attr);
        }
        out.endArray();
        out.name("size").value(coll.size());
        out.name("measurements").beginArray();
        for (XVector xv : coll.getXVectorsAsList()) {
            xVector_TA.write(out, xv);
        }
        out.endArray();
        out.endObject();
    }

}
