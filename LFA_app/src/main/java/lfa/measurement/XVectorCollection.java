package lfa.measurement;

import com.google.gson.TypeAdapter;
import lfa.storage.IGsonConvertible;
import xubiker.XLearnTools.base.XAttributeLabels;
import xubiker.XLearnTools.base.XVector;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

//**********************************************************************************************************************
public class XVectorCollection implements IGsonConvertible {

    private ConcurrentMap<TexID, XVector> measurements = new ConcurrentHashMap<>();

    public XVectorCollection() {}

    XVectorCollection(int numOfMeasurements) {
        measurements = new ConcurrentHashMap<>(numOfMeasurements);
    }

    public void clear() {
        measurements.clear();
    }

    public void addXVector(XVector v) {
        measurements.put(new TexID(v.sysVals()), v);
    }

    public Optional<XVector> getXVector(TexID key) {
        return Optional.ofNullable(measurements.get(key));
    }

    public List<XVector> getXVectors(Predicate<TexID> keyPredicate) {
        return measurements.keySet().stream()
                .filter(keyPredicate)
                .map(measurements::get)
                .collect(Collectors.toList());
    }

    List<XVector> getXVectorsAsList() {
        return measurements.values().stream().collect(Collectors.toList());
    }

    public int size() {
        return measurements.size();
    }

    public List<TexID> getXVectorsKeysAsList() {
        return measurements.keySet().stream().collect(Collectors.toList());
    }

    @Override
    public Class<? extends TypeAdapter> getGsonTypeAdapter() {
        return XVectorCollection_TA.class;
    }
}
