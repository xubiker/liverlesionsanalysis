package lfa.measurement

import lfa.data.LiverCutType
import lfa.image.{ImageAdjustMethod, ImagePreprocessMethod}

case class TexID(pid: Int, sid: Int, rid: Int,
                 rad: Int, cut: LiverCutType,
                 quant: Int, ipm: ImagePreprocessMethod, iam: ImageAdjustMethod) {

  def toArray: Array[String] =
    Array[String](pid.toString, sid.toString, rid.toString, rad.toString, cut.toString,
      quant.toString, ipm.toString, iam.toString)

  def this(data: Array[String]) = this(
    data(0).toInt,
    data(1).toInt,
    data(2).toInt,
    data(3).toInt,
    LiverCutType.valueOf(data(4)),
    data(5).toInt,
    ImagePreprocessMethod.valueOf(data(6)),
    ImageAdjustMethod.valueOf(data(7))
  )

}
