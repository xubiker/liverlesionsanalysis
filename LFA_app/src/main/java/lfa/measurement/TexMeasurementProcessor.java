package lfa.measurement;

import base.backgroundexecution.*;
import base.collections.Pair;
import base.collections.Triplet;
import base.time.TimeManager;
import com.google.inject.Inject;
import lfa.AppConf;
import lfa.image.ImageAdjustMethod;
import lfa.image.ImagePreprocessMethod;
import lfa.data.*;
import lfa.datamanager.IDataManager;
import lfa.major.Major_V;
import xubiker.XImageTools.base.XImage;
import xubiker.XImageTools.base.roi.SqRoi;
import xubiker.XLearnTools.base.XVector;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//**************************************************************************************************
class TexMeasurementProcessor implements IMeasurementProcessor {

    private final IDataManager dataManager;
    private final IBackgroundFactory backgroundFactory;
    private final Major_V majorV;
    
    private Set<SampleCategory> categories;
    private Set<Triplet<Integer, ImagePreprocessMethod, ImageAdjustMethod>> texParams;
    private Set<Integer> rads;

    private ScalaImagePreprocessor scalaPreprocessor;

    //==============================================================================================
    @Inject
    public TexMeasurementProcessor(
            IDataManager dataManager,
            IBackgroundFactory backgroundFactory,
            Major_V majorV
    ) {
        this.dataManager = dataManager;
        this.backgroundFactory = backgroundFactory;
        this.majorV = majorV;
        scalaPreprocessor = new ScalaImagePreprocessor(dataManager);
        initialize();
    }
    
    //==============================================================================================
    private void initialize() {
        texParams = AppConf.texParamsJ().stream().collect(Collectors.toSet());
        categories = AppConf.preprocessCategoriesJ().stream().collect(Collectors.toSet());
        rads = AppConf.radsJ().stream().collect(Collectors.toSet());
    }

    //==============================================================================================
    @Override
    public void performMeasurements() {
        IBackgroundWizard wizard = backgroundFactory.createWizard(majorV);
        wizard.addTaskAndStart(gt_performMeasurements());
    }

    //==============================================================================================
    @Override
    public XBackgroundTask gt_performMeasurements() {
        return new XBackgroundTask(controller -> {
            initialize();
            dataManager.getMeasurements().clear();
            gt_performAllTexMeasurements(categories, rads, texParams).get().apply(controller);
            return null;
        });
    }

    //==============================================================================================
    @Override
    public void performPreprocessing() {
        scalaPreprocessor.preprocessEverything(categories, rads, texParams);
    }
    
    //==============================================================================================
    private XBackgroundTask gt_performAllTexMeasurements(
            Set<SampleCategory> cats,
            Set<Integer> rads,
            Set<Triplet<Integer, ImagePreprocessMethod, ImageAdjustMethod>> params
    ) {
            return new XBackgroundTask(controller -> {
            long startTime = TimeManager.getTimeInMillis();

            PatientCollection patients = dataManager.getPatients();
            SampleCollection samples = dataManager.getSamples();
            RoiCollection roiColl = dataManager.getRoiCollection();
            XVectorCollection measColl = dataManager.getMeasurements();

            // --------------- pid-sid-rad to preprocessAndAdjustImage ---------------
            List<Triplet<Integer, Integer, Integer>> pidSidRads = patients.toList().stream()
                    .flatMap(p -> p.getUltrasound().toList().stream().map(s -> new Pair<>(p.getId(), s.getId())))
                    .flatMap(pair -> {
                        int pid = pair.getFirst();
                        int sid = pair.getSecond();
                        Stream<Integer> allRads = roiColl.getRois(pid, sid).stream().map(Roi::getRadius).distinct().filter(rads::contains);
                        Stream<Integer> apprRads = allRads.filter(rad
                                -> roiColl.getRois(pid, sid).stream()
                                        .filter(r -> r.getRadius() == rad)
                                        .map(r -> samples.getCategory(pid, sid, r.getId()))
                                        .anyMatch(cats::contains)
                        );
                        return apprRads.map(rad -> new Triplet<>(pid, sid, rad));
                    })
                    .collect(Collectors.toList());
            int numOfPreprocess = pidSidRads.size() * params.size();
            System.out.println("numOfPreprocess: " + numOfPreprocess);
            
            // --------------- num of measurements to process ---------------
            int numOfMeasurements = (int) patients.toList().stream().mapToLong(p -> {
                return p.getUltrasound().toList().stream().mapToLong(s -> {
                    return roiColl.getRois(p.getId(), s.getId()).stream()
                            .filter(roi -> rads.contains(roi.getRadius()))
                            .map(r -> samples.getCategory(p.getId(), s.getId(), r.getId()))
                            .filter(cats::contains)
                            .count();
                }).sum();
            }).sum();
            numOfMeasurements *= params.size();
            System.out.println("numOfMeasurements: " + numOfMeasurements);

            
            AtomicInteger counter = new AtomicInteger(0);

            //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
            pidSidRads.forEach(pidSidRad -> {
                //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
                if (controller.isWorkerCancelled()) return;
                int pid = pidSidRad.getFirst();
                int sid = pidSidRad.getSecond();
                int rad = pidSidRad.getThird();
                Set<Roi> rois = roiColl.getRois(pid, sid).stream()
                        .filter(r -> r.getRadius() == rad)
                        .collect(Collectors.toSet());

                int initProgress = 100 * counter.get() / numOfPreprocess;
                controller.updateProgress(initProgress, "performing tex measurements (patient " + pid + ", snapshot " + sid + ")", null);
                params.forEach(param -> {
                //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
                    if (controller.isWorkerCancelled()) return;
                    XImage img = scalaPreprocessor.loadPreprocessed(pid, sid, rois, param.getSecond());
                    img = scalaPreprocessor.adjust(img, rois, param.getThird(), false);
                    performImageTexMeasurements(pid, sid, rois, param, cats, img, controller);
                    System.gc();
                    System.out.println("MeasurementCollection texMeasurementsCnt: " + measColl.size());
                    int counterVal = counter.incrementAndGet();
                    int progress = 100 * counterVal / numOfPreprocess;
                    controller.updateProgress(progress, null, null);
                });
            });
            
            double timeDuration = TimeManager.getTimeInSecondsSince(startTime);
            System.out.println("measurement processing finished in: " + timeDuration);
            return null;
        });
    }

    //==============================================================================================
    private void performImageTexMeasurements(
            int pid, int sid, Set<Roi> rois,
            Triplet<Integer, ImagePreprocessMethod, ImageAdjustMethod> params, Set<SampleCategory> cats,
            XImage preprocessed,
            IBackgroundMonitor controller
    ) {
        LiverCutType cut = dataManager.getPatients().toMap().get(pid).getUltrasound().toMap().get(sid).getCutType();
        XVectorCollection measColl = dataManager.getMeasurements();
        SampleCollection samples = dataManager.getSamples();
        cats.forEach(cat -> {
            Stream<Roi> rRois = rois.stream().filter(r -> samples.getCategory(pid, sid, r.getId()) == cat);
            rRois.forEach(r -> {
                TexID texId = new TexID(pid, sid, r.getId(), r.getRadius(), cut, params.getFirst(), params.getSecond(), params.getThird());
                if (controller.isWorkerCancelled()) return;
                Optional<XVector> m = measColl.getXVectors(key -> key.equals(texId)).stream().findFirst();
                if (!m.isPresent()) {
                    // need to recalculate
                    try {
                        scala.Tuple2<Object, Object> c = new scala.Tuple2<>(r.getCenterX(), r.getCenterY());
                        SqRoi rr = new SqRoi(r.getId(), c, r.getRadius(), r.getAlpha(), r.getLastUpdateTime());
                        XImage roiImg = XImage.XImageStatRoi(preprocessed).extractRoiImage(rr);
                        XVector v = RoiTexturalProcessor.roiFeatures(roiImg, texId);
                        measColl.addXVector(v);
                    } catch (Exception ex) {
                        System.err.println("error performing texture measurements: " + ex);
                    }
                }
            });
        });
    }

}
