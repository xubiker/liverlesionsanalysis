package lfa.measurement

import xubiker.XImageTools.base.XImage

class FeatureCalculator {

  def apply(i: XImage, featureLabels: Iterable[String], additionalParams: Double*): Iterable[Double] = {
    featureLabels.map {
      case "min" | "MIN" => i.min
      case "max" | "MAX" => i.max
      case "asmn" | "ASMN" | "energy" => i.asmn
      case "mean" | "MEAN" => i.mean
      case "sd" | "SD" => i.sd
      case "variation" | "var" | "VAR" => i.variance
      case "skewness" | "SKEW" => i.skewness
      case "kurtosis" | "KURT" => i.kurtosis
      case "contrast" | "CON" => i.GLCM_stats.contrast
      case "homogeneity" | "HOM" => i.GLCM_stats.homogeneity
      case "entropy" | "ENT" => i.GLCM_stats.entropy
      case "dissimilarity" | "DIS" => i.GLCM_stats.dissimilarity
      case "correlation" | "COR" => i.GLCM_stats.correlation
      case "SRE" => i.GLRLM_stats.SRE
      case "LRE" => i.GLRLM_stats.LRE
      case "LGRE" => i.GLRLM_stats.LGRE
      case "HGRE" => i.GLRLM_stats.HGRE
      case "SRLGE" => i.GLRLM_stats.SRLGE
      case "SRHGE" => i.GLRLM_stats.SRHGE
      case "LRLGE" => i.GLRLM_stats.LRLGE
      case "LRHGE" => i.GLRLM_stats.LRHGE
      case "GLNU" => i.GLRLM_stats.GLNU
      case "RLNU" => i.GLRLM_stats.RLNU
      case "RP" => i.GLRLM_stats.RP(additionalParams(0).toInt)
    }
  }
}
