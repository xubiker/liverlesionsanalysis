package lfa.measurement

import java.util.{Set => JSet}

import base.collections.Triplet
import batch.{BatchProcessor, Task}
import com.typesafe.scalalogging.LazyLogging
import lfa.AppConf
import lfa.data.{Patient, Roi, SampleCategory}
import lfa.datamanager.IDataManager
import lfa.image.{ImageAdjustMethod => IAM, ImagePreprocessMethod => IPM}
import xubiker.XImageTools.base.roi.SqRoi
import xubiker.XImageTools.base.{XColorSpace, XImage}
import xubiker.XImageTools.ultrasound.erf.{ERF_CorrectAlgorithm, ERF_RestoreAlgorithm}
import xubiker.XImageTools.ultrasound.speckle.{MNEController, SRAD}

import scala.collection.JavaConversions._
import scala.collection.mutable.ArrayBuffer

//**********************************************************************************************************************
class ScalaImagePreprocessor(dataManager: IDataManager) extends LazyLogging {

  private case class PreprocessTaskData(pid: Int, sid: Int, rois: Seq[Roi], ipm: IPM)

  def preprocessEverything(cats: JSet[SampleCategory], roiRads: JSet[Integer], params: JSet[Triplet[Integer, IPM, IAM]]): Unit = {
    val samples = dataManager.getSamples
    val roiCol = dataManager.getRoiCollection
    val patients: List[Patient] = dataManager.getPatients.toList.toList

    val pid_sid_pairs = patients.flatMap(p => p.getUltrasound.toList.toList.map(s => (p.getId, s.getId)))
    val pid_sid_rad_triplets: List[(Int, Int, Int)] = pid_sid_pairs.flatMap(ps => {
      val allRads = roiCol.getRois(ps._1, ps._2).toList.map(_.getRadius).distinct.filter(roiRads.contains(_))
      val appRads = allRads.filter(rad =>
        roiCol.getRois(ps._1, ps._2, rad).map(roi => samples.getCategory(ps._1, ps._2, roi.getId)).exists(cats.contains)
      )
      appRads.map((ps._1, ps._2, _))
    })

    val ipms = params.map(_.getSecond)

    val numOfPreprocess = pid_sid_rad_triplets.length * ipms.size
    logger.info(s"numOfPreprocess: $numOfPreprocess")

    // ---------- create preprocess tasks ----------
    val tasks = pid_sid_rad_triplets.flatMap(psr => {
      val rois: List[Roi] = roiCol.getRois(psr._1, psr._2).toList.filter(_.getRadius == psr._3)
      ipms.map(ipm => Task(PreprocessTaskData(psr._1, psr._2, rois, ipm), preprocessEach))
    })

    // ---------- perform preprocess tasks ----------
    val timeStart = System.currentTimeMillis()
//    tasks.foreach(task => task.method(task.data))
    BatchProcessor.process(tasks, AppConf.performance.numOfActors, timeoutPerTaskInSecs = 300, displayProgress = false)
    System.gc()
    val timeEnd = System.currentTimeMillis()
    logger.info(s"total time: ${(timeEnd - timeStart).toDouble / 1000 / 60} minutes")
  }

  private def preprocessEach(data: PreprocessTaskData): Unit = {
    logger.info(s"performing preprocess task (pid=${data.pid} sid=${data.sid} nRois=${data.rois.size})")
    System.out.flush()
    val cacheName = CacheNameManager.getPreprocessedCacheName(data.pid, data.sid, data.rois, data.ipm)
    // check if this image is already preprocessed
    if (dataManager.getImageCacheStorage.exists(cacheName, AppConf.cache.imageCacheType)) {
      logger.info(s"file $cacheName found in storage! No need to preprocess.")
      return
    }
    logger.info(s"file $cacheName not found in storage!")

    // TODO: fix hardcoded ranges
    val cropRowRange = 200 to 600
    val cropColRange = 250 to 550
    val restoreErfAlgo = new ERF_RestoreAlgorithm(ERF_RestoreAlgorithm.Params((cropRowRange, cropColRange), 10))
    val correctErfAlgo = new ERF_CorrectAlgorithm(ERF_CorrectAlgorithm.Params(20, 60))

    val src = dataManager.loadImage(data.pid, data.sid, true)
    val src_norm = correctErfAlgo(restoreErfAlgo(src))

    val tRois: Seq[xubiker.XImageTools.base.roi.Roi] = data.rois.map(r =>
      SqRoi(r.getId, (r.getCenterX, r.getCenterY), r.getRadius, r.getAlpha, r.getLastUpdateTime)
    )
    val mneCtrl1 = new MNEController(MNEController.Params(MNEController.StopRule.MaxEntropy(lag = 20)))
    val mneCtrl2 = new MNEController(MNEController.Params(MNEController.StopRule.MinEntropyDeriv(lag = 20)))
    val sradAuto = new SRAD(SRAD.Params(0.015/*0.002*/, iterationController = mneCtrl1))
    val sradAutoDer = new SRAD(SRAD.Params(0.015, iterationController = mneCtrl2))

    val preprocessed: XImage = data.ipm match {
      case IPM.None =>
        src
      case IPM.ERF =>
        src_norm
      case IPM.SradMne =>
        sradAuto((src / 256).exp, tRois).log * 256
      case IPM.SradMneDer =>
        sradAutoDer((src / 256).exp, tRois).log * 256
      case IPM.ErfSradMne =>
        sradAuto((src_norm / 256).exp, tRois).log * 256
      case IPM.ErfSradMneDer =>
        sradAutoDer((src_norm / 256).exp, tRois).log * 256
    }
    dataManager.getImageCacheStorage.exportImage(preprocessed, cacheName, AppConf.cache.imageCacheType, true)
  }

  def loadPreprocessed(pid: Int, sid: Int, rois: JSet[Roi], ipm: IPM): XImage = {
    val cacheName = CacheNameManager.getPreprocessedCacheName(pid, sid, rois, ipm)
    if (!dataManager.getImageCacheStorage.exists(cacheName, AppConf.cache.imageCacheType)) {
      logger.error(s"file $cacheName not found in storage!")
      return null
    }
    dataManager.loadImage(pid, sid, true)
  }

  def adjust(image: XImage, rois: JSet[Roi], adjustMethod: IAM, returnColor: Boolean = false): XImage = {

    val img = if (!image.is1Ch) image.toColorSpace(XColorSpace.RGB2GRAY) else image
    val res = if (rois.isEmpty) img else {
      val tRois = rois.toList
        .map(r => SqRoi(r.getId, (r.getCenterX, r.getCenterY), r.getRadius, r.getAlpha, r.getLastUpdateTime))
      val roiData = tRois.foldLeft(ArrayBuffer.empty[Double])((buff, r) => buff ++ img.extractRoiData(r))
      val min = roiData.min
      val max = roiData.max
      val mean = roiData.sum / roiData.length
      val std = Math.sqrt(roiData.foldLeft(0.0)((s, v) => s + (v - mean) * (v - mean)) / roiData.length)

      // ---------- apply linear transform ----------
      adjustMethod match {
        case IAM.MINMAX =>
          (img - min) * 255.0 / (max - min)
        case IAM.MEAN =>
          img - mean + 128
        case IAM.MEANSTD =>
          img * 30.0 / std - (mean * 30 / std) + 128
        case IAM.ORIGINAL =>
          img
        case _ =>
          throw new AssertionError(adjustMethod.name)
      }
    }
    if (returnColor) res.toColorSpace(XColorSpace.GRAY2RGB) else res

  }

}