package lfa.measurement;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

//**************************************************************************************************
public class MeasurementModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(IMeasurementProcessor.class).to(TexMeasurementProcessor.class).in(Singleton.class);
    }
    
}
