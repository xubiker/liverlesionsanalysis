package lfa.measurement

import com.typesafe.scalalogging.LazyLogging
import lfa.AppConf
import xubiker.XImageTools.algo.matrix.{GLCM, GLRLM}
import xubiker.XImageTools.base.XImage
import xubiker.XImageTools.filters.Laws
import xubiker.XImageTools.filters.wavelets.{WaveletTransform, WaveletType}
import xubiker.XLearnTools.base.{XAttributeLabels, XVector}

import scala.collection.mutable

/**
  * Created by xubiker on 21-Feb-17.
  */
object RoiTexturalProcessor extends LazyLogging {

  private val calculator = new FeatureCalculator()

  private val fosLabels = List("VAR", "ASMN", "SKEW", "KURT", "ENT")
  private val lawsLabels = List("MEAN", "SD", "SKEW", "KURT", "ASMN")
  private val glcmLabels = List("ASMN", "ENT", "CON", "HOM", "DIS", "COR")
  private val glrlmLabels = List("SRE", "LRE", "LGRE", "HGRE", "SRLGE", "SRHGE", "LRLGE", "LRHGE", "GLNU", "RLNU", "RP")
  private val waveletLabels = List("ASMN", "MEAN", "SD", "ENT", "CON", "HOM", "SKEW", "KURT")

  private val laws1D = Laws.Kernel.values
  private val lawsMasks: Iterable[(Laws.Kernel.I, Laws.Kernel.I)] = laws1D.indices.flatMap(i => (0 to i).map(j => (laws1D(i), laws1D(j))))
  private val glcmParams = for (i <- 0 to 9; j <- 0 to 9) yield (i, j)
  private val glrlmParams = GLRLM.Direction.values
  private val waveletIterations = 3
  private val waveletResNum = 1 + waveletIterations * 3
  private val waveletParams = List(
    (WaveletType.Haar, 2),
    (WaveletType.Daubechies, 4),
    (WaveletType.Daubechies, 6),
    (WaveletType.Daubechies, 8),
    (WaveletType.Coiflet, 6),
    (WaveletType.Symmlet, 4),
    (WaveletType.Symmlet, 5)
  )

  private val lawsLabelsAll = lawsMasks.flatMap(m => lawsLabels.map(lbl => "LAWS_" + m._1 + "_" + m._2 + "_" + lbl))
  private val glcmLabelsAll = glcmParams.flatMap(p => glcmLabels.map(lbl => "GLCM_" + p._1 + "_" + p._2 + "_" + lbl))
  private val glrlmLabelsAll = glrlmParams.flatMap(p => glrlmLabels.map(lbl => "GLRLM_" + p + "_" + lbl))
  private val waveletLabelsAll = (1 to waveletResNum).flatMap(i => waveletParams.flatMap(p => waveletLabels.map(lbl => "WV_" + p + "_" + i + "_" + lbl)))

  private val featureLabels = mutable.ListBuffer.empty[String]
  featureLabels ++= fosLabels
  if (AppConf.texture.use_laws) featureLabels ++= lawsLabelsAll
  if (AppConf.texture.use_glcm) featureLabels ++= glcmLabelsAll
  if (AppConf.texture.use_glrlm) featureLabels ++= glrlmLabelsAll
  if (AppConf.texture.use_wavelet) featureLabels ++= waveletLabelsAll

  val attributes = XAttributeLabels(featureLabels.toList)

  def roiFeatures(roiImg: XImage, texId: TexID): XVector = {
    import collection.JavaConverters._
    val quantLevels = texId.quant
    val roiImgQ = XImage.XImageOps(roiImg).quantize(0, 255, quantLevels, saveBright = false)
    val features = mutable.ListBuffer.empty[Double]
    // --- FOS features:
    features ++= calculator(roiImgQ, fosLabels)
    // --- LAWS features:
    if (AppConf.texture.use_laws) {
      val laws = new Laws(null)
      features ++= lawsMasks.flatMap(mask => calculator(laws(Laws.Params(mask._1, mask._2))(roiImgQ), lawsLabels))
    }
    // --- GLCM features:
    if (AppConf.texture.use_glcm) {
      val glcm = new GLCM(null)
      glcmParams.foreach(p => {
        val matrix = glcm(GLCM.Params(texId.quant, p._1, p._2))(roiImgQ)
        calculator(new XImage(matrix), glcmLabels)
      })
    }
    // --- GLRLM features:
    if (AppConf.texture.use_glrlm) {
      val glrlm = new GLRLM(null)
      features ++= glrlmParams.flatMap(p =>
        calculator(new XImage(glrlm(GLRLM.Params(p, quantLevels))(roiImgQ)), glrlmLabels, roiImgQ.total)
      )
    }
    // --- wavelet features:
    if (AppConf.texture.use_wavelet) {
      val wvFeatures = waveletParams.flatMap(wp => {
        val wavelets = WaveletTransform.performWithExtAndSplit(roiImgQ, wp._1, wp._2, waveletIterations, false).asScala.toList
        wavelets.flatMap(wv => calculator(wv, waveletLabels))
      })
      features ++= wvFeatures
    }
    XVector(texId.toArray, features.toArray)
  }

}
