package lfa.measurement;

import lfa.image.ImagePreprocessMethod;
import lfa.data.Roi;

import java.util.Collection;
import java.util.Comparator;
import java.util.stream.Collectors;

public class CacheNameManager {

    public static String getPreprocessedCacheName(int pid, int sid, Collection<Roi> rois,
                                                  ImagePreprocessMethod ipm) {

        //int roisHash = rois.hashCode();
        int roisHash = rois.stream().sorted(Comparator.comparingInt(Roi::getId)).collect(Collectors.toList()).hashCode();
        return String.valueOf(pid) + "_" + sid + "_" + roisHash + "_" + ipm;
    }
}
