package lfa

import base.backgroundexecution.XBackgroundModule
import base.logging.LoggingModule
import base.resource.ResourceLoaderModule
import com.google.inject.Guice
import lfa.cl.ClassificationModule
import lfa.colortags.ColorTagModule
import lfa.datamanager.{DataManagerModule, IDataManager}
import lfa.diagnosis.DiagnosisDeterminatorModule
import lfa.gui.dialog.DialogModule
import lfa.gui.evalresult.EvalResult_Module
import lfa.keyprocessor.KeyProcessor_Module
import lfa.major.{IMajor_VC, IMajor_VM, Major_Module, Major_V}
import lfa.measurement.MeasurementModule
import lfa.roiStatistics.RoiStatistics_Module
import lfa.roilayouteditor.RoiLayout_Module
import lfa.storage.StorageModule

import scala.collection.JavaConverters._

object ManualRun extends App {

  val modules = List(
    new ResourceLoaderModule, new StorageModule, new DialogModule,
    new XBackgroundModule, new ColorTagModule, new DataManagerModule,
    new ClassificationModule, new KeyProcessor_Module, new RoiLayout_Module,
    new Major_Module, new RoiStatistics_Module, new MeasurementModule,
    new DiagnosisDeterminatorModule, new EvalResult_Module, new LoggingModule)

  VisualStyleManager.applyDefaultSystemVisualStyle()
  val injector = Guice.createInjector(modules.asJava)
  val majorVM = injector.getInstance(classOf[IMajor_VM])
  val majorV = injector.getInstance(classOf[Major_V])
  majorVM.setView(majorV)
  majorV.setViewModel(majorVM.asInstanceOf[IMajor_VC])
  majorVM.run()

  val dataManager = injector.getInstance(classOf[IDataManager])
  if (AppConf.manualRun.patientsPath.isDefined)
    dataManager.loadPatients(AppConf.manualRun.patientsPath.get)
  if (AppConf.manualRun.roisPath.isDefined)
    dataManager.loadRois(AppConf.manualRun.roisPath.get)
  if (AppConf.manualRun.samplePath.isDefined)
    dataManager.loadSamples(AppConf.manualRun.samplePath.get)

}
