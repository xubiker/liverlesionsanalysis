package lfa.events;

import base.events.XEvent;
import base.events.XEventBroadcaster;

public class PatientSwitchXEvent extends XEvent {
    
    public static enum SwitchDirection {
        NEXT,
        PREV,
        NEXT_5,
        PREV_5,
        FIRST,
        LAST
    }
    
    private final SwitchDirection switchDirection;
    
    public PatientSwitchXEvent(XEventBroadcaster sender, SwitchDirection switchDirection) {
        super(sender);
        this.switchDirection = switchDirection;
    }
    
    public SwitchDirection getSwitchDirection() {
        return switchDirection;
    }
        
}
