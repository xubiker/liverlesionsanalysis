package lfa.events;

import base.events.XEvent;
import base.events.XEventBroadcaster;
import lfa.image.ImageAdjustMethod;

//**************************************************************************************************
public class ImageAdjustXEvent extends XEvent {
    
    private final ImageAdjustMethod adjustMethod;
    
    public ImageAdjustXEvent(XEventBroadcaster sender, ImageAdjustMethod method) {
        super(sender);
        this.adjustMethod = method;
    }
    
    public ImageAdjustMethod getImageAdjustMethod() {
        return adjustMethod;
    }
    
}
