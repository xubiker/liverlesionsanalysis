package lfa.events;

import base.events.XEvent;
import base.events.XEventBroadcaster;
import base.events.XEventExtension;
import lfa.data.LiverCutType;

//**************************************************************************************************
public class SnapshotModifiedXEvent extends XEvent {
    
    public SnapshotModifiedXEvent(XEventBroadcaster sender) {
        super(sender);
    }
    
    //==============================================================================================
    public static class CutTypeChanged extends XEventExtension {
        
        private final LiverCutType cutType;
        
        public CutTypeChanged(XEventBroadcaster sender, LiverCutType cutType) {
            super(sender, SnapshotModifiedXEvent.class);
            this.cutType = cutType;
        }
        
        public LiverCutType getCutType() {
            return cutType;
        }
    }
        
}
