package lfa.events;

import base.events.XEvent;
import base.events.XEventBroadcaster;

//**************************************************************************************************
public class MouseXEvent extends XEvent {

    public static enum MouseAction {
        DRAGGED,
        MOVED,
        PRESSED,
        RELEASED
    }

    private final MouseAction action;
    private final int x, y;

    //==============================================================================================
    public MouseXEvent(XEventBroadcaster sender, int x, int y, MouseAction action) {
        super(sender);
        this.x = x;
        this.y = y;
        this.action = action;
    }

    //==============================================================================================
    public int getX() {
        return x;
    }

    //==============================================================================================
    public int getY() {
        return y;
    }
    
    //==============================================================================================
    public MouseAction getAction() {
        return action;
    }

}
