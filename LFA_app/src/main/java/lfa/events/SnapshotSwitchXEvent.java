package lfa.events;

import base.events.XEvent;
import base.events.XEventBroadcaster;

//**************************************************************************************************
public class SnapshotSwitchXEvent extends XEvent {
    
    public static enum SwitchDirection {
        NEXT,
        PREV
    }
    
    private final SwitchDirection switchDirection;
    
    public SnapshotSwitchXEvent(XEventBroadcaster sender, SwitchDirection switchDirection) {
        super(sender);
        this.switchDirection = switchDirection;
    }
    
    public SwitchDirection getSwitchDirection() {
        return switchDirection;
    }
        
}
