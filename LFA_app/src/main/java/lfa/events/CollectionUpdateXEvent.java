package lfa.events;

import base.collections.Pair;
import base.collections.Triplet;
import base.events.XEvent;
import base.events.XEventBroadcaster;
import base.events.XEventExtension;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

//**************************************************************************************************
public class CollectionUpdateXEvent<T> extends XEvent {

    //******************************************************************************
    CollectionUpdateXEvent(XEventBroadcaster sender) {
        super(sender);
    }
    
    //******************************************************************************
    public static class AdditionalInfoPSR {
        private int pid, sid, rid;
        
        public AdditionalInfoPSR(int pid, int sid, int rid) {
            this.pid = pid;
            this.sid = sid;
            this.rid = rid;
        }
        
        public int getPid() {
            return pid;
        }
        
        public int getSid() {
            return sid;
        }
        
        public int getRid() {
            return rid;
        }
    };
    
    //******************************************************************************
    public static class ElementsAddedWithInfo<T, I> extends XEventExtension {
        
        private final List<Pair<T, I>> addedElements;
        private final Class<?> dataType, infoType;
        
        public ElementsAddedWithInfo(XEventBroadcaster sender, List<Pair<T, I>> addedElements) {
            super(sender, CollectionUpdateXEvent.class);
            this.addedElements = addedElements;
            this.dataType = addedElements.iterator().next().getFirst().getClass();
            this.infoType = addedElements.iterator().next().getSecond().getClass();
        }

        public ElementsAddedWithInfo(XEventBroadcaster sender, T element, I info) {
            super(sender, CollectionUpdateXEvent.class);
            this.addedElements = Arrays.asList(new Pair<>(element, info));
            this.dataType = element.getClass();
            this.infoType = info.getClass();
        }

        public List<Pair<T, I>> getElementsWithInfo() {
            return addedElements;
        }
        
        public List<T> getElements() {
            return addedElements.stream().map(p -> p.getFirst()).collect(Collectors.toList());
        }

        public Class<?> getDataType() {
            return dataType;
        }
        
        public Class<?> getInfoType() {
            return infoType;
        }
    }

    //******************************************************************************
    public static class ElementsAdded<T> extends XEventExtension {
        
        private final List<T> addedElements;
        private final Class<?> type;
        
        public ElementsAdded(XEventBroadcaster sender, List<T> addedElements) {
            super(sender, CollectionUpdateXEvent.class);
            this.addedElements = addedElements;
            this.type = addedElements.iterator().next().getClass();
        }

        public ElementsAdded(XEventBroadcaster sender, T element) {
            super(sender, CollectionUpdateXEvent.class);
            this.addedElements = Arrays.asList(element);
            this.type = element.getClass();
        }

        public List<T> getElements() {
            return addedElements;
        }

        public Class<?> getClassType() {
            return type;
        }
    }
    
    //******************************************************************************
    public static class ElementsRemovedWithInfo<T, I> extends XEventExtension {

        private final List<Pair<T, I>> removedElements;
        private final Class<?> dataType, infoType;
        
        public ElementsRemovedWithInfo(XEventBroadcaster sender, List<Pair<T, I>> removedElements) {
            super(sender, CollectionUpdateXEvent.class);
            this.removedElements = removedElements;
            this.dataType = removedElements.iterator().next().getFirst().getClass();
            this.infoType = removedElements.iterator().next().getSecond().getClass();
        }

        public ElementsRemovedWithInfo(XEventBroadcaster sender, T removedElement, I ai) {
            super(sender, CollectionUpdateXEvent.class);
            this.removedElements = Arrays.asList(new Pair<>(removedElement, ai));
            this.dataType = removedElement.getClass();
            this.infoType = ai.getClass();
        }

        public List<Pair<T,I>> getElementsWithInfo() {
            return removedElements;
        }
        
        public List<T> getElements() {
            return removedElements.stream().map(p -> p.getFirst()).collect(Collectors.toList());
        }

        public Class<?> getDataType() {
            return dataType;
        }
        
        public Class<?> getInfoType() {
            return infoType;
        }
    }

    //******************************************************************************
    public static class ElementsRemoved<T> extends XEventExtension {

        private final List<T> removedElements;
        private final Class<?> type;
        
        public ElementsRemoved(XEventBroadcaster sender, List<T> removedElements) {
            super(sender, CollectionUpdateXEvent.class);
            this.removedElements = removedElements;
            if (removedElements.isEmpty()) {
                this.type = null;
            } else {
                this.type = removedElements.iterator().next().getClass();
            }
        }

        public ElementsRemoved(XEventBroadcaster sender, T removedElement) {
            super(sender, CollectionUpdateXEvent.class);
            this.removedElements = Arrays.asList(removedElement);
            this.type = removedElement.getClass();
        }
        
        public List<T> getElements() {
            return removedElements;
        }
        
        public Class<?> getClassType() {
            return type;
        }
        
    }

    //******************************************************************************
    public static class ElementsModifiedWithInfo<T, I> extends XEventExtension {
        
        private final List<Triplet<T, T, I>> modifiedElements;
        private final Class<?> dataType, infoType;
        
        public ElementsModifiedWithInfo(XEventBroadcaster sender, List<Triplet<T, T, I>> modifiedElements) {
            super(sender, CollectionUpdateXEvent.class);
            this.modifiedElements = modifiedElements;
            this.dataType = modifiedElements.iterator().next().getFirst().getClass();
            this.infoType = modifiedElements.iterator().next().getThird().getClass();
        }
        
        public ElementsModifiedWithInfo(XEventBroadcaster sender, T oldVal, T newVal, I info) {
            super(sender, CollectionUpdateXEvent.class);
            this.modifiedElements = Arrays.asList(new Triplet<>(oldVal, newVal, info));
            this.dataType = oldVal.getClass();
            this.infoType = info.getClass();
        }

        public List<Triplet<T, T, I>> getElementsWithInfo() {
            return modifiedElements;
        }
        
        public List<Pair<T, T>> getElements() {
            return modifiedElements.stream().map(t -> new Pair<>(t.getFirst(), t.getSecond())).collect(Collectors.toList());
        }

        public Class<?> getDataType() {
            return dataType;
        }

        public Class<?> getInfoType() {
            return infoType;
        }
    }

    //******************************************************************************
    public static class ElementsModified<T> extends XEventExtension {
        
        private final List<Pair<T,T>> modifiedElements;
        private final Class<?> type;
        
        public ElementsModified(XEventBroadcaster sender, List<Pair<T,T>> modifiedElements) {
            super(sender, CollectionUpdateXEvent.class);
            this.modifiedElements = modifiedElements;
            this.type = modifiedElements.iterator().next().getFirst().getClass();
        }
        
        public ElementsModified(XEventBroadcaster sender, T oldVal, T newVal) {
            super(sender, CollectionUpdateXEvent.class);
            this.modifiedElements = Arrays.asList(new Pair<>(oldVal, newVal));
            this.type = oldVal.getClass();
        }

        public List<Pair<T,T>> getElements() {
            return modifiedElements;
        }
        
        public Class<?> getDataType() {
            return type;
        }
    }

}
