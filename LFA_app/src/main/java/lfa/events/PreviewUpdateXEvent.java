package lfa.events;

import base.events.XEvent;
import base.events.XEventBroadcaster;
import java.awt.image.BufferedImage;

//**************************************************************************************************
public class PreviewUpdateXEvent extends XEvent {

    private final BufferedImage image;

    //==============================================================================================
    public PreviewUpdateXEvent(XEventBroadcaster sender, BufferedImage image) {
        super(sender);
        this.image = image;
    }

    //==============================================================================================
    public BufferedImage getImage() {
        return image;
    }
    
}
