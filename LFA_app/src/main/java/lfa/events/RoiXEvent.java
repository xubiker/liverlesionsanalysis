package lfa.events;

import base.events.XEvent;
import base.events.XEventBroadcaster;
import base.events.XEventExtension;
import java.util.List;
import lfa.data.SampleCategory;

//**************************************************************************************************
public class RoiXEvent extends XEvent {
    
    public RoiXEvent(XEventBroadcaster sender) {
        super(sender);
    }
    
    //==============================================================================================
    public static class SelectAll extends XEventExtension {

        public SelectAll(XEventBroadcaster sender) {
            super(sender, RoiXEvent.class);
        }
    }
    
    //==============================================================================================
    public static class ResetSelection extends XEventExtension {

        public ResetSelection(XEventBroadcaster sender) {
            super(sender, RoiXEvent.class);
        }
    }
    
    //==============================================================================================
    public static class DeleteSelection extends XEventExtension {

        public DeleteSelection(XEventBroadcaster sender) {
            super(sender, RoiXEvent.class);
        }
    }

    //==============================================================================================
    public static class SelectionModeChanged extends XEventExtension {

        private final boolean selectionMode;
        
        public SelectionModeChanged(XEventBroadcaster sender, boolean selectionMode) {
            super(sender, RoiXEvent.class);
            this.selectionMode = selectionMode;
        }
        
        public boolean getSelectionMode() {
            return selectionMode;
        }
    }
    
    //==============================================================================================
    public static class ActiveRadChanged extends XEventExtension {
        
        private final List<Integer> activeRads;
        
        public ActiveRadChanged(XEventBroadcaster sender, List<Integer> activeRads) {
            super(sender, RoiXEvent.class);
            this.activeRads = activeRads;
        }
        
        public List<Integer> getActiveRad() {
            return activeRads;
        }
    }
    
    //==============================================================================================
    public static class DisplayRoisModeChanged extends XEventExtension {

        private final boolean disp;
        
        public DisplayRoisModeChanged(XEventBroadcaster sender, boolean disp) {
            super(sender, RoiXEvent.class);
            this.disp = disp;
        }
        
        public boolean getDispValue() {
            return disp;
        }
        
    }
    
    //==============================================================================================
    public static class RotationModeChanged extends XEventExtension {
        
        private final boolean rotation;
        
        public RotationModeChanged(XEventBroadcaster sender, boolean rotationVal) {
            super(sender, RoiXEvent.class);
            this.rotation = rotationVal;
        }
        
        public boolean getRotationValue() {
            return rotation;
        }
    }
    
    //==============================================================================================
    public static class DispOutliersModeChanged extends XEventExtension {
        
        private final boolean dispOutliers;
        
        public DispOutliersModeChanged(XEventBroadcaster sender, boolean dispOutliers) {
            super(sender, RoiXEvent.class);
            this.dispOutliers = dispOutliers;
        }
        
        public boolean getDispOutliersValue() {
            return dispOutliers;
        }
    }

    //==============================================================================================
    public static class ImagePreprocessModeChanged extends XEventExtension {
        
        boolean dispPreprocess;
        
        public ImagePreprocessModeChanged(XEventBroadcaster sender, boolean dispPreprocess) {
            super(sender, RoiXEvent.class);
            this.dispPreprocess = dispPreprocess;
        }
        
        public boolean getDispImagePreprocessValue() {
            return dispPreprocess;
        }
    }
    
    //==============================================================================================
    public static class SelectionSampleChanged extends XEventExtension {
        
        private final SampleCategory category;
        
        public SelectionSampleChanged(XEventBroadcaster sender, SampleCategory category) {
            super(sender, RoiXEvent.class);
            this.category = category;
        }
        
        public SampleCategory getCategory() {
            return category;
        }
    }
}
