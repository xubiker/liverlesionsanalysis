package lfa.events;

import base.events.XEvent;
import base.events.XEventBroadcaster;
import lfa.data.SampleCategory;

//**************************************************************************************************
public class SampleCategoryModifiedXEvent extends XEvent {
    
    private final SampleCategory category;
    
    public SampleCategoryModifiedXEvent(XEventBroadcaster sender, SampleCategory category) {
        super(sender);
        this.category = category;
    }
    
    public SampleCategory getCategory() {
        return category;
    }
    
}
