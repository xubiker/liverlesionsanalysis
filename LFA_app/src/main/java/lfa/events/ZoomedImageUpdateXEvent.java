package lfa.events;

import base.events.XEvent;
import base.events.XEventBroadcaster;
import java.awt.image.BufferedImage;

//**************************************************************************************************
public class ZoomedImageUpdateXEvent extends XEvent {
    
    private final BufferedImage image;
    private final int x, y;
    
    public ZoomedImageUpdateXEvent(XEventBroadcaster sender, BufferedImage image, int x, int y) {
        super(sender);
        this.image = image;
        this.x = x;
        this.y = y;
    }
    
    public ZoomedImageUpdateXEvent(XEventBroadcaster sender, BufferedImage image) {
        super(sender);
        this.image = image;
        this.x = -1;
        this.y = -1;
    }

    public BufferedImage getImage() {
        return image;
    }
    
    public int getX() {
        return x;
    }
    
    public int getY() {
        return y;
    }
    
}
