package lfa

import java.io.{BufferedWriter, File, FileOutputStream, OutputStreamWriter}

import base.backgroundexecution.XBackgroundModule
import base.logging.LoggingModule
import base.resource.ResourceLoaderModule
import com.google.inject.Guice
import com.typesafe.scalalogging.LazyLogging
import lfa.cl.ClassificationModule
import lfa.colortags.ColorTagModule
import lfa.data.{Patient, Roi, SampleCategory, USnapshot}
import lfa.datamanager.{DataManagerModule, IDataManager}
import lfa.diagnosis.{DiagnosisDeterminatorModule, IDiagnosisDeterminator}
import lfa.gui.dialog.DialogModule
import lfa.gui.evalresult.EvalResult_Module
import lfa.image.{ImageAdjustMethod => IAM, ImagePreprocessMethod => IPM}
import lfa.keyprocessor.KeyProcessor_Module
import lfa.major.Major_Module
import lfa.measurement.{MeasurementModule, ScalaImagePreprocessor}
import lfa.roiStatistics.RoiStatistics_Module
import lfa.roilayouteditor.RoiLayout_Module
import lfa.storage.StorageModule
import xubiker.XImageTools.base.{XColorSpace, XImage}
import xubiker.XImageTools.base.roi.SqRoi

import scala.collection.JavaConverters._
import scala.collection.mutable

object DataExporter extends App with LazyLogging {

  private val modules = List(
    new ResourceLoaderModule, new StorageModule, new DialogModule,
    new XBackgroundModule, new ColorTagModule, new DataManagerModule,
    new KeyProcessor_Module, new RoiLayout_Module, new Major_Module,
    new RoiStatistics_Module, new MeasurementModule, new ClassificationModule,
    new DiagnosisDeterminatorModule, new EvalResult_Module, new LoggingModule)
  private val injector = Guice.createInjector(modules.asJava)
  private val diagnosisDeterminator = injector.getInstance(classOf[IDiagnosisDeterminator])
  private val dataManager = injector.getInstance(classOf[IDataManager])
  dataManager.loadPatients(AppConf.validation.patientsPath)
  dataManager.loadRois(AppConf.validation.roisPath)

  private val folderSfx: String = "_n04"
  exportSampleToFile("1560081164", AppConf.export.exportAsImages)

  logger.info("export finished")

  private def getLabel(p: Patient, removeChar: Boolean = true): String = {
    val s = diagnosisDeterminator.getLabel(p, AppConf.diagnostics.disease, AppConf.diagnostics.score).get
    if (removeChar) s.substring(1) else s
  }

  private def createDir(path: String): Unit = {
    val dir = new File(path)
    dir.mkdir()
  }

  def exportSampleToFile(sampleName: String, asImage: Boolean = false): Unit = {
    dataManager.loadSamples(AppConf.validation.samplesDir + "\\r_" + sampleName + ".xml")
    val patients = dataManager.getPatients.toList.asScala
    val trainIds = patients.filter(p => dataManager.getSamples.getCategory(p.getId) == SampleCategory.TRAIN_SET).map(_.getId)
    val testIds = patients.filter(p => dataManager.getSamples.getCategory(p.getId) == SampleCategory.TEST_SET).map(_.getId)
    val _train = patients.filter(p => dataManager.getSamples.getCategory(p.getId) == SampleCategory.TRAIN_SET).map(p => (p.getId, getLabel(p)))
    val _test = patients.filter(p => dataManager.getSamples.getCategory(p.getId) == SampleCategory.TEST_SET).map(p => (p.getId, getLabel(p)))
    println("train: " + _train.sortBy(_._2).mkString(" "))
    println("test: " + _test.sortBy(_._2).mkString(" "))
    val filePathTrain = AppConf.export.roisExportDir + "\\train" + folderSfx + "\\"
    val filePathTest = AppConf.export.roisExportDir + "\\test" + folderSfx + "\\"
    println(filePathTest)
    println(filePathTrain)

    createDir(filePathTrain)
    createDir(filePathTest)
    val PSR: List[(Patient, USnapshot, Roi)] = patients.flatMap(p => {
      p.getUltrasound.toList.asScala.flatMap(s =>
        dataManager.getRois(p.getId, s.getId).asScala.map(r => (p, s, r)))
    }).toList
    for (rad <- AppConf.rads) {
      for (cut <- AppConf.cuts) {
        for (texParams <- AppConf.texParams) {
          val fileNamePrefix = sampleName + "_" + rad + "_" + cut + "_" + texParams._1 + "_" + texParams._2 + "_" + texParams._3
          val psr_filtered = PSR.filter(_._2.getCutType == cut).filter(_._3.getRadius == rad)
          val psr_train = psr_filtered.filter(psr => trainIds.contains(psr._1.getId))
          val psr_test = psr_filtered.filter(psr => testIds.contains(psr._1.getId))
          if (asImage) {
            writeByPSR(psr_train, texParams, filePath = filePathTrain + fileNamePrefix + "_")
            writeByPSR(psr_test, texParams, filePath = filePathTest + fileNamePrefix + "_")
          } else {
            val writerTrain = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePathTrain + fileNamePrefix + ".txt")))
            val writerTest = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePathTest + fileNamePrefix + ".txt")))
            writeByPSR(psr_train, texParams, writerTrain)
            writeByPSR(psr_test, texParams, writerTest)
            writerTrain.close()
            writerTest.close()
          }
        }
      }
    }
  }

  private def writeByPSR(psr: List[(Patient, USnapshot, Roi)], tParams: (Int, IPM, IAM), writer: BufferedWriter = null, filePath: String = null): Unit = {
    val scalaPreprocessor = new ScalaImagePreprocessor(dataManager)
    psr.groupBy(_._1).foreach(pp => {
      pp._2.groupBy(_._2).foreach(ss => {
        val rois = ss._2.map(_._3)
        val roisJ = mutable.Set(rois:_*).asJava
        val roisR = rois.map(r => SqRoi(r.getId, (r.getCenterX, r.getCenterY), r.getRadius, r.getAlpha, r.getLastUpdateTime))
        val preprocessed = scalaPreprocessor.loadPreprocessed(pp._1.getId, ss._1.getId, roisJ, tParams._2)
        val adjusted = scalaPreprocessor.adjust(preprocessed, roisJ, tParams._3)
        for (roi <- roisR) {
          val supportString = List(pp._1.getId, ss._1.getId, roi.id, roi.size._1, roi.size._2).mkString(" ") + " " + getLabel(pp._1) + "\n"
          val name = filePath + List(pp._1.getId, ss._1.getId, roi.id).mkString("_") + "_F" + getLabel(pp._1) + ".png"
          val rrr = adjusted.extractRoiImage(roi)
          write(rrr, supportString, writer, name)
        }
      })
    })
  }

  private def write(r: XImage, supportString: String, writer: BufferedWriter, fileName: String): Unit = {
    if (writer != null) {
      writer.write(supportString)
      r.writeToText(writer, oneLine = true)
      writer.write("\n")
      writer.flush()
    } else {
      val rr = r.toColorSpace(XColorSpace.GRAY2RGB)
      rr.write(fileName)
    }
  }

}
