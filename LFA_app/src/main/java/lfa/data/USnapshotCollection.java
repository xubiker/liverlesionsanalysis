package lfa.data;

import base.events.XEvent;
import base.events.XEventBroadcaster;
import base.events.XEventBroadcasterManager;
import base.events.XEventFactory;
import base.events.XEventListener;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import lfa.data.packed.USnapshotCollection_packed;
import lfa.events.CollectionUpdateXEvent;
import lfa.packing.IPackable;
import lfa.packing.IUnpackable;

//**************************************************************************************************
public final class USnapshotCollection implements IPackable, XEventBroadcaster {

    private String path;
    private final ConcurrentMap<Integer, USnapshot> map = new ConcurrentHashMap<>();
    private final XEventBroadcasterManager xbm = XEventFactory.createBroadcaster();

    //==============================================================================================
    public USnapshotCollection() { }

    //==============================================================================================
    public USnapshotCollection(String path, Collection<USnapshot> snapshots) {
        this.path = path;
        try {
            addSnapshots(snapshots);
        } catch (Exception ex) {
            Logger.getLogger(USnapshotCollection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //==============================================================================================
    public void addSnapshot(USnapshot snapshot) throws Exception {
        Integer id = snapshot.getId();
        if (id == -1) {
            int newId = (map.isEmpty()) ? 0 : Collections.max(map.keySet()) + 1;
            snapshot.setId(newId);
        } else if (map.containsKey(id)) {
            throw new Exception("collection already contains this snapshot");
        }
        map.put(snapshot.getId(), snapshot);
        notifyXEventListeners(new CollectionUpdateXEvent.ElementsAdded<>(this, snapshot));
    }

    //==============================================================================================
    public void addSnapshots(Collection<USnapshot> snapshots) throws Exception {
        for (USnapshot snapshot : snapshots) {
            addSnapshot(snapshot);
        }
    }
    
    //==============================================================================================
    public void removeSnapshot(int id) throws Exception {
        USnapshot oldSnapshot = map.remove(id);
        if (oldSnapshot == null) {
            throw new Exception("remove snapshot error: snapshot not found");
        }
        notifyXEventListeners(new CollectionUpdateXEvent.ElementsRemoved<>(this, oldSnapshot));
    }
    
    //==============================================================================================
    public void modifySnapshot(int id, USnapshot newSnapshot) throws Exception {
        USnapshot oldSnapshot = map.get(id);
        if (oldSnapshot == null) {
            throw new Exception("modify snapshot error: snapshot not found");
        }
        newSnapshot.setId(id);
        map.put(id, newSnapshot);
        notifyXEventListeners(new CollectionUpdateXEvent.ElementsModified<>(this, oldSnapshot, newSnapshot));
    }

    //==============================================================================================
    public String getPath() {
        return path;
    }

    //==============================================================================================
    public void setPath(String path) {
        this.path = path;
    }

    //==============================================================================================
    public ConcurrentMap<Integer, USnapshot> toMap() {
        return map;
    }

    //==============================================================================================
    public List<USnapshot> toList() {
        return map.values().stream().collect(Collectors.toList());
    }

    //==============================================================================================
    @Override
    public Function<IPackable, IUnpackable> getPackAction() {
        Function<IPackable, IUnpackable> pack = input -> {
            return new USnapshotCollection_packed((USnapshotCollection) input);
        };
        return pack;
    }

    //==============================================================================================
    @Override
    public Class<?> getPackT() {
        return USnapshotCollection_packed.class;
    }

    //==============================================================================================
    @Override
    public void registerXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        xbm.registerXEventListener(event_t, listener);
    }

    //==============================================================================================
    @Override
    public void unregisterXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        xbm.unregisterXEventListener(event_t, listener);
    }
    
    //==============================================================================================
    private void notifyXEventListeners(XEvent event) {
        xbm.notifyXEventListeners(event);
    }
    
}
