package lfa.data;

import base.events.XEvent;
import base.events.XEventBroadcaster;
import base.events.XEventBroadcasterManager;
import base.events.XEventFactory;
import base.events.XEventListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import lfa.data.packed.RoiCollection_packed;
import lfa.events.CollectionUpdateXEvent;
import lfa.packing.IPackable;
import lfa.packing.IUnpackable;

//**************************************************************************************************
public final class RoiCollection implements IPackable, XEventBroadcaster {

    private final ConcurrentMap<Integer, ConcurrentMap<Integer, List<Roi>>> rois = new ConcurrentHashMap<>(); // patientId -> snapshotId -> rois

    private final XEventBroadcasterManager xbm = XEventFactory.createBroadcaster();
    
    //==============================================================================================
    public RoiCollection() {}

    //==============================================================================================
    public RoiCollection(Collection<Roi> rois) {
        rois.stream().forEach(r -> addRoi(r));
    }
    
    //==============================================================================================
    public void addRoi(Roi roi) {
        ConcurrentMap<Integer, List<Roi>> sMap = rois.get(roi.getPatientId());
        if (sMap == null) {
            ConcurrentMap<Integer, List<Roi>> new_sMap = new ConcurrentHashMap<>();
            rois.put(roi.getPatientId(), new_sMap);
            sMap = new_sMap;
        }
        List<Roi> rList = sMap.get(roi.getSnapshotId());
        if (rList == null) {
            List<Roi> new_rList = new ArrayList<>();
            sMap.put(roi.getSnapshotId(), new_rList);
            rList = new_rList;
        }
        if (roi.getId() == -1) {
            int newId = rList.isEmpty() ? 0 : rList.get(rList.size() - 1).getId() + 1;
            roi.setId(newId);
        }
        rList.add(roi);
        notifyXEventListeners(new CollectionUpdateXEvent.ElementsAdded<>(this, roi));
    }

    //==============================================================================================
    public void removeRoi(int patientId, int snapshotId, Roi roi) {
        removeRoiById(patientId, snapshotId, roi.getId());
    }

    //==============================================================================================
    public void removeRoiById(int patientId, int snapshotId, int roiId) {
        System.out.println("RoiCollection. Removing item. " + patientId + " " + snapshotId + " " + roiId);
        ConcurrentMap<Integer, List<Roi>> sMap = rois.get(patientId);
        if (sMap == null) {
            System.err.println("trying to remove not existing roi...");
            return;
        }
        List<Roi> rList = sMap.get(snapshotId);
        if (rList == null) {
            System.err.println("trying to remove not existing roi...");
            return;
        }
        List<Roi> toRemove = rList.stream().filter(r -> r.getId() == roiId).collect(Collectors.toList());
        if (toRemove.size() != 1) {
            System.err.println("trying to remove not existing roi...");
            return;
        }
        Roi roiToRemove = toRemove.iterator().next();
        if (rList.remove(roiToRemove)) {
            notifyXEventListeners(new CollectionUpdateXEvent.ElementsRemoved<>(this, roiToRemove));
        } else {
            System.err.println("removeRoi: failure");
        }
    }

    //==============================================================================================
    public void removeRoi(int patientId, int snapshotId, List<Roi> rois) {
        rois.stream().forEach(r -> removeRoi(patientId, snapshotId, r));
    }
    
    //==============================================================================================
    public void removeRoiById(int patientId, int snapshotId, List<Integer> roiIds) {
        roiIds.stream().forEach(rid -> removeRoiById(patientId, snapshotId, rid));
    }

    //==============================================================================================
    public void removeAllRois() {
        //rois.clear();
        rois.entrySet().stream().forEach(es -> {
            Integer patientId = es.getKey();
            es.getValue().entrySet().stream().forEach(es2 -> {
                Integer snapshotId = es2.getKey();
                removeRoi(patientId, snapshotId, es2.getValue());
            });
        });
    }

    //==============================================================================================
    public List<Roi> getRois(int patientId, int snapshotId) {
        ConcurrentMap<Integer, List<Roi>> sMap = rois.get(patientId);
        if (sMap == null) {
            return new ArrayList<>();
        }
        List<Roi> rList = sMap.get(snapshotId);
        if (rList == null) {
            return new ArrayList<>();
        }
        List<Roi> res = new ArrayList<>();
        res.addAll(rList);
        return res;
    }

    //==============================================================================================
    public List<Roi> getRois(int patientId, int snapshotId, int radius) {
        ConcurrentMap<Integer, List<Roi>> sMap = rois.get(patientId);
        if (sMap == null) {
            return new ArrayList<>();
        }
        List<Roi> rList = sMap.get(snapshotId);
        if (rList == null) {
            return new ArrayList<>();
        }
        return rList.stream().filter(r -> r.getRadius() == radius).collect(Collectors.toList());
    }

    //==============================================================================================
    public List<Roi> getRois(int patientId) {
        ConcurrentMap<Integer, List<Roi>> sMap = rois.get(patientId);
        if (sMap == null) {
            return new ArrayList<>();
        }
        List<Roi> res = new ArrayList<>();
        sMap.values().stream().forEach(r -> {
            res.addAll(r);
        });
        return res;
    }

    //==============================================================================================
    public List<Roi> getRois() {
        List<Roi> res = new ArrayList<>();
        rois.values().stream().forEach(sMap -> {
            sMap.values().stream().forEach(r -> {
                res.addAll(r);
            });
        });
        return res;
    }
    
    //==============================================================================================
    public List<Roi> getRoisSorted() {
        List<Roi> res = new ArrayList<>();
        List<Integer> keys1 = new ArrayList<>(rois.keySet());
        Collections.sort(keys1);
        keys1.stream().forEach((Integer k1) -> {
            ConcurrentMap<Integer, List<Roi>> sMap = rois.get(k1);
            List<Integer> keys2 = new ArrayList<>(sMap.keySet());
            Collections.sort(keys2);
            keys2.stream().forEach(k2 -> {
                List<Roi> sRois = sMap.get(k2);
                res.addAll(sRois);
            });
        });
        return res;
    }
        
    //==============================================================================================
    public Roi getRoi(int patientId, int snapshotId, int roiId) {
        List<Roi> rs = getRois(patientId, snapshotId);
        List<Roi> collect = rs.stream().filter(r -> r.getId() == roiId).collect(Collectors.toList());
        if (collect.isEmpty()) {
            return null;
        }
        return collect.iterator().next();
    }

    //==============================================================================================
    @Override
    public Function<IPackable, IUnpackable> getPackAction() {
        Function<IPackable, IUnpackable> pack = input -> {
            return new RoiCollection_packed((RoiCollection) input);
        };
        return pack;
    }

    //==============================================================================================
    @Override
    public Class<?> getPackT() {
        return RoiCollection_packed.class;
    }

    //==============================================================================================
    @Override
    public void registerXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        xbm.registerXEventListener(event_t, listener);
    }

    //==============================================================================================
    @Override
    public void unregisterXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        xbm.unregisterXEventListener(event_t, listener);
    }
    
    //==============================================================================================
    private void notifyXEventListeners(XEvent event) {
        xbm.notifyXEventListeners(event);
    }
}
