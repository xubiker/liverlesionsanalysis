package lfa.data;

import javax.xml.bind.annotation.XmlEnum;

@XmlEnum
public enum PatientGender {
    MALE,
    FEMALE,
    UNKNOWN
}
