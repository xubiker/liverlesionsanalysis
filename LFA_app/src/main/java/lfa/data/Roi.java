package lfa.data;

import java.util.function.Function;
import lfa.data.packed.Roi_packed;
import lfa.packing.IPackable;
import lfa.packing.IUnpackable;
import base.time.TimeManager;

//**************************************************************************************************
public final class Roi implements IPackable {

    private int id = -1, patientId = -1, snapshotId = -1;
    private int centerX, centerY, radius;
    private double alpha;
    
    private long lastUpdateTime = -1;

    //==============================================================================================
    public Roi(Roi roi) {
        this.centerX = roi.centerX;
        this.centerY = roi.centerY;
        this.radius = roi.radius;
        this.alpha = roi.alpha;
        this.patientId = roi.patientId;
        this.snapshotId = roi.snapshotId;
        this.lastUpdateTime = TimeManager.getTimeInMillis();
    }
    
    //==============================================================================================
    public Roi(int centerX, int centerY, int radius, double alpha, int patientId, int snapshotId) {
        this.centerX = centerX;
        this.centerY = centerY;
        this.radius = radius;
        this.alpha = alpha;
        this.patientId = patientId;
        this.snapshotId = snapshotId;
        this.lastUpdateTime = TimeManager.getTimeInMillis();
    }
    
    //==============================================================================================
    public Roi(int centerX, int centerY, int radius, double alpha, int patientId, int snapshotId, int id) {
        this.centerX = centerX;
        this.centerY = centerY;
        this.radius = radius;
        this.alpha = alpha;
        this.patientId = patientId;
        this.snapshotId = snapshotId;
        this.id = id;
        this.lastUpdateTime = TimeManager.getTimeInMillis();
    }

    //==============================================================================================
    @Deprecated
    public Roi(int centerX, int centerY, int radius, double alpha, int patientId, int snapshotId, int id, long lastUpdateTime) {
        this(centerX, centerY, radius, alpha, patientId, snapshotId, id);
        this.lastUpdateTime = lastUpdateTime;
    }

    //==============================================================================================
    public Roi() {
        this.lastUpdateTime = TimeManager.getTimeInMillis();
    }
    
    //==============================================================================================
    public int getId() {
        return id;
    }

    //==============================================================================================
    public void setId(int id) {
        this.id = id;
        this.lastUpdateTime = TimeManager.getTimeInMillis();
    }

    //==============================================================================================
    public int getCenterX() {
        return centerX;
    }

    //==============================================================================================
    public void setCenterX(int centerX) {
        this.centerX = centerX;
        this.lastUpdateTime = TimeManager.getTimeInMillis();
    }

    //==============================================================================================
    public int getCenterY() {
        return centerY;
    }

    //==============================================================================================
    public void setCenterY(int centerY) {
        this.centerY = centerY;
        this.lastUpdateTime = TimeManager.getTimeInMillis();
    }

    //==============================================================================================
    public int getRadius() {
        return radius;
    }

    //==============================================================================================
    public void setRadius(int radius) {
        this.radius = radius;
        this.lastUpdateTime = TimeManager.getTimeInMillis();
    }

    //==============================================================================================
    public double getAlpha() {
        return alpha;
    }

    //==============================================================================================
    public void setAlpha(double alpha) {
        this.alpha = alpha;
        this.lastUpdateTime = TimeManager.getTimeInMillis();
    }

    //==============================================================================================
    public int getPatientId() {
        return patientId;
    }

    //==============================================================================================
    public void setPatientId(int patientId) {
        this.patientId = patientId;
        this.lastUpdateTime = TimeManager.getTimeInMillis();
    }

    //==============================================================================================
    public int getSnapshotId() {
        return snapshotId;
    }

    //==============================================================================================
    public void setSnapshotId(int snapshotId) {
        this.snapshotId = snapshotId;
        this.lastUpdateTime = TimeManager.getTimeInMillis();
    }
    
    //==============================================================================================
    public long getLastUpdateTime() {
        return lastUpdateTime;
    }

    //==============================================================================================
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(200);
        sb.append("id: ").append(id).append("\n");
        sb.append("patientId: ").append(patientId).append("\n");
        sb.append("snapshotId: ").append(snapshotId).append("\n");
        sb.append("centerX: ").append(centerX).append("\n");
        sb.append("centerY: ").append(centerY).append("\n");
        sb.append("radius: ").append(radius).append("\n");
        sb.append("alpha: ").append(alpha).append("\n");
        sb.append("last updated: ").append(TimeManager.convertMillisToTimeStamp(lastUpdateTime)).append("\n");
        return sb.toString();
    }

    //==============================================================================================
    @Override
    public Function<IPackable, IUnpackable> getPackAction() {
        return (input) -> {
            return new Roi_packed((Roi) input);
        };
    }

    //==============================================================================================
    @Override
    public Class<?> getPackT() {
        return Roi_packed.class;
    }

    //==============================================================================================
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + this.id;
        hash = 83 * hash + this.patientId;
        hash = 83 * hash + this.snapshotId;
        hash = 83 * hash + this.centerX;
        hash = 83 * hash + this.centerY;
        hash = 83 * hash + this.radius;
        hash = 83 * hash + (int) (Double.doubleToLongBits(this.alpha) ^ (Double.doubleToLongBits(this.alpha) >>> 32));
        hash = 83 * hash + (int) (this.lastUpdateTime ^ (this.lastUpdateTime >>> 32));
        return hash;
    }

    //==============================================================================================
    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Roi other = (Roi) obj;
        return (this.id == other.id
                && this.patientId == other.patientId
                && this.snapshotId == other.snapshotId
                && this.centerX == other.centerX
                && this.centerY == other.centerY
                && this.radius == other.radius
                && Double.doubleToLongBits(this.alpha) == Double.doubleToLongBits(other.alpha)
                && this.lastUpdateTime == other.lastUpdateTime);
    }
        
    
}