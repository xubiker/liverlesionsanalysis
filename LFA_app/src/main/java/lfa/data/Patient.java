package lfa.data;

import lfa.data.packed.Patient_packed;
import lfa.diagnosis.Diagnosis;
import lfa.diagnosis.FIBROSIS$;
import lfa.diagnosis.METAVIR$;
import lfa.diagnosis.MultiDiagnosis;
import lfa.gui.advancedtable.IAdvancedTableSuitable;
import lfa.gui.detailpanel.IDetailInfoProvider;
import lfa.packing.IPackable;
import lfa.packing.IUnpackable;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

//**************************************************************************************************
public final class Patient implements IPackable, IAdvancedTableSuitable, IDetailInfoProvider {

    private int id = -1;
    private String name, surname, label;
    private int age;
    private PatientGender gender;
    private String path;

    private MultiDiagnosis multiDiagnosis;
    private USnapshotCollection ultrasound = new USnapshotCollection();
    private Analysis analysis = new Analysis();

    //==============================================================================================
    public static Patient create() {
        return new Patient();
    }

    //==============================================================================================
    public static Patient create(String name, String surname, int age, PatientGender gender, MultiDiagnosis d, String path) {
        return new Patient(name, surname, age, gender, d, path);
    }

    //==============================================================================================
    private Patient() {}

    //==============================================================================================
    private Patient(String name, String surname, int age, PatientGender gender, MultiDiagnosis d, String path) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.gender = gender;
        this.multiDiagnosis = d;
        this.path = path;
    }
    
    //==============================================================================================
    public String getName() {
        return name;
    }

    //==============================================================================================
    public void setName(String name) {
        this.name = name;
    }

    //==============================================================================================
    public String getSurname() {
        return surname;
    }

    //==============================================================================================
    public void setSurname(String surname) {
        this.surname = surname;
    }

    //==============================================================================================
    public int getAge() {
        return age;
    }

    //==============================================================================================
    public void setAge(int age) {
        this.age = age;
    }

    //==============================================================================================
    public PatientGender getGender() {
        return gender;
    }

    //==============================================================================================
    public void setGender(PatientGender gender) {
        this.gender = gender;
    }

    //==============================================================================================
    public int getId() {
        return id;
    }

    //==============================================================================================
    public void setId(int id) {
        this.id = id;
    }

    //==============================================================================================
    public USnapshotCollection getUltrasound() {
        return ultrasound;
    }

    //==============================================================================================
    public void setUltrasound (USnapshotCollection ultrasound) {
        this.ultrasound = ultrasound;
    }
    
    //==============================================================================================
    public Analysis getAnalysis() {
        return this.analysis;
    }
    
    //==============================================================================================
    public void setAnalysis(Analysis analysis) {
        this.analysis = analysis;
    }

    //==============================================================================================
    public String getPath() {
        return path;
    }

    //==============================================================================================
    public void setPath(String path) {
        this.path = path;
    }

    //==============================================================================================
    public MultiDiagnosis getMultiDiagnosis() {
        return multiDiagnosis;
    }

    //==============================================================================================
    public int getFibrosisStage() {
        Diagnosis fibrosisDiagnosis = multiDiagnosis.getDiagnosis(FIBROSIS$.MODULE$, METAVIR$.MODULE$).get();
        return fibrosisDiagnosis.stage();
    }

    //==============================================================================================
    public void setMultiDiagnosis(MultiDiagnosis multiDiagnosis) {
        this.multiDiagnosis = multiDiagnosis;
    }

    //==============================================================================================
    public String getLabel() {
        return label;
    }

    //==============================================================================================
    public void setLabel(String label) {
        this.label = label;
    }
    
    //==============================================================================================
    @Override
    public String toString() {
        return "[" + id + "] " + surname + " " + name;
    }
    
    //==============================================================================================
    @Override
    public String getDetailInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("Name: ").append(name).append("\n");
        sb.append("Surname: ").append(surname).append("\n");
        sb.append("Gender: ").append(gender).append("\n");
        sb.append("Age: ").append(age).append("\n");
        String fibrosis = multiDiagnosis.getDiagnosis(FIBROSIS$.MODULE$, METAVIR$.MODULE$).get().toString();
        sb.append("Fibrosis: ").append(fibrosis).append("\n");
        sb.append("Elastography: ").append(analysis.getArfi()).append("\n");
        return sb.toString();
    }

    //==============================================================================================
    @Override
    public Function<IPackable, IUnpackable> getPackAction() {
        return (input) -> new Patient_packed((Patient) input);
    }

    //==============================================================================================
    @Override
    public Class<?> getPackT() {
        return Patient_packed.class;
    }

    //==============================================================================================
    @Override
    public List<String> getLabels() {
        return Arrays.asList("Id", "Name", "F", "Comment");
    }

    //==============================================================================================
    @Override
    public List<String> getValues() {
        String fibrosis = multiDiagnosis.getDiagnosis(FIBROSIS$.MODULE$, METAVIR$.MODULE$).get().toString();
        return Arrays.asList(Integer.toString(id), surname + " " + name, fibrosis, label);
    }

    //==============================================================================================
    @Override
    public List<Integer> getColorTags() {
        int f = multiDiagnosis.getDiagnosis(FIBROSIS$.MODULE$, METAVIR$.MODULE$).get().stage();
        //int f = fibrosis.getIntValue();
        return Arrays.asList(f, f, f, f);
    }
    
}
