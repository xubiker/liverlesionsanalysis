package lfa.data;

import java.util.function.Function;
import lfa.data.packed.Analysis_packed;
import lfa.packing.IPackable;
import lfa.packing.IUnpackable;

//**************************************************************************************************
public class Analysis implements IPackable {

    private double arfi, iha, echo, contour;

    //==============================================================================================
    public Analysis() {
    }

    //==============================================================================================
    public Analysis(double arfi, double iha, double echo, double contour) {
        this.arfi = arfi;
        this.iha = iha;
        this.echo = echo;
        this.contour = contour;
    }

    //==============================================================================================
    public double getArfi() {
        return arfi;
    }

    //==============================================================================================
    public double getIha() {
        return iha;
    }
    //==============================================================================================
    public double getEcho() {
        return echo;
    }

    //==============================================================================================
    public double getContour() {
        return contour;
    }

    //==============================================================================================
    @Override
    public Function<IPackable, IUnpackable> getPackAction() {
        return (input) -> new Analysis_packed((Analysis) input);
    }

    //==============================================================================================
    @Override
    public Class<?> getPackT() {
        return Analysis_packed.class;
    }

}
