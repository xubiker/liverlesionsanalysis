package lfa.data;

import base.collections.Pair;
import base.collections.Triplet;
import base.events.XEvent;
import base.events.XEventBroadcaster;
import base.events.XEventBroadcasterManager;
import base.events.XEventFactory;
import base.events.XEventListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import lfa.data.packed.SampleCollection_packed;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import lfa.events.CollectionUpdateXEvent;
import lfa.packing.IPackable;
import lfa.packing.IUnpackable;

//**************************************************************************************************
public final class SampleCollection implements IPackable, XEventBroadcaster {

    private final ConcurrentMap<Integer, ConcurrentMap<Integer, ConcurrentMap<Integer, SampleCategory>>> map = new ConcurrentHashMap<>();
    private final XEventBroadcasterManager xbm = XEventFactory.createBroadcaster();
    
    //==============================================================================================
    public SampleCategory getCategory(int patientId, int snapshotId, int roiId) {
        ConcurrentMap<Integer, ConcurrentMap<Integer, SampleCategory>> sMap = map.get(patientId);
        if (sMap == null) {
            return null;
        }
        ConcurrentMap<Integer, SampleCategory> rMap = sMap.get(snapshotId);
        if (rMap == null) {
            return null;
        }
        SampleCategory category = rMap.get(roiId);
        if (category == null) {
            return null;
        }
        return category;
    }

    //==============================================================================================
    public SampleCategory getCategory(int patientId, int snapshotId) {
        ConcurrentMap<Integer, ConcurrentMap<Integer, SampleCategory>> sMap = map.get(patientId);
        if (sMap == null) {
            return null;
        }
        ConcurrentMap<Integer, SampleCategory> rMap = sMap.get(snapshotId);
        if (rMap == null) {
            return null;
        }
        Collection<SampleCategory> categories = rMap.values();
        if (categories.isEmpty()) {
            return null;
        }
        return getMutualCategory(categories);
    }
    
    //==============================================================================================
    public SampleCategory getCategory(int patientId) {
        ConcurrentMap<Integer, ConcurrentMap<Integer, SampleCategory>> sMap = map.get(patientId);
        if (sMap == null) {
            return null;
        }
        Collection<ConcurrentMap<Integer, SampleCategory>> rMaps = sMap.values();
        if (rMaps.isEmpty()) {
            return null;
        }
        List<SampleCategory> categories = new ArrayList<>();
        rMaps.stream().forEach(rMap -> categories.addAll(rMap.values()));
        if (categories.isEmpty()) {
            return null;
        }
        return getMutualCategory(categories);
    }
    
    //==============================================================================================
    private SampleCategory getMutualCategory(Collection<SampleCategory> categories) {
        SampleCategory fcc = categories.iterator().next();
        if (categories.stream().allMatch(cc -> cc.equals(fcc))) {
            return fcc;
        } else {
            return SampleCategory.MIXED;
        }
    }
    
    //==============================================================================================
    public void setCategory(int patientId, int snapshotId, int roiId, SampleCategory newVal) throws Exception {    
        ConcurrentMap<Integer, ConcurrentMap<Integer, SampleCategory>> sMap = map.get(patientId);
        if (sMap == null) {
            throw new Exception("entry with this patientId was not found");
        }
        ConcurrentMap<Integer, SampleCategory> rMap = sMap.get(snapshotId);
        if (rMap == null) {
            throw new Exception("entry with this snapshotId was not found");
        }
        if ( rMap.get(roiId) == null) {
            throw new Exception("entry with this roiId was not found");
        }
        SampleCategory oldVal = rMap.put(roiId, newVal);
        notifyXEventListeners(new CollectionUpdateXEvent.ElementsModifiedWithInfo<>(
            this, oldVal, newVal, new CollectionUpdateXEvent.AdditionalInfoPSR(patientId, snapshotId, roiId)
        ));
    }
    
    //==============================================================================================
    public void setCategory(int patientId, int snapshotId, List<Pair<Integer,SampleCategory>> pList) throws Exception {    
        ConcurrentMap<Integer, ConcurrentMap<Integer, SampleCategory>> sMap = map.get(patientId);
        if (sMap == null) {
            throw new Exception("entry with this patientId was not found");
        }
        ConcurrentMap<Integer, SampleCategory> rMap = sMap.get(snapshotId);
        if (rMap == null) {
            throw new Exception("entry with this snapshotId was not found");
        }
        ConcurrentMap<Integer, SampleCategory> rMap2 = rMap;
        
        List<Pair<SampleCategory, CollectionUpdateXEvent.AdditionalInfoPSR>> aList = new ArrayList<>();
        List<Triplet<SampleCategory, SampleCategory, CollectionUpdateXEvent.AdditionalInfoPSR>> mList = new ArrayList<>();
        pList.stream().forEach(p -> {
            int rid = p.getFirst();
            SampleCategory val = p.getSecond();
            SampleCategory oldVal = rMap2.put(rid, val);
            if (oldVal == null) {
                aList.add(new Pair<>(val, new CollectionUpdateXEvent.AdditionalInfoPSR(patientId, snapshotId, rid)));
            } else {
                mList.add(new Triplet<>(oldVal, val, new CollectionUpdateXEvent.AdditionalInfoPSR(patientId, snapshotId, rid)));
            }
        });
        if (!aList.isEmpty()) {
            throw new Exception("entry with this roiId was not found");
            //notifyXEventListeners(new CollectionUpdateXEvent.ElementsAddedWithInfo<>(this, aList));
        }
        if (!mList.isEmpty()) {
            notifyXEventListeners(new CollectionUpdateXEvent.ElementsModifiedWithInfo<>(this, mList));
        }
    }

    //==============================================================================================
    public void setCategory(int patientId, int snapshotId, SampleCategory newVal) throws Exception {    
        ConcurrentMap<Integer, ConcurrentMap<Integer, SampleCategory>> sMap = map.get(patientId);
        if (sMap == null) {
            throw new Exception("entry with this patientId was not found");
        }
        ConcurrentMap<Integer, SampleCategory> rMap = sMap.get(snapshotId);
        if (rMap == null) {
            throw new Exception("entry with this snapshotId was not found");
        }
        List<Triplet<SampleCategory, SampleCategory, CollectionUpdateXEvent.AdditionalInfoPSR>> list
        = rMap.entrySet().stream().map(e -> {
            int rid = e.getKey();
            SampleCategory oldVal = rMap.put(rid, newVal);
            return new Triplet<>(oldVal, newVal, new CollectionUpdateXEvent.AdditionalInfoPSR(patientId, snapshotId, rid));
        }).collect(Collectors.toList());
        if (!list.isEmpty()) {
            notifyXEventListeners(new CollectionUpdateXEvent.ElementsModifiedWithInfo<>(this, list));
        }
    }

    //==============================================================================================
    public void setCategory(int patientId, SampleCategory newVal) throws Exception {
        ConcurrentMap<Integer, ConcurrentMap<Integer, SampleCategory>> sMap = map.get(patientId);
        if (sMap == null) {
            throw new Exception("entry with this patientId was not found");
        }
        //System.out.println("pid: " + patientId);
        List<Triplet<SampleCategory, SampleCategory, CollectionUpdateXEvent.AdditionalInfoPSR>> mList
                = new ArrayList<>();
        sMap.entrySet().stream().forEach(e1 -> {
            int sid = e1.getKey();
            ConcurrentMap<Integer, SampleCategory> rMap = e1.getValue();
            rMap.entrySet().stream().forEach(e2 -> {
                int rid = e2.getKey();
                SampleCategory oldVal = rMap.put(rid, newVal);
                mList.add(new Triplet<>(oldVal, newVal, new CollectionUpdateXEvent.AdditionalInfoPSR(patientId, sid, rid)));
            });
        });
        if (!mList.isEmpty()) {
            notifyXEventListeners(new CollectionUpdateXEvent.ElementsModifiedWithInfo<>(this, mList));
        }
    }

    //==============================================================================================
    public void addCategory(int patientId, int snapshotId, int roiId, SampleCategory category) throws Exception {
        ConcurrentMap<Integer, ConcurrentMap<Integer, SampleCategory>> sMap = map.get(patientId);
        if (sMap == null) {
            ConcurrentMap<Integer, ConcurrentMap<Integer, SampleCategory>> new_sMap = new ConcurrentHashMap<>();
            map.put(patientId, new_sMap);
            sMap = new_sMap;
        }
        ConcurrentMap<Integer, SampleCategory> rMap = sMap.get(snapshotId);
        if (rMap == null) {
            ConcurrentMap<Integer, SampleCategory> new_rMap = new ConcurrentHashMap<>();
            sMap.put(snapshotId, new_rMap);
            rMap = new_rMap;
        }
        if (rMap.containsKey(roiId)) {
            throw new Exception("trying to add category: the same roiId already exsits");
        }
        rMap.put(roiId, category);
        notifyXEventListeners(new CollectionUpdateXEvent.ElementsAddedWithInfo<>(this, category,
                new CollectionUpdateXEvent.AdditionalInfoPSR(patientId, snapshotId, roiId)));
    }
    
    //==============================================================================================
    public void removeCategory(int patientId, int snapshotId, int roiId) throws Exception {
        ConcurrentMap<Integer, ConcurrentMap<Integer, SampleCategory>> sMap = map.get(patientId);
        if (sMap == null) {
            throw new Exception("trying to remove category: invalid patientId");
        }
        ConcurrentMap<Integer, SampleCategory> rMap = sMap.get(snapshotId);
        if (rMap == null) {
            throw new Exception("trying to remove category: invalid snapshotId");
        }
        SampleCategory oldVal = rMap.remove(roiId);
        if (oldVal == null) {
            throw new Exception("trying to remove category: invalid roiId");
        }
        notifyXEventListeners(new CollectionUpdateXEvent.ElementsRemovedWithInfo<>(this, oldVal,
                new CollectionUpdateXEvent.AdditionalInfoPSR(patientId, snapshotId, roiId)));
    }
    
    //==============================================================================================
    public void removeCategories(int patientId, int snapshotId, List<Integer> roiIds) throws Exception {
        ConcurrentMap<Integer, ConcurrentMap<Integer, SampleCategory>> sMap = map.get(patientId);
        if (sMap == null) {
            throw new Exception("trying to remove category: invalid patientId");
        }
        ConcurrentMap<Integer, SampleCategory> rMap = sMap.get(snapshotId);
        if (rMap == null) {
            throw new Exception("trying to remove category: invalid snapshotId");
        }
        List<Pair<SampleCategory, CollectionUpdateXEvent.AdditionalInfoPSR>> rList = roiIds.stream()
                .map(rid -> new Pair<>(rMap.remove(rid), new CollectionUpdateXEvent.AdditionalInfoPSR(patientId, snapshotId, rid)))
                .filter(p -> p.getFirst() != null).
                collect(Collectors.toList());
        if (roiIds.size() != rList.size()) {
            throw new Exception("trying to remove category: invalid roiId");
        }
        notifyXEventListeners(new CollectionUpdateXEvent.ElementsRemovedWithInfo<>(this, rList));
    }

    //==============================================================================================
    public void removeCategories(int patientId, int snapshotId) throws Exception {
        ConcurrentMap<Integer, ConcurrentMap<Integer, SampleCategory>> sMap = map.get(patientId);
        if (sMap == null) {
            throw new Exception("trying to remove category: invalid patientId");
        }
        ConcurrentMap<Integer, SampleCategory> rMap = sMap.remove(snapshotId);
        if (rMap == null) {
            throw new Exception("trying to remove snapshot: invalid snapshotId");
        }
        List<Pair<SampleCategory, CollectionUpdateXEvent.AdditionalInfoPSR>> rList = rMap
                .entrySet().stream()
                .map(e -> new Pair<>(e.getValue(), new CollectionUpdateXEvent.AdditionalInfoPSR(patientId, snapshotId, e.getKey())))
                .collect(Collectors.toList());
        notifyXEventListeners(new CollectionUpdateXEvent.ElementsRemoved<>(this, rList));
    }

    //==============================================================================================
    public void removeCategories(int patientId) throws Exception {
        ConcurrentMap<Integer, ConcurrentMap<Integer, SampleCategory>> sMap = map.remove(patientId);
        if (sMap == null) {
            throw new Exception("trying to remove patient: invalid patientId");
        }
        List<Pair<SampleCategory, CollectionUpdateXEvent.AdditionalInfoPSR>> rList = new ArrayList<>();
        sMap.entrySet().stream().forEach(e -> {
            int sid = e.getKey();
            ConcurrentMap<Integer, SampleCategory> rMap = e.getValue();
            rMap.entrySet().stream().forEach(e2 -> {
                int rid = e2.getKey();
                SampleCategory val = e2.getValue();
                rList.add(new Pair<>(val, new CollectionUpdateXEvent.AdditionalInfoPSR(patientId, sid, sid)));
            });
        });
        notifyXEventListeners(new CollectionUpdateXEvent.ElementsRemoved<>(this, rList));
    }
    
    //==============================================================================================
    public void removeAllCategories() {
        List<Pair<SampleCategory, CollectionUpdateXEvent.AdditionalInfoPSR>> rList = new ArrayList<>();
        map.entrySet().forEach(e1 -> {
            int pid = e1.getKey();
            e1.getValue().entrySet().stream().forEach(e2 -> {
                int sid = e2.getKey();
                e2.getValue().entrySet().stream().forEach(e3 -> {
                    int rid = e3.getKey();
                    SampleCategory val = e3.getValue();
                    rList.add(new Pair<>(val, new CollectionUpdateXEvent.AdditionalInfoPSR(pid, sid, rid)));
                });
            });
        });
        map.clear();
        notifyXEventListeners(new CollectionUpdateXEvent.ElementsRemoved<>(this, rList));
    }
    
    //==============================================================================================
    public List<Pair<Triplet<Integer, Integer, Integer>, SampleCategory>> toList() {
        List<Pair<Triplet<Integer, Integer, Integer>, SampleCategory>> list = new ArrayList<>();
        map.entrySet().stream().forEach(pSet -> {
            int pid = pSet.getKey();
            pSet.getValue().entrySet().stream().forEach(sSet -> {
                int sid = sSet.getKey();
                sSet.getValue().entrySet().stream().forEach(rSet -> {
                    int rid = rSet.getKey();
                    SampleCategory category = rSet.getValue();
                    list.add(new Pair<>(new Triplet<>(pid, sid, rid), category));
                });
            });
        });
        return list;
    }
    
    //==============================================================================================
    public List<Pair<Triplet<Integer, Integer, Integer>, SampleCategory>> toSortedList() {
        List<Pair<Triplet<Integer, Integer, Integer>, SampleCategory>> toList = toList();
        Collections.sort(toList, (p1, p2) -> {
            Triplet<Integer, Integer, Integer> t1 = p1.getFirst();
            Triplet<Integer, Integer, Integer> t2 = p2.getFirst();
            if (t1.getFirst() < t2.getFirst()) return -1;
            if (t1.getFirst() > t2.getFirst()) return 1;
            if (t1.getSecond() < t2.getSecond()) return -1;
            if (t1.getSecond() > t2.getSecond()) return 1;
            if (t1.getThird() < t2.getThird()) return -1;
            if (t1.getThird() > t2.getThird()) return 1;
            return 0;
        });
        return toList;
    }

    //==============================================================================================
    @Override
    public Function<IPackable, IUnpackable> getPackAction() {
        Function<IPackable, IUnpackable> pack = input -> {
            SampleCollection unpacked = (SampleCollection) input;
            SampleCollection_packed packed = new SampleCollection_packed(unpacked);
            return packed;
        };
        return pack;
    }
    
    //==============================================================================================
    @Override
    public Class<?> getPackT() {
        return SampleCollection_packed.class;
    }

    //==============================================================================================
    @Override
    public void registerXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        xbm.registerXEventListener(event_t, listener);
    }

    //==============================================================================================
    @Override
    public void unregisterXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        xbm.unregisterXEventListener(event_t, listener);
    }

    //==============================================================================================
    private void notifyXEventListeners(XEvent e) {
        xbm.notifyXEventListeners(e);
    }

}
