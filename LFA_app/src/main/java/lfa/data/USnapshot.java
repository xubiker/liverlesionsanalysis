package lfa.data;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import lfa.gui.advancedtable.IAdvancedTableSuitable;
import lfa.data.packed.USnapshot_packed;
import lfa.packing.IPackable;
import lfa.packing.IUnpackable;

//**************************************************************************************************
public final class USnapshot implements IPackable, IAdvancedTableSuitable {

    private int id = -1;
    private String fileName;
    private LiverCutType cutType;

    private int usConeX, usConeY;

    //==============================================================================================
    public USnapshot() {}
    
    //==============================================================================================
    public USnapshot (String fileName, LiverCutType cut) {
        this.fileName = fileName;
        this.cutType = cut;
    }
    
    //==============================================================================================
    public USnapshot(USnapshot snapshot) {
        this.id = snapshot.id;
        this.fileName = snapshot.fileName;
        this.cutType = snapshot.cutType;
        this.usConeX = snapshot.usConeX;
        this.usConeY = snapshot.usConeY;
    }
    
    //==============================================================================================
    public String getFileName() {
        return fileName;
    }

    //==============================================================================================
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    //==============================================================================================
    public LiverCutType getCutType() {
        return cutType;
    }

    //==============================================================================================
    public void setCutType(LiverCutType cutType) {
        this.cutType = cutType;
    }

    //==============================================================================================
    public int getUsConeX() {
        return usConeX;
    }

    //==============================================================================================
    public void setUsConeX(int usConeX) {
        this.usConeX = usConeX;
    }

    //==============================================================================================
    public int getUsConeY() {
        return usConeY;
    }

    //==============================================================================================
    public void setUsConeY(int usConeY) {
        this.usConeY = usConeY;
    }

    //==============================================================================================
    public int getId() {
        return id;
    }

    //==============================================================================================
    public void setId(int id) {
        this.id = id;
    }

    //==============================================================================================
    @Override
    public Function<IPackable, IUnpackable> getPackAction() {
        return (input) -> {
            return new USnapshot_packed((USnapshot) input);
        };
    }

    //==============================================================================================
    @Override
    public Class<?> getPackT() {
        return USnapshot_packed.class;
    }

    //==============================================================================================
    @Override
    public List<String> getLabels() {
        return Arrays.asList("Id", "Cut");
    }

    //==============================================================================================
    @Override
    public List<String> getValues() {
        return Arrays.asList(Integer.toString(id), cutType.toString());
    }

    //==============================================================================================
    @Override
    public List<Integer> getColorTags() {
        return Arrays.asList(cutType.getIntValue(), cutType.getIntValue());
    }
    
}
