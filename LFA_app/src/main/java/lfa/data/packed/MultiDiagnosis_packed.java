package lfa.data.packed;

import lfa.diagnosis.Diagnosis;
import lfa.diagnosis.MultiDiagnosis;
import lfa.packing.IPackable;
import lfa.packing.IUnpackable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MultiDiagnosis_packed implements IUnpackable {

    @XmlElements({
            @XmlElement(name="diagnosis")
    })
    private final List<Diagnosis_packed> diagnosis = new ArrayList<>();

    public MultiDiagnosis_packed() {}

    public MultiDiagnosis_packed(MultiDiagnosis multiDiagnosis) {
        multiDiagnosis.getPrimaryDiagnoses().forEach(d ->
                diagnosis.add((Diagnosis_packed) d.getPackAction().apply(d))
        );
    }

    @Override
    public Function<IUnpackable, IPackable> getUnpackAction() {
        return (input) -> {
            MultiDiagnosis_packed packed = (MultiDiagnosis_packed) input;
            Collection<Diagnosis> list = packed.diagnosis.stream().map(dp ->
                    (Diagnosis) dp.getUnpackAction().apply(dp)
            ).collect(Collectors.toList());
            return new MultiDiagnosis(list);
        };
    }

    @Override
    public Class<?> getUnpackT() {
        return MultiDiagnosis.class;
    }
}
