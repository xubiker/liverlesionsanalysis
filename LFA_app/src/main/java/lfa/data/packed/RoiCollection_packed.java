package lfa.data.packed;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import lfa.data.Roi;
import lfa.data.RoiCollection;
import lfa.packing.IPackable;
import lfa.packing.IUnpackable;

//**************************************************************************************************
@XmlRootElement(name = "rois_collection")
public class RoiCollection_packed implements IUnpackable {

    @XmlElements({ 
        @XmlElement(name="roi")
    })
    @XmlElementWrapper(name = "rois")
    private final List<Roi_packed> rois = new ArrayList<>();

    //==============================================================================================
    public RoiCollection_packed() {}
    
    //==============================================================================================
    public RoiCollection_packed(RoiCollection roiCollection) {
        roiCollection.getRoisSorted().stream().forEach(r -> {
            rois.add((Roi_packed) r.getPackAction().apply(r));
        });
    }
    
    //==============================================================================================
    @Override
    public Function<IUnpackable, IPackable> getUnpackAction() {
        return (input) -> {
            RoiCollection_packed packed = (RoiCollection_packed) input;
            RoiCollection unpacked = new RoiCollection();
            packed.rois.stream().forEach(r -> {
                unpacked.addRoi((Roi) r.getUnpackAction().apply(r));
            });
            return unpacked;
        };
    }

    //==============================================================================================
    @Override
    public Class<?> getUnpackT() {
        return RoiCollection_packed.class;
    }
    
}
