package lfa.data.packed;

import java.util.function.Function;
import javax.xml.bind.annotation.XmlAttribute;
import lfa.data.Analysis;
import lfa.packing.IPackable;
import lfa.packing.IUnpackable;

//**************************************************************************************************
public class Analysis_packed implements IUnpackable {

    @XmlAttribute
    private double arfi;
    @XmlAttribute
    private double iha;
    @XmlAttribute
    private double echo;
    @XmlAttribute
    private double contour;

    //==============================================================================================
    public Analysis_packed() { }

    //==============================================================================================
    public Analysis_packed(Analysis analysis) {
        this.arfi = analysis.getArfi();
        this.iha = analysis.getIha();
        this.echo = analysis.getEcho();
        this.contour = analysis.getContour();
    }

    //==============================================================================================
    @Override
    public Function<IUnpackable, IPackable> getUnpackAction() {
        return (input) -> {
            Analysis_packed packed = (Analysis_packed) input;
            Analysis unpacked = new Analysis(packed.arfi, packed.iha, packed.echo, packed.contour);
            return unpacked;
        };
    }

    //==============================================================================================
    @Override
    public Class<?> getUnpackT() {
        return Analysis.class;
    }

}
