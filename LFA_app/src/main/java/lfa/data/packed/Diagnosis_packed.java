package lfa.data.packed;

import lfa.diagnosis.Diagnosis;
import lfa.diagnosis.Diagnosis$;
import lfa.diagnosis.Disease;
import lfa.packing.IPackable;
import lfa.packing.IUnpackable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import java.util.function.Function;

@XmlType(propOrder = {"disease", "score", "stage"})
public class Diagnosis_packed implements IUnpackable {

    @XmlAttribute
    private String disease;

    @XmlAttribute
    private String score;

    @XmlAttribute
    private int stage;

    public Diagnosis_packed(){}

    public Diagnosis_packed(Diagnosis d) {
        this.disease = d.disease().toString();
        this.score = d.score().toString();
        this.stage = d.stage();
    }

    @Override
    public Function<IUnpackable, IPackable> getUnpackAction() {
        return (input) -> {
            Diagnosis_packed packed = (Diagnosis_packed)input;
            return Diagnosis$.MODULE$.apply(packed.disease, packed.score, packed.stage).get();
        };
    }

    @Override
    public Class<?> getUnpackT() {
        return Disease.class;
    }
}
