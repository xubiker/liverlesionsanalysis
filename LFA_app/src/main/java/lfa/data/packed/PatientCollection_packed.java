package lfa.data.packed;

import lfa.data.Patient;
import lfa.data.PatientCollection;
import lfa.packing.IPackable;
import lfa.packing.IUnpackable;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;

//**************************************************************************************************
@XmlRootElement(name = "patients_collection")
public class PatientCollection_packed implements IUnpackable {

    @XmlAttribute
    private final String path;

    @XmlElements({
        @XmlElement(name="patient")
    })
    @XmlElementWrapper(name = "patients")
    private final List<Patient_packed> patients = new ArrayList<>();

    //==============================================================================================
    public PatientCollection_packed() {
        path = "";
    }

    //==============================================================================================
    public PatientCollection_packed(PatientCollection patientsCollection) {
        this.path = patientsCollection.getPath();
        patientsCollection.toList().stream().forEach(patient -> {
            Patient_packed pp = (Patient_packed) patient.getPackAction().apply(patient);
            this.patients.add(pp);
        });
    }

    //==============================================================================================
    @Override
    public Function<IUnpackable, IPackable> getUnpackAction() {
        return (input) -> {
            PatientCollection_packed packed = (PatientCollection_packed) input;
            PatientCollection unpacked = new PatientCollection();
            unpacked.setPath(packed.path);
            packed.patients.stream().forEach(pp -> {
                Patient p = (Patient) pp.getUnpackAction().apply(pp);
                try {
                    unpacked.addPatient(p);
                } catch (Exception ex) {
                    Logger.getLogger(PatientCollection_packed.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            return unpacked;
        };
    }

    //==============================================================================================
    @Override
    public Class<?> getUnpackT() {
        return PatientCollection.class;
    }
    
}
