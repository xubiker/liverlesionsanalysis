package lfa.data.packed;

import base.collections.Pair;
import base.collections.Triplet;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import lfa.data.SampleCategory;
import lfa.data.SampleCollection;
import lfa.packing.IPackable;
import lfa.packing.IUnpackable;

//**************************************************************************************************
@XmlRootElement
public class SampleCollection_packed implements IUnpackable {

    //**********************************************************************************************
    @XmlType(propOrder = {"pid", "sid", "rid", "category"})
    private static class Sample {

        @XmlAttribute
        int pid, sid, rid;
        
        @XmlAttribute
        String category;
        
        Sample() {}
        
        Sample(int pid, int sid, int rid, String category) {
            this.pid = pid;
            this.sid = sid;
            this.rid = rid;
            this.category = category;
        }

        @Override
        public int hashCode() {
            int hash = 17;
            hash = hash * 17 + pid;
            hash = hash * 17 + sid;
            hash = hash * 17 + rid;
            hash = hash * 17 + category.toString().hashCode();
            return hash;
        }
    }

    @XmlElement(name = "samples")
    private final List<Sample> samples = new ArrayList<>();
    
    //==============================================================================================
    public SampleCollection_packed() { }
    
    //==============================================================================================
    public SampleCollection_packed(SampleCollection sampleCollection) {
        List<Pair<Triplet<Integer, Integer, Integer>, SampleCategory>> list = sampleCollection.toSortedList();
        list.stream().forEach(p -> {
            Triplet<Integer, Integer, Integer> id = p.getFirst();
            SampleCategory category = p.getSecond();
            samples.add(new Sample(id.getFirst(), id.getSecond(), id.getThird(), category.toString()));
        });
    }
    
    //==============================================================================================
    @Override
    public Function<IUnpackable, IPackable> getUnpackAction() {
        return (input) -> {
            SampleCollection_packed packed = (SampleCollection_packed) input;
            SampleCollection unpacked = new SampleCollection();
            packed.samples.stream().forEach(sample -> {
                try {
                    unpacked.addCategory(sample.pid, sample.sid, sample.rid, SampleCategory.valueOf(sample.category));
                } catch (Exception ex) {
                    Logger.getLogger(SampleCollection_packed.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            return unpacked;
        };
    }

    //==============================================================================================
    @Override
    public Class<?> getUnpackT() {
        return SampleCollection.class;
    }

    //==============================================================================================
    @Override
    public int hashCode() {
        int hash = 17;
        for (Sample s : samples) {
            hash = hash * 17 + s.hashCode();
        }
        return hash;
    }
}
