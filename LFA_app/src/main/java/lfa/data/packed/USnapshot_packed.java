package lfa.data.packed;

import java.util.function.Function;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lfa.data.LiverCutType;
import lfa.data.USnapshot;
import lfa.packing.IPackable;
import lfa.packing.IUnpackable;

//**************************************************************************************************
@XmlType(propOrder = {"id", "fileName", "cutType", "usConeX", "usConeY"})
public class USnapshot_packed implements IUnpackable {

    @XmlAttribute
    private int id, usConeX, usConeY;
    
    @XmlAttribute
    private String fileName, cutType;
    
    //==============================================================================================
    public USnapshot_packed() {}
    
    //==============================================================================================
    public USnapshot_packed (USnapshot snapshot) {
        this.id = snapshot.getId();
        this.fileName = snapshot.getFileName();
        this.cutType = snapshot.getCutType().toString();
        this.usConeX = snapshot.getUsConeX();
        this.usConeY = snapshot.getUsConeY();
    }
    
    //==============================================================================================
    @Override
    public Function<IUnpackable, IPackable> getUnpackAction() {
        return (input) -> {
            USnapshot_packed packed = (USnapshot_packed) input;
            USnapshot unpacked = new USnapshot(packed.fileName, LiverCutType.valueOf(packed.cutType));
            unpacked.setId(packed.id);
            unpacked.setUsConeX(packed.usConeX);
            unpacked.setUsConeY(packed.usConeY);
            return unpacked;
        };
    }

    //==============================================================================================
    @Override
    public Class<?> getUnpackT() {
        return USnapshot.class;
    }
    
}
