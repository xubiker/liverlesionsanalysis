package lfa.data.packed;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import lfa.data.USnapshot;
import lfa.data.USnapshotCollection;
import lfa.packing.IPackable;
import lfa.packing.IUnpackable;

//**************************************************************************************************
public class USnapshotCollection_packed implements IUnpackable {

    @XmlAttribute
    private final String path;
    
    @XmlElements({ 
        @XmlElement(name="usnapshot")
    })
    @XmlElementWrapper(name = "snapshots")
    private final List<USnapshot_packed> snapshots = new ArrayList<>();

    //==============================================================================================
    public USnapshotCollection_packed() {
        path = "";
    }

    //==============================================================================================
    public USnapshotCollection_packed(USnapshotCollection uc) {
        this.path = uc.getPath();
        uc.toMap().values().stream().forEach(u -> {
            USnapshot_packed up = (USnapshot_packed) u.getPackAction().apply(u);
            snapshots.add(up);
        });
    }

    //==============================================================================================
    @Override
    public Function<IUnpackable, IPackable> getUnpackAction() {
        return (input) -> {
            USnapshotCollection_packed packed = (USnapshotCollection_packed) input;
            USnapshotCollection unpacked = new USnapshotCollection();
            unpacked.setPath(packed.path);
            packed.snapshots.stream().forEach(sp -> {
                USnapshot s = (USnapshot) sp.getUnpackAction().apply(sp);
                try {
                    unpacked.addSnapshot(s);
                } catch (Exception ex) {
                    Logger.getLogger(USnapshotCollection_packed.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            return unpacked;
        };
    }

    //==============================================================================================
    @Override
    public Class<?> getUnpackT() {
        return USnapshotCollection.class;
    }
    
}
