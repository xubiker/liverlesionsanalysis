package lfa.data.packed;

import java.util.function.Function;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import lfa.data.Roi;
import lfa.packing.IPackable;
import lfa.packing.IUnpackable;
import base.time.TimeManager;

//**************************************************************************************************
@XmlRootElement
@XmlType(propOrder = {"patientId", "snapshotId", "id", "radius", "centerX", "centerY", "alpha", "lastUpdate"})
public class Roi_packed implements IUnpackable {

    @XmlAttribute
    private int id, centerX, centerY, radius, patientId, snapshotId;
    
    @XmlAttribute
    private double alpha;

    @XmlAttribute
    private String lastUpdate;
    
    //==============================================================================================
    public Roi_packed() {}
    
    //==============================================================================================
    public Roi_packed(Roi roi) {
        this.id = roi.getId();
        this.centerX = roi.getCenterX();
        this.centerY = roi.getCenterY();
        this.radius = roi.getRadius();
        this.patientId = roi.getPatientId();
        this.snapshotId = roi.getSnapshotId();
        this.alpha = roi.getAlpha();
        this.lastUpdate = TimeManager.convertMillisToTimeStamp(roi.getLastUpdateTime());
    }
    
    //==============================================================================================
    @Override
    public Function<IUnpackable, IPackable> getUnpackAction() {
        return (input) -> {
            Roi_packed packed = (Roi_packed)input;
            Roi unpacked = new Roi(packed.centerX, packed.centerY, packed.radius, packed.alpha,
                    packed.patientId, packed.snapshotId, id,
                    TimeManager.convertTimeStampToMillis(lastUpdate));
            return unpacked;
        };
    }

    //==============================================================================================
    @Override
    public Class<?> getUnpackT() {
        return Roi.class;
    }
    
}
