package lfa.data.packed;

import lfa.diagnosis.MultiDiagnosis;
import lfa.data.*;
import lfa.packing.IPackable;
import lfa.packing.IUnpackable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.function.Function;

//**************************************************************************************************
@XmlType(propOrder = {"id", "path", "name", "surname", "gender", "age", "label", "diagnosis", "analysis", "ultrasound"})
public class Patient_packed implements IUnpackable {

    @XmlAttribute
    private int id, age;

    @XmlAttribute
    private String name, surname, gender, path, label;

    @XmlElement(name = "multiDiagnosis")
    private MultiDiagnosis_packed diagnosis;

    @XmlElement(name = "ultrasound")
    private USnapshotCollection_packed ultrasound;

    @XmlElement(name = "analysis")
    private Analysis_packed analysis;

    //==============================================================================================
    public Patient_packed() {}

    //==============================================================================================
    public Patient_packed(Patient p) {
        this.id = p.getId();
        this.name = p.getName();
        this.surname = p.getSurname();
        this.gender = p.getGender().toString();
        this.path = p.getPath();
        this.label = p.getLabel();
        this.age = p.getAge();
        this.diagnosis = new MultiDiagnosis_packed(p.getMultiDiagnosis());
        this.ultrasound = new USnapshotCollection_packed(p.getUltrasound());
        this.analysis = new Analysis_packed(p.getAnalysis());
    }
    
    //==============================================================================================
    @Override
    public Function<IUnpackable, IPackable> getUnpackAction() {
        return (input) -> {
            Patient_packed packed = (Patient_packed) input;
            Patient unpacked = Patient.create();
            unpacked.setId(packed.id);
            unpacked.setName(packed.name);
            unpacked.setSurname(packed.surname);
            unpacked.setGender(PatientGender.valueOf(packed.gender));
            unpacked.setPath(packed.path);
            unpacked.setLabel(packed.label);
            unpacked.setAge(packed.age);
            unpacked.setMultiDiagnosis((MultiDiagnosis) packed.diagnosis.getUnpackAction().apply(packed.diagnosis));
            unpacked.setUltrasound((USnapshotCollection) packed.ultrasound.getUnpackAction().apply(packed.ultrasound));
            unpacked.setAnalysis((Analysis) packed.analysis.getUnpackAction().apply(packed.analysis));
            return unpacked;
        };
    }

    //==============================================================================================
    @Override
    public Class<?> getUnpackT() {
        return Patient.class;
    }
    
}
