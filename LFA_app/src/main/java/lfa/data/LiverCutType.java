package lfa.data;

//**************************************************************************************************
public enum LiverCutType {
    UNUSED(0),
    CUT1(1),
    CUT2(2),
    CUT3(3),
    CUT4(4);
    
    private final int value;
    
    private LiverCutType(int value) {
        this.value = value;
    }

    public int getIntValue() {
        return value;
    }

    public static String[] stringValues() {
        LiverCutType[] vals = values();
        String[] names = new String[vals.length];
        for (int i = 0; i < vals.length; i++) {
            names[i] = vals[i].toString();
        }
        return names;
    }
    
}
