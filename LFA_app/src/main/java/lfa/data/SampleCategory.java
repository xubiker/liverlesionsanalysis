package lfa.data;

//**************************************************************************************************
public enum SampleCategory {
    TRAIN_SET(0),
    TEST_SET(1),
    UNUSED(2),
    MIXED(3),
    NONE(4);
    
    private final int value;
    
    SampleCategory(int value) {
        this.value = value;
    }

    public int getIntValue() {
        return value;
    }

    
}
