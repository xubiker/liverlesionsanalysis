package lfa.data;

import base.events.*;
import lfa.data.packed.PatientCollection_packed;
import lfa.events.CollectionUpdateXEvent;
import lfa.packing.IPackable;
import lfa.packing.IUnpackable;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

//**************************************************************************************************
public final class PatientCollection implements IPackable, XEventBroadcaster {

    private String path;
    private final ConcurrentMap<Integer, Patient> map = new ConcurrentHashMap<>();
    private final XEventBroadcasterManager xbm =  XEventFactory.createBroadcaster();

    //==============================================================================================
    public PatientCollection() { }

    //==============================================================================================
    public PatientCollection(String path, Collection<Patient> patients) {
        this.path = path;
        try {
            addPatients(patients);
        } catch (Exception ex) {
            Logger.getLogger(PatientCollection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //==============================================================================================
    public void clearAll() {
        try {
            while (map.keySet().iterator().hasNext()) {
                removePatient(map.keySet().iterator().next());
            }
        } catch (Exception ex) {
            Logger.getLogger(PatientCollection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //==============================================================================================
    public void addPatient(Patient patient) throws Exception {
        Integer id = patient.getId();
        if (id == -1) {
            int newId = (map.isEmpty()) ? 1 : Collections.max(map.keySet()) + 1;
            patient.setId(newId);
        } else if (map.containsKey(id)) {
            throw new Exception("add patient error: collection already contains this patient");
        }
        map.put(patient.getId(), patient);
        notifyXEventListeners(new CollectionUpdateXEvent.ElementsAdded<>(this, patient));
    }
    
    //==============================================================================================
    public void addPatients(Collection<Patient> patients) throws Exception {
        for (Patient p : patients) {
            addPatient(p);
        }
    }

    //==============================================================================================
    public void removePatient(int id) throws Exception {
        Patient oldPatient = map.remove(id);
        if (oldPatient == null) {
            throw new Exception("remove patient error: patient not found");
        }
        notifyXEventListeners(new CollectionUpdateXEvent.ElementsRemoved<>(this, oldPatient));
    }
    
    //==============================================================================================
    public void modifyPatient(int id, Patient newPatient) throws Exception {
        Patient oldPatient = map.get(id);
        if (oldPatient == null) {
            throw new Exception("modify patient error: patient not found");
        }
        newPatient.setId(id);
        map.put(id, newPatient);
        notifyXEventListeners(new CollectionUpdateXEvent.ElementsModified<>(this, oldPatient, newPatient));
    }

    //==============================================================================================
    public String getPath() {
        return path;
    }

    //==============================================================================================
    public void setPath(String path) {
        this.path = path;
    }

    //==============================================================================================
    @Override
    public Function<IPackable, IUnpackable> getPackAction() {
        Function<IPackable, IUnpackable> pack = input -> {
            return new PatientCollection_packed((PatientCollection) input);
        };
        return pack;
    }

    //==============================================================================================
    @Override
    public Class<?> getPackT() {
        return PatientCollection_packed.class;
    }
    
    //==============================================================================================
    public List<Patient> toList() {
        return map.values().stream().collect(Collectors.toList());
    }
    
    //==============================================================================================
    public Map<Integer, Patient> toMap() {
        return map;
    }

    //==============================================================================================
    @Override
    public void registerXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        xbm.registerXEventListener(event_t, listener);
    }

    //==============================================================================================
    @Override
    public void unregisterXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        xbm.unregisterXEventListener(event_t, listener);
    }

    //==============================================================================================
    private void notifyXEventListeners(XEvent event) {
        xbm.notifyXEventListeners(event);
    }
}
