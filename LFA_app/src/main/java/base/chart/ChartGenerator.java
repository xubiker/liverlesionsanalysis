package base.chart;

import base.collections.Pair;
import java.awt.Color;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

//**************************************************************************************************
public class ChartGenerator {

    //==============================================================================================
    public JFreeChart generateXYChart1(Collection<Pair<List<Double>, String>> series,
            String chartTitle, String xTitle, String yTitle) {

        List<XYSeries> xySeries = series.stream().map(s -> {
            final XYSeries ser = new XYSeries(s.getSecond());
            int i = 0;
            for (Double d : s.getFirst()) {
                ser.add(i, d);
                i++;
            }
            return ser;
        }).collect(Collectors.toList());
        return generateXYChart0(xySeries, chartTitle, xTitle, yTitle);
    }
    
    //==============================================================================================
    public JFreeChart generateXYChart2(Collection<Pair<List<Pair<Double, Double>>, String>> series,
            String chartTitle, String xTitle, String yTitle) {

        List<XYSeries> xySeries = series.stream().map(s -> {
            final XYSeries ser = new XYSeries(s.getSecond());
            s.getFirst().stream().forEach(p -> ser.add(p.getFirst(), p.getSecond()));
            return ser;
        }).collect(Collectors.toList());
        return generateXYChart0(xySeries, chartTitle, xTitle, yTitle);
    }
    
    //==============================================================================================
    public JFreeChart generateXYChart0(Collection<XYSeries> series,
            String chartTitle, String xTitle, String yTitle) {
        
        final XYSeriesCollection dataset = new XYSeriesCollection();
        series.stream().forEach(s -> dataset.addSeries(s));
        
        // create the chart...
        final JFreeChart chart = ChartFactory.createXYLineChart(
            chartTitle,      // chart title
            xTitle,                      // x axis label
            yTitle,                      // y axis label
            dataset,                  // data
            PlotOrientation.VERTICAL,
            true,                     // include legend
            true,                     // tooltips
            false                     // urls
        );

        // NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...
        chart.setBackgroundPaint(Color.white);

//        final StandardLegend legend = (StandardLegend) chart.getLegend();
  //      legend.setDisplaySeriesShapes(true);
        
        // get a reference to the plot for further customisation...
        final XYPlot plot = chart.getXYPlot();
        plot.setBackgroundPaint(Color.lightGray);
    //    plot.setAxisOffset(new Spacer(Spacer.ABSOLUTE, 5.0, 5.0, 5.0, 5.0));
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);
        
        final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
//        renderer.setSeriesLinesVisible(0, false);
//        renderer.setSeriesShapesVisible(1, false);
        plot.setRenderer(renderer);

        // change the auto tick unit selection to integer units only...
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        // OPTIONAL CUSTOMISATION COMPLETED.
                
        return chart;        
        
    }
    

}
