package base.chart;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;

//**************************************************************************************************
public class XChartPanel extends ChartPanel {

    public XChartPanel() {
        super(null);
    }
    
    public void update(JFreeChart chart) {
        super.setChart(chart);
    }
    
}
