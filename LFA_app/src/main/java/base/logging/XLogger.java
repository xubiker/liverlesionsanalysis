package base.logging;

import base.time.TimeManager;

import java.io.FileWriter;
import java.io.IOException;

class XLogger implements ILogger {

    private String fileName, sysFileName;

    private FileWriter fileWriter;
    private FileWriter sysFileWriter;
    private boolean fwIsActive = false;
    private boolean sysFwIsActive = false;

    XLogger() {
        String hash = Long.toString(TimeManager.getTimeInMillis());
        fileName = "logs/" + hash + ".log";
        sysFileName = "logs/" + "_" + hash + ".log";
        try {
            fileWriter = new FileWriter(fileName, false);
            sysFileWriter = new FileWriter(sysFileName, false);
            fwIsActive = true;
            sysFwIsActive = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getFilePath() {
        return fileName;
    }

    @Override
    public String getSysFilePath() {
        return sysFileName;
    }

    @Override
    public void message(String message) {
        if (fwIsActive && fileWriter != null) {
            try {
                fileWriter.write(message + "\n");
                fileWriter.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void sysMessage(String message) {
        if (sysFwIsActive && sysFileWriter != null) {
            try {
                sysFileWriter.write(message + "\n");
                sysFileWriter.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void close() {
        if (fwIsActive && fileWriter != null) {
            try {
                fileWriter.close();
                fwIsActive = false;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (sysFwIsActive && sysFileWriter != null) {
            try {
                sysFileWriter.close();
                sysFwIsActive = false;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
