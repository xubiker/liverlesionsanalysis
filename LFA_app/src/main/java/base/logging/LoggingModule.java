package base.logging;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

public class LoggingModule extends AbstractModule {

    protected void configure() {
        bind(ILogger.class).to(XLogger.class).in(Singleton.class);
    }
}