package base.logging;

public interface ILogger {

    void message(String message);
    void sysMessage(String message);

    String getFilePath();
    String getSysFilePath();

    void close();

}
