package base.backgroundexecution;
 
import com.google.inject.Guice;
import com.google.inject.Injector;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

//**************************************************************************************************
public final class XBackgroundDemo extends JPanel {
 
    private static Injector injector;

    private Double foo(IBackgroundMonitor controller) {
        controller.updateProgress(0);
        double sum = 0;
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < 100; j++) {
                for (int k = 0; k < 100; k++) {
                    for (int z = 0; z < 20; z++) {
                        if (controller.isWorkerCancelled()) {
                            return null;
                        }
                        sum += (Math.sin((double)i * j * Math.exp(k / 10))) / 20;
                    }
                }
            }
            controller.updateProgress(i + 1);
        }
        return sum;
    }
 
    private XBackgroundDemo(JFrame frame) {
        super(new BorderLayout());
        JButton startButton = new JButton("Start");
        startButton.setActionCommand("start");
        startButton.addActionListener(ae ->  action(ae, frame));
        add(startButton, BorderLayout.PAGE_START);
        setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
    }
 
    private void action(ActionEvent evt, JFrame frame) {
        IBackgroundFactory factory = injector.getInstance(IBackgroundFactory.class);
        IBackgroundWizard wizard = factory.createWizard(frame, new XProgressMonitorSettings(true, true));
        wizard.addTask(new XBackgroundTask(this::foo));
        wizard.addTask(new XBackgroundTask(this::foo));
        wizard.addTask(new XBackgroundTask(this::foo));
        wizard.start();
    }
 
    private static void createAndShowGUI() {
        JFrame frame = new JFrame("ProgressMonitorDemo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 
        JComponent newContentPane = new XBackgroundDemo(frame);
        newContentPane.setOpaque(true); //content panes must be opaque
        frame.setContentPane(newContentPane);
 
        frame.pack();
        frame.setVisible(true);
    }
 
    public static void main(String[] args) {
        injector = Guice.createInjector(
                new XBackgroundModule()
        );
        javax.swing.SwingUtilities.invokeLater(XBackgroundDemo::createAndShowGUI);
    }
}