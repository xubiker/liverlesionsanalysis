package base.backgroundexecution;

//**************************************************************************************************
public class XBackgroundResult {
    
    private final Object result;
    private final boolean cancelled, done;
    
    //==============================================================================================
    XBackgroundResult(Object result, boolean cancelled, boolean done) {
        this.result = result;
        this.cancelled = cancelled;
        this.done = done;
    }
    
    //==============================================================================================
    Object getResult() {
        return result;
    }
    
    //==============================================================================================
    boolean canceled() {
        return cancelled;
    }
    
    //==============================================================================================
    boolean succeeded() {
        return done;
    }
    
}
