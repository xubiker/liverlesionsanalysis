package base.backgroundexecution;

//**************************************************************************************************
public interface IBackgroundMonitor {

    void updateProgress(int progressValue);
    void updateProgressInc(double progressValueInc);
    void updateProgress(int progressValue, String message, String shortMessage);
    void updateProgressInc(double progressValueInc, String message, String shortMessage);
    boolean isWorkerCancelled();
}
