package base.backgroundexecution;

import base.GraphicsConverter;
import base.resource.IResourceLoader;
import com.mortennobel.imagescaling.ResampleOp;
import java.awt.Frame;
import java.awt.Image;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;
import javax.swing.JDialog;

//**************************************************************************************************
class XProgressMonitorV extends JDialog {

    private XProgressMonitorVM vm;

    //==============================================================================================
    public XProgressMonitorV(Frame parent, IResourceLoader resourceLoader) {
        super(parent, false);
        initComponents();
        try {
            Image progressImage = resourceLoader.loadImage("waitIcon.png");
            BufferedImage buffered = GraphicsConverter.imageToBufferedImage(progressImage);
            ResampleOp resampleOp = new ResampleOp(imageLabel.getSize().width, imageLabel.getSize().height);
            BufferedImage resampled = resampleOp.filter(buffered, null);
            imageLabel.setIcon(new ImageIcon(resampled));
        } catch (Exception ex) {
            System.err.println("exception: " + ex);
        }
        setLocationRelativeTo(null);
        progressBar.setValue(0);
    }
    
    //==============================================================================================
    public void setViewModel(XProgressMonitorVM vm) {
        this.vm = vm;
    }
    
    //==============================================================================================
    public void updateProgess(int progressValue, String message, String shortMessage) {
        this.progressBar.setValue(progressValue);
        if (message != null) {
            this.textPane.setText(message);
        }
        if (shortMessage != null) {
            this.progressBar.setStringPainted(true);
            this.progressBar.setString(shortMessage);
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        textPane = new javax.swing.JTextPane();
        progressBar = new javax.swing.JProgressBar();
        imageLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        textPane.setEditable(false);
        textPane.setFocusable(false);
        textPane.setOpaque(false);
        jScrollPane2.setViewportView(textPane);

        progressBar.setMinimumSize(new java.awt.Dimension(10, 100));

        imageLabel.setFocusable(false);
        imageLabel.setMaximumSize(new java.awt.Dimension(150, 150));
        imageLabel.setMinimumSize(new java.awt.Dimension(150, 150));
        imageLabel.setPreferredSize(new java.awt.Dimension(150, 150));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(imageLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 481, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(progressBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(imageLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //==============================================================================================
    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        if (vm != null) {
            vm.cancel();
        }
    }//GEN-LAST:event_formWindowClosing

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel imageLabel;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JTextPane textPane;
    // End of variables declaration//GEN-END:variables

}
