package base.backgroundexecution;

import com.google.inject.assistedinject.Assisted;

import javax.annotation.*;
import java.awt.*;

public interface IBackgroundFactory {

    IBackgroundWizard createWizard(
            @Assisted("baseFrame") @Nonnull Frame baseFrame,
            @Assisted("settings") @Nonnull XProgressMonitorSettings settings
    );
    IBackgroundWizard createWizard(
            @Assisted("baseFrame") @Nonnull Frame baseFrame
    );
}
