package base.backgroundexecution;

import javax.swing.*;
import  java.util.function.Consumer;

//**************************************************************************************************
public class XBackgroundWorker extends SwingWorker<Object, Integer> implements IBackgroundMonitor {

    private final XBackgroundTask task;
    private final Consumer<XBackgroundResult> afterAction;
    private XProgressMonitorVM progressMonitorVM;
    
    private String messageBackup = "";
    private double progressValueBackup = 0;
    
    //==============================================================================================
    XBackgroundWorker(XBackgroundTask task,
                      Consumer<XBackgroundResult> afterAction) {
        this.task = task;
        this.afterAction = afterAction;
    }

    //==============================================================================================
    void setProgressMonitor(XProgressMonitorVM monitor) {
        this.progressMonitorVM = monitor;
    }

    //==============================================================================================
    @Override
    protected Object doInBackground() throws Exception {
        return task.get().apply(this);
    }
    
    //==============================================================================================
    @Override
    public synchronized void updateProgress(int progressValue) {
        progressValueBackup = progressValue;
        String shortMessage = progressValue + "%";
        if (progressMonitorVM != null) {
            progressMonitorVM.updateProgress(progressValue, messageBackup, shortMessage);
        }
    }

    //==============================================================================================
    @Override
    public synchronized void updateProgress(int progressValue, String message, String shortMessage) {
        progressValueBackup = progressValue;
        if (message != null) {
            messageBackup = message;
        }
        if (shortMessage == null) {
            shortMessage = progressValue + "%";
        }
        if (progressMonitorVM != null) {
            progressMonitorVM.updateProgress(progressValue, messageBackup, shortMessage);
        }
    }

    //==============================================================================================
    @Override
    public synchronized void updateProgressInc(double progressValueInc, String message, String shortMessage) {
        progressValueBackup += progressValueInc;
        if (message != null) {
            messageBackup = message;
        }
        if (shortMessage == null) {
            shortMessage = (int) progressValueBackup + "%";
        }
        if (progressMonitorVM != null) {
            progressMonitorVM.updateProgress((int) progressValueBackup, messageBackup, shortMessage);
        }
    }

    //==============================================================================================
    @Override
    public synchronized void updateProgressInc(double progressValueInc) {
        progressValueBackup += progressValueInc;
        String shortMessage = (int) progressValueBackup + "%";
        if (progressMonitorVM != null) {
            progressMonitorVM.updateProgress((int) progressValueBackup, messageBackup, shortMessage);
        }
    }

    //==============================================================================================
    @Override
    public synchronized boolean isWorkerCancelled() {
        return super.isCancelled();
    }
    
    //==============================================================================================
    @Override
    protected void done() {
        if (progressMonitorVM != null) {
            progressMonitorVM.done(false);
        }
        if (afterAction != null) {
            if (isCancelled()) {
                afterAction.accept(new XBackgroundResult(null, true, false));
            } else if (isDone()) {
                afterAction.accept(new XBackgroundResult(null, false, true));
            }
        }
    }

    //==============================================================================================
    void start() {
        super.execute();
    }

    //==============================================================================================
    void cancel() {
        super.cancel(true);
    }

}