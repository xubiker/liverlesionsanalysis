package base.backgroundexecution;

import java.awt.*;

//**************************************************************************************************
public class XProgressMonitorVM {
    
    private XProgressMonitorV view;
    private XProgressMonitorSettings settings;
    private Frame baseFrame;
    private XBackgroundWorker worker;

    //==============================================================================================
    XProgressMonitorVM(XBackgroundWorker worker, XProgressMonitorV view, XProgressMonitorSettings settings, Frame baseFrame) {
        this.worker = worker;
        this.settings = settings;
        this.baseFrame = baseFrame;
        this.view = view;
        view.setViewModel(this);
    }

    //==============================================================================================
    void setBackgroundWorker(XBackgroundWorker worker) {
        this.worker = worker;
    }

    //==============================================================================================
    void updateProgress(int i) {
        if (view != null) {
            view.updateProgess(i, null, null);
        }
    }

    //==============================================================================================
    void updateProgress(int i, String message, String shortMessage) {
        if (view != null) {
            view.updateProgess(i, message, shortMessage);
        }
    }

    //==============================================================================================
    void showDialog(boolean show) {
        if (baseFrame != null) {
            baseFrame.setEnabled(!show);
        }
        if (view != null) {
            view.setVisible(show);
        }
    }

    //==============================================================================================
    void done(boolean forceCancelled) {
        if (forceCancelled && settings.getBeepOnCancel()) {
                Toolkit.getDefaultToolkit().beep();
        }
        if (!forceCancelled && settings.getBeepOnFinish()) {
            Toolkit.getDefaultToolkit().beep();
        }
        showDialog(false);
    }

    //==============================================================================================
    void cancel() {
        worker.cancel();
        if (settings.getBeepOnCancel()) {
            Toolkit.getDefaultToolkit().beep();
        }
        showDialog(false);
    }

}
