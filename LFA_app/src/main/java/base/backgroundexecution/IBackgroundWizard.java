package base.backgroundexecution;

import java.util.Collection;
import java.util.Map;

public interface IBackgroundWizard {

    void addTask(XBackgroundTask task);
    void addTask(XBackgroundTask... tasks);
    void addTask(Collection<XBackgroundTask> tasks);
    void start();
    void addTaskAndStart(XBackgroundTask task);

    Map<Integer, XBackgroundResult> getResults();
}
