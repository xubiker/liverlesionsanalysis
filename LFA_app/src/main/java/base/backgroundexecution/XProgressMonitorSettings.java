package base.backgroundexecution;

//**************************************************************************************************
public class XProgressMonitorSettings {
    
    private final boolean beepOnFinish;
    private final boolean beepOnCancel;
    
    public XProgressMonitorSettings(boolean beepOnFinish, boolean beepOnCancel) {
        this.beepOnFinish = beepOnFinish;
        this.beepOnCancel = beepOnCancel;
    }
    
    public XProgressMonitorSettings() {
        this.beepOnCancel = true;
        this.beepOnFinish = false;
    }

    public boolean getBeepOnFinish() {
        return beepOnFinish;
    }

    public boolean getBeepOnCancel() {
        return beepOnCancel;
    }
}
