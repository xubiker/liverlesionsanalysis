package base.backgroundexecution;

import base.resource.ResourceLoaderModule;
import com.google.inject.AbstractModule;
import com.google.inject.assistedinject.FactoryModuleBuilder;

//**************************************************************************************************
public class XBackgroundModule extends AbstractModule {

    @Override
    protected void configure() {
        install(
                new FactoryModuleBuilder()
                        .implement(IBackgroundWizard.class, XBackgroundWizard.class)
                        .build(IBackgroundFactory.class)
        );

        install(new ResourceLoaderModule());
    }

}
