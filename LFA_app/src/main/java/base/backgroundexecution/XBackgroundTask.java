package base.backgroundexecution;

import base.id.IdGenerator;

import java.util.function.Function;

public class XBackgroundTask {

    private int id;

    private Function<IBackgroundMonitor, Object> workAction;

    public XBackgroundTask(Function<IBackgroundMonitor, Object> workAction) {
        this.id = IdGenerator.next();
        this.workAction = workAction;
    }

    public XBackgroundTask(int id, Function<IBackgroundMonitor, Object> workAction) {
        this.id = id;
        this.workAction = workAction;
    }

    public Function<IBackgroundMonitor, Object> get() {
        return workAction;
    }

    public int getId() {
        return id;
    }

}
