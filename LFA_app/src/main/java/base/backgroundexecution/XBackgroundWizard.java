package base.backgroundexecution;

import base.resource.IResourceLoader;
import com.google.inject.assistedinject.Assisted;
import com.google.inject.assistedinject.AssistedInject;

import javax.annotation.Nonnull;
import java.awt.*;
import java.util.*;
import java.util.List;

//**********************************************************************************************************************
class XBackgroundWizard implements IBackgroundWizard {

    private XProgressMonitorVM progressMonitorVM;

    private List<Collection<XBackgroundTask>> tasks = new ArrayList<>();
    private XBackgroundTask currentTask;
    private Iterator<Collection<XBackgroundTask>> extIterator = tasks.iterator();
    private Iterator<XBackgroundTask> intIterator;

    private Map<Integer, XBackgroundResult> resultMap = new HashMap<>();

    @AssistedInject
    public XBackgroundWizard(
            @Assisted("baseFrame") @Nonnull Frame baseFrame,
            @Assisted("settings") @Nonnull XProgressMonitorSettings settings,
                IResourceLoader resourceLoader
    ) {
        XProgressMonitorV progressMonitorV = new XProgressMonitorV(baseFrame, resourceLoader);
        progressMonitorVM = new XProgressMonitorVM(null, progressMonitorV, settings, baseFrame);
    }

    @AssistedInject
    public XBackgroundWizard(
            @Assisted("baseFrame") @Nonnull Frame baseFrame,
            IResourceLoader resourceLoader
    ) {
        this(baseFrame, new XProgressMonitorSettings(false, true), resourceLoader);
    }

    @Override
    public void addTask(XBackgroundTask task) {
        List<XBackgroundTask> list = new ArrayList<>();
        list.add(task);
        tasks.add(list);
    }

    @Override
    public void addTask(XBackgroundTask... tasks) {
        List<XBackgroundTask> list = new ArrayList<>();
        Collections.addAll(list, tasks);
        this.tasks.add(list);
    }

    @Override
    public void addTask(Collection<XBackgroundTask> tasks) {
        this.tasks.add(tasks);
    }

    private XBackgroundTask getNextTask() {
        if (intIterator != null && intIterator.hasNext()) {
            currentTask = intIterator.next();
            return currentTask;
        } else {
            if (extIterator.hasNext()) {
                intIterator = extIterator.next().iterator();
                if (intIterator.hasNext()) {
                    currentTask = intIterator.next();
                    return currentTask;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }
    }

    private void next(XBackgroundResult prevRes) {
        XBackgroundTask task = getNextTask();
        if (prevRes.succeeded() && task != null) {
            resultMap.put(currentTask.getId(), prevRes);
            XBackgroundWorker worker = new XBackgroundWorker(task, this::next);
            worker.setProgressMonitor(progressMonitorVM);
            progressMonitorVM.setBackgroundWorker(worker);
            progressMonitorVM.showDialog(true);
            worker.start();
        } else {
            if (progressMonitorVM != null) {
                progressMonitorVM.done(prevRes.canceled());
            }
            if (prevRes.canceled()) {
                System.out.println("XBackgroundWizard: canceled.");
            } else {
                System.out.println("XBackgroundWizard: all done.");
            }
        }
    }

    @Override
    public void start() {
        this.extIterator = tasks.iterator();
        progressMonitorVM.showDialog(true);
        next(new XBackgroundResult(null, false, true));
    }

    @Override
    public void addTaskAndStart(XBackgroundTask task) {
        addTask(task);
        start();
    }

    @Override
    public Map<Integer, XBackgroundResult> getResults() {
        return resultMap;
    }

}
