package base.collections;

import java.io.Serializable;

//**************************************************************************************************
public class Triplet<A, B, C> implements Serializable {
    
    private A first;
    private B second;
    private C third;

    //==============================================================================================
    public Triplet(A first, B second, C third) {
    	super();
    	this.first = first;
    	this.second = second;
        this.third = third;
    }

    //==============================================================================================
    @Override
    public int hashCode() {
    	int hash1 = first != null ? first.hashCode() : 0;
    	int hash2 = second != null ? second.hashCode() : 0;
    	int hash3 = third != null ? third.hashCode() : 0;
    	return (hash1 + hash2 + hash3) * hash3 + (hash1 + hash2) * hash2 + hash1;
    }

    //==============================================================================================
    @Override
    public boolean equals(Object other) {
    	if (other instanceof Triplet) {
            Triplet otherTriplet = (Triplet) other;
            return ((this.first.equals(otherTriplet.first))
                    && (this.second.equals(otherTriplet.second))
                    && (this.third.equals(otherTriplet.third)));
    	}
    	return false;
    }

    //==============================================================================================
    @Override
    public String toString()
    { 
           return "(" + first + ", " + second + ", " + third + ")"; 
    }

    //==============================================================================================
    public A getFirst() {
    	return first;
    }

    //==============================================================================================
    public void setFirst(A first) {
    	this.first = first;
    }

    //==============================================================================================
    public B getSecond() {
    	return second;
    }

    //==============================================================================================
    public void setSecond(B second) {
    	this.second = second;
    }

    //==============================================================================================
    public C getThird() {
    	return third;
    }

    //==============================================================================================
    public void setThird(C third) {
    	this.third = third;
    }

}