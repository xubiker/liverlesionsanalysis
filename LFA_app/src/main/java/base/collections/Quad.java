package base.collections;

import java.io.Serializable;
import java.util.Objects;

//**************************************************************************************************
public class Quad<A, B, C, D> implements Serializable {
    
    private A first;
    private B second;
    private C third;
    private D fourth;

    //==============================================================================================
    public Quad(A first, B second, C third, D fourth) {
    	super();
    	this.first = first;
    	this.second = second;
        this.third = third;
        this.fourth = fourth;
    }

    //==============================================================================================
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.first);
        hash = 53 * hash + Objects.hashCode(this.second);
        hash = 53 * hash + Objects.hashCode(this.third);
        hash = 53 * hash + Objects.hashCode(this.fourth);
        return hash;
    }

    //==============================================================================================
    @Override
    public boolean equals(Object other) {
    	if (other instanceof Quad) {
            Quad otherQuad = (Quad) other;
            return ((this.first.equals(otherQuad.first))
                    && (this.second.equals(otherQuad.second))
                    && (this.third.equals(otherQuad.third))
                    && (this.fourth.equals(otherQuad.fourth))
                    );
    	}
    	return false;
    }

    //==============================================================================================
    @Override
    public String toString()
    { 
           return "(" + first + ", " + second + ", " + third + ", " + fourth + ")"; 
    }

    //==============================================================================================
    public A getFirst() {
    	return first;
    }

    //==============================================================================================
    public void setFirst(A first) {
    	this.first = first;
    }

    //==============================================================================================
    public B getSecond() {
    	return second;
    }

    //==============================================================================================
    public void setSecond(B second) {
    	this.second = second;
    }

    //==============================================================================================
    public C getThird() {
    	return third;
    }

    //==============================================================================================
    public void setThird(C third) {
    	this.third = third;
    }

    //==============================================================================================
    public D getFourth() {
    	return fourth;
    }

    //==============================================================================================
    public void setFourth(D fourth) {
    	this.fourth = fourth;
    }
}
