package base.events;

//**************************************************************************************************
public interface XEventBroadcaster {
    
    public void registerXEventListener(Class<? extends XEvent> event_t, XEventListener listener);

    public void unregisterXEventListener(Class<? extends XEvent> event_t, XEventListener listener);
    
    //public void notifyXEventListeners(XEvent event);
        
}
