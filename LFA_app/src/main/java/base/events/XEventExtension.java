package base.events;

//**************************************************************************************************
public class XEventExtension extends XEvent {

    private final Class<?> parentEvent_t;

    //==============================================================================================
    protected XEventExtension(XEventBroadcaster sender, Class<?> parentEvent_t) {
        super(sender);
        this.parentEvent_t = parentEvent_t;
    }
    
    //==============================================================================================
    protected Class<?> getParentEvent_t() {
        return parentEvent_t;
    }
    
}
