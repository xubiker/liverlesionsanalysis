package base.events;

//**************************************************************************************************
public interface XEventListener {
    
    public void eventHappened(XEvent event);

}
