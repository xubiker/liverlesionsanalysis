package base.events;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//**************************************************************************************************
public class XEventBroadcasterManager implements XEventBroadcaster {

    private final Map<Class<?>, List<XEventListener>> xEventListenerMap = new HashMap<>();

    //==============================================================================================
    private XEventBroadcasterManager() {}

    //==============================================================================================
    public static XEventBroadcasterManager create() {
        return new XEventBroadcasterManager();
    }

    //==============================================================================================
    @Override
    public void registerXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        if (xEventListenerMap.containsKey(event_t)) {
            xEventListenerMap.get(event_t).add(listener);
        } else {
            List<XEventListener> l = new ArrayList<>();
            l.add(listener);
            xEventListenerMap.put(event_t, l);
        }
    }
    
    //==============================================================================================
    @Override
    public void unregisterXEventListener(Class<? extends XEvent> event_t, XEventListener listener) {
        if (!xEventListenerMap.containsKey(event_t)) {
            System.err.println("XEventBroadcaster: trying to unregister listener for non-registered event");
        } else {
            xEventListenerMap.get(event_t).remove(listener);
        }
    }

    //==============================================================================================
    public void notifyXEventListeners(XEvent event) {
        Class<?> key = event.getClass();
        if (event.getClass().getSuperclass() == XEventExtension.class) {
            key = ((XEventExtension)event).getParentEvent_t();
        }
        List<XEventListener> list = xEventListenerMap.get(key);
        if (list != null) {
            list.stream().forEach(l -> l.eventHappened(event));
        }
    }
    
}
