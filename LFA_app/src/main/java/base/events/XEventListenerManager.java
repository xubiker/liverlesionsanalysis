package base.events;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

//**************************************************************************************************
public class XEventListenerManager implements XEventListener {

    private final Map<Class<? extends XEvent>, Consumer<XEvent>> xEventHandlerMap = new HashMap<>();

    //==============================================================================================
    private XEventListenerManager() {}

    //==============================================================================================
    public static XEventListenerManager create() {
        return new XEventListenerManager();
    }
    
    //==============================================================================================
    public void bindXEventHandler(Class<? extends XEvent> event_t, Consumer<XEvent> consumer) {
        if (xEventHandlerMap.containsKey(event_t)) {
            System.err.println("XEventListener: rebinding event " + event_t);
        }
        xEventHandlerMap.put(event_t, consumer);
    }
    
    //==============================================================================================
    @Override
    public void eventHappened(XEvent event) {
        Class<? extends XEvent> key = event.getClass();
        if (event.getClass().getSuperclass() == XEventExtension.class) {
            key = (Class<? extends XEvent>) ((XEventExtension)event).getParentEvent_t();
        }
        if (!xEventHandlerMap.containsKey(key)) {
            System.err.println("XEventListener: action for event not found");
            System.err.println("event: " + event.getClass() + ", sender: " + event.getSender().getClass());
            System.out.println(Arrays.toString(Thread.currentThread().getStackTrace()));
        } else {
            xEventHandlerMap.get(key).accept(event);
        }
    }
}
