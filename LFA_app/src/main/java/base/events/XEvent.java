package base.events;

//**************************************************************************************************
public class XEvent {
    
    private final XEventBroadcaster sender;
    
    //==============================================================================================
    protected XEvent(XEventBroadcaster sender) {
        this.sender = sender;
    }
    
    //==============================================================================================
    public XEventBroadcaster getSender() {
        return sender;
    }
    
}
