package base.events;

//**************************************************************************************************
public class XEventFactory {

    //==============================================================================================
    private XEventFactory() {
    }

    //==============================================================================================
    public static XEventBroadcasterManager createBroadcaster() {
        return XEventBroadcasterManager.create();
    }

    //==============================================================================================
    public static XEventListenerManager createListener() {
        return XEventListenerManager.create();
    }
    
}
