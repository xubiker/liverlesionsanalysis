package base.resource;

import java.awt.Image;
import java.net.URL;
import javax.swing.ImageIcon;

//**************************************************************************************************
class ResourceLoader implements IResourceLoader {

    //==============================================================================================
    @Override
    public Image loadImage(String fileName) throws Exception {
        URL resource = ResourceLoader.class.getClassLoader().getResource(fileName);
        if (resource == null) {
            throw new Exception("loading failed: invalid URL");
        }
        ImageIcon imageIcon = new ImageIcon(resource);
        return imageIcon.getImage();
    }
        
    //==============================================================================================
    @Override
    public Image loadAppIcon() {
        try {
            return loadImage("icon4.png");
        } catch (Exception ex) {
            System.err.println("error while loading appIcon: " + ex);
            return null;
        }
    }
    
}
