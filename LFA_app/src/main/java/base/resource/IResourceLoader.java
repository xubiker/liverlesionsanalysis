package base.resource;

import java.awt.Image;

//**************************************************************************************************
public interface IResourceLoader {

    Image loadImage(String fileName) throws Exception;
    Image loadAppIcon() throws Exception;
}
