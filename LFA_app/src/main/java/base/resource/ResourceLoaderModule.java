package base.resource;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

//**************************************************************************************************
public class ResourceLoaderModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(IResourceLoader.class).to(ResourceLoader.class).in(Singleton.class);
    }
    
}
