package base;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;
import xubiker.XImageTools.base.XImage;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

//**************************************************************************************************
public class GraphicsConverter {

    //==============================================================================================
    public static BufferedImage xImageToBufferedImage(XImage image) {
        return matToBufferedImage(image.cvData());
    }

        //==============================================================================================
    public static BufferedImage matToBufferedImage(Mat image) {
        if (image == null) {
            return null;
        }
        
        MatOfByte matOfByte = new MatOfByte();
        Imgcodecs.imencode(".bmp", image, matOfByte);

        byte[] byteArray = matOfByte.toArray();
        BufferedImage bufImage = null;
        try {
            InputStream in = new ByteArrayInputStream(byteArray);
            bufImage = ImageIO.read(in);
        } catch (IOException ex) {
            System.err.println(ex);
        }
        return bufImage;
    }
    
    //==============================================================================================
    public static Mat bufferedImageToMat(BufferedImage image) {
        if (image == null) {
            return null;
        }
        byte[] pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        Mat resImage = new Mat(image.getHeight(), image.getWidth(), CvType.CV_8UC3);
        resImage.put(0, 0, pixels);
        return resImage;
    }

    //==============================================================================================
    public static XImage bufferedImageToXImage(BufferedImage image) {
        return new XImage(bufferedImageToMat(image), true);
    }

    //==============================================================================================
    public static BufferedImage imageToBufferedImage(Image img) {
        if (img instanceof BufferedImage) {
            return (BufferedImage) img;
        }
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();
        return bimage;
    }

}
