package base.time;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

//**************************************************************************************************
public class TimeManager {
    
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd_HH.mm.ss.SSS");
    
    //==============================================================================================
    public static long getTimeInMillis() {
        return System.currentTimeMillis();
    }
    
    //==============================================================================================
    public static String getTimeInString() {
        return sdf.format(Calendar.getInstance().getTime());        
    }
    
    //==============================================================================================
    public static long getTimeInMillisSince(long startTime) {
        return getTimeInMillis() - startTime;
    }
    
    //==============================================================================================
    public static double getTimeInSecondsSince(long startTime) {
        return convertMillisToSeconds(getTimeInMillis() - startTime);
    }

    //==============================================================================================
    public static String getTimeInStringSince(long startTime) {
        return convertMillisToTimeStamp(getTimeInMillis() - startTime);
    }

    //==============================================================================================
    public static long convertTimeStampToMillis(String timeStamp) {
        try {
            return sdf.parse(timeStamp).getTime();
        } catch (ParseException ex) {
            Logger.getLogger(TimeManager.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }

    //==============================================================================================
    public static String convertMillisToTimeStamp(long millis) {
        if (millis == -1) {
            return "unknown";
        }
        return sdf.format(millis);
    }
    
    //==============================================================================================
    public static double convertMillisToSeconds(long millis) {
        return ((double)millis) / 1000;
    }
    
}
