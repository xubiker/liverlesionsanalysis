package proscalafx.ch06

import javafx.beans.{binding => jfxbb}
import javafx.{concurrent => jfxc}

import scalafx.Includes._
import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.beans.property.DoubleProperty
import scalafx.concurrent.Task
import scalafx.event.ActionEvent
import scalafx.geometry.{Insets, Pos}
import scalafx.scene.Scene
import scalafx.scene.control.{Button, ProgressBar}
import scalafx.scene.layout.{BorderPane, HBox}


class WorkerAndTaskExample extends JFXApp {

  hookupEvents()
  stage = new PrimaryStage {
    title = "Worker and Task Example"
    scene = View.scene
  }

  private def hookupEvents() {
    View.startButton.onAction = {ae: ActionEvent => new Thread(Model.Worker).start()}
    View.cancelButton.onAction = {ae: ActionEvent => Model.Worker.cancel}
  }


  private object Model {
    object Worker extends Task(new jfxc.Task[String] {
      protected def call(): String = {
        println("_________________________________")
        val total = 250
        updateProgress(0, total)
        for (i <- 1 to total) {
          try {
            Thread.sleep(20)
          } catch {
            case e: InterruptedException => return "Canceled at " + System.currentTimeMillis
          }
          updateTitle("Example Task (" + i + ")")
          updateMessage("Processed " + i + " of " + total + " items.")
          updateProgress(i, total)
        }
        "phi"
      }
    })
  }

  val pv = new DoubleProperty()
  pv.value = 0.0

  private object View {
    //val stateProperty = Model.Worker.state

    val progressBar = new ProgressBar() {
      minWidth = 250
      //progress <== pv// Model.Worker.progress
    }
    val startButton = new Button("Start") {
      //disable <== stateProperty =!= jfxc.Worker.State.READY
    }
    val cancelButton = new Button("Cancel") {
      //disable <== stateProperty =!= jfxc.Worker.State.RUNNING
    }

    val topPane = new HBox() {
      padding = Insets(10)
      spacing = 10
      alignment = Pos.Center
      children = progressBar
    }

    val buttonPane = new HBox {
      padding = Insets(10)
      spacing = 10
      alignment = Pos.Center
      children = List(
        startButton,
        cancelButton
      )
    }
    val scene = new Scene {
      root = new BorderPane {
        top = topPane
        //center = centerPane
        bottom = buttonPane
      }
    }
  }

}