package charts

import java.io.File
import javafx.scene.{chart => jfxsc}

import breeze.linalg._

import scalafx.collections.ObservableBuffer
import scalafx.scene.chart.XYChart.Series
import scalafx.scene.chart._

import scala.collection.JavaConversions._

class Relation(title: String, relData: Seq[(Double, Double)]) {

  def this(title: String, relData: Seq[Double], startIdx: Int = 1, step: Int = 1) =
    this(title, (startIdx to startIdx + relData.size * step by step).map(_.toDouble) zip relData)

  private[charts] def toChart: XYChart.Series[Number, Number] = {
    new Series[Number, Number] {
      name = title
      data = relData.map(v => XYChart.Data[Number, Number](v._1, v._2))
    }
  }

  def data: Seq[(Double, Double)] = relData
}

object Relation {
  def apply(title: String, relData: Seq[(Double, Double)]) = new Relation(title, relData)
  //def apply(title: String, relData: Seq[Double], startIdx: Int = 1, step: Int = 1) = new Relation(title, relData, startIdx, step)
  def apply(title: String, relData: DenseVector[(Double, Double)]) = new Relation(title, relData.toArray.toSeq)
  def apply(title: String, relData: DenseVector[Double], startIdx: Int = 1, step: Int = 1) = new Relation(title, relData.toArray.toSeq, startIdx, step)
}

object ChartGenerator {

  def generate2DLineChart(chartName: String, data: Relation*): LineChart[Number, Number] = {
    val unitedChart = new ObservableBuffer[jfxsc.XYChart.Series[Number, Number]]()
    data.foreach(d => unitedChart.add(d.toChart))
    val xAxis = new NumberAxis()
    val yAxis = new NumberAxis()
    val lineChart = LineChart(xAxis, yAxis)
    lineChart.title = chartName
    lineChart.data = unitedChart
    lineChart
  }

  def saveToCSV(lineChart: LineChart[Number, Number], path: String) = {
    val series: List[jfxsc.XYChart.Series[Number, Number]] = lineChart.data.get.toList
    val data: List[(DenseMatrix[Double], String)] = series.map(chart => {
      val points = chart.getData
      val xVals = points.map(_.getXValue.doubleValue)
      val yVals = points.map(_.getYValue.doubleValue)
      val matrix = DenseMatrix(xVals, yVals).t
      (matrix, chart.getName)
    })
    assert(data.map(_._1.size).distinct.size == 1) // all matrices are of one size
    val unitedMatrix = DenseMatrix.horzcat(data.map(_._1):_*)
    csvwrite(new File(path), unitedMatrix)
  }

//  def savePng(chart: LineChart[Number, Number]): Unit = {
//    val img = chart.snapshot(null, new WritableImage(500, 250))
//    val file = new File("C\\chart.png")
//    ImageIO.write(SwingFXUtils.fromFXImage(img, null), "png", file)
//  }

}
