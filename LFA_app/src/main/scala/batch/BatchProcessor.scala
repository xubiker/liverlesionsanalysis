package batch

import javafx.embed.swing.JFXPanel
import javafx.event.EventHandler
import javafx.stage.WindowEvent

import akka.actor._
import akka.pattern.ask
import akka.routing.{BalancingPool, Broadcast}
import akka.util.Timeout
import com.typesafe.scalalogging.LazyLogging


import scala.collection.mutable.ArrayBuffer
import scala.concurrent.Await
import scala.concurrent.duration._
import scalafx.Includes._
import scalafx.geometry.{Insets, Pos}
import scalafx.scene.Scene
import scalafx.scene.control.{Button, ProgressBar}
import scalafx.scene.layout.{BorderPane, HBox}
import scalafx.stage.{Stage, StageStyle}

case class Task[A, B](data: A, method: A => B)
case class TaskResult[A, B](result: B, srcTask: Task[A, B])
case class BatchResult[A, B](taskResults: Seq[TaskResult[A, B]])

private case class Enough()

private object GuiMessages {
  case class GuiAddTask()
  case class GuiCancel()
  case class GuiTaskFinished()
  case class GuiClose()
}

object BatchProcessor extends LazyLogging {
  def process[A, B]
  (tasks: Seq[Task[A, B]], numOfActors: Int, timeoutPerTaskInSecs: Int, displayProgress: Boolean = false)
  : BatchResult[A, B] = {
    implicit val timeout = Timeout((tasks.length + 1) * timeoutPerTaskInSecs.seconds)
    val system = ActorSystem()
    val master = system.actorOf(Props(new BatchJobMaster(numOfActors, displayProgress)), name = "master")
    tasks.foreach(master ! _)
    val batchResultFuture = (master ? Enough).mapTo[BatchResult[A, B]]
    val batchResult = Await.result(batchResultFuture, timeout.duration)
    system.terminate()
    batchResult
  }
}

private class JobActor[A, B] extends Actor with ActorLogging {
  override def receive = {
    case Task(data, method) =>
      val m = method.asInstanceOf[A => B]
      val x = data.asInstanceOf[A]
      sender ! TaskResult(m.apply(x), Task(data, method))
    case Enough =>
      context stop self
    case _ => log.warning("Unsupported type of message")
  }
}

private class BatchJobMaster[A, B](numOfActors: Int, displayProgress: Boolean) extends Actor with ActorLogging {

  val processed = ArrayBuffer.empty[TaskResult[A, B]]
  val router = context.actorOf(Props(new JobActor).withRouter(BalancingPool(numOfActors)), "router")
  var resActor = ActorRef.noSender

  if (displayProgress) {
    new JFXPanel
  }

  val progressActor = if (displayProgress)
    context.actorOf(Props[ProgressAndControlActor].withDispatcher("jfx-dispatcher"), "gui")
  else context.actorOf(Props.empty)

  override def preStart = {
    context watch router
  }

  override def receive = {
    case TaskResult(data, srcTask) =>
      //println("master received result")
      progressActor ! GuiMessages.GuiTaskFinished
      //Thread.sleep(300)
      processed += TaskResult(data.asInstanceOf[B], srcTask.asInstanceOf[Task[A, B]])
    case Task(data, method) =>
      resActor = sender
      //println("processing...")
      progressActor ! GuiMessages.GuiAddTask
      //Thread.sleep(200)
      router ! Task(data, method)
    case Enough =>
      //println("enough...")
      resActor = sender
      router ! Broadcast(Enough)
    case GuiMessages.GuiCancel =>
      println("gui cancel...")
      resActor ! BatchResult(ArrayBuffer.empty)
      context stop self
    case Terminated(ref) =>
      if (ref == router) {
        //println(s"actor $ref died. Processing finished")
        resActor ! BatchResult(processed.toSeq)
        progressActor ! GuiMessages.GuiClose
      }
    case _ => log.warning("Unsupported type of message")
  }

  override def postStop = {
    context stop progressActor
  }

}

private class ProgressAndControlActor extends Actor with ActorLogging {

  var numOfTasks = 0.0
  var numOfFinishedTasks = 0.0

  val dialog = new Stage {
    val btn = new Button {
      minHeight = 30
      minWidth = 50
      text = "Cancel"
      onAction = () => onClose()
    }
    val progressBar = new ProgressBar {
      minWidth = 200
      minHeight = 30
      //progress <== currentProgress
    }
    val topPane = new HBox {
      padding = Insets(10)
      spacing = 10
      alignment = Pos.Center
      children = List(progressBar, btn)
    }
    scene = new Scene {
      root = new BorderPane {
        top = topPane
      }
    }
    onCloseRequest = new EventHandler[WindowEvent] {
      override def handle(event: WindowEvent) = onClose()
    }

    def onClose() = {
      println("closing dialog")
      self ! GuiMessages.GuiCancel
      hide()
    }

    //alwaysOnTop = true
    resizable = false
    initStyle(StageStyle.Undecorated)
  }

  override def preStart = {
    dialog.show()
  }

  override def receive = {
    case GuiMessages.GuiAddTask =>
      numOfTasks += 1
      dialog.progressBar.progress = numOfFinishedTasks / numOfTasks
    case GuiMessages.GuiTaskFinished =>
      numOfFinishedTasks += 1
      dialog.progressBar.progress = numOfFinishedTasks / numOfTasks
    case GuiMessages.GuiCancel =>
      context.parent ! GuiMessages.GuiCancel
      dialog.close()
      context stop self
    case GuiMessages.GuiClose =>
      dialog.close()
      context stop self
  }

  override def postStop = {
    dialog.close()
  }
}